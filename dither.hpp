//
//  dither.hpp
//  Halftone Tools
//
//  Created by Chris Cox on June 25, 2023
//

#ifndef dither_hpp
#define dither_hpp

#include <cstdint>
#include <vector>
#include <cassert>
#include <cstring>
#include "image.hpp"
 
/******************************************************************************/

// forward declarations
struct dither_options;

/******************************************************************************/

typedef void dither_func( image &output, const image &input,
                    const int64_t seed, const dither_options &opt );

/******************************************************************************/

struct dither_description {

    // simple version for no parameters
    dither_description( dither_func *f, const char *n ) : func(f), name(n), param_count(0) {}
    
    dither_description( dither_func *f, const char *n, size_t c,
                std::vector<const char *> &&names, std::vector<double> &&def,
                std::vector<double> &&pmin, std::vector<double> &&pmax ) :
                func(f), name(n), param_count(c),
                param_names(names), param_defaults(def),
                param_min(pmin), param_max(pmax)
                {}

    dither_func *func;
    const char *name;
    
    size_t param_count;
    std::vector<const char *>param_names;   // could be localizable strings
    std::vector<double>param_defaults;
    
    std::vector<double>param_min;   // parameter limits for UI
    std::vector<double>param_max;
};

// global list of all dither function descriptors
extern std::vector<dither_description> ditherList;
extern std::vector<dither_description> power_ditherList;

/******************************************************************************/

struct dither_options {
    dither_options() {}
    
    dither_options( const dither_description &desc ) : params(desc.param_defaults) {}
    
    // dither function options
    std::vector<double> params;
};

/******************************************************************************/
/******************************************************************************/

#endif /* dither_hpp */
