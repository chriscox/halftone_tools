//
//  image.cpp
//  Halftone Tools
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cassert>
#include <vector>
#include "image.hpp"

/******************************************************************************/

/* convert little endian 32 bit values to big endian, and vice-versa
    This is a no-op on little endian machines
        PFM is always 32 bit binary, always little endian for some reason
*/
static
void BYTESWAP32( void *in, size_t itemCount )
    {
    const uint32_t testLong = 0x01020304;
    const uint8_t *testChar = (uint8_t *)&testLong;
    
    if (testChar[0] == 0x01)
        {
        // this is a big endian machine, do the work
        uint8_t *charPtr = (uint8_t *) in;
        
        for (size_t i = 0; i < itemCount; ++i )
            {
            uint8_t val0 = charPtr[0];
            uint8_t val1 = charPtr[1];
            uint8_t val2 = charPtr[2];
            uint8_t val3 = charPtr[3];
            
            charPtr[0] = val3;
            charPtr[1] = val2;
            charPtr[2] = val1;
            charPtr[3] = val0;
            
            charPtr += 4;
            }
        }
    }

/******************************************************************************/

/* convert big endian 16 bit values to little endian, and vice-versa
    This is a no-op on big endian machines
        PBM specifies binary values to be big endian
*/
static
void BYTESWAP16( void *in, size_t itemCount )
    {
    const uint16_t testLong = 0x0102;
    const uint8_t *testChar = (uint8_t *)&testLong;
    
    if (testChar[0] == 0x02)
        {
        // this is a little endian machine, do the work
        uint8_t *charPtr = (uint8_t *) in;
        
        for (size_t i = 0; i < itemCount; ++i )
            {
            uint8_t val0 = charPtr[0];
            uint8_t val1 = charPtr[1];
            
            charPtr[0] = val1;
            charPtr[1] = val0;
            
            charPtr += 2;
            }
        }
    }

/******************************************************************************/

/// Allocate the image buffer, set the image size
void image::Allocate( size_t width, size_t height )
{
    if (this->width == width && this->height == height)
        return;

    this->width = width;
    this->height = height;
    
    delete [] this->buffer; // just in case it was already allocated
    this->buffer = new uint8_t[ width * height ];
    
    // be safe on new allocations
    memset( this->buffer, 0, this->width * this->height );
}

/******************************************************************************/

// DEFERRED - optimize for 8bit and 16 bit values
template <typename T>
T RGB2Gray( T red, T green, T blue )
{
    // NTSC primary coefficients - not perfect, but widely used as default
    T result = T( red * 0.3 + green * 0.59 + blue * 0.11 );
    return result;
}

/******************************************************************************/

void ConsumePBMComments(FILE *input)
{
    int maybeComment = getc( input );
    if (maybeComment != '#')
        ungetc( maybeComment, input );
    else
        {
        // read to next line ending
        int c;
        do {
            c = getc( input );
            } while ( c != '\r' && c != '\n' );
        }
}

/******************************************************************************/

/// Read the file into an image buffer
void image::ReadPGM( const char *filename )
{
    FILE *input = fopen( filename, "rb" );
    if (input == NULL)
        {
        printf("Could not open file %s\n", filename);
        exit(-1);
        }
    
    // read header
    unsigned char magic[3];
    unsigned char junk[4];
    
    size_t num_read = fread( magic, 1, 3, input );
 
    if ( num_read != 3 || magic[0] != 'P')
        {
        printf("Unrecognized PortableBitMap format %c%c in %s\n", magic[0], magic[1], filename);
        exit(-1);
        }

// TODO: PNM, PAM
    int fileFormat = -1;
    bool binary = false;
    
    if (magic[1] == '2')
        {
        binary = false;
        fileFormat = k8BitGrayFormat;
        }
    else if (magic[1] == '3')
        {
        binary = false;
        fileFormat = k8BitRGBFormat;
        }
    else if (magic[1] == '5')
        {
        binary = true;
        fileFormat = k8BitGrayFormat;
        }
    else if (magic[1] == '6')
        {
        binary = true;
        fileFormat = k8BitRGBFormat;
        }
    else if (magic[1] == 'f' )
        {
        binary = true;
        fileFormat = kFloatGrayFormat;
        }
    else if (magic[1] == 'F')
        {
        binary = true;
        fileFormat = kFloatRGBFormat;
        }
#if 0
// DEFERRED - read bitmap (but why would we halftone that?)
    else if (magic[1] == '1')
        {
        binary = false;
        fileFormat = kBitmapFormat;
        }
    else if (magic[1] == '4')
        {
        binary = true;
        fileFormat = kBitmapFormat;
        }
#endif

    else
        {
        printf("Unrecognized PortableBitMap format %c%c in %s\n", magic[0], magic[1], filename);
        exit(-1);
        }

    // ignore comment lines starting with #
    ConsumePBMComments(input);


    long width, height;
    int items_matched = fscanf(input, "%ld %ld", &width, &height );
    if ( items_matched != 2)
        {
        printf("Failed to parse size from PortableBitMap in %s\n", filename);
        exit(-1);
        }
    
    num_read = fread( junk, 1, 1, input );    // consume the line feed
    assert( width > 0 );
    assert( height > 0 );

    ConsumePBMComments(input);
    
    float maxValue = 0.0;
    
    if (fileFormat != kBitmapFormat)
        {
        // read the max value
        items_matched = fscanf(input, "%f", &maxValue );
        if ( items_matched != 1)
            {
            printf("Failed to parse range from PortableBitMap in %s\n", filename);
            exit(-1);
            }
        num_read = fread( junk, 1, 1, input );   // consume the line feed
        
        // TODO - scale input if max < 65535
        // TODO - check PFM bogus max value
        if (maxValue > 255)
            {
            if (fileFormat == k8BitGrayFormat)
                fileFormat = k16BitGrayFormat;
            else if (fileFormat == k8BitRGBFormat)
                fileFormat = k16BitRGBFormat;
            else if (fileFormat == kFloatRGBFormat)
                fileFormat = kFloatRGBFormat;   // do nothing for now
            else
                {
                printf("Unrecognized maxValue format %f in %s\n", maxValue, filename);
                exit(-1);
                }
            }
        }

    // size this array
    Allocate( width, height );
    
    ConsumePBMComments(input);
    
    if (binary && fileFormat == k8BitGrayFormat)
        {
        // just read the raw bytes
        num_read = fread( this->buffer, 1, width * height, input );
        if ( num_read != width*height)
            {
            printf("Failed to read PortableBitMap bytes in %s\n", filename);
            exit(-1);
            }
        return;
        }
    
    if (binary && fileFormat == k16BitGrayFormat)   // Format specifies big endian
        {
        // create temp buffer, convert values to 8 bit/channel
        uint16_t *temp = new uint16_t[ this->width ];
        
        for (size_t y = 0; y < this->height; ++y)
            {
            // read row into temp buffer
            num_read = fread( temp, 2, width, input );
            if ( num_read != width)
                {
                printf("Failed to read PortableBitMap bytes in %s\n", filename);
                exit(-1);
                }
            BYTESWAP16( temp, width );      // byteswap if needed
            
            // convert values into 8 bit, write to output
            for (size_t x = 0; x < this->width; ++x)
                {
                uint16_t in = temp[x];
                uint8_t value = in >> 8;
                WritePixelUnclipped( x, y, value );
                }
            }
        
        delete[] temp;
        return;
        }
    
    if (binary && fileFormat == k8BitRGBFormat)
        {
        // create temp buffer, convert RGB values to Gray 8 bit/channel
        uint8_t *temp = new uint8_t[ 3*this->width ];
        
        for (size_t y = 0; y < this->height; ++y)
            {
            // read row into temp buffer
            num_read = fread( temp, 3, width, input );
            if ( num_read != width)
                {
                printf("Failed to read PortableBitMap bytes in %s\n", filename);
                exit(-1);
                }
        
            // convert values into 8 bit, write to output
            for (size_t x = 0; x < this->width; ++x)
                {
                uint8_t red   = temp[3*x+0];
                uint8_t green = temp[3*x+1];
                uint8_t blue  = temp[3*x+2];
                uint8_t value = RGB2Gray( red, green, blue );
                WritePixelUnclipped( x, y, value );
                }
            }
        
        delete[] temp;
        return;
        }
    
    if (binary && fileFormat == k16BitRGBFormat)
        {
        // create temp buffer, convert RGB values to Gray 8 bit/channel
        uint16_t *temp = new uint16_t[ 3*this->width ];
        
        for (size_t y = 0; y < this->height; ++y)
            {
            // read row into temp buffer
            num_read = fread( temp, 3*2, width, input );
            if ( num_read != width)
                {
                printf("Failed to read PortableBitMap bytes in %s\n", filename);
                exit(-1);
                }
            BYTESWAP16( temp, 3*width );      // byteswap if needed
            
            // convert values into 8 bit, write to output
            for (size_t x = 0; x < this->width; ++x)
                {
                uint16_t red   = temp[3*x+0];
                uint16_t green = temp[3*x+1];
                uint16_t blue  = temp[3*x+2];
                uint16_t gray = RGB2Gray( red, green, blue );
                uint8_t value = gray >> 8;
                WritePixelUnclipped( x, y, value );
                }
            }
        
        delete[] temp;
        return;
        }
    
    if (binary && fileFormat == kFloatGrayFormat)
        {
        // create temp buffer, convert values to 8 bit/channel
        float *temp = new float[ this->width ];
        
        for (size_t y = 0; y < this->height; ++y)
            {
            // read row into temp buffer
            num_read = fread( temp, 4, width, input );
            if ( num_read != width)
                {
                printf("Failed to read PortableBitMap bytes in %s\n", filename);
                exit(-1);
                }
            
            if (maxValue < 0.0)
                BYTESWAP32( temp, width );      // byteswap if needed
            
            // convert values into 8 bit, write to output
            for (size_t x = 0; x < this->width; ++x)
                {
                float gray   = temp[x];
                gray = fmin( 1.0f, gray );
                gray = fmax( 0.0f, gray );
                uint8_t value = uint8_t(255.0f * gray + 0.5f);
                WritePixelUnclipped( x, y, value );
                }
            }
        
        delete[] temp;
        return;
        }
    
    if (binary && fileFormat == kFloatRGBFormat)
        {
        // create temp buffer, convert RGB values to Gray 8 bit/channel
        float *temp = new float[ 3*this->width ];
        
        for (size_t y = 0; y < this->height; ++y)
            {
            // read row into temp buffer
            num_read = fread( temp, 3*4, width, input );
            if ( num_read != width)
                {
                printf("Failed to read PortableBitMap bytes in %s\n", filename);
                exit(-1);
                }
            
            if (maxValue < 0.0)
                BYTESWAP32( temp, 3*width );      // byteswap if needed
            
            // convert values into 8 bit, write to output
            for (size_t x = 0; x < this->width; ++x)
                {
                float red   = temp[3*x+0];
                float green = temp[3*x+1];
                float blue  = temp[3*x+2];
// TODO - leave the RGB values linear (gamma 1.0, not perceptual) for now
// maybe add an option for gamma encoding
                float gray = RGB2Gray( red, green, blue );
                gray = fmin( 1.0f, gray );
                gray = fmax( 0.0f, gray );
                uint8_t value = uint8_t(255.0f * gray + 0.5f);
                WritePixelUnclipped( x, y, value );
                }
            }
        
        delete[] temp;
        return;
        }


    // TODO - binary bitmap?
    

    // read in ASCII values, convert as needed
    assert( binary == false );
    assert( fileFormat != kFloatGrayFormat );
    assert( fileFormat != kFloatRGBFormat );
    for (size_t y = 0; y < this->height; ++y)
        {
        for (size_t x = 0; x < this->width; ++x)
            {
            uint8_t value = 0;
            
            if (fileFormat == k8BitGrayFormat)
                {
                unsigned gray;
                items_matched = fscanf( input, "%u", &gray );
                if ( items_matched != 1)
                    {
                    printf("Failed to parse pixel from PortableBitMap in %s\n", filename);
                    exit(-1);
                    }
                value = gray;
                }
            else if (fileFormat == k16BitGrayFormat)
                {
                unsigned gray;
                items_matched = fscanf( input, "%u", &gray );
                if ( items_matched != 1)
                    {
                    printf("Failed to parse pixel from PortableBitMap in %s\n", filename);
                    exit(-1);
                    }
                value = gray >> 8;
                }
            else if (fileFormat == k8BitRGBFormat)
                {
                unsigned red, green, blue;
                items_matched = fscanf( input, "%u %u %u", &red, &green, &blue );
                if ( items_matched != 3)
                    {
                    printf("Failed to parse pixel from PortableBitMap in %s\n", filename);
                    exit(-1);
                    }
                unsigned gray = RGB2Gray( red, green, blue );
                value = gray;
                }
            else if (fileFormat == k16BitRGBFormat)
                {
                unsigned red, green, blue;
                items_matched = fscanf( input, "%u %u %u", &red, &green, &blue );
                if ( items_matched != 3)
                    {
                    printf("Failed to parse pixel from PortableBitMap in %s\n", filename);
                    exit(-1);
                    }
                unsigned gray = RGB2Gray( red, green, blue );
                value = gray >> 8;
                }
            
            WritePixelUnclipped( x, y, value );
            }
        }
}

/******************************************************************************/

/// Write the image buffer to a binary 8 bit/channel Portable Gray Map (PGM) file
void image::WritePGM( const char *name )  const
{
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
    // write PGM file header
    fprintf(outfile,"P5\n# CREATOR: halftone_tools\n%ld %ld\n%d\n",this->width,this->height,255 );
    // write the image buffer
    fwrite( this->buffer, this->width, this->height, outfile );
    // and close the file
    fclose(outfile);
}

/******************************************************************************/

/// Write the image buffer to a binary 1 bit/channel Portable BitMap (PBM) file
void image::WritePBM( const char *name )  const
{
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
    // write PBM binary file header
    fprintf(outfile,"P4\n# CREATOR: halftone_tools\n%ld %ld\n",this->width,this->height );
    
    // convert binary gray values, to actual binary bits
    size_t lineBytes = (width+7) / 8;
    uint8_t *bitmap = new uint8_t[ height*lineBytes ];
    memset( bitmap, 0, height*lineBytes );
    
    for (size_t y = 0; y < height; ++y)
        {
        for (size_t x = 0; x < width; ++x)
            {
            if ( ReadPixelUnclipped(x,y) == 0 )
                bitmap[ (y*lineBytes) + (x / 8) ] |= (1 << (7 - (x%8)));    // 0x80 >> (x%8) ??
            }
        }

    // write the image buffer
    fwrite( bitmap, lineBytes, this->height, outfile );
    // close the file
    fclose(outfile);
    // free temp memory
    delete[] bitmap;
}

/******************************************************************************/

/// Write a pixel value, clipped to image bounds
void image::WritePixel( int64_t x, int64_t y, uint8_t value )
{
    if (x >= 0 && x < this->width && y >= 0 && y < this->height)
        buffer[ y*this->width + x ] = value;
}

/******************************************************************************/

/// Write a pixel value, without clipping coordinates (caller must ensure coordinates are valid)
void image::WritePixelUnclipped( int64_t x, int64_t y, uint8_t value )
{
    assert( x >= 0 );
    assert( x < width );
    assert( y >= 0 );
    assert( y < height );
    
    buffer[ y*this->width + x ] = value;
}

/******************************************************************************/

/// Read a pixel value, clipped to image bounds
uint8_t image::ReadPixel( int64_t x, int64_t y )  const
{
    x = std::max( x, (int64_t)0 );
    x = std::min( x, (int64_t)(this->width)-1 );
    y = std::max( y, (int64_t)0 );
    y = std::min( y, (int64_t)(this->height)-1 );
    
    return buffer[ y*this->width + x ];
}

/******************************************************************************/

/// Read a pixel value, without clipping coordinates (caller must ensure coordinates are valid)
uint8_t image::ReadPixelUnclipped( int64_t x, int64_t y )  const
{
    assert( x >= 0 );
    assert( x < width );
    assert( y >= 0 );
    assert( y < height );
    return buffer[ y*this->width + x ];
}

/******************************************************************************/

/// Fill specified rectangular area with value, after clipping to image bounds
void image::FillRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value )
{
    top = std::max( top, (int64_t)0 );
    left = std::max( left, (int64_t)0 );
    bottom = std::min( bottom, (int64_t)(this->height) );
    right = std::min( right, (int64_t)(this->width) );
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );

    for (int64_t y = top; y < bottom; ++y)
        for (int64_t x = left; x < right; ++x)
            this->buffer[ y * this->width + x ] = value;
}

/******************************************************************************/

/// Outline the given rectangle with the number of pixels specified
/// using clipped rectangle fills for safety
void image::OutlineRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value, int64_t weight )
{
    FillRectClipped( top, left, top+weight, right, value );
    FillRectClipped( bottom-weight, left, bottom, right, value );
    FillRectClipped( top, left, bottom, left+weight, value );
    FillRectClipped( top, right-weight, bottom, right, value );
}

/******************************************************************************/

/// This is more or less a "threshold" layer blend mode
void image::ApplyThresholdScreen( const image &screen, image &output ) const
{
    assert( width == screen.width );
    assert( height == screen.height );

    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        for (size_t x = 0; x < width; ++x)
            {
            uint8_t value_in = ReadPixelUnclipped( x, y );
            uint8_t thresh = screen.ReadPixelUnclipped( x, y );
            
            uint8_t value_out = (value_in <= thresh) ? 0: 255;
            
            // make sure extreme values are preserved (no scum dots!)
            if (value_in == 255)
                value_out = 255;
            if (value_in == 0)
                value_out = 0;
            
            output.WritePixelUnclipped( x, y, value_out );
            }
        }
}

/******************************************************************************/

void image::DrawVerticalGradientUnclipped( int64_t top, int64_t left, int64_t bottom, int64_t right, float topValue, float bottomValue )
{
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );
    assert( topValue >= 0.0 );
    assert( topValue <= 1.0 );
    assert( bottomValue >= 0.0 );
    assert( bottomValue <= 1.0 );
    
    int64_t gradHeight = bottom - top;
    float base = topValue;
    float range = bottomValue - topValue;

    for (int64_t y = top; y < bottom; ++y)
        {
        float rely = (y-top) / float(gradHeight-1);
        uint8_t out = (255.0 * (base + rely*range) + 0.5);
        for (int64_t x = left; x < right; ++x)
            {
            buffer[ y*width + x ] = out;
            }
        }
}

/******************************************************************************/

void image::DrawHorizontalGradientUnclipped( int64_t top, int64_t left, int64_t bottom, int64_t right, float leftValue, float rightValue )
{
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );
    assert( leftValue >= 0.0 );
    assert( leftValue <= 1.0 );
    assert( rightValue >= 0.0 );
    assert( rightValue <= 1.0 );
    
    int64_t gradWidth = right - left;
    float base = leftValue;
    float range = rightValue - leftValue;
    
    for (int64_t y = top; y < bottom; ++y)
        {
        for (int64_t x = left; x < right; ++x)
            {
            float relx = (x-left) / float(gradWidth-1);
            uint8_t out = (255.0 * (base + relx*range) + 0.5);
            buffer[ y*width + x ] = out;
            }
        }
}

/******************************************************************************/

void image::CopyRectToDouble( const pointInt &topLeft, size_t size, double *output ) const
{
    int64_t top = topLeft.y;
    int64_t left = topLeft.x;
    int64_t bottom = top + size;
    int64_t right = left + size;
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );

    for (int64_t y = top; y < bottom; ++y)
        for (int64_t x = left; x < right; ++x)
            output[ (y-top)*size + (x-left) ] = buffer[ y*width + x ] / 255.0;
}

/******************************************************************************/

void image::CopyRectToDoubleComplex( const pointInt &topLeft, size_t size, double *output ) const
{
    int64_t top = topLeft.y;
    int64_t left = topLeft.x;
    int64_t bottom = top + size;
    int64_t right = left + size;
    
    assert( top >= 0 );
    assert( bottom <= height );
    assert( left >= 0 );
    assert( right <= width );
    assert( top <= bottom );
    assert( left <= right );
    size_t rowStep = 2*size;

    for (int64_t y = top; y < bottom; ++y)
        for (int64_t x = left; x < right; ++x) {
            // real, imaginary
            output[ (y-top)*rowStep + 2*(x-left) + 0 ] = buffer[ y*width + x ] / 255.0;
            output[ (y-top)*rowStep + 2*(x-left) + 1 ] = 0.0;
        }
}

/******************************************************************************/

// Yes, there are already a lot of test charts available.
// But I need one that isn't copyrighted, and is flexible in rendering.
// Going across as well as down helps on laser printers that have drum artifacts, and certain presses that spread dots in one direction.
void image::RenderDensityTestChart( double image_ppi )
{
    const float backgroundGray = 0.77;  // 23% gray, 77% ink, not the same as any square
    const float outlineWidth = 0.5;     // 0.5 mm outline
    const float interior = 0.93;
    const std::vector<int> percentLeft =  {  0, 2, 4, 6, 8,10,20,30,40,50 };
    const std::vector<int> percentRight = { 50,60,70,80,90,92,94,96,98,100 };
    assert( percentLeft.size() == percentRight.size() );

    // calc sizes and margins
    size_t sampleCount = percentLeft.size();
    size_t sampleSizeVert = (size_t) floor( height * interior / sampleCount );
    size_t sampleSizeHoriz = (size_t) floor( width * interior / sampleCount );
    float marginTopBottom = 0.5 * (height - sampleSizeVert * sampleCount);
    float marginLeftRight = 0.5 * (width - sampleSizeHoriz * sampleCount);
    
    int temp = (int) round( outlineWidth * image_ppi / mm_per_inch );
    int64_t borderPixels = std::max(1, temp);

    // double check dimensions for fit
    assert( (5*borderPixels) <= marginTopBottom );
    assert( (5*borderPixels) <= marginLeftRight );
    assert( (2*marginTopBottom + sampleCount*sampleSizeVert) <= height );
    assert( (2*marginLeftRight + sampleCount*sampleSizeHoriz) <= width );

    // fill image with background value
    FillRectClipped( 0, 0, height, width, 255 * backgroundGray );
    
    // loop over top
    for (size_t j = 1; j < percentLeft.size()-1; ++j)
        {
        int64_t top = marginTopBottom;
        int64_t left = marginLeftRight + j * sampleSizeHoriz;
        int64_t bottom = top + sampleSizeVert;
        int64_t right = left + sampleSizeHoriz;
        uint8_t value = (255 * percentLeft[j] + 50) / 100;
        assert( bottom > top );
        assert( right > left );
        FillRectClipped( top, left, bottom, right, value );
        OutlineRectClipped( top-borderPixels, left-borderPixels, bottom, right+borderPixels, 255, borderPixels );
        }
    
    // loop over bottom
    for (size_t j = 1; j < percentRight.size()-1; ++j)
        {
        int64_t top = height - marginTopBottom - sampleSizeVert;
        int64_t left = marginLeftRight + j * sampleSizeHoriz;
        int64_t bottom = top + sampleSizeVert;
        int64_t right = left + sampleSizeHoriz;
        uint8_t value = (255 * percentRight[j] + 50) / 100;
        assert( bottom > top );
        assert( right > left );
        FillRectClipped( top, left, bottom, right, value );
        OutlineRectClipped( top-borderPixels, left-borderPixels, bottom+borderPixels, right+borderPixels, 0, borderPixels );
        }

    // loop over left
    for (size_t j = 0; j < percentLeft.size(); ++j)
        {
        int64_t top = marginTopBottom + j * sampleSizeVert;
        int64_t left = marginLeftRight;
        int64_t bottom = top+sampleSizeVert;
        int64_t right = left+sampleSizeHoriz;
        uint8_t value = (255 * percentLeft[j] + 50) / 100;
        assert( bottom > top );
        assert( right > left );
        FillRectClipped( top, left, bottom, right, value );
        OutlineRectClipped( top-borderPixels, left-borderPixels, bottom+borderPixels, right+borderPixels, 255, borderPixels );
        }

    // loop over right
    for (size_t j = 0; j < percentRight.size(); ++j)
        {
        int64_t top = marginTopBottom + j * sampleSizeVert;
        int64_t right = width - marginLeftRight;
        int64_t bottom = top+sampleSizeVert;
        int64_t left = right - sampleSizeHoriz;
        uint8_t value = (255 * percentRight[j] + 50) / 100;
        assert( bottom > top );
        assert( right > left );
        FillRectClipped( top, left, bottom, right, value );
        OutlineRectClipped( top-borderPixels, left-borderPixels, bottom+borderPixels, right+borderPixels, 0, borderPixels );
        }
    
    // left vertical gradient in middle: black to 50%
    int64_t top = 2*marginTopBottom + sampleSizeVert;
    int64_t left = 2*marginLeftRight + sampleSizeHoriz;
    int64_t bottom = height - (2*marginTopBottom + sampleSizeVert);
    int64_t right = width/2 - marginLeftRight/2;
    DrawVerticalGradientUnclipped( top, left, bottom, right, 0, 0.5 );
    OutlineRectClipped( top-borderPixels, left-borderPixels, bottom+borderPixels, right+borderPixels, 255, borderPixels );
    
    
    // right gradient in middle: 50% to white
    top = 2*marginTopBottom + sampleSizeVert;
    left = width/2 + marginLeftRight/2;
    bottom = height - (2*marginTopBottom + sampleSizeVert);
    right = width - (2*marginLeftRight + sampleSizeHoriz);
    DrawVerticalGradientUnclipped( top, left, bottom, right, 0.5, 1.0 );
    OutlineRectClipped( top-borderPixels, left-borderPixels, bottom+borderPixels, right+borderPixels, 0, borderPixels );
    
    
    // dots to indicate orientation of chart, just in case
    FillRectClipped( marginTopBottom-4*borderPixels, marginLeftRight-4*borderPixels,
                    marginTopBottom-1*borderPixels, marginLeftRight-1*borderPixels, 0 );
    FillRectClipped( marginTopBottom-4*borderPixels, width-marginLeftRight+1*borderPixels,
                    marginTopBottom-1*borderPixels, width-marginLeftRight+4*borderPixels, 0 );
    

    // TODO: labels?
    // pictoral labels?
    // or use a template for numerals and hack resampling?

}

/******************************************************************************/

void image::RenderDocumentationQuarters()
{
    size_t quarter_x = width / 4;
    size_t half_x = 2*quarter_x;

    // left solid
    const double percentage = 63.0;
    const uint8_t back = uint8_t( (percentage * 255.0 / 100.0) + 0.5 );
    FillRectClipped( 0, 0, height, quarter_x, back );
    
    // second horizontal gradient
    DrawHorizontalGradientUnclipped( 0, quarter_x, height, half_x, 0.0, 1.0 );

    // third vertical gradient
    DrawVerticalGradientUnclipped( 0, half_x, height, half_x+quarter_x, 0.0, 1.0 );

    // fourth/right = 5 solid values in horizontal strips (0, 1/4, 1/2, 3/4, 1.0 )
    const int64_t count = 5;
    const size_t segment = height / count;
    for (int64_t j = 0; j < (count-1); ++j)
        {
        uint8_t value = uint8_t( (j * 255 + ((count-1)>>1)) / (count-1) );
        FillRectClipped( (j)*segment, 3*quarter_x, (j+1)*segment, width, value );
        }
    FillRectClipped( (count-1)*segment, 3*quarter_x, height, width, 255 );

}

/******************************************************************************/

void image::RenderGradientQuarters()
{
    size_t half_x = width / 2;
    size_t half_y = height / 2;
    size_t remain_x = width - half_x;
    size_t remain_y = height - half_y;

    // top left horizontal gradient
    DrawHorizontalGradientUnclipped( 0, 0, half_y, half_x, 0.0, 1.0 );

    // top right vertical gradient
    DrawVerticalGradientUnclipped( 0, half_x, half_y, width, 0.0, 1.0 );

    // bottom left 5 solid values in horizontal strips (0, 1/4, 1/2, 3/4, 1.0 )
    const size_t segment = remain_y / 5;
    FillRectClipped( half_y+0*segment, 0, half_y+1*segment, half_x, 0 );
    FillRectClipped( half_y+1*segment, 0, half_y+2*segment, half_x, 64 );
    FillRectClipped( half_y+2*segment, 0, half_y+3*segment, half_x, 128 );
    FillRectClipped( half_y+3*segment, 0, half_y+4*segment, half_x, 192 );
    FillRectClipped( half_y+4*segment, 0,           height, half_x, 255 );
    
    // bottom right centered radial gradient
    double center_x = half_x + remain_x / 2.0;
    double center_y = half_y + remain_y / 2.0;
    double scale = hypot(remain_x,remain_y) / 2.0;
    for (int64_t y = half_y; y < height; ++y)
        {
        double fy = y - center_y;
        for (int64_t x = half_x; x < width; ++x)
            {
            double fx = x - center_x;
            double radius = hypot( fx, fy );
            double value = radius / scale;
            uint8_t out = (255.0 * value + 0.5);
            buffer[ y*width + x ] = out;
            }
        }
}

/******************************************************************************/

// really halves for now, because diagonals and radials are too cramped
void image::RenderSinesQuarters()
{
    const size_t min_octave = 4;
    const size_t octave_count = 7;
    
    size_t half_x = width / 2;
    size_t remain_x = width - half_x;
    
    // left horizontal sines
    size_t segment = (height+octave_count-1) / octave_count;
    for (int64_t y = 0; y < height; ++y)
        {
        size_t index = y / segment;
        float scale = 2.0 * M_PI / float( min_octave << index );
        for (int64_t x = 0; x < half_x; ++x)
            {
            float value = 0.5 * (1.0 + sin( x*scale) );
            uint8_t out = (255.0 * value + 0.5);
            buffer[ y*width + x ] = out;
            }
        }

    // right vertical sines
    segment = (remain_x+octave_count-1) / octave_count;
    for (int64_t y = 0; y < height; ++y)
        {
        for (int64_t x = half_x; x < width; ++x)
            {
            size_t index = (x-half_x) / segment;
            float scale = 2.0 * M_PI / float( min_octave << index );
            float value = 0.5 * (1.0 + sin( y*scale) );
            uint8_t out = (255.0 * value + 0.5);
            buffer[ y*width + x ] = out;
            }
        }
}

/******************************************************************************/

/// Write the buffer to a binary 32 bit/channel Portable Float Map (PFM) file
/// assumes single channel input
void WritePFM( const char *name, double *in_data, size_t width, size_t height )
{
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", name);
        exit(-1);
        }
    
    // write PGM file header
    fprintf(outfile,"Pf\n# CREATOR: halftone_tools\n%lu %lu\n-1.0\n",width,height );
    
    {
    // create a temporary buffer
    std::vector<float> tempBuffer;
    size_t pixelCount = width * height;
    tempBuffer.resize( pixelCount );
    
    // copy data to change depth
    for (size_t i = 0; i < pixelCount; ++i )
        {
        float gray = in_data[i];
        tempBuffer[i] = gray;
        }
    
    // convert temp data to Little Endian if needed
    BYTESWAP32( &(tempBuffer[0]), pixelCount );
    
    // write the temp buffer
    fwrite( &(tempBuffer[0]), height, width*sizeof(float), outfile );
    }
    
    // and close the file
    fclose(outfile);
}

/******************************************************************************/
/******************************************************************************/
