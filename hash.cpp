//
//  hash.cpp
//  Halftone Tools
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include "hash.hpp"

/******************************************************************************/

/// returns 0.0..1.0
double hash_to_double(const int64_t x)
{
    const int BITS = 30;
    const uint64_t MASK = (1ULL << BITS)-1;
    const double scale = 1.0 / double(MASK);
    
    double result = (x & MASK) * scale;
    return result;
}

/******************************************************************************/

/// simple but useful hash that gets us reproducible values
/* based on crand64 from benchmark_algorithms.h */
int64_t hash64( const int64_t x )
{
    const uint64_t a = 6364136223846793005ULL;
    const uint64_t c = 1442695040888963407ULL;
    uint64_t temp = (x * a) + c;
    
    // without bit mixing the result is really bad, shows lots of periodicity
    temp = (temp >> 20) ^ (temp << 23) ^ temp;        // looks better
    return int64_t(temp);
}

/******************************************************************************/

/// Hash value plus seed value
/// seed should be a hashed value (random bits) not derived from x
int64_t hash64( const int64_t x, const int64_t seed )
{
    return hash64( hash64(x) ^ seed );
}

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from x or y
int64_t hashXY( const int64_t x, const int64_t y, const int64_t seed )
{
    return hash64( hash64(x) ^ (y^seed) );
}

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const int64_t x, const int64_t y, const int64_t seed )
{
    int64_t temp = hashXY(x,y,seed);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// just casting double to int64 shows too many bit patterns, especially near zero
static
int64_t double_to_hashable( const double x )
{
// uint64_t tempy = std::bit_cast<std::uint64_t>x);   // require C++20
    double tempx = x;
    uint64_t tempy = *((uint64_t*)(&tempx));
    
    uint64_t tempz = rotate_left( tempy, 12 );
    uint64_t tempw = rotate_right( tempy, 20 );
    tempy = tempz ^ tempw;
    
    return (int64_t)tempy;
}

/******************************************************************************/

// just casting float to int32 shows too many bit patterns, especially near zero
static
int64_t float_to_hashable( const float x )
{
// uint32_t tempy = std::bit_cast<std::uint32_t>x);   // require C++20
    float tempx = x;
    uint32_t tempy = *((uint32_t*)(&tempx));
    
    uint32_t tempz = rotate_left( tempy, 9 );
    uint32_t tempw = rotate_right( tempy, 11 );
    tempy = tempz ^ tempw;
    
    return (int64_t)( (((uint64_t)tempy) << 32) | (uint64_t)tempz );
}

/******************************************************************************/

int64_t hash64( const double x )
{
    return hash64( double_to_hashable(x) );
}

/******************************************************************************/

int64_t hash64( const float x )
{
    return hash64( float_to_hashable(x) );
}

/******************************************************************************/

// seed should be a hashed (random bits) value not derived from x or y
int64_t hashXY( const double x, const double y, const int64_t seed )
{
    return hash64( hash64(x) ^ (double_to_hashable(y)^seed) );
}

/******************************************************************************/

// seed should be a hashed (random bits) value not derived from x or y
int64_t hashXY( const float x, const float y, const int64_t seed )
{
    return hash64( hash64(x) ^ (float_to_hashable(y)^seed) );
}

/******************************************************************************/

int64_t hashXY( const int64_t x, const int64_t y )
{
    return hash64( hash64(x) ^ y );
}

/******************************************************************************/

int64_t hashXYZ( const int64_t x, const int64_t y, const int64_t z )
{
    return hash64( hash64( hash64(x) ^ y ) ^ z );
}

/******************************************************************************/

// seed should be a hashed (random bits) value not derived from x,y, or z
int64_t hashXYZ( const int64_t x, const int64_t y, const int64_t z, const int64_t seed )
{
    return hash64( hash64( hash64(x) ^ y ) ^ (z^seed) );
}

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const double x, const double y, const int64_t seed )
{
    int64_t temp = hashXY(x,y,seed);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// returns 0.0..1.0
double hash64_float( const int64_t x )
{
    int64_t temp = hash64(x);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// returns 0.0..1.0
double hash64_float( const int64_t x, const int64_t seed )
{
    int64_t temp = hash64( hash64(x) ^ seed );
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// returns 0.0..1.0
double hashXY_float( const int64_t x, const int64_t y )
{
    int64_t temp = hashXY(x,y);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// returns 0.0..1.0
double hashXYZ_float( const int64_t x, const int64_t y, const int64_t z )
{
    int64_t temp = hashXYZ(x,y,z);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/

// returns 0.0..1.0
// seed should be a hashed (random bits) value not derived from x,y, or z
double hashXYZ_float( const int64_t x, const int64_t y, const int64_t z, const int64_t seed )
{
    int64_t temp = hashXYZ(x,y,z,seed);
    double result = hash_to_double(temp);
    return result;
}

/******************************************************************************/
/******************************************************************************/
