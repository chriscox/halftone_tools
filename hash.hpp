//
//  hash.hpp
//  Halftone Tools
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef hash_hpp
#define hash_hpp

#include <cstdint>
#include <cstddef>
#include <cassert>

/******************************************************************************/

/// returns 0.0..1.0
double hash_to_double( const int64_t x);

/******************************************************************************/

/// simple but useful hash
int64_t hash64( const int64_t x );

int64_t hash64( const int64_t x, const int64_t seed );

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// seed should be a hashed value (random bits) not derived from x or y
int64_t hashXY( const int64_t x, const int64_t y, const int64_t seed );

/******************************************************************************/

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const int64_t x, const int64_t y, const int64_t seed );

/******************************************************************************/

int64_t hash64( const double x );
int64_t hashXY( const double x, const double y, const int64_t seed );

/// Hash 2 coordinates, plus seed value
/// returns 0.0..1.0
/// seed should be a hashed value (random bits) not derived from x or y
double hashXY_float( const double x, const double y, const int64_t seed );

int64_t hashXY( const int64_t x, const int64_t y );
int64_t hashXYZ( const int64_t x, const int64_t y, const int64_t z );

// seed should be a hashed (random bits) value not derived from x,y, or z
int64_t hashXYZ( int64_t x, int64_t y, int64_t z, int64_t seed );

// returns 0.0..1.0
double hash64_float( const int64_t x );
double hash64_float( const int64_t x, const int64_t seed );

// returns 0.0..1.0
double hashXY_float( const int64_t x, const int64_t y );

// returns 0.0..1.0
double hashXYZ_float( const int64_t x, const int64_t y, const int64_t z );

// returns 0.0..1.0
// seed should be a hashed (random bits) value not derived from x,y, or z
double hashXYZ_float( const int64_t x, const int64_t y, const int64_t z, const int64_t seed );

/******************************************************************************/

template<typename T>
constexpr bool isSigned() { return (T(-1) < T(1)); }

template<typename T>
constexpr bool isUnsigned() { return (T(-1) > T(1)); }

// these could be constexpr as well, not sure if that gains anything
template <typename T>
inline T rotate_right(T input, const size_t shift)
    { static_assert( isUnsigned<T>() == true ); return ((input >> shift) | (input << ((8*sizeof(T)) - shift))); }

template <typename T>
inline T rotate_left(T input, const size_t shift)
    { static_assert( isUnsigned<T>() == true ); return ((input << shift) | (input >> ((8*sizeof(T)) - shift))); }

/******************************************************************************/

template<typename RandomIterator>
void seeded_random_shuffle(RandomIterator first, RandomIterator last, int64_t seed) {
    auto count = last - first;
    int64_t pos = 0;
    for (RandomIterator i = first; i != last; ++i ) {
        size_t offset( size_t(hash64(pos, seed)) % count );
        auto temp = *i;
        *i = first[ offset ];
        first[ offset ] = temp;
        ++pos;
    }
}

/******************************************************************************/

#endif /* hash_hpp */
