//
//  halftone.cpp
//  Halftone Tools
//
//  Created by Chris Cox on June 5, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <array>
#include "hash.hpp"
#include "image.hpp"
#include "cells.hpp"
#include "noise.hpp"
#include "offgrid.hpp"
#include "halftone.hpp"

/******************************************************************************/

// some pre-hashed values we can use in dot functions
//  usuallly modifying an existing hash, or defining a fixed seed
const int64_t extraHash1 = hash64((int64_t)0xAABBCCDDEEFF);
const int64_t extraHash2 = hash64((int64_t)0x424242424242);
const int64_t extraHash3 = hash64((int64_t)0xFEEDFACEBEEF);
const int64_t extraHash4 = hash64((int64_t)0x123456789A);
const int64_t extraHash5 = hash64((int64_t)0xABCDEF01234567);
const int64_t hashJitterX = hash64((int64_t)0x4a69747465725858);
const int64_t hashJitterY = hash64((int64_t)0x59594a6974746572);


const int64_t extraHashList[] = { extraHash1, extraHash2, extraHash3, extraHash4,  extraHash5 };
const size_t extraHashCount = sizeof(extraHashList)/sizeof(extraHashList[0]);

/******************************************************************************/
/******************************************************************************/
    
pointFloat geom_transform::apply( const pointFloat &in ) const
{
    pointFloat tmp(in);
    const double angle = rotation * (M_PI/180.0);   // caching would be nice
    
    tmp.x = in.x * scale;
    tmp.y = in.y * scale;
    
    pointFloat out(tmp);
    
    if (angle != 0.0)
        {
        double SA = sin(angle);     // caching would be nice, but complicated, need to benchmark
        double CA = cos(angle);
        out.x = tmp.x * CA - tmp.y * SA;
        out.y = tmp.x * SA + tmp.y * CA;
        }
    
    out.x += offsetX;
    out.y += offsetY;
    
    return out;
}

/******************************************************************************/
/******************************************************************************/

// for testing optimal parameter values
#define OPTIMIZE_TEST   0

#if OPTIMIZE_TEST
const int bias_opt_index = 0;
#endif

#define CSV    OPTIMIZE_TEST && 1

// used for documentation and expimentation with biased dot functions
#define HISTOGRAM   0



// output should be already allocated
void CreateScreenTile(  image &output,
                        const pointInt topLeft,
                        const pointInt bottomRight,
                        const pointInt centerPt,
                        const geom_transform &trans,
                        dotfunc dotgen, const int64_t seed,
                        const screen_options &opt )
{
    const bool dither = opt.dither;
    const bool invert = opt.invert;
    const double jitter_scale = (1.0/100.0) * opt.jitter;
    assert( jitter_scale >= 0.0 && jitter_scale <= 1.0 );

// Here we are dealing with the creation of a quantized screen from a continuous dot function.
// In theory, the best scale for dithering is +- 0.5
// But the output is 0..255, and the input image has a quantization of 1/255.
// Dithering here means the best is about +- 0.5/255
// Dithering when thresholding would be best as +- 1.0/255
// Using a value < 1.0/255.0 will only rarely show an effect in the output.
    const double ditherscale = 1.0 / 255.0;


// DEFERRED - Might it be better to sample the pixel boundaries and get the mean value within the pixel?
//      multisample, or average corner samples?
// all options would be slower, but could be interesting
// But averaging would make it near impossible to get extreme values!

    assert( topLeft.x >= 0 );
    assert( topLeft.y >= 0 );
    assert( bottomRight.x <= output.width );
    assert( bottomRight.y <= output.height );

#if HISTOGRAM
    size_t histogram[256];
    memset( histogram, 0, 256*sizeof(size_t) );
#endif
    
    for (int64_t y = topLeft.y; y < bottomRight.y; ++y)
        {
        for (int64_t x = topLeft.x; x < bottomRight.x; ++x)
            {
            pointFloat imagePt( x - centerPt.x, y - centerPt.y );
            if (jitter_scale > 0.0)
                {
                pointFloat jitterPt;
                jitterPt.x = jitter_scale * (hashXY_float(x,y,seed^hashJitterX) - 0.5);
                jitterPt.y = jitter_scale * (hashXY_float(x,y,seed^hashJitterY) - 0.5);
                imagePt += jitterPt;
                }
            
            pointFloat transPt = trans.apply( imagePt );
            
            // call dot generator and record value
            double rawdot = dotgen( transPt.x, transPt.y, seed, opt );
            
            assert( rawdot >= 0.0 );
            assert( rawdot <= 1.0 );
            
            if (dither)
                {
                int64_t hashval = hashXY(x,y,seed^extraHash5);
                if ((hashval & 0x80) == 0)
                    {
                    rawdot += ditherscale;
                    rawdot = std::min( rawdot, 1.0 );
                    }
                else
                    {
                    rawdot -= ditherscale;
                    rawdot = std::max( rawdot, 0.0 );
                    }
                }
            
            if (invert)
                rawdot = 1.0 - rawdot;
            
            // convert float result to 8 bit integer
            uint8_t pixel_out = floor(rawdot * 255.0 + 0.5);
            
#if HISTOGRAM
            assert( pixel_out >= 0 && pixel_out <= 255 );
            histogram[pixel_out]++;
#endif
            // and save in output image
            output.WritePixelUnclipped( x, y, pixel_out );
            }
        }

#if HISTOGRAM
// count
    size_t total_values = 0;
    size_t duplicates = 0;
    size_t max_duplicates = 0;
    for (size_t j=0; j < 256; ++j )
        {
        size_t count = histogram[j];
        if (count != 0)
            {
            total_values++;
            if (count > 1)
                duplicates++;
            max_duplicates = std::max( max_duplicates, count );
            }
        }

#if CSV
// bias total_val duplicates max_dup
    std::cout << opt.params[bias_opt_index] << ",";
    std::cout << total_values << ",";
    std::cout << duplicates << ",";
    std::cout << max_duplicates << "\n";
#else   // CSV
    std::cout << "screen: " << opt.name << "    " << output.width << "x" << output.height << "\n";
#if OPTIMIZE_TEST
    std::cout << "bias: " << opt.params[bias_opt_index] << "\n";
#endif
    std::cout << "unique values: " << total_values << "\n";
    std::cout << "duplicates: " << duplicates << "\n";
    std::cout << "max duplication: " << max_duplicates << "\n\n";
#endif  // CSV

#endif  // HISTOGRAM

}

/******************************************************************************/

// render an entire screen threshold image
void CreateScreenGeneral( image &output, const size_t width, const size_t height,
                        const geom_transform &trans,
                        dotfunc dotgen, const int64_t seed,
                        const screen_options &opt )
{
    pointInt topLeft( 0, 0 );
    pointInt bottomRight( width, height );
    pointInt centerPt( width/2, height/2 );
    
    output.Allocate(width,height);

#if OPTIMIZE_TEST
#if CSV
    std::cout << opt.name << " " << output.width << "x" << output.height << "\n";
    std::cout << "bias,unique_values,duplicate values,max duplication\n";
#endif
    
    screen_options tempOpt = opt;
    for( double bias = 0.0; bias <= 1.01; bias += 0.01 )
        {
        tempOpt.params[bias_opt_index] = bias;
        CreateScreenTile(output, topLeft, bottomRight, centerPt,
                        trans, dotgen, seed, tempOpt );
        }
#endif

    CreateScreenTile(output, topLeft, bottomRight, centerPt,
                    trans, dotgen, seed, opt );


// TODO - break it up and call tiles on threads
    // horizontal strips would probably be best (least cache conflict)

}

/******************************************************************************/
/******************************************************************************/

// simplest possible dot function
double dot_threshold( double /* x */, double /* y */, int64_t /* seed */, const screen_options &opt )
{
    return opt.params[0];
}

dot_description desc_threshold( dot_threshold, "threshold",
                                1,
                                { "threshold value" },
                                { 0.5 },
                                { 0.0 },
                                { 1.0 } );

/******************************************************************************/

// simple random dot function:  no screen frequency, just random pixel values
double dot_random( double x, double y, int64_t seed, const screen_options &opt )
{
    // repeatable, but looks random
    return hashXY_float( x, y, seed );
}

dot_description desc_random( dot_random, "random" );

/******************************************************************************/

// not-so-simple random dot function - random value is the same across a screen dot cell
// and we allow for walls that don't fill in until 100% black
double dot_randomSquares( double x, double y, int64_t seed, const screen_options &opt )
{
    // scale our parameters from percents to unit range
    double minval = 0.01 * opt.params[0];
    double maxval = 0.01 * opt.params[1];
    double edge = 0.01 * opt.params[2];
    
    // calculate the actual range we'll be using
    double range = maxval - minval;

    // get integer coordinates for the screen cells
    double ix = floor(x);
    double iy = floor(y);
    
    // get fractional coordinates within the cell
    double fx = x - ix;
    double fy = y - iy;
    
    // see if we're inside an edge
    if (fx < edge || fy < edge)
        {
        // it's an edge: return zero, which will only fill in at 100% black
        return 0.0;
        }
    else
        {
        // otherwise generate a repeatable value based on the integer cell coordinates and range parameters
        double value = minval + range * hashXY_float( (int64_t)ix, (int64_t)iy, seed );
        // and pin the value for good measure
        value = std::min(1.0,std::max(0.0,value));
        return value;
        }
}

dot_description desc_randomSquares( dot_randomSquares, "random squares",
                                3,
                                { "min value", "max_value", "edge width" },
                                {    5.0,  95.0,  10.0 },
                                {    0.0,   1.0,   0.0 },
                                {   99.0, 100.0,  90.0 } );

/******************************************************************************/

// not-so-simple random dot function - random value is the same across a circle in the screen cell
// outside the circle won't fill until 100% black
double dot_randomCircles( double x, double y, int64_t seed, const screen_options &opt )
{
    // scale our parameters from percents to unit range
    double minval = 0.01 * opt.params[0];
    double maxval = 0.01 * opt.params[1];
    double radius = 0.005 * opt.params[2];
    
    // calculate the actual range we'll be using
    double range = maxval - minval;

    // get integer coordinates for the screen cells
    double ix = floor(x);
    double iy = floor(y);
    
    // get fractional coordinates within the cell
    double fx = x - ix;
    double fy = y - iy;
    
    if ( hypot(fx-0.5,fy-0.5) < radius )
        {
        // inside, generate a repeatable value based on the integer cell coordinates and range parameters
        double value = minval + range * hashXY_float( (int64_t)ix, (int64_t)iy, seed );
        // and pin the value for good measure
        value = std::min(1.0,std::max(0.0,value));
        return value;
        }
    else
        {
        //no t inside the circle, return zero, which will only fill in at 100% black
        return 0.0;
        }
}

dot_description desc_randomCircles( dot_randomCircles, "random circles",
                                3,
                                { "min value", "max_value", "diameter" },
                                {    5.0,  95.0,   85.0 },
                                {    0.0,   1.0,    0.0 },
                                {   99.0, 100.0,  100.0 } );

/******************************************************************************/

// not-so-simple random dot function - randomizing max value points for round dots
double dot_randomRound( double x, double y, int64_t seed, const screen_options &opt )
{
    // scale our parameters from percents to unit range
    double minval = 0.01 * opt.params[0];
    double maxval = 0.01 * opt.params[1];
    
    // calculate the actual range we'll be using
    double range = maxval - minval;

    // get integer coordinates for the screen cells
    double ix = floor(x);
    double iy = floor(y);
    
    // get fractional coordinates within the cell
    double fx = x - ix;
    double fy = y - iy;

    // generate a repeatable value based on the integer cell coordinates and range parameters
    double value = minval + range * hashXY_float( (int64_t)ix, (int64_t)iy, seed );

    // use this as the maximum dot value
    double dot = 1.0 - (M_SQRT2 * hypot(fx-0.5,fy-0.5));
    value *= dot;

    // and pin the value for good measure
    value = std::min(1.0,std::max(0.0,value));
    return value;
}

dot_description desc_randomRound( dot_randomRound, "random rounds",
                                2,
                                { "min value", "max_value" },
                                {   55.0,  100.0 },
                                {    0.0,   10.0 },
                                {   99.0,  100.0 } );

/******************************************************************************/

// simple line dot function (triangle/sawtooth)
double dot_line( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    return value;
}

dot_description desc_line( dot_line, "line" );

/******************************************************************************/

double dot_lineRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    if ( (cell_rand & 0x80) == 0)
        return fx;
    else
        return fy;
}

dot_description desc_lineRandom( dot_lineRandom, "line random" );

/******************************************************************************/

double dot_lineRandom2( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    double fx = x - ix;
    double fy = y - iy;
    
    double value = fx;
    if ( (cell_rand & 0x80) == 0)
        value = fy;
    
    if ( (cell_rand & 0x8) == 0)
        value = 1.0 - value;
    
    return value;
}

dot_description desc_lineRandom2( dot_lineRandom2, "line random2" );

/******************************************************************************/

// simple line with breaks
double dot_brokenline( double x, double y, int64_t seed, const screen_options &opt )
{
    double length = opt.params[0];
    double gap_fraction = (1.0/100.0) * opt.params[1];
    const double length_variation = length * (1.0/100.0) * opt.params[2];
    const double offset_variation = length * (1.0/100.0) * opt.params[3];
    const double gap_variation = gap_fraction * (1.0/100.0) * opt.params[4];
    
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    
    double offset = offset_variation * hashXY_float( (int64_t)ix, seed^extraHash1 );
    y += offset;

    double mod = length_variation * (hashXY_float( (int64_t)ix, seed ) - 0.5);
    length += mod;
    
    y /= length;
    double iy = floor(y);
    double fy = y - iy;
    
    double gap_mod = gap_variation * ( hashXY_float( (int64_t)ix, (int64_t)iy, seed^extraHash2 ) - 0.5);
    
    if (fy < (gap_fraction+gap_mod))
        value *= 0.2;       // make it darker, but still fill in at high density
    
    return value;
}

dot_description desc_brokenline( dot_brokenline, "broken line",
                                5,
                                { "line length", "gap percent", "length variation", "offset variation", "gap variation" },
                                {   3.0,  10.0,  10.0,  90.0,  70.0 },
                                {   0.1,   0.1,   0.0,   0.0,   0.0 },
                                {  50.0,  99.0,  50.0, 200.0, 200.0 } );

/******************************************************************************/

// dashes with rounded ends, and dots
// https://www.youtube.com/watch?v=Yj_yFpL6uoE
double dot_dashdot( double x, double y, int64_t seed, const screen_options &opt )
{
    double length = 1.0 + opt.params[0];
    const double length_variation = length * (1.0/100.0) * opt.params[1];
    const double line_offset = length * (1.0/100.0) * opt.params[2];
    const double offset_variation = length * (1.0/100.0) * opt.params[3];
    const double scale = M_SQRT2;
    
    double ix = floor(x);
    double fx = x - ix - 0.5;   // -0.5..+0.5
    double value = scale * fabs(fx);
    
    y -= ix * line_offset;
    
    double offset = offset_variation * hashXY_float( (int64_t)ix, seed^extraHash1 );
    y += offset;

    double mod = length_variation * (hashXY_float( (int64_t)ix, seed ) - 0.5);
    length += mod;

    y /= length;
    double iy = floor(y);
    double fy = y - iy;
    
    fy *= length;
    
    // dot if in last integer end, half dot near ends of dash, abs(x) in between
    if (fy >= (length-1.0))
        {
        double dy = fy - (length-1.0);  // 0..1
        value = scale * hypot(fx, dy-0.5);
        }
    else if (fy < 0.5)
        {
        // top half circle
        value = scale * hypot(fx, 0.5-fy);
        }
    else if (fy > (length-1.5))
        {
        // bottom half circle
        double dy = fy - (length-1.5);  // 0..0.5
        value = scale * hypot(fx, dy);
        }
    
    value = std::min(1.0,std::max(0.0,value));
    return 1.0 - value;
}

dot_description desc_dashdot( dot_dashdot, "dash dot",
                                4,
                                { "dash length", "length variation", "line offset", "offset variation" },
                                {   3.0,   0.0,   33.3,    0.0 },
                                {   1.0,   0.0,    0.0,    0.0 },
                                {  50.0,  50.0,  100.0,  200.0 } );

/******************************************************************************/

// dual lines
double dot_duallines( double x, double y, int64_t seed, const screen_options &opt )
{
    const double angle = (M_PI / 180.0) * opt.params[0];
    
    const double CA = cos(angle);   // cos(-x) = cos(x)
    const double SA = sin(angle);   // sin(-x) = -sin(x)
    
    double x1 = CA * x + SA * y;
    
    double ix = floor(x);
    double ix1 = floor(x1);
    
    double fx = x - ix;
    double fx1 = x1 - ix1;

    double value = 1.0 - std::min(fx,fx1);
    return value*value;
}

dot_description desc_duallines( dot_duallines, "Dual lines",
                                1,
                                { "Angle" },
                                {   8.0 },
                                {   0.1 },
                                { 179.9 } );

/******************************************************************************/

// hex lines
double dot_hexlines( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C60 = cos(60.0*M_PI/180.0);    // = 0.5    // cos(-x) = cos(x)
    const double S60 = sin(60.0*M_PI/180.0);                // sin(-x) = -sin(x)
    
    double x1 = C60 * x - S60 * y;
    double x2 = C60 * x + S60 * y;
    
    double ix = floor(x);
    double ix1 = floor(x1);
    double ix2 = floor(x2);
    
    double fx = x - ix;
    double fx1 = x1 - ix1;
    double fx2 = x2 - ix2;

    double value = 1.0 - std::min( fx, std::min( fx1, fx2 ) );  // unbalanced, but gets good lines
    return value*value;
}

dot_description desc_hexlines( dot_hexlines, "hexlines" );

/******************************************************************************/

// quad lines
double dot_quadlines( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C45 = cos(45.0*M_PI/180.0);    // = 0.707    // cos(-x) = cos(x)
    const double S45 = sin(45.0*M_PI/180.0);    // = 0.707    // sin(-x) = -sin(x)
    
    double x1 = C45 * x - S45 * y;
    double x2 = C45 * x + S45 * y;
    
    double ix = floor(x);
    double iy = floor(y);
    double ix1 = floor(x1);
    double ix2 = floor(x2);
    
    double fx = x - ix;
    double fy = y - iy;
    double fx1 = x1 - ix1;
    double fx2 = x2 - ix2;

    double min1 = std::min(fx,fy);
    double min2 = std::min( fx1, fx2 );
    double value = 1.0 - std::min( min1, min2 );  // unbalanced, but gets good lines
    return value*value;
}

dot_description desc_quadlines( dot_quadlines, "quadlines" );

/******************************************************************************/

static
double equalize_count( const double value, const size_t N )
{
// based on N, return value^X
// X == Log2(N) ???  Nope, needs to be larger
    if (N > 20)
        {
        return pow(value,N/1.50);
        }
    else if (N <= 8)
        {
        if (N <= 2)
            return value;
        else if (N <= 4)
            return value*value;
        else if (N <= 6)
            return value*value*value;
        else
            return value*value*value*value;
        }
    else
        {
        if (N <= 12)
            return value*value*value*value*value;
        else if (N <= 16)
            return value*value*value*value*value*value*value;
        else
            return value*value*value*value*value*value*value*value;
        }
}

/******************************************************************************/

// N lines, dividing 180
double dot_Nlines( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t N = (size_t) round(opt.params[0]);
    static size_t cachedN = 0;
    static std::vector<double> sincosList;
    
    if (N != cachedN)
        {
        sincosList.resize(2*N);
        for (size_t i = 0; i < N; ++i)
            {
            const double angle = i * M_PI / N;
            const double CA = cos(angle);
            const double SA = sin(angle);
            sincosList[2*i+0] = CA;
            sincosList[2*i+1] = SA;
            }
        cachedN = N;
        }
    
    double value = 1.0;
    
    for (size_t i = 0; i < N; ++i)
        {
#if 1
        double CA = sincosList[2*i+0];
        double SA = sincosList[2*i+1];
#else
        const double angle = i * M_PI / N;  // 0; 0,90; 0,60,120; 0,45,90,135; etc.
        const double CA = cos(angle);
        const double SA = sin(angle);
#endif
        double x1 = CA * x - SA * y;
        double ix = floor(x1);
        double fx = x1 - ix;
        value = std::min( value, fx );
        }

    value = 1.0 - value;
    
    return equalize_count( value, N );
}

dot_description desc_Nlines( dot_Nlines, "N-lines",
                                1,
                                { "line count" },
                                {  8.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// N random lines
double dot_randomlines( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t N = (size_t) round(opt.params[0]);
    const double block_period = 1.0 / opt.params[1];
    const int cached_count = 3;
    static size_t cachedN = 0;
    static int64_t cached_seed = 0;
    static std::vector<double> sincosList;
    
    if ( (N != cachedN) || (seed != cached_seed) )
        {
        sincosList.resize(cached_count*N);
        for (size_t i = 0; i < N; ++i)
            {
            double angle = i * (M_PI / N);  // evenly distributed angle
            angle += hashXY_float(i,seed) * (0.517 * (M_PI / N));   // add a little randomness
            const double offset = hashXY_float(i,seed^extraHash1);
            const double CA = cos(angle);
            const double SA = sin(angle);
            sincosList[cached_count*i+0] = CA;
            sincosList[cached_count*i+1] = SA;
            sincosList[cached_count*i+2] = offset;
            }
        cachedN = N;
        cached_seed = seed;
        }
    
    x *= block_period;
    y *= block_period;

    double value = 1.0;
    
    for (size_t i = 0; i < N; ++i)
        {
#if 1
        double CA = sincosList[cached_count*i+0];
        double SA = sincosList[cached_count*i+1];
        double offset = sincosList[cached_count*i+2];
#else
        const double angle = i * (M_PI / N);
        const double CA = cos(angle);
        const double SA = sin(angle);
        const double offset = hashXY_float(i,seed^extraHash1);
#endif
        double x1 = CA * x - SA * y + offset;
        double ix = floor(x1);
        double fx = x1 - ix;
        value = std::min( value, fx );
        }

    value = 1.0 - value;
    
    return equalize_count( value, N );
}

dot_description desc_randomlines( dot_randomlines, "random lines",
                                2,
                                { "line count", "block period" },
                                {  73.0,    23.0 },
                                {   1.0,     5.0 },
                                { 200.0,  5000.0 } );

/******************************************************************************/

// N random waves
double dot_randomwaves( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t N = (size_t) round(opt.params[0]);
    const double block_period = 1.0 / opt.params[1];
    const double wave_frequency = opt.params[2];
    const double wave_amplitude = opt.params[3] * block_period;
    const int cached_count = 3;
    static size_t cachedN = 0;
    static int64_t cached_seed = 0;
    static std::vector<double> sincosList;
    
    if ( (N != cachedN) || (seed != cached_seed) )
        {
        sincosList.resize(cached_count*N);
        for (size_t i = 0; i < N; ++i)
            {
            double angle = i * (M_PI / N);  // evenly distributed angle
            angle += hashXY_float(i,seed) * (0.517 * (M_PI / N));   // add a little randomness
            const double offset = hashXY_float(i,seed^extraHash1);
            const double CA = cos(angle);
            const double SA = sin(angle);
            sincosList[cached_count*i+0] = CA;
            sincosList[cached_count*i+1] = SA;
            sincosList[cached_count*i+2] = offset;
            }
        cachedN = N;
        cached_seed = seed;
        }
    
    x *= block_period;
    y *= block_period;

    double value = 1.0;
    
    for (size_t i = 0; i < N; ++i)
        {
#if 1
        double CA = sincosList[cached_count*i+0];
        double SA = sincosList[cached_count*i+1];
        double offset = sincosList[cached_count*i+2];
#else
        const double angle = i * (M_PI / N);
        const double CA = cos(angle);
        const double SA = sin(angle);
        const double offset = hashXY_float(i,seed^extraHash1);
#endif
        double x1 = CA * x - SA * y + offset;
        double y1 = SA * x + CA * y;
            x1 += wave_amplitude * sin( y1*wave_frequency );
        double ix = floor(x1);
        double fx = x1 - ix;
        value = std::min( value, fx );
        }

    value = 1.0 - value;
    
    return equalize_count( value, N );
}

dot_description desc_randomwaves( dot_randomwaves, "random waves",
                                4,
                                { "line count", "block period", "wave frequency", "wave_amplitude" },
                                {  51.0,    23.0,   32.0,   0.5 },
                                {   1.0,     5.0,    0.1,   0.0 },
                                { 200.0,  5000.0,  200.0,  20.0 } );

// almost scribble like
dot_description desc_randomwaves2( dot_randomwaves, "random waves2",
                                4,
                                { "line count", "block period", "wave frequency", "wave_amplitude" },
                                {  92.0,    41.0,  131.0,   3.7 },
                                {   1.0,     5.0,    0.1,   0.0 },
                                { 200.0,  5000.0,  200.0,  20.0 } );

/******************************************************************************/

// Moire lines
double dot_moirelines( double x, double y, int64_t seed, const screen_options &opt )
{
    const double angle = (M_PI/180.0) * opt.params[0];
    
    const double CA = cos(angle);   // cos(-x) = cos(x)
    const double SA = sin(angle);   // sin(-x) = -sin(x)
    
    double x1 = CA * x + SA * y;
    double y1 = SA * x - CA * y;
    
    double ix = floor(x);
    double ix1 = floor(x1);
    double iy = floor(y);
    double iy1 = floor(y1);
    
    double fx = x - ix;
    double fx1 = x1 - ix1;
    double fy = y - iy;
    double fy1 = y1 - iy1;
    
    double min1 = std::min( fx, fy );
    double min2 = std::min( fx1, fy1 );

    double value = 1.0 - std::min(min1,min2);
    return value*value*value;
}

dot_description desc_moirelines( dot_moirelines, "Moiré lines",
                                1,
                                { "Angle" },
                                {   9.0 },
                                {   0.1 },
                                { 179.9 } );

/******************************************************************************/

// N lines at 90 degrees, offset and buliding up for darker tones
double dot_OffsetLines( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t N = (size_t) round(opt.params[0]);
    const double coverage = opt.params[1] / double(N/2);
    
    double value = 1.0;

    for (size_t i = 0; i < N; ++i)
        {
        double offset = 3.5 * double(i) / N;
        
        double t = ((i & 0x01) == 0) ? x : y;
        
        t += offset;

        double it = floor(t);
        double ft = t - it;
        
        if (ft < coverage)
            {
            double tmp = double(i+1) / double(N+1);
            value = std::min( value, tmp );
            }
        }

    value = 1.0 - value;

    return value;
}

dot_description desc_OffsetLines( dot_OffsetLines, "Offset Lines",
                                2,
                                { "line count", "line coverage" },
                                { 16.0,  0.90 },
                                {  1.0,  0.01 },
                                { 40.0,  1.00 } );

/******************************************************************************/

// N sets of crossed lines, offset and building up for darker tones
double dot_OffsetLines2( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t N = (size_t) round(opt.params[0]);
    const double coverage = 0.5 * opt.params[1] / double(N/2);
    
    double value = 1.0;

    for (size_t i = 0; i < N; ++i)
        {
        double offset = 7.0 * double(i) / N;
        
        double t = x + offset;

        double it = floor(t);
        double ft = t - it;
        
        if (ft < coverage)
            {
            double tmp = double(i+1) / double(N+1);
            value = std::min( value, tmp );
            }
        
        double k = y + offset;
        double ik = floor(k);
        double fk = k - ik;
        
        if (fk < coverage)
            {
            double tmp = double(i+1) / double(N+1);
            value = std::min( value, tmp );
            }
        
        }

    value = 1.0 - value;

    return value;
}

dot_description desc_OffsetLines2( dot_OffsetLines2, "Offset Lines2",
                                2,
                                { "line count", "line coverage" },
                                { 12.0, 0.90 },
                                {  1.0,  0.01 },
                                { 40.0,  1.00 } );

/******************************************************************************/

// mix of line and periodic
double dot_cone( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double fx = x - ix;
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 * fx + 0.25 + 0.25 * sy;
    return value;
}

dot_description desc_cone( dot_cone, "cone" );

/******************************************************************************/

// mix of wedge and periodic
double dot_sawtooth( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scaley = opt.params[0];
    const double scalex = 1.0 - scaley;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.75 * (scaley * fy + scalex * fx) + 0.125 + 0.125 * sy;
    return value;
}

dot_description desc_sawtooth( dot_sawtooth, "sawtooth",
                                1,
                                { "bias scale" },
                                { 0.15 },
                                { 0.00 },
                                { 1.00 } );

/******************************************************************************/

// saw blade lines
double dot_sawblade( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C60 = cos(60*M_PI/180);    // = 0.5    // cos(-x) = cos(x)
    const double S60 = sin(60*M_PI/180);                // sin(-1) = -sin(x)
    
    double x1 = C60 * x - S60 * y;
    double x2 = C60 * x + S60 * y;
    
    double ix = floor(x);
    double ix1 = floor(x1);
    double ix2 = floor(x2);
    
    double fx = x - ix;
    double fx1 = x1 - ix1;
    double fx2 = x2 - ix2;
    
    double value = std::max( fx, std::min( fx1, fx2 ) );
    return value;
}

dot_description desc_sawblade( dot_sawblade, "saw blade" );

/******************************************************************************/

// 60 degree slanted diamonds, but not in a unit cell
double dot_diamond2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C60 = cos(60.0*M_PI/180.0);    // = 0.5    // cos(-x) = cos(x)
    const double S60 = sin(60.0*M_PI/180.0);                // sin(-x) = -sin(x)
    
    double x1 = C60 * x - S60 * y;
    
    double ix = floor(x);
    double ix1 = floor(x1);
    
    double fx = x - ix;
    double fx1 = x1 - ix1;

    double value = std::min( fx, fx1 );
    return value;
}

dot_description desc_diamond2( dot_diamond2, "diamond2" );

/******************************************************************************/

// symmetric line dot function (tent)
double dot_line_symmetric( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double fx = x - ix;
    double value = fabs((fx * 2.0) - 1.0);
    return 1.0 - value;
}

dot_description desc_line_symmetric( dot_line_symmetric, "symmetric line" );

/******************************************************************************/

// symmetric line dot function, random vert or horiz
double dot_line_symmetric_random( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    double value = fx;
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    if ( (cell_rand & 0x80) == 0)
        value = fy;
    
    value = fabs((value * 2.0) - 1.0);
    return 1.0 - value;
}

dot_description desc_line_symmetric_random( dot_line_symmetric_random, "symmetric line random" );

/******************************************************************************/

// symmetric line dot function, random vert, horiz, or diagonal
double dot_line_symmetric_random2( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    double value = fx;
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    int64_t index = cell_rand & 0x03;
    switch( index )
        {
        case 0:
            value = fabs((fx+fx) - 1.0);
            break;
        case 1:
            value = fabs((fy+fy) - 1.0);
            break;
        case 2:
            value = 1.3 * fabs((fy + fx) - 1.0);    // 1.414 would be closer to correct, but leaves ugly gaps in shadows
            break;
        case 3:
            value = 1.3 * fabs((fy - fx));
            break;
        }
    
    return 1.0 - std::max(0.0, std::min(1.0, value ));
}

dot_description desc_line_symmetric_random2( dot_line_symmetric_random2, "symmetric line random2" );

/******************************************************************************/

// wavey line
double dot_waveyline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double wave_period = (2.0*M_PI)/ opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
        x += distort_scale * dx;
        }
    
    double offset = wave_amplitude * cos( y * wave_period );
    x += offset;
    
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    return value;
}

dot_description desc_waveyline( dot_waveyline, "wavey line",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  7.0,  1.2,   0.0 },
                                {  1.0,  0.01,  0.0 },
                                { 50.0, 10.0,  50.0 } );

/******************************************************************************/

// wavey grid
double dot_waveygrid( double x, double y, int64_t seed, const screen_options &opt )
{
    const double wave_period = (2.0*M_PI)/ opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
        double dy = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash1 );
        x += distort_scale * dx;
        y += distort_scale * dy;
        }
    
    double offsetX = wave_amplitude * cos( y * wave_period );
    x += offsetX;
    double offsetY = wave_amplitude * cos( x * wave_period );
    y += offsetY;
    
    double ix = floor(x);
    double fx = x - ix;
    double iy = floor(y);
    double fy = y - iy;
    
    double value = 1.0 - std::min(fx,fy);
    return value*value;
}

dot_description desc_waveygrid( dot_waveygrid, "wavey grid",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  7.0,  1.2,   0.0 },
                                {  1.0,  0.01,  0.0 },
                                { 50.0, 10.0,  50.0 } );

/******************************************************************************/

// wavey grid
double dot_waveygrid2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C45 = cos(45.0*M_PI/180.0);    // = 0.707    // cos(-x) = cos(x)
    const double S45 = sin(45.0*M_PI/180.0);    // = 0.707    // sin(-x) = -sin(x)
    
    const double wave_period = (2.0*M_PI)/ opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    double x1 = C45 * x - S45 * y;
    double y1 = C45 * x + S45 * y;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
        double dy = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash1 );
        x += distort_scale * dx;
        x1 += distort_scale * dx;
        y += distort_scale * dy;
        y1 += distort_scale * dy;
        }
    
    double offsetX = wave_amplitude * cos( y * wave_period );
    double offsetY = wave_amplitude * cos( x * wave_period );
    double offsetX1 = wave_amplitude * cos( y1 * wave_period );
    double offsetY1 = wave_amplitude * cos( x1 * wave_period );
    
    x += offsetX;
    y += offsetY;
    x1 += offsetX1;
    y1 += offsetY1;
    
    double ix = floor(x);
    double iy = floor(y);
    double ix1 = floor(x1);
    double iy1 = floor(y1);
    
    double fx = x - ix;
    double fy = y - iy;
    double fx1 = x1 - ix1;
    double fy1 = y1 - iy1;
    
    double min1 = std::min(fx,fy);
    double min2 = std::min(fx1,fy1);
    double value = 1.0 - std::min(min1,min2);
    return value*value*value*value;
}

dot_description desc_waveygrid2( dot_waveygrid2, "wavey grid2",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  6.15,  1.7,   0.0 },
                                {  1.0,   0.01,  0.0 },
                                { 50.0,  10.0,  50.0 } );

/******************************************************************************/

// herringbone / bent line
double dot_herringbone( double x, double y, int64_t seed, const screen_options &opt )
{
    const double wave_period = 1.0 / opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash3 );
        x += distort_scale * dx;
        }
    
    y *= wave_period;
    double iy = floor(y);
    double fy = y - iy;
    if (fy > 0.5)
        fy = 1.0 - fy;
    
    double offset = wave_amplitude * fy;
    x += offset;
    
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    return value;
}

dot_description desc_herringbone( dot_herringbone, "herringbone",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  4.0,  3.0,   0.0 },
                                {  1.0,  0.01,  0.0 },
                                { 50.0, 10.0,  50.0 } );

/******************************************************************************/

// double herringbone / bent grid
double dot_bentgrids( double x, double y, int64_t seed, const screen_options &opt )
{
    const double wave_period = 1.0 / opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
        double dy = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash1 );
        x += distort_scale * dx;
        y += distort_scale * dy;
        }
    
    double yy = y * wave_period;
    double xx = x * wave_period;
    
    double iy = floor(yy);
    double fy = yy - iy;
    if (fy > 0.5)
        fy = 1.0 - fy;
    
    double ix = floor(xx);
    double fx = xx - ix;
    if (fx > 0.5)
        fx = 1.0 - fx;
    
    y += wave_amplitude * fx;
    x += wave_amplitude * fy;
    
    ix = floor(x);
    iy = floor(y);
    fx = x - ix;
    fy = y - iy;
    
    double value = 1.0 - std::min(fx,fy);
    
    return value*value;
}

dot_description desc_bentgrids( dot_bentgrids, "bent grids",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  2.2,  3.0,   0.0 },
                                {  1.0,  0.01,  0.0 },
                                { 50.0, 10.0,  50.0 } );

/******************************************************************************/

// bent grid
double dot_bentgrids2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C45 = cos(45.0*M_PI/180.0);    // = 0.707    // cos(-x) = cos(x)
    const double S45 = sin(45.0*M_PI/180.0);    // = 0.707    // sin(-x) = -sin(x)
    
    const double wave_period = (2.0*M_PI)/ opt.params[0];
    const double wave_amplitude = opt.params[1];
    double distort_scale = opt.params[2];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    double x1 = C45 * x - S45 * y;
    double y1 = C45 * x + S45 * y;
    
    if (distort_scale > 0.0)
        {
        double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
        double dy = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash1 );
        x += distort_scale * dx;
        x1 += distort_scale * dx;
        y += distort_scale * dy;
        y1 += distort_scale * dy;
        }
    
    double yy = y * wave_period;
    double xx = x * wave_period;
    double yy1 = y1 * wave_period;
    double xx1 = x1 * wave_period;
    
    double iy = floor(yy);
    double fy = yy - iy;
    if (fy > 0.5)
        fy = 1.0 - fy;
    
    double ix = floor(xx);
    double fx = xx - ix;
    if (fx > 0.5)
        fx = 1.0 - fx;
    
    double iy1 = floor(yy1);
    double fy1 = yy1 - iy1;
    if (fy1 > 0.5)
        fy1 = 1.0 - fy1;
    
    double ix1 = floor(xx1);
    double fx1 = xx1 - ix1;
    if (fx1 > 0.5)
        fx1 = 1.0 - fx1;
    
    y += wave_amplitude * fx;
    x += wave_amplitude * fy;
    y1 += wave_amplitude * fx1;
    x1 += wave_amplitude * fy1;
   
    ix = floor(x);
    iy = floor(y);
    ix1 = floor(x1);
    iy1 = floor(y1);
    
    fx = x - ix;
    fy = y - iy;
    fx1 = x1 - ix1;
    fy1 = y1 - iy1;
    
    double min1 = std::min(fx,fy);
    double min2 = std::min(fx1,fy1);
    double value = 1.0 - std::min(min1,min2);
    return value*value*value*value;
}

dot_description desc_bentgrids2( dot_bentgrids2, "bent grids2",
                                3,
                                { "wave period", "wave amplitude", "distortion" },
                                {  8.2,  1.7,   0.0 },
                                {  1.0,  0.01,  0.0 },
                                { 50.0, 10.0,  50.0 } );

/******************************************************************************/

// concentric circle wedge dot function
double dot_concentric( double x, double y, int64_t seed, const screen_options &opt )
{
    double r = hypot(x,y);
    double ir = floor(r);
    double value = r - ir;
    //double value = 0.5 + 0.5 * cos( (2.0*M_PI) * r );   // needs normalization, but symmetric and smooth
    return value;
}

dot_description desc_concentric( dot_concentric, "concentric" );

/******************************************************************************/

// concentric circle wedge dot function, with waves
double dot_concentricwaves( double x, double y, int64_t seed, const screen_options &opt )
{
    const double amplitude = opt.params[0];
    const double freq = round(opt.params[1]) * (2.0*M_PI);    // should be whole integer, but what if not?
    const double phase = opt.params[2] * (2.0*M_PI/100.0);
    const double swirl = opt.params[3] / 100.0;

    double r = hypot(x,y);  // 0..inf
    double angle = 0.5 + atan2(x,y) / (2.0*M_PI);   // 0..1

    r += amplitude * sin( phase + (angle*freq) + (r*swirl) );
    
    double ir = floor(r);
    double value = r - ir;
    return value;
}

dot_description desc_concentricwaves( dot_concentricwaves, "concentric waves",
                                4,
                                { "amplitude", "frequency", "phase offset", "swirl" },
                                {     4.0,    10.00,     10.0,     0.0 },
                                { -1000.0,  -200.00,   -100.0,  -500.0 },
                                {  1000.0,   200.00,    100.0,   500.0 } );

/******************************************************************************/

double dot_concentricdistorted( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -4;
    const int32_t octave_high = 1;
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash2 ) - 0.5;
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash3 ) - 0.5;

    x += distort_scale * dx;
    y += distort_scale * dy;
    
    double r = hypot(x,y);
    double ir = floor(r);
    double value = r - ir;
    return value;
}

dot_description desc_concentricdistorted( dot_concentricdistorted, "distorted concentric",
                                1,
                                { "distortion" },
                                { 10.0 },
                                { 0.1 },
                                { 50.0 } );

/******************************************************************************/

double dot_radialdistorted( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    double distort_scale = opt.params[1];
    
    const int32_t octave_low = -4;
    const int32_t octave_high = 1;
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash2 ) - 0.5;
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash3 ) - 0.5;

    x += distort_scale * dx;
    y += distort_scale * dy;
    
    double angle = count * (0.5 + atan2(x,y) / (2*M_PI));
    double ia = floor(angle);
    double value = angle - ia;
    return value;
}

dot_description desc_radialdistorted( dot_radialdistorted, "distorted radial",
                                2,
                                { "line count", "distortion" },
                                {   150.0,  10.0 },
                                {     2.0,   0.1 },
                                { 10000.0,  50.0 } );

/******************************************************************************/

// concentric N-gons
double dot_concentricPoly( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round(opt.params[0]);

    double angle1 = atan2(x,y);
    if (angle1 < 0.0)
        angle1 = 2.0*M_PI + angle1;
    
    double angle = angle1 / (2.0*M_PI);   // 0..1
    assert( angle >= 0.0 && angle <= 1.0 );
    
    int64_t quantizedAngle = round( angle * sides );

    double angle2 = (quantizedAngle) * ((2.0*M_PI) / sides);
    double CA = cos(angle2);
    double SA = sin(angle2);
    //double distance = CA * x - SA * y;        // x
    double distance = SA * x + CA * y;        // y

    double ir = floor(distance);
    double value = distance - ir;
    return value;
}

dot_description desc_concentriPoly( dot_concentricPoly, "concentric polygon",
                                1,
                                { "sides" },
                                {   6.0 },
                                {   3.0 },
                                { 100.0 } );

/******************************************************************************/

// radial N-gons
double dot_radialPoly( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round(opt.params[0]);
    
    double angle1 = atan2(x,y);
    if (angle1 < 0.0)
        angle1 = 2.0*M_PI + angle1;
    
    double angle = angle1 / (2.0*M_PI);   // 0..1
    assert( angle >= 0.0 && angle <= 1.0 );
    
    int64_t quantizedAngle = round( angle * sides );

    double angle2 = (quantizedAngle) * ((2.0*M_PI) / sides);
    double CA = cos(angle2);
    double SA = sin(angle2);
    double distance = CA * x - SA * y;        // x
    //double distance = SA * x + CA * y;        // y

    double ir = floor(distance);
    double value = distance - ir;
    return value;
}

dot_description desc_radialPoly( dot_radialPoly, "radial polygon",
                                1,
                                { "sides" },
                                {   6.0 },
                                {   3.0 },
                                { 100.0 } );

/******************************************************************************/

// spiral r = b*angle
double dot_spiralline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = 1.0;    // let scale handle it, don't use a parameter
    double angle = 0.5 + atan2(y,x)/(2.0*M_PI);     // 0..1
    double r = hypot(x,y);                          // 0..INF
    double temp = angle + r * ring_spacing;         // integral when exactly on the spiral
    double wasted;
    double fract = modf(temp,&wasted);
    double dist = fract;
    return dist;
}

dot_description desc_spiralline( dot_spiralline, "spiral line" );

/******************************************************************************/

// radial lines dot function
double dot_radial( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    double angle = count * (0.5 + atan2(x,y) / (2*M_PI));
    double ia = floor(angle);
    double value = angle - ia;
    return value;
}

dot_description desc_radial( dot_radial, "radial",
                                1,
                                { "line count" },
                                {   200.0 },
                                {     2.0 },
                                { 10000.0 } );

/******************************************************************************/

// radial lines dot function, inverted every other radial space
double dot_radial3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    double angle = count * (0.5 + atan2(x,y) / (2*M_PI));
    double r = hypot(x,y);
    double ia = floor(angle);
    int64_t ir = floor(r);
    double value = angle - ia;
    
    if ((ir & 1) != 0)
        value = 1.0 - value;
    
    return value;
}

dot_description desc_radial3( dot_radial3, "radial3",
                                1,
                                { "line count" },
                                {   100.0 },
                                {     2.0 },
                                { 10000.0 } );

/******************************************************************************/

// radial lines, twisted, oscillating
double dot_radialzigzag( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double amplitude = opt.params[1] * (2.0*M_PI/100.0);
    const double freq = opt.params[2] * (2.0*M_PI/100.0);
    const double phase = opt.params[3] * (2.0*M_PI/100.0);
    const double swirl = opt.params[4] / 100.0;
    
    double r = hypot(x,y);
    double angle = atan2(x,y);
    
    double dist = phase + r * freq;
    double zig = sin( dist );
    
    angle += (r * swirl) + amplitude * zig;
    
    double fan = count * (0.5 + angle / (2.0*M_PI));
    
    double ia = floor(fan);
    double value = fan - ia;
    return value;
}

dot_description desc_radialzigzag( dot_radialzigzag, "radial zigzag",
                                5,
                                { "line count", "twist percent", "frequency", "phase offset", "swirl" },
                                {    60.0,     5.00,     5.00,    10.0,     0.0 },
                                {     2.0,  -200.00,     0.01,  -100.0,  -500.0 },
                                {  1000.0,   200.00,   200.00,   100.0,   500.0 } );

/******************************************************************************/

// radial lines, twisted, oscillating, sharp edges
double dot_radialzigzag2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double amplitude = opt.params[1] * (2.0*M_PI/100.0);
    const double freq = opt.params[2] * (1.0/100.0);
    const double phase = opt.params[3] * (1.0/100.0);
    const double swirl = opt.params[4] / 100.0;
    
    double r = hypot(x,y);
    double angle = atan2(x,y);
    
    // -1 -> 1 -> -1
    double dist = phase + r * freq;
    double idist = dist - floor( dist );
    double sawtooth = (3.0 * idist) - 1.0;
    if (sawtooth > 1.0)
        sawtooth = 2.0 - sawtooth;
    
    angle += (r * swirl) + amplitude * (1.0 - sawtooth);
    
    double fan = count * (0.5 + angle / (2.0*M_PI));
    
    double ia = floor(fan);
    double value = fan - ia;
    return value;
}

dot_description desc_radialzigzag2( dot_radialzigzag2, "radial zigzag sharp",
                                5,
                                { "line count", "twist percent", "frequency", "phase offset", "swirl" },
                                {    60.0,     5.00,     5.00,     0.0,     0.0 },
                                {     2.0,  -200.00,     0.01,  -100.0,  -500.0 },
                                {  1000.0,   200.00,   200.00,   100.0,   500.0 } );

/******************************************************************************/

// radial lines, with gaps
double dot_radial2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double length = opt.params[1];
    const double gap = opt.params[2] / 100.0;
    
    double rr = hypot(x,y) / length;
    double angle = count * (0.5 + atan2(x,y) / (2*M_PI));
    double ir = floor(rr);
    double fr = rr - ir;
    
    double ia = floor(angle);
    double value = angle - ia;
    if (fr < gap)
        return 0.01;
    return value;
}

dot_description desc_radial2( dot_radial2, "radial divided",
                                3,
                                { "line count", "line length", "line gap" },
                                {   200.0,   2.0,   10.0 },
                                {     2.0,   0.2,    0.0 },
                                { 10000.0, 100.0,  100.0 } );

/******************************************************************************/

// radial lines, swirled
double dot_swirl( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double swirl = 0.1 * opt.params[1];
    double r = hypot(x,y);
    double swirl_angle = r * swirl;
    double angle = swirl_angle + (count * (0.5 + atan2(x,y) / (2*M_PI)));
    double ir = floor(angle);
    double value = angle - ir;
    return value;
}

dot_description desc_swirl( dot_swirl, "swirl",
                                2,
                                { "line count", "rotation" },
                                {    57.0,     4.0 },
                                {     2.0,  -500.0 },
                                { 10000.0,   500.0 } );

/******************************************************************************/

// radial lines, swirled with gaps
double dot_swirl2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double swirl = 0.1 * opt.params[1];
    const double length = opt.params[2];
    const double gap = opt.params[3] / 100.0;
    
    double r = hypot(x,y);
    double rr = r / length;
    double ir = floor(rr);
    double fr = rr - ir;
    double swirl_angle = r * swirl;
    double angle = swirl_angle + (count * (0.5 + atan2(x,y) / (2*M_PI)));
    double ia = floor(angle);
    double value = angle - ia;
    if (fr < gap)
        return 0.01;
    return value;
}

dot_description desc_swirl2( dot_swirl2, "swirl divided",
                            4,
                            { "line count", "rotation", "line length", "line gap" },
                            {    91.0,    -6.0,   3.0,   15.0 },
                            {     2.0,  -500.0,   0.2,    0.0 },
                            { 10000.0,   500.0, 100.0,  100.0 } );

/******************************************************************************/

// multiple radial lines arranged along a line
double dot_radial_moire_linear( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double xc = center_width * (((double)center / double(center_count-1)) - 0.5);
        double yc = 0.0;
        
        double angle = line_count * (0.5 + (atan2(x-xc,y-yc) / (2*M_PI))) + 0.1*center;
        double ia = floor(angle);
        value = std::min( value, angle - ia );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_radial_moire_linear( dot_radial_moire_linear, "radial moiré linear",
                                3,
                                { "line count", "center count", "center spread" },
                                {   200.0,   3.0,  4.3 },
                                {     2.0,   2.0,  0.1 },
                                { 10000.0,  50.0, 50.0 } );

/******************************************************************************/

// multiple radial lines spread on a circle
double dot_radial_moire_circle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    const double cr = center_width / 2.0;
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double ca = ((double)center / double(center_count)) * (2.0*M_PI);
        double xc = cr * cos(ca);
        double yc = cr * sin(ca);
        
        double angle = line_count * (0.5 + (atan2(x-xc,y-yc) / (2*M_PI))) + 0.1*center;
        double ia = floor(angle);
        value = std::min( value, angle - ia );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_radial_moire_circle( dot_radial_moire_circle, "radial moiré circle",
                                3,
                                { "line count", "center count", "center spread" },
                                {   200.0,   5.0,  7.0 },
                                {     2.0,   2.0,  0.1 },
                                { 10000.0,  50.0, 50.0 } );

/******************************************************************************/

// multiple radial lines spread randomly
double dot_radial_moire_random( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double xc = center_width * (hash64_float( center^extraHash1, seed^extraHash3 ) - 0.5);
        double yc = center_width * (hash64_float( center^extraHash2, seed^extraHash4 ) - 0.5);
        
        double angle = line_count * (0.5 + (atan2(x-xc,y-yc) / (2*M_PI))) + 0.1*center;
        double ia = floor(angle);
        value = std::min( value, angle - ia );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_radial_moire_random( dot_radial_moire_random, "radial moiré random",
                                3,
                                { "line count", "center count", "center spread" },
                                {   200.0,   5.0,  7.0 },
                                {     2.0,   2.0,  0.1 },
                                { 10000.0,  50.0, 50.0 } );

/******************************************************************************/

// multiple concentric lines with centers arranged along a line
double dot_concentric_moire_linear( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double xc = center_width * (((double)center / double(center_count-1)) - 0.5);
        double yc = 0.0;
        
        double r = line_count * hypot(x-xc,y-yc);
        double ir = floor(r);
        value = std::min( value, r - ir );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_concentric_moire_linear( dot_concentric_moire_linear, "concentric moiré linear",
                                3,
                                { "line count", "center count", "center spread" },
                                {   1.0,   3.0,  4.3 },
                                {   0.1,   2.0,  0.1 },
                                { 100.0,  50.0, 50.0 } );

/******************************************************************************/

// multiple concentric lines with centers arranged along a circle
double dot_concentric_moire_circle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    const double cr = center_width / 2.0;
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double ca = ((double)center / double(center_count)) * (2.0*M_PI);
        double xc = cr * cos(ca);
        double yc = cr * sin(ca);
        
        double r = line_count * hypot(x-xc,y-yc);
        double ir = floor(r);
        value = std::min( value, r - ir );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_concentric_moire_circle( dot_concentric_moire_circle, "concentric moiré circle",
                                3,
                                { "line count", "center count", "center spread" },
                                {     1.0,   5.0,  7.0 },
                                {     0.1,   2.0,  0.1 },
                                {   100.0,  50.0, 50.0 } );

/******************************************************************************/

// multiple concentric lines with centers arranged randomly
double dot_concentric_moire_random( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = opt.params[0];
    const int center_count = round(opt.params[1]);
    const double center_width = opt.params[2];
    
    double value = 1.0;
    
    for (int center = 0; center < center_count; ++center )
        {
        double xc = center_width * (hash64_float( center^extraHash1, seed^extraHash3 ) - 0.5);
        double yc = center_width * (hash64_float( center^extraHash2, seed^extraHash4 ) - 0.5);
        
        double r = line_count * hypot(x-xc,y-yc);
        double ir = floor(r);
        value = std::min( value, r - ir );
        }
    
    return pow( value, 1.0/(center_count-1.0) );  // sqrt(value);
}

dot_description desc_concentric_moire_random( dot_concentric_moire_random, "concentric moiré random",
                                3,
                                { "line count", "center count", "center spread" },
                                {     1.0,   5.0,  7.0 },
                                {     0.1,   2.0,  0.1 },
                                {   100.0,  50.0, 50.0 } );

/******************************************************************************/

typedef double inner_dot_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size );

double circle_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    double dist = hypot( dx, dy );
    return dist;
}

double diamond_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    double dist = fabs(dx) + fabs(dy);
    return dist;
}

double square_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    double dist = std::max( fabs(dx), fabs(dy) );
    return dist;
}

double cross_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    double dist = 1.4 * std::min( fabs(dx), fabs(dy) );
    return dist;
}

double radialburst_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    ca += M_PI/2.0;
    double dv = cos(ca) * dx + sin(ca) * dy;
    double dz = sin(ca) * dx - cos(ca) * dy;
    double dist = 1.7 * fabs(dv);
    if ( fabs(dz) > (0.8*dot_size) )
        return dot_size + 0.1;
    else
        return dist;
}

double dashline_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    ca += M_PI/2.0;
    double dv = cos(ca) * dx + sin(ca) * dy;    // angle
    double dz = sin(ca) * dx - cos(ca) * dy;    // radius
    double dist = 0.9 * fabs(dz);
    if ( fabs(dv) > (0.8*dot_size) )
        return dot_size + 0.1;
    else
        return dist;
}

double hexagon_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    const double CA1 = cos(ca);
    const double SA1 = sin(ca);
    const double C60 = cos(ca+60.0*(M_PI/180.0));
    const double S60 = sin(ca+60.0*(M_PI/180.0));
    const double C602 = cos(ca-60.0*(M_PI/180.0));
    const double S602 = sin(ca-60.0*(M_PI/180.0));

    double xx = CA1 * dx - SA1 * dy;
    double x1 = C60 * dx - S60 * dy;
    double x2 = C602 * dx - S602 * dy;

    double distance = std::max( fabs(xx), std::max( fabs(x1), fabs(x2) ) );
    return 1.4 * distance;
}

// points left
double triangle_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    const double CA1 = cos(ca);
    const double SA1 = sin(ca);
    const double C60 = cos(ca+60.0*(M_PI/180.0));
    const double S60 = sin(ca+60.0*(M_PI/180.0));
    const double C602 = cos(ca-60.0*(M_PI/180.0));
    const double S602 = sin(ca-60.0*(M_PI/180.0));

    double xx = CA1 * dx - SA1 * dy;
    double x1 = C60 * dx - S60 * dy;
    double x2 = C602 * dx - S602 * dy;

    double distance = std::max( -xx, std::max( x1, x2 ) );
    return 1.4 * distance;
}

// rotated 90 degrees, point upward
double triangle_dist_func2(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    return triangle_dist_func( -dy, dx, cell_id, cx, cy, ca, dot_size );
}

double random_dist_func(double dx, double dy, int64_t cell_id, double cx, double cy, double ca, double dot_size )
{
    switch (((uint64_t)cell_id) % 4)
        {
        default:
        case 0:
            return circle_dist_func(dx,dy,cell_id,cx,cy,ca,dot_size);
            break;
        case 1:
            return diamond_dist_func(dx,dy,cell_id,cx,cy,ca,dot_size);
            break;
        case 2:
            return square_dist_func(dx,dy,cell_id,cx,cy,ca,dot_size);
            break;
        case 3:
            return cross_dist_func(dx,dy,cell_id,cx,cy,ca,dot_size);
            break;
        }
}

/******************************************************************************/

// concentric rings of shapes
// TODO - single center option!  goes with dots centered on 1.0 instead of half
double concentric_common( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun, double ring_spacing, double dot_spacing, double angle_offset, double angle_randomness, double angle_jitter = 0.0, double damage = 0.0 )
{
    const double dot_size = opt.params[0];
assert(dot_spacing > 0.0);
    dot_spacing = 1.0 / dot_spacing;
    angle_offset *= M_PI;
    angle_randomness *= M_PI;
    
    double r = hypot(x,y);          // 0..INF

#if 0
// single shape in center, doesn't always look right
    if (r < (0.5*ring_spacing))
        {
        // single dot in center
        if (r > dot_size)
            return 0.0;
        else
            return 1.0 - (r / dot_size);
        }
#endif

    r /= ring_spacing;
    
    double in_angle = atan2(y,x);      // -M_PI..M_PI
    double ir = floor(r);
    
    double angle_random = angle_randomness * hash64_float( ir );
    
    double scaled_radius = (ir+0.5)*ring_spacing;
    double dots_allowed = std::max(1.0, floor( dot_spacing * scaled_radius*2.0*M_PI ));
    double dots_factor = dots_allowed / (2.0*M_PI);       // assuming radius 1.0, midway in ring

    double offset = ir*angle_offset + angle_random;
    double angle = (in_angle+offset) * dots_factor;
    
    double ia = floor(angle);

    // handle wrap/discontinuity in quantized angle around +-PI
    if ((2*ia) >= dots_allowed)
        ia -= dots_allowed;
    if ((2*ia) <= -dots_allowed)
        ia += dots_allowed;

    int64_t cell_id = hashXY( (int64_t)ir, (int64_t)ia, seed );
    
    double dropout_chance = hashXY_float( cell_id, seed^extraHash2 );
    
    if (dropout_chance < damage)
        return 0.0;

    double cr = scaled_radius;
    double ca = ((ia+0.5)/dots_factor) - offset;
    
    double cx = cr * cos(ca);
    double cy = cr * sin(ca);
    
    if (angle_jitter > 0.0)
        {
        double jitter = angle_jitter * hash_to_double( cell_id ^ extraHash1 ) * (2.0*M_PI/100.0);
        ca += jitter;
        }
    
    double dist = fun( (cx-x), (cy-y), cell_id, cx, cy, ca, dot_size );

// DEBUG
#if 0
    dist = 0.5 + (ia/dots_factor) / (2.0*M_PI);
    return std::min(1.0, std::max(0.0, dist));      // debug - shows modified angle values
#elif 0
    return hash_to_double( cell_id );      // debug - shows cell/hash problems along angle wrap at +-PI
#endif
    
    if (dist > dot_size)
        return 0.01;
    else
        return 1.0 - (dist / dot_size);

}

/******************************************************************************/

// concentric dots
double dot_concentricdots( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double damage = opt.params[5] / 100.0;
    return concentric_common(x,y,seed,opt, circle_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, 0.0, damage );
}

dot_description desc_concentricdots( dot_concentricdots, "concentric dots",
                                6,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "dropout percent" },
                                {  1.0,  1.4,   1.5,  0.02,  0.2,   0.0 },
                                {  0.1,  0.5,   0.1,  0.00,  0.00,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 99.0 } );

/******************************************************************************/

// concentric diamonds
double dot_concentricdiamonds( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double damage = opt.params[5] / 100.0;
    return concentric_common(x,y,seed,opt, diamond_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, 0.0, damage );
}

dot_description desc_concentricdiamonds( dot_concentricdiamonds, "concentric diamonds",
                                6,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "dropout percent" },
                                {  1.2,  1.4,   1.5,  0.02,  0.2,   0.0 },
                                {  0.1,  0.5,   0.1,  0.00,  0.00,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 99.0 } );

/******************************************************************************/

// concentric squares
double dot_concentricsquare( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double damage = opt.params[5] / 100.0;
    return concentric_common(x,y,seed,opt, square_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, 0.0, damage );
}

dot_description desc_concentricsquares( dot_concentricsquare, "concentric squares",
                                6,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "dropout percent" },
                                {  1.0,  1.4,   1.5,  0.02,  0.2,   0.0 },
                                {  0.1,  0.5,   0.1,  0.00,  0.00,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 99.0 } );

/******************************************************************************/

// concentric crosses
double dot_concentriccross( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double damage = opt.params[5] / 100.0;
    return concentric_common(x,y,seed,opt, cross_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, 0.0, damage );
}

dot_description desc_concentriccrosses( dot_concentriccross, "concentric crosses",
                                6,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "dropout percent" },
                                {  0.5,  1.4,   1.5,  0.02,  0.2,   0.0 },
                                {  0.1,  0.5,   0.1,  0.00,  0.00,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 99.0 } );

/******************************************************************************/

// concentric random shapes
double dot_concentricrandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double damage = opt.params[5] / 100.0;
    return concentric_common(x,y,seed,opt, random_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, 0.0, damage );
}

dot_description desc_concentricrandom( dot_concentricrandom, "concentric random shapes",
                                6,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "dropout percent"  },
                                {  1.0,  1.4,   1.5,  0.02,  0.2,   0.0 },
                                {  0.1,  0.5,   0.1,  0.00,  0.00,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 99.0 } );

/******************************************************************************/

// concentric radial lines
double dot_concentricradial( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double angle_jitter = opt.params[5];
    const double damage = opt.params[6] / 100.0;
    return concentric_common(x,y,seed,opt, radialburst_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, angle_jitter, damage );
}

dot_description desc_concentricradial( dot_concentricradial, "concentric radial lines",
                                7,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "line jitter", "dropout percent" },
                                {  0.5,  1.0,   0.6,  0.02,  0.2,    0.0,  0.0 }, // jitter 3-5 looks good
                                {  0.1,  0.5,   0.1,  0.00,  0.00,   0.0,  0.0 },
                                { 20.0, 20.0, 100.0, 10.00, 10.00, 100.0, 99.0 } );

/******************************************************************************/

// concentric perpendicular lines
double dot_concentricdash( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ring_spacing = opt.params[1];
    const double dot_spacing = opt.params[2];
    const double angle_offset = opt.params[3];
    const double angle_random = opt.params[4];
    const double angle_jitter = opt.params[5];
    const double damage = opt.params[6] / 100.0;
    return concentric_common(x,y,seed,opt, dashline_dist_func, ring_spacing, dot_spacing, angle_offset, angle_random, angle_jitter, damage );
}

dot_description desc_concentricdash( dot_concentricdash, "concentric dashed lines",
                                7,
                                { "dot size", "ring spacing", "dot spacing", "angle offset", "angle randomness", "line jitter", "dropout percent" },
                                {  0.50,  0.9,   1.00,  0.02,  0.2,    0.0,  0.0 }, // jitter 3-5 looks good
                                {  0.05,  0.2,   0.01,  0.00,  0.00,   0.0,  0.0 },
                                { 20.00, 20.0, 100.00, 10.00, 10.00, 100.0, 99.0} );

/******************************************************************************/


// spiral of shapes
/*
    r = a + b*angle      find nearest quantized r and angle?
    b = (r-a)/angle
    angle = (r-a)/b

    length = (b/2) * (angle*sqrt(1+angle*angle) + ln(angle + sqrt(1+angle*angle)));
    but quantizing length means changing the angle as well!
    2*length/b = angle*sqrt(1+angle*angle) + ln(angle + sqrt(1+angle*angle));
    no analytic inversion for angle!!!!!!!!!!!!!!!

    theta = atan(y,x)/2PI + 0.5;    // normalized angle 0....1
    r = hypot(x,y);                 // radius
    dist = fabs( fract(theta+r*b) - .5)
    //integral when on spiral, can use round()

    https://math.stackexchange.com/questions/2335055/placing-points-equidistantly-along-an-archimedean-spiral-from-parametric-equatio
    approximation (works away from zero) for equally spaced points
    d = desired distance
    thetaN = sqrt( 2*d*n/b );
    sqr(theta) = 2*d*n/b;
    n = sqr(theta) / (2*d/b)
    r = b*sqrt(2*d*n/b)
    r/b = theta = sqrt(2*d*n/b);
    sqr(r/b) = 2*d*n/b;
    n = (r*r)/(2*d*b)

    theta = 2 * Pi * sqrt(2*d*n / b)
    theta/2PI = sqrt(2*d*n/b)
    sqr(theta/2PI) = 2*d*n/b
    b*sqr(theta/2PI)/(2*d) = n

    n = (theta*theta) / (2.0*dot_spacing);
    theta^2 = 2 * n * dot_spacing;
    theta = sqrt(2*n*dot_spacing);

*/
double spiral_common( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun )
{
    const double dot_size = (1.0/sqrt(2.0)) * opt.params[0];
    const double ring_spacing = opt.params[1];                              // should equal b on curve
    const double dot_spacing = opt.params[2] / (ring_spacing*2.0*M_PI);     // d

    double raw_angle = atan2(y,x);                  // -M_PI .. M_PI
    double angle = 0.5 + raw_angle/(2.0*M_PI);      // 0..1
    double r = hypot(x,y);

#if 0
// Keep the center dot clean
// looks really wrong when dots are tightly packed
    if (r < dot_size)
        {
        double cx = 0.0;
        double cy = 0.0;
        int64_t cell_id = hash64( 0, seed );
        double dist = fun( (cx-x), (cy-y), cell_id );
        if (dist > dot_size)
            return 0.0;
        else
            return 1.0 - (dist / dot_size);
        }
#endif

    double temp = angle + r / ring_spacing;         // integral when exactly on the spiral
    double it = round(temp);                        // round to find nearest spiral arm
    double r2 = (it - angle) * ring_spacing;        // r for the nearest spiral arm at this angle
    double theta = r2 / ring_spacing;               // how far along the curve are we to reach this r?

    // correct length function can't be inverted, so we use Clarkson scroll approximation
    double n = (theta*theta) / (2.0*dot_spacing);   // find length along curve
    double in = round(n);                           // round to nearest dot center along curve
    double theta2 = sqrt(in*2.0*dot_spacing);       // recalc angle
    double r3 = ring_spacing * theta2;              // recalc radius

    int64_t cell_id = hash64( (int64_t)in, seed );  // use quantized length along curve for dot ID
    
    // convert to cartesian coordinates
    double ca = theta2*2.0*M_PI;
    double cx = -r3 * cos(ca);
    double cy = r3 * sin(ca);
    
    double dist = fun( (cx-x), (cy-y), cell_id, ca, cy, ca, dot_size );

#if 0
    return hash_to_double( cell_id );      // debug, looks good so far
#endif

    if (dist > dot_size)
        return 0.0;
    else
        return 1.0 - (dist / dot_size);

}

/******************************************************************************/

// spiral dots
double dot_spiraldots( double x, double y, int64_t seed, const screen_options &opt )
{
    return spiral_common(x,y,seed,opt, circle_dist_func );
}

dot_description desc_spiraldots( dot_spiraldots, "spiral dots",
                                3,
                                { "dot size", "ring spacing", "dot spacing"  },
                                {  1.0,  1.0,   1.0 },
                                {  0.1,  0.5,   0.1 },
                                { 20.0, 20.0, 100.0 } );

/******************************************************************************/

// spiral diamonds
double dot_spiraldiamonds( double x, double y, int64_t seed, const screen_options &opt )
{
    return spiral_common(x,y,seed,opt, diamond_dist_func );
}

dot_description desc_spiraldiamonds( dot_spiraldiamonds, "spiral diamonds",
                                3,
                                { "dot size", "ring spacing", "dot spacing"  },
                                {  1.2,  1.0,   1.0 },
                                {  0.1,  0.5,   0.1 },
                                { 20.0, 20.0, 100.0 } );

/******************************************************************************/

// spiral squares
double dot_spiralsquares( double x, double y, int64_t seed, const screen_options &opt )
{
    return spiral_common(x,y,seed,opt, square_dist_func );
}

dot_description desc_spiralsquares( dot_spiralsquares, "spiral squares",
                                3,
                                { "dot size", "ring spacing", "dot spacing"  },
                                {  1.0,  1.0,   1.0 },
                                {  0.1,  0.5,   0.1 },
                                { 20.0, 20.0, 100.0 } );

/******************************************************************************/

// spiral crosses
double dot_spiralcrosses( double x, double y, int64_t seed, const screen_options &opt )
{
    return spiral_common(x,y,seed,opt, cross_dist_func );
}

dot_description desc_spiralcrosses( dot_spiralcrosses, "spiral crosses",
                                3,
                                { "dot size", "ring spacing", "dot spacing"  },
                                {  1.0,  1.0,   1.0 },
                                {  0.1,  0.5,   0.1 },
                                { 20.0, 20.0, 100.0 } );

/******************************************************************************/

// spiral random shapes
double dot_spiralrandom( double x, double y, int64_t seed, const screen_options &opt )
{
    return spiral_common(x,y,seed,opt, random_dist_func );
}

dot_description desc_spiralrandom( dot_spiralrandom, "spiral random shapes",
                                3,
                                { "dot size", "ring spacing", "dot spacing"  },
                                {  1.0,  1.0,   1.0 },
                                {  0.1,  0.5,   0.1 },
                                { 20.0, 20.0, 100.0 } );

/******************************************************************************/

// split shapes, with half of cell inverted
// can only use a subset of distance functions
double split_common( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun,
        const double normalize, bool checkerboard = false, bool angle = false )
{
    double ix = floor( x );
    double fx = x - ix;
    double iy = floor( y );
    double fy = y - iy;
    double dx = 2.0 * (fx - 0.5);
    double dy = 2.0 * (fy - 0.5);

    double dist = normalize * fun( dx, dy, 0, 0.0, 0.0, 0.0, 1.0 );
    dist = std::min( 1.0, std::max( 0.0, dist ));
    
    if (checkerboard && (((int(ix)+int(iy)) & 0x01) != 0))
        dist = 1.0 - dist;
    
    if (angle)
        {
        if ((fx+fy) > 1.0)
            return dist;
        else
            return 1.0 - dist;
        }
    else
        {
        if (fx > 0.5)
            return dist;
        else
            return 1.0 - dist;
        }
}

/******************************************************************************/

double dot_splitcircle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, circle_dist_func, M_SQRT1_2, false, false );
}

dot_description desc_splitcircle( dot_splitcircle, "split circles" );

/******************************************************************************/

double dot_splitcircleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, circle_dist_func, M_SQRT1_2, true, false );
}

dot_description desc_splitcircleAlt( dot_splitcircleAlt, "split circles alt" );

/******************************************************************************/

double dot_splitcircleAngle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, circle_dist_func, M_SQRT1_2, false, true );
}

dot_description desc_splitcircleAngle( dot_splitcircleAngle, "split circles angle" );

/******************************************************************************/

double dot_splitcircleAngleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, circle_dist_func, M_SQRT1_2, true, true );
}

dot_description desc_splitcircleAngleAlt( dot_splitcircleAngleAlt, "split circles angle alt" );

/******************************************************************************/

double dot_splitsquare( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, square_dist_func, 1.0, false, false );
}

dot_description desc_splitsquare( dot_splitsquare, "split squares" );

/******************************************************************************/

double dot_splitsquareAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, square_dist_func, 1.0, true, false );
}

dot_description desc_splitsquareAlt( dot_splitsquareAlt, "split squares alt" );

/******************************************************************************/

double dot_splitsquareAngle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, square_dist_func, 1.0, false, true );
}

dot_description desc_splitsquareAngle( dot_splitsquareAngle, "split squares angle" );

/******************************************************************************/

double dot_splitsquareAngleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, square_dist_func, 1.0, true, true );
}

dot_description desc_splitsquareAngleAlt( dot_splitsquareAngleAlt, "split squares angle alt" );

/******************************************************************************/

double dot_splitdiamond( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, diamond_dist_func, 0.5, false, false );
}

dot_description desc_splitdiamond( dot_splitdiamond, "split diamonds" );

/******************************************************************************/

double dot_splitdiamondAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, diamond_dist_func, 0.5, true, false );
}

dot_description desc_splitdiamondAlt( dot_splitdiamondAlt, "split diamonds alt" );

/******************************************************************************/

double dot_splitdiamondAngle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, diamond_dist_func, 0.5, false, true );
}

dot_description desc_splitdiamondAngle( dot_splitdiamondAngle, "split diamonds angle" );

/******************************************************************************/

double dot_splitdiamondAngleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, diamond_dist_func, 0.5, true, true );
}

dot_description desc_splitdiamondAngleAlt( dot_splitdiamondAngleAlt, "split diamonds angle alt" );

/******************************************************************************/

double dot_splittriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, triangle_dist_func2, 0.5, false, false );
}

dot_description desc_splittriangle( dot_splittriangle, "split triangles" );

/******************************************************************************/

double dot_splittriangleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, triangle_dist_func2, 0.5, true, false );
}

dot_description desc_splittriangleAlt( dot_splittriangleAlt, "split triangles alt" );

/******************************************************************************/

double dot_splithexagon( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, hexagon_dist_func, 0.5, false, false );
}

dot_description desc_splithexagon( dot_splithexagon, "split hexagons" );

/******************************************************************************/

double dot_splithexagonAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, hexagon_dist_func, 0.5, true, false );
}

dot_description desc_splithexagonAlt( dot_splithexagonAlt, "split hexagons alt" );

/******************************************************************************/

double dot_splithexagonAngle( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, hexagon_dist_func, 0.5, false, true );
}

dot_description desc_splithexagonAngle( dot_splithexagonAngle, "split hexagons angle" );

/******************************************************************************/

double dot_splithexagonAngleAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return split_common(x,y,seed,opt, hexagon_dist_func, 0.5, true, true );
}

dot_description desc_splithexagonAngleAlt( dot_splithexagonAngleAlt, "split hexagons angle alt" );

/******************************************************************************/

/*

screen: coil    8x8
unique values: 64
duplicates: 0
max duplication: 1

screen: coil    9x9
unique values: 81
duplicates: 0
max duplication: 1

screen: coil    10x10
unique values: 98
duplicates: 2
max duplication: 2

screen: coil    11x11
unique values: 116
duplicates: 5
max duplication: 2

screen: coil    12x12
unique values: 130
duplicates: 14
max duplication: 2

screen: coil    13x13
unique values: 149
duplicates: 20
max duplication: 2

screen: coil    14x14
unique values: 157
duplicates: 39
max duplication: 2

screen: coil    15x15
unique values: 165
duplicates: 59
max duplication: 3

DEFERRED - could try optimizing all parameters - but would probably need simulated annealing
    ring_spacing, dot_spacing, power, center_offset, dist_scale (inner loop)

*/

// a coil/spiral shaped dot slightly squeezed for an ellipse
double dot_coildot( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = 0.75;       // TODO - make this a real parameter, but that requires dynamic scaling
    const double ring_spacing = 1.0 / 12.0;     // get about 12 loops per cell, ideally
    const double dot_spacing = 0.8;
    const double dist_scale = 1.0 / 34.0;      // TODO: analytically determine max value for scale!
    const double power = 0.7;

#if 0
    const double n_temp = dot_spacing / ring_spacing;
    const double n_test = n_temp * n_temp;
    const double dist_scale = (1.0 / n_test) * 1.04;
#endif

    const double ix = floor(x);
    const double iy = floor(y);
    const double fx = EccX * (x - ix - 0.5);
    const double fy = EccY * (y - iy - 0.5);

    double raw_angle = atan2(fy,fx);                // -M_PI .. M_PI
    double angle = 0.5 + raw_angle/(2.0*M_PI);      // 0..1
    double r = hypot(fx,fy);

    double temp = angle + r / ring_spacing;         // integral when exactly on the spiral
    double it = round(temp);                        // round to find nearest spiral arm
    double r2 = (it - angle) * ring_spacing;        // r for the nearest spiral arm at this angle
    double theta = r2 / ring_spacing;               // how far along the curve are we to reach this r?

    // correct length function can't be inverted, so we use Clarkson scroll approximation
    double n = (theta*theta) / (2.0*dot_spacing);   // find length along curve

    double value = dist_scale * n;
    value = std::min(1.0, std::max( 0.0, value ));
    value = pow(value, power);    // sqrt is too much, linear is too little
    value = 1.0 - value;
    
    return value;
}

dot_description desc_coildot( dot_coildot, "coil" );

/******************************************************************************/

/*

screen: balanced coil    8x8
unique values: 63
duplicates: 1
max duplication: 2

screen: balanced coil    9x9
unique values: 81
duplicates: 0
max duplication: 1

screen: balanced coil    10x10
unique values: 99
duplicates: 1
max duplication: 2

screen: balanced coil    11x11
unique values: 117
duplicates: 4
max duplication: 2

screen: balanced coil    12x12
unique values: 132
duplicates: 12
max duplication: 2

screen: balanced coil    13x13
unique values: 153
duplicates: 16
max duplication: 2

screen: balanced coil    14x14
unique values: 164
duplicates: 31
max duplication: 3

screen: balanced coil    15x15
unique values: 174
duplicates: 51
max duplication: 2


Hand optimized by guesswork and visual evaluation, values are fragile
No idea how to automatically optimize all parameters, because it needs visual eval for tone reversal
 */

// a coil/spiral shaped dot slightly squeezed for an ellipse
// balanced from black and white directions, sort of
double dot_coildot2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = 0.75;       // TODO - make this a real parameter, but that requires dynamic scaling
    const double ring_spacing = 1.0 / 18.0;
    const double dot_spacing = 0.8;
    const double dist_scale = 1.0 / 65.0;      // TODO: analytically determine max value for scale!
    const double dist_scale2 = 1.0 / 130.0;

    const double ix = floor(x);
    const double iy = floor(y);
    double fx = EccX * (x - ix - 0.5);
    double fy = EccY * (y - iy - 0.5);
    
    double r = hypot(fx,fy);
    
    const double reverse_point =  M_SQRT1_2 * 0.6;
    bool reverse = (r >= reverse_point);
    if (reverse)
        {
        // reset to centered on zero
        fx -= 0.5*EccX;
        fy -= 0.5*EccY;
        if (fx < (-0.5*EccX)) fx += EccX;
        if (fy < (-0.5*EccY)) fy += EccY;
        r = hypot(fx,fy);
        }

    double raw_angle = atan2(fy,fx);                // -M_PI .. M_PI
    double angle = 0.5 + raw_angle/(2.0*M_PI);      // 0..1
    
    double temp = angle + r / ring_spacing;         // integral when exactly on the spiral
    double it = round(temp);                        // round to find nearest spiral arm
    double r2 = (it - angle) * ring_spacing;        // r for the nearest spiral arm at this angle
    double theta = r2 / ring_spacing;               // how far along the curve are we to reach this r?

    // correct length function can't be inverted, so we use Clarkson scroll approximation
    double n = (theta*theta) / (2.0*dot_spacing);   // find length along curve

    double value = dist_scale * n;
    if (!reverse)
        {
        value = pow(value, 0.7);
        value = 1.0 - value;
        }
    else
        {
        value = dist_scale2 * n;
        value = pow(value, 0.8);
        }
    value = std::min(1.0, std::max( 0.0, value ));
    
    value = pow(value, 0.75);   // get 50% close to correct area
    return value;
}

dot_description desc_coildot2( dot_coildot2, "balanced coil" );

/******************************************************************************/

/*

screen: coil blend    8x8
unique values: 60
duplicates: 4
max duplication: 2

screen: coil blend    9x9
unique values: 68
duplicates: 10
max duplication: 3

screen: coil blend    10x10
unique values: 90
duplicates: 9
max duplication: 3

screen: coil blend    11x11
unique values: 97
duplicates: 23
max duplication: 3

screen: coil blend    12x12
unique values: 114
duplicates: 28
max duplication

screen: coil blend    13x13
unique values: 124
duplicates: 34
max duplication: 3

screen: coil blend    14x14
unique values: 144
duplicates: 39
max duplication: 4

screen: coil blend    15x15
unique values: 146
duplicates: 58
max duplication: 5

 */
 
// a coil/spiral shaped dot slightly squeezed for an ellipse
// sort of balanced, and blended with an elliptical dot
double dot_coildot3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias = opt.params[0];
    const double EccX = 1.0;
    const double EccY = 0.75;       // TODO - make this a real parameter, but that requires dynamic scaling
    const double ring_spacing = 1.0 / 18.0;
    const double dot_spacing = 0.8;
    const double dist_scale = 1.0 / 65.0;      // TODO: analytically determine max value for scale!
    const double dist_scale2 = 1.0 / 130.0;

    const double ix = floor(x);
    const double iy = floor(y);
    double fx = EccX * (x - ix - 0.5);
    double fy = EccY * (y - iy - 0.5);
    
    double sx = EccX * cos( x * (2.0*M_PI) );
    double sy = EccY * cos( y * (2.0*M_PI) );
    double ellipse = 1.0 - (0.5 + (sx+sy) / (2.0*(EccX+EccY)));
    
    double r = hypot(fx,fy);
    
    const double reverse_point =  M_SQRT1_2 * 0.6;
    bool reverse = (r >= reverse_point);
    if (reverse)
        {
        // reset to centered on zero
        fx -= 0.5*EccX;
        fy -= 0.5*EccY;
        if (fx < (-0.5*EccX)) fx += EccX;
        if (fy < (-0.5*EccY)) fy += EccY;
        r = hypot(fx,fy);
        }

    double raw_angle = atan2(fy,fx);                // -M_PI .. M_PI
    double angle = 0.5 + raw_angle/(2.0*M_PI);      // 0..1
    
    double temp = angle + r / ring_spacing;         // integral when exactly on the spiral
    double it = round(temp);                        // round to find nearest spiral arm
    double r2 = (it - angle) * ring_spacing;        // r for the nearest spiral arm at this angle
    double theta = r2 / ring_spacing;               // how far along the curve are we to reach this r?

    // correct length function can't be inverted, so we use Clarkson scroll approximation
    double n = (theta*theta) / (2.0*dot_spacing);   // find length along curve

    double value = dist_scale * n;
    if (!reverse)
        {
        value = pow(value, 0.7);
        value = 1.0 - value;
        }
    else
        {
        value = dist_scale2 * n;
        value = pow(value, 0.8);
        }
    value = std::min(1.0, std::max( 0.0, value ));
    value = pow(value, 0.75);   // get 50% close to correct area
    
    // blend the ellipse and coil
    value = (1.0-bias)*ellipse + bias*value;
    
    return value;
}

dot_description desc_coildot3( dot_coildot3, "coil blend",
                                1,
                                { "bias scale" },
                                { 0.61 },       // optimums near 0.33, 0.40, 0.61, 0.68, 0.76, 1.0
                                                // probably want close to halfway, leaning toward 1.0 for the balanced coil
                                { 0.00 },
                                { 1.00 } );

/******************************************************************************/

// concentric rings of shapes, with perspective
double tunnel_common( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun, const double angle_offset, const double angle_randomness, const double scale_factor, const int64_t dot_count, const double angle_jitter )
{
    assert( dot_count > 0 );
    assert( scale_factor > 0.0 );
    
    double radius = hypot(x,y);          // 0..INF
    radius = std::max( 1.0e-12, radius );   // limit lower end so we don't divide by zero
    
    double in_angle = atan2(y,x);      // -M_PI..M_PI
    
    double r = scale_factor/ radius;
    double ir = floor(r);
    
    double angle_random = angle_randomness * hash64_float( ir, seed );
    
    double scaled_radius = (ir+0.5);
    double dots_factor = dot_count / (2.0*M_PI);       // assuming radius 1.0, midway in ring

    double offset = ir*angle_offset + angle_random;
    double angle = (in_angle+offset) * dots_factor;
    
    double ia = floor(angle);

    // handle wrap/discontinuity in quantized angle around +-PI
    if ((2*ia) >= dot_count)
        ia -= dot_count;
    if ((2*ia) <= -dot_count)
        ia += dot_count;

    int64_t cell_id = hashXY( (int64_t)ir, (int64_t)ia, seed );

    double cr = scale_factor / scaled_radius;
    double dot_width = cr * (0.9 * 2.0*M_PI) / dot_count;       // still not perfect
    
    double ca = ((ia+0.5)/dots_factor) - offset;
    
    double cx = cr * cos(ca);
    double cy = cr * sin(ca);
    
    if (angle_jitter > 0.0)
        {
        double jitter = angle_jitter * hash_to_double( cell_id ^ extraHash1 ) * (2.0*M_PI/100.0);
        ca += jitter;
        }
    
    assert( dot_width > 0.0 );
    double dist = fun( (cx-x), (cy-y), cell_id, cx, cy, ca, dot_width ) / dot_width;

// DEBUG
#if 0
    dist = 0.5 + (ia/dots_factor) / (2.0*M_PI);
    return std::min(1.0, std::max(0.0, dist));      // debug - shows modified angle values
#elif 0
    return hash_to_double( cell_id );      // debug - shows cell/hash problems along angle wrap at +-PI
#endif
    
    double value = std::min(1.0,std::max(0.0,dist));
    return 1.0 - value;

}

/******************************************************************************/

// tunnel dots
double dot_tunneldots( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, circle_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunneldots( dot_tunneldots, "tunnel dots",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.05,   0.00 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// tunnel diamonds
double dot_tunneldiamonds( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, diamond_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunneldiamonds( dot_tunneldiamonds, "tunnel diamonds",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.05,   0.00 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// tunnel squares
double dot_tunnelsquares( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, square_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunnelsquares( dot_tunnelsquares, "tunnel squares",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.05,   0.00 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// tunnel radial lines
// NOTE -- dashed lines all run together
double dot_tunnelradial( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, radialburst_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunnelradial( dot_tunnelradial, "tunnel radial lines",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.05,   0.00 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// tunnel triangles
double dot_tunneltriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, triangle_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunneltriangle( dot_tunneltriangle, "tunnel triangles",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.00,   0.50 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// tunnel hexagons
double dot_tunnelhexagon( double x, double y, int64_t seed, const screen_options &opt )
{
    const int64_t dot_count = round(opt.params[0]);
    const double growth = opt.params[1]  * 100.0;
    const double angle_offset = opt.params[2] * M_PI;
    const double angle_random = opt.params[3] * M_PI;
    return tunnel_common(x,y,seed,opt, hexagon_dist_func, angle_offset, angle_random, growth, dot_count, 0.0 );
}

dot_description desc_tunnelhexagon( dot_tunnelhexagon, "tunnel hexagons",
                                4,
                                { "dot count", "scale", "angle offset", "angle randomness" },
                                {  30.0,   1.0,   0.05,   0.00 },
                                {   1.0,   0.5,  -2.00,   0.00 },
                                { 200.0,  10.0,   2.00,   2.00 } );

/******************************************************************************/

// concentric rings, with perspective
double dot_tunnelline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale_factor = 300.0 * opt.params[0];
    
    double radius = hypot(x,y);          // 0..INF
    radius = std::max( 1.0e-12, radius );   // limit lower end so we don't divide by zero
    
    double r = scale_factor / radius;
    double ir = floor(r);
    double dr = r - ir;
    return dr;
}

dot_description desc_tunnelline( dot_tunnelline, "tunnel concentric",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.5 },
                                { 10.0 } );

/******************************************************************************/

// dots with perspective to center Y line
double perspective_common( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun, const double scale_factor )
{
    assert( scale_factor > 0.0 );
    
    x = fabs(x);
    
    double radius = x;                  // 0..INF
    radius = std::max( 1.0e-20, radius );   // limit lower end so we don't divide by zero
    
    double r = scale_factor / radius;
    
    x = r;
    y = y * 0.1 * r;

    double ix = floor(x);
    double iy = floor(y);
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    double dx = x - ix;         // 0..1
    double dy = y - iy;
    
    dx = 2.0 * (dx - 0.5);      // -1..1
    dy = 2.0 * (dy - 0.5);
    
    double dot_width = M_SQRT2;
    double dist = fun( dx, dy, cell_id, 0.5, 0.5, 0.0, dot_width ) / dot_width;
    
    double value = std::min(1.0,std::max(0.0,dist));
    return 1.0 - value; // to get white center threshold values

}

/******************************************************************************/

// perspective dots
double dot_perspectiveDots( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, circle_dist_func, growth );
}

dot_description desc_perspectiveDots( dot_perspectiveDots, "perspective dots",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective diamonds
double dot_perspectiveDiamonds( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, diamond_dist_func, growth );
}

dot_description desc_perspectiveDiamonds( dot_perspectiveDiamonds, "perspective diamonds",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective squares
double dot_perspectiveSquares( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, square_dist_func, growth );
}

dot_description desc_perspectiveSquares( dot_perspectiveSquares, "perspective squares",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective crosses
double dot_perspectiveCrosses( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, cross_dist_func, growth );
}

dot_description desc_perspectiveCrosses( dot_perspectiveCrosses, "perspective crosses",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective hexagons
double dot_perspectiveHex( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, hexagon_dist_func, growth );
}

dot_description desc_perspectiveHex( dot_perspectiveHex, "perspective hexagons",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective triangles
double dot_perspectiveTriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, triangle_dist_func, growth );
}

dot_description desc_perspectiveTriangle( dot_perspectiveTriangle, "perspective triangles",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective triangles2
double dot_perspectiveTriangle2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, triangle_dist_func2, growth );
}

dot_description desc_perspectiveTriangle2( dot_perspectiveTriangle2, "perspective triangles2",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// perspective random
double dot_perspectiveRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double growth = opt.params[0] * 100.0;
    return perspective_common( x, y, seed, opt, random_dist_func, growth );
}

dot_description desc_perspectiveRandom( dot_perspectiveRandom, "perspective random",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// dots with perspective to center Y line
double dot_perspectiveLineHoriz( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale_factor = opt.params[0] * 100.0;
    assert( scale_factor > 0.0 );
    
    x = fabs(x);
    
    double radius = x;                  // 0..INF
    radius = std::max( 1.0e-20, radius );   // limit lower end so we don't divide by zero
    
    double r = scale_factor / radius;
    
//    x = r;
    y = y * 0.1 * r;

//    double ix = floor(x);
    double iy = floor(y);
    
//    double dx = x - ix;         // 0..1
    double dy = y - iy;
    
    double value = dy;
    return value;
}

dot_description desc_perspectiveLineHoriz( dot_perspectiveLineHoriz, "perspective line horizontal",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// dots with perspective to center Y line
double dot_perspectiveLineVertical( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale_factor = opt.params[0] * 100.0;
    assert( scale_factor > 0.0 );
    
    x = fabs(x);
    
    double radius = x;                  // 0..INF
    radius = std::max( 1.0e-20, radius );   // limit lower end so we don't divide by zero
    
    double r = scale_factor / radius;
    
    x = r;
//    y = y * 0.1 * r;

    double ix = floor(x);
//    double iy = floor(y);
    
    double dx = x - ix;         // 0..1
//    double dy = y - iy;
    
    double value = dx;
    return value;
}

dot_description desc_perspectiveLineVertical( dot_perspectiveLineVertical, "perspective line vertical",
                                1,
                                { "scale" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// shapes with a slowly varying angle
double slow_motion_common( double x, double y, int64_t seed, const screen_options &opt,
                            inner_dot_func fun, double angle_start, double angle_range )
{
    const int32_t octave_low = -3;
    const int32_t octave_high = -3;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;

    double dx = ChaosBicubic( ix, iy, octave_low, octave_high, seed^extraHash2 );

    double ca = angle_start + (dx-0.5) * M_PI * angle_range;

    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    double dist = fun( (0.5-fx), (0.5-fy), cell_id, 0.5, 0.5, ca, 1.0 );

    dist = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - dist;
}

/******************************************************************************/

// slowly varying dashes
double dot_slowdash( double x, double y, int64_t seed, const screen_options &opt )
{
    double angle_start = opt.params[0] * (M_PI/180.0);
    double angle_range = (1.0/100.0) * opt.params[1];
    return slow_motion_common(x,y,seed,opt, radialburst_dist_func, angle_start, angle_range );
}

dot_description desc_slowdash( dot_slowdash, "slow dash",
                                2,
                                { "start angle", "angle ramge"  },
                                {   0.0,   100.0 },
                                {   0.0,     0.0 },
                                { 180.0,   100.0} );

/******************************************************************************/

// slowly varying triangles
double dot_slowtriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    double angle_start = opt.params[0] * (M_PI/180.0);
    double angle_range = (1.0/100.0) * opt.params[1];
    return slow_motion_common(x,y,seed,opt, triangle_dist_func, angle_start, angle_range );
}

dot_description desc_slowtriangle( dot_slowtriangle, "slow triangle",
                                2,
                                { "start angle", "angle ramge"  },
                                {  90.0,   100.0 },
                                {   0.0,     0.0 },
                                { 180.0,   100.0} );

/******************************************************************************/

// random short strokes in one direction
double dot_strokerandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double block_height = opt.params[0];
    const double offset_amp = opt.params[2];   // should be at least 2x block length
    const double block_length =opt.params[1];
    int64_t block_y = floor(y * block_height);
    x += offset_amp * hash_to_double(hash64( block_y, seed ));
    int64_t ix = floor(x/block_length);
    double value = hash_to_double( hashXY( ix, block_y, seed ) );
    return value;
}

dot_description desc_strokerandom( dot_strokerandom, "random strokes",
                                3,
                                { "block height", "block length", "offset amplitude" },
                                { 7.1, 1.3, 8.2 },
                                { 0.1, 0.1, 0.0 },
                                { 100.0, 100.0, 100.0 } );

/******************************************************************************/

// random short gradients in one direction
double dot_linegradient( double x, double y, int64_t seed, const screen_options &opt )
{
    const double block_height = opt.params[0];
    const double block_length = opt.params[1];
    const double offset_amp = block_length * opt.params[2];   // should be at least 2x block length
    const uint64_t flip_freq = (uint64_t) round(10.0 * opt.params[3]);
    const double length_variation = (2.0 / 100.0) * opt.params[3];
    int64_t block_y = floor(y / block_height);
    int64_t hash1 = hash64( block_y, seed );
    bool flip = (uint64_t(hash1) % 1000) < flip_freq;
    int64_t hashy = hash64( hash1 );
    double offset = offset_amp * hash_to_double(hashy);
    double freq_mod = length_variation * block_length * (hash_to_double( hash64( hashy ) ) - 0.5);
    x += offset;
    x /= (block_length+freq_mod);
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    if (flip)
        value = 1.0 - fx;
    return value;
}

dot_description desc_linegradient( dot_linegradient, "line gradient",
                                5,
                                { "block height", "block length", "offset amplitude", "flip frequency", "length variation" },
                                {   0.2,   1.6,   5.5,  50.0,  10.0 },
                                {  0.01,  0.01,   0.0,   0.0,   0.0 },
                                { 100.0, 100.0, 100.0, 100.0, 100.0 } );

/******************************************************************************/

/*
screen: round       10x10
unique values: 19
duplicates: 17
max duplication: 18

octant symmetry greatly reduces the number of possible values
Only 20 gray levels instead of 101

screen: round       11x11
unique values: 21
duplicates: 20
max duplication: 8

Only 22 gray levels instead of 122
*/

// simple round dot function
double dot_round( double x, double y, int64_t seed, const screen_options &opt )
{
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    return value;
}

dot_description desc_round( dot_round, "round" );

/******************************************************************************/

/*
screen: round_half_bias      10x10
unique values: 32
duplicates: 24
max duplication: 10

counts don't change much with scale
We still have vertical and octant symmetry
33 gray levels instead of 101


screen: round_half_bias     11x11
unique values: 40
duplicates: 35
max duplication: 4

41 gray levels instead of 122
*/

// round dot function, with half_x bias to get more tone values
// for documentation and learning
double dot_roundhalf( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[0] / (100.0);
    const double round_scale = (1.0-bias_scale) * 0.25;
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double fx = x - ix;
    double bias = (fx > 0.5) ? -1.0 : 1.0;
    
    double value = 0.5 + bias * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_roundhalf( dot_roundhalf, "round_half_bias",
                                1,
                                { "bias scale" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

/*
screen: round_quarter_bias      10x10
unique values: 56
duplicates: 30
max duplication: 5

counts don't change much with scale
and we still have octant symmetry
57 gray levels instead of 101

screen: round_quarter_bias      11x11
unique values: 68
duplicates: 43
max duplication: 4

69 gray levels instead of 122
*/

// round dot function, with quarter bias to get more tone values
// for documentation and learning
double dot_roundquarter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[0] / (100.0);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) * 0.25;
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double bias = 0.0;
    if (fx < 0.5)
        {
        if (fy < 0.5)
            bias = -1.0;
        else
            bias = 1.0;
        }
    else
        {
        if (fy < 0.5)
            bias = -0.5;
        else
            bias = 0.5;
        }
    
    double value = 0.5 + bias * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_roundqurter( dot_roundquarter, "round_quarter_bias",
                                1,
                                { "bias scale" },
                                {   5.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

/*
screen: round_linear_bias      10x10
unique values: 56
duplicates: 38
max duplication: 4

counts don't change much with scale
57 gray levels instead of 101

screen: round_linear_bias       11x11
unique values: 60
duplicates: 51
max duplication: 4

61 gray levels instead of 122
*/

// round dot function, with linear bias to get more tone values
// for documentation and learning
double dot_roundlinear( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[0] / (100.0);
    const double round_scale = (1.0-bias_scale) * 0.25;
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double fx = x - ix;
    double dx = 2.0 * (fx - 0.5); // so we have centered -1.0 to +1.0
    
    double value = 0.5 + dx * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_roundlinear( dot_roundlinear, "round_linear_bias",
                                1,
                                { "bias scale" },
                                {   5.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

/*
screen: round_bilinear_bias  10x10
unique values: 50
duplicates: 41
max duplication: 6

counts don't change much with scale
because it is diagonally symmetric
51 gray levels instead of 101

screen: round_bilinear_bias     11x11
unique values: 57
duplicates: 48
max duplication: 4

58 gray levels instead of 122
*/

// round dot function, with bilinear bias to get more tone values
// for documentation and learning
double dot_roundbilinear( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[0] / (200.0);
    const double round_scale = (1.0-bias_scale) * 0.25;
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double bias = dx + dy; // so we have centered -1.0 to +1.0
    
    double value = 0.5 + bias * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_roundbilinear( dot_roundbilinear, "round_bilinear_bias",
                                1,
                                { "bias scale" },
                                {   5.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// need false color version of spot values to see duplication - use high frequency gradient
/*
screen: roundspiral     8x8
bias: 0.0
unique values: 13
duplicates: 11
max duplication: 14

screen: roundspiral     8x8
bias: 2.94, 3.4, 3.56, 4.05, 12.4
unique values: 64
duplicates: 0
max duplication: 1


screen: roundspiral     9x9
bias: 0.0
unique values: 15
duplicates: 14
max duplication: 8

screen: roundspiral     9x9
bias: 8.66, 8.71
unique values: 80
duplicates: 1
max duplication: 2


screen: roundspiral     10x10
bias: 0.0
unique values: 19
duplicates: 17
max duplication: 18

screen: roundspiral     10x10
bias: 4.0, 4.01, 5.85, 5.86
unique values: 96
duplicates: 4
max duplication: 2


screen: roundspiral    11x11
bias: 0.0
unique values: 21
duplicates: 20
max duplication: 8

screen: roundspiral    11x11
bias: 4.40
unique values: 112
duplicates: 8
max duplication: 3


screen: roundspiral    12x12
bias: 0.0
unique values: 21
duplicates: 19
max duplication: 22

screen: roundspiral    12x12
bias: 4.44
unique values: 127
duplicates: 15
max duplication: 3


screen: roundspiral    13x13
bias: 0.0
unique values: 28
duplicates: 27
max duplication: 8

screen: roundspiral    13x13
bias: 4.40
unique values: 139
duplicates: 26
max duplication: 3


screen: roundspiral    14x14
bias: 0.0
unique values: 33
duplicates: 31
max duplication: 26

screen: roundspiral    14x14
bias: 3.74
unique values: 155
duplicates: 37
max duplication: 4


screen: roundspiral   15x15
bias: 0.0
unique values: 36
duplicates: 35
max duplication: 8

screen: roundspiral   15x15
bias: 3.12
unique values: 166
duplicates: 49
max duplication: 4


TODO: can this be improved upon?

TODO: is there an analytical optimum value for the bias percentage based on dot size?
    best value decreases with radius
    should be related to value steps along a radial line
        so one revolution brings value to next ring
        radial values are 0.5 + 0.5*cos(r)
        approx as (1.0-rfraction)
            difference is (1-rmin)-(1-rmax) = rmax-rmin as fractions = 1/(half_dot_size) = 2/dot_size
            That means 20 or 18 percent!!
            we're using -1 to +1, so half that value: 10 to 9 percent.
            but that doesn't match experimental results
 */

// round dot function, with spiral bias to get more tone values
// correct bias value depends on the resolution of the screen - which we don't know here
double dot_roundspiral( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[0] / (100.0*M_PI);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) * 0.25;
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double bias = atan2( dy, dx );
    
    double value = 0.5 + bias * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_roundspiral( dot_roundspiral, "roundspiral",
                                1,
                                { "bias scale" },
                                {   4.4 },      // about 4.0 for 10x10, 4.4 for 11x11
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// periodic width multiplier
// "I heard you want more dem dots?"
double dot_multiround( double x, double y, int64_t seed, const screen_options &opt )
{
    const double hscale = opt.params[0];
    double sx = cos( x * (hscale*2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sy + sx);
    return value;
}

dot_description desc_multiround( dot_multiround, "multiple round",
                                1,
                                { "horizontal scale" },
                                {  2.5 },
                                {  1.0 },
                                { 10.0 } );

/******************************************************************************/

// Moire dots - 2 sets of round dots at different rotations
// Something to annoy Wiley
double dot_moiredots( double x, double y, int64_t seed, const screen_options &opt )
{
    const double angle = (M_PI/180.0) * opt.params[0];
    const double scale = opt.params[1];
    
    const double CA = cos(angle);   // cos(-x) = cos(x)
    const double SA = sin(angle);   // sin(-x) = -sin(x)
    
    double x1 = scale * (CA * x + SA * y);
    double y1 = scale * (SA * x - CA * y);
    
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double dx = cos( x1 * (2.0*M_PI) );
    double dy = cos( y1 * (2.0*M_PI) );
    
    double value1 = 0.5 + 0.25 * (sx+sy);
    double value2 = 0.5 + 0.25 * (dx+dy);

    double value = std::max(value1,value2);
    return value*value;
}

dot_description desc_moiredots( dot_moiredots, "Moiré dots",
                                2,
                                { "angle", "scale" },
                                {   23.0,    1.00 },
                                {    0.1,    0.01 },
                                {  179.9,  100.00 } );

/******************************************************************************/

// Moire dots - 4 sets of round dots at different rotations
// Even more to annoy Wiley
double dot_moiredots4( double x, double y, int64_t seed, const screen_options &opt )
{
    const double angle1 = (M_PI/180.0) * opt.params[0];
    const double angle2 = (M_PI/180.0) * opt.params[1];
    const double angle3 = (M_PI/180.0) * opt.params[2];
    
    const double CA1 = cos(angle1);
    const double SA1 = sin(angle1);
    const double CA2 = cos(angle2);
    const double SA2 = sin(angle2);
    const double CA3 = cos(angle3);
    const double SA3 = sin(angle3);
    
    double x1 = CA1 * x + SA1 * y;
    double y1 = SA1 * x - CA1 * y;
    double x2 = CA2 * x + SA2 * y;
    double y2 = SA2 * x - CA2 * y;
    double x3 = CA3 * x + SA3 * y;
    double y3 = SA3 * x - CA3 * y;
    
    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double dx1 = cos( x1 * (2.0*M_PI) );
    double dy1 = cos( y1 * (2.0*M_PI) );
    double dx2 = cos( x2 * (2.0*M_PI) );
    double dy2 = cos( y2 * (2.0*M_PI) );
    double dx3 = cos( x3 * (2.0*M_PI) );
    double dy3 = cos( y3 * (2.0*M_PI) );
    
    double value1 = 0.5 + 0.25 * (sx+sy);
    double value2 = 0.5 + 0.25 * (dx1+dy1);
    double value3 = 0.5 + 0.25 * (dx2+dy2);
    double value4 = 0.5 + 0.25 * (dx3+dy3);

    double value = std::max( std::max(value1,value2), std::max(value3,value4) );
    return value*value*value;
}

dot_description desc_moiredots4( dot_moiredots4, "Moiré dots4",
                                3,
                                { "Angle 1", "Angle 2", "Angle 3" },
                                {   15.0,  45.0,  75.0 },
                                {    0.1,   0.1,   0.1 },
                                {  179.9, 179.9, 179.9 } );

/******************************************************************************/

// simple round dot function
double dot_quartercircle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_quartercircle( dot_quartercircle, "quarter circle" );

/******************************************************************************/

double dot_quartercirclerandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_rand & 0x80) == 0)
        fx = 1.0 - fx;
    
    if ((cell_rand & 0x08) == 0)
        fy = 1.0 - fy;
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_quartercirclerandom( dot_quartercirclerandom, "quarter circle random" );

/******************************************************************************/

double dot_quartercirclerandom2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_rand & 0x80) == 0)
        fx = 1.0 - fx;
    
    if ((cell_rand & 0x08) == 0)
        fy = 1.0 - fy;
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    
    if ((cell_rand & 0x20) == 0)
        return value;
    else
        return 1.0 - value;
}

dot_description desc_quartercirclerandom2( dot_quartercirclerandom2, "quarter circle random2" );

/******************************************************************************/

double dot_quartercirclealt1( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = (int64_t)(ix + iy);
    int64_t index = cell_id & 0x03;

    switch (index)
        {
        case 0:
            break;
        case 2:
            fx = 1.0 - fx;
            break;
        case 3:
            fx = 1.0 - fx;
            fy = 1.0 - fy;
            break;
        case 1:
            fy = 1.0 - fy;
            break;
        }
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_quartercirclealt1( dot_quartercirclealt1, "quarter circle alternating1" );

/******************************************************************************/

double dot_quartercirclealt2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = (int64_t)(ix + iy);
    int64_t index = cell_id & 0x03;

    switch (index)
        {
        case 0:
            break;
        case 2:
            fx = 1.0 - fx;
            break;
        case 1:
            fx = 1.0 - fx;
            fy = 1.0 - fy;
            break;
        case 3:
            fy = 1.0 - fy;
            break;
        }
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_quartercirclealt2( dot_quartercirclealt2, "quarter circle alternating2" );

/******************************************************************************/

// a rather twisted round dot function
double dot_rope( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = opt.params[0];
    const bool alternate = opt.params[1] > 0.5;
    
    double ix = floor(x);
    double fx = x - ix;

    // really need high slope on ends and flat in middle
    // cos is the reverse of that, but looks interesting
    double offset = scale * cos( fx * M_PI );
    
    if (alternate && ((int64_t)ix & 1) == 0)
        offset = -offset;
    
    y += offset;
    
    double sx = sin( fx * M_PI );
    double sy = cos( y * (2.0*M_PI) );
    
    double value = sx * (0.5 + 0.5 * sy);
    return sqrt(value);
}

dot_description desc_rope( dot_rope, "rope",
                            2,
                            { "scale", "alternate columns" },
                            {  1.5,   0.0 },
                            {  0.1,   0.0 },
                            { 10.0,   1.0 } );

/******************************************************************************/

// a rather twisted round dot function
double dot_rope2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double vert_scale = 1.5;
    const double scale = vert_scale * opt.params[0];
    const bool alternate = opt.params[1] > 0.5;
    
    y *= vert_scale;
    
    double ix = floor(x);
    double fx = x - ix;

    double temp = (fx - 0.5) * 2.0;
    double offset = scale * temp*temp*temp;
    
    if (alternate && ((int64_t)ix & 1) == 0)
        offset = -offset;
    
    y += offset;
    
    double sx = sin( fx * M_PI );
    double sy = cos( y * (2.0*M_PI) );
    
    double value = sx * (0.5 + 0.5 * sy);
    return sqrt(value);
}

dot_description desc_rope2( dot_rope2, "rope2",
                            2,
                            { "scale", "alternate columns" },
                            {  1.5,   0.0 },
                            {  0.1,   0.0 },
                            { 10.0,   1.0 } );

/******************************************************************************/

// intersection of opposing quater circles
double dot_petal( double x, double y, int64_t seed, const screen_options &opt )
{
    const double power = opt.params[0];
    const double scale = 2.0 / pow(2.0,1.0/power);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;

    double dist = 0.0;
    if (power == 2.0)
        {
        double dist1 = hypot(fx,fy);
        double dist2 = hypot( 1.0-fx, 1.0-fy );
        dist = std::max( dist1, dist2 );
        }
    else
        {
        double dist1 = pow(fx,power) + pow(fy,power);
        double dist2 = pow(1.0-fx,power) + pow(1.0-fy,power);
        dist1 = pow(dist1,1.0/power);
        dist2 = pow(dist2,1.0/power);
        dist = std::max( dist1, dist2 );
        }
    
    double value = dist * scale - 1.0;
    value = std::min( 1.0, std::max( 0.0, value ));
    value = pow(value,1.0/1.4);
    return 1.0 - value;
}

dot_description desc_petal( dot_petal, "petals",
                            1,
                            { "Squareness" },
                            {  2.0 },
                            {  1.0 },
                            { 20.0 } );

/******************************************************************************/

// intersection of opposing quater circles, anternating direction on a checkerboard
// How many banks use this in their logo?
double dot_petalalt( double x, double y, int64_t seed, const screen_options &opt )
{
    const double power = opt.params[0];
    const double scale = 2.0 / pow(2.0,1.0/power);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    if ( (((int64_t)ix + (int64_t)iy) & 1) == 0)
        fx = 1.0-fx;

    double dist = 0.0;
    if (power == 2.0)
        {
        double dist1 = hypot(fx,fy);
        double dist2 = hypot( 1.0-fx, 1.0-fy );
        dist = std::max( dist1, dist2 );
        }
    else
        {
        double dist1 = pow(fx,power) + pow(fy,power);
        double dist2 = pow(1.0-fx,power) + pow(1.0-fy,power);
        dist1 = pow(dist1,1.0/power);
        dist2 = pow(dist2,1.0/power);
        dist = std::max( dist1, dist2 );
        }
    
    double value = dist * scale - 1.0;
    value = std::min( 1.0, std::max( 0.0, value ));
    value = pow(value,1.0/1.4);
    return 1.0 - value;
}

dot_description desc_petalalt( dot_petalalt, "petals alternating",
                            1,
                            { "Squareness" },
                            {  2.0 },
                            {  1.0 },
                            { 20.0 } );

/******************************************************************************/

// very simple round dot function
double dot_circle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    
    double dist = hypot(fx,fy);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_circle( dot_circle, "circle" );

/******************************************************************************/

// rounded square / squircle dot function
double dot_squircle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double power = 2.0 + opt.params[0];
    const double scale = 1.0 / pow(2.0,1.0/power);
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    
    fx = fabs(fx);
    fy = fabs(fy);
    double len = pow(fx,power) + pow(fy,power);
    double dist = pow(len,1.0/power);
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_squircle( dot_squircle, "squircle",
                                1,
                                { "Squareness" },
                                {  2.0 },
                                {  0.0 },
                                { 20.0 } );

/******************************************************************************/

// not quite as simple hexagon dot function
// TODO - add in-cell rotation, which requires undoing some optimizations
// TODO - version with random rotation per cell, plus master angle
double dot_hexagon( double x, double y, int64_t seed, const screen_options &opt )
{
    const double C60 = cos(60.0*(M_PI/180.0));  // cos(-x) = cos(x)
    const double S60 = sin(60.0*(M_PI/180.0));  // sin(-x) = -sin(x)
    const double scale = 1.0 / sqrt(2.0);
    
    double ix = floor(x);
    double iy = floor(y);
    double xx = x - ix;
    double yy = y - iy;
    
    xx = (2.0*xx) - 1.0;
    yy = (2.0*yy) - 1.0;
    
    double x1 = C60 * xx - S60 * yy;
    double x2 = C60 * xx + S60 * yy;

    double dist = std::max( fabs(xx), std::max( fabs(x1), fabs(x2) ) );
    
    double value = dist * scale;
    return 1.0 - value;
}

dot_description desc_hexagon( dot_hexagon, "hexagon" );

/******************************************************************************/

// not quite as simple Ngon dot function
// TODO - add in-cell rotation, which requires undoing some optimizations
// TODO - version with random rotation per cell, plus master angle
double dot_Ngon( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round(opt.params[0]);
    const double scale = 1.0 / sqrt(2.0);
    
    double ix = floor(x);
    double iy = floor(y);
    double xx = x - ix;
    double yy = y - iy;
    
    xx = (2.0*xx) - 1.0;
    yy = (2.0*yy) - 1.0;
    
    double distance = 0.0;
    static size_t cached_sides = 0;
    static std::vector<double> cached_factors;

    if (sides != cached_sides)
        {
        cached_factors.resize( sides * 2 );
        for (size_t j = 0; j < sides; ++j)
            {
            double angle = j * 2.0*M_PI / sides;
            cached_factors[2*j+0] = cos(angle);
            cached_factors[2*j+1] = sin(angle);
            }
        cached_sides = sides;
        }

#if 0
// Try this as a polar problem: use atan2 to find angle, quantize, test single dist

// 1200dpi 8inches square, screen_lpi = image_ppi / 20.0;   // 20x20 dot
// 2580ms for 3 sided
// 2581ms for 5 sided
// 2583ms for 21 sided
// 2581ms for 51 sided
//          atan2 is just too slow!      781..802ms

    // -xx to preserve orientation of old shapes
    double angle = M_PI + atan2( yy, -xx );  // 0..2PI
    if (angle >= (2.0*M_PI))
        angle -= (2.0*M_PI);
    assert( angle >= 0.0 );
    assert( angle < (2.0*M_PI) );
    
    size_t index = round( angle * sides / (2.0*M_PI));
    
    if (index >= sides)
        index -= sides;
    
    assert( index >= 0 );
    assert( index < sides );
    
    double CA = cached_factors[2*index+0];
    double SA = cached_factors[2*index+1];
    double xtemp = CA * xx - SA * yy;
    assert( xtemp >= 0.0 );
    
    distance = xtemp;
    
#else

// 1200dpi 8inches square, screen_lpi = image_ppi / 20.0;   // 20x20 dot
// 539ms for 3 sided
// 367ms for 4 sided
// 602ms for 5 sided
// 985ms for 20 sided       70ms for vector alloc
// 1120ms for 21 sided
// 2140ms for 48 sided      73ms vector alloc
// 1410ms for 50 sided      72ms vector alloc
// 2420ms for 51 sided

    if ( (sides & 3) == 0 )
        {   // multiple of 4, can use fabs, y, and quarter the side count
        distance = std::max( fabs(xx), fabs(yy) );
        for (size_t k = 1; k < (sides/4); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            distance = std::max( fabs(xtemp), distance );
            distance = std::max( fabs(ytemp), distance );
            }
        }
    else if ( (sides & 1) == 0 )
        {   // multiple of 2, can use fabs and half the side count
        distance = fabs(xx);
        for (size_t k = 1; k < (sides/2); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            distance = std::max( fabs(xtemp), distance );
            }
        }
    else
        {   // odd, have to run all angles
        distance = std::max(0.0,xx);
        for (size_t k = 1; k < sides; ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            distance = std::max( xtemp, distance );
            }
        }
#endif

    double value = distance * scale;
    value = std::max(0.0, std::min(1.0, value ));
    return 1.0 - value;
}

dot_description desc_Ngon( dot_Ngon, "Ngon",
                                1,
                                { "sides" },
                                {  5.0 },
                                {  3.0 },
                                { 20.0 } );

/******************************************************************************/

// not quite as simple star dot function
// TODO - add in-cell rotation, which requires undoing some optimizations
// TODO - version with random rotation per cell, plus master angle
double dot_stars( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round(opt.params[0]);
    const double adjust = 1.0 / opt.params[1];
    const double scale = 0.9;
    
    double ix = floor(x);
    double iy = floor(y);
    double xx = x - ix;
    double yy = y - iy;
    
    xx = (2.0*xx) - 1.0;
    yy = (2.0*yy) - 1.0;
    
    double distance = 0.0;
    
    static size_t cached_sides = 0;
    static double cached_adjust = -99.0;
    static std::vector<double> cached_factors;
    static double cached_ratio = 0.0;
    
    if ( (sides != cached_sides) || (adjust != cached_adjust) )
        {
        cached_factors.resize( sides * 2 );
        for (size_t j = 0; j < sides; ++j)
            {
            double angle = j * 2.0*M_PI / sides;
            cached_factors[2*j+0] = cos(angle);
            cached_factors[2*j+1] = sin(angle);
            }
        cached_sides = sides;
        double width = adjust * tan(M_PI/double(sides));
        cached_ratio = std::max( 1.0e-6, std::min( 0.7, width ));
        cached_adjust = adjust;
        }

    if ((sides & 3) == 0)
        {   // multiple of 4, can use fabs, y, and quarter the side count
        double point1 = fabs(xx) - cached_ratio*fabs(yy);
        double point2 = fabs(yy) - cached_ratio*fabs(xx);
        distance = std::max( point1, distance );
        distance = std::max( point2, distance );
        for (size_t k = 1; k < (sides/4); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            double point1 = fabs(xtemp) - cached_ratio*fabs(ytemp);
            double point2 = fabs(ytemp) - cached_ratio*fabs(xtemp);
            distance = std::max( point1, distance );
            distance = std::max( point2, distance );
            }
        }
    else if ( (sides & 1) == 0)
        {   // multiple of 2, can use fabs and half the side count
        double point = fabs(xx) - cached_ratio*fabs(yy);
        distance = std::max(0.0,point);
        for (size_t k = 1; k < (sides/2); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            double point = fabs(xtemp) - cached_ratio*fabs(ytemp);
            distance = std::max( point, distance );
            }
        }
    else
        {   // odd, have to run all angles
        if (xx >= 0.0)
            {
            double point = xx - cached_ratio*fabs(yy);
            distance = std::max(0.0,point);
            }
        for (size_t k = 1; k < sides; ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            if (xtemp >= 0.0)
                {
                double point = xtemp - cached_ratio*fabs(ytemp);
                distance = std::max( point, distance );
                }
            }
        }
    
    double value = scale * distance;
    value = std::max(0.0, std::min(1.0, value ));
    return 1.0 - value;
}

dot_description desc_stars( dot_stars, "stars",
                                2,
                                { "sides", "point width" },
                                {  5.0,  1.0 },
                                {  3.0,  0.1 },
                                { 20.0, 10.0 } );

/******************************************************************************/

// not quite as simple splat dot function
// TODO - version with random rotation per cell, plus master angle?  transform xx and yy outside loop.
double dot_splats( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round(opt.params[0]);
    const double angle_range = opt.params[1] * (1.0 / 100.0);
    const double end_size = opt.params[2];
    const double arm_min = 0.01;     // parameter?
    const double scale = 0.9;
    
    double ix = floor(x);
    double iy = floor(y);
    double xx = x - ix;
    double yy = y - iy;
    
    xx = (2.0*xx) - 1.0;    // -1..+1
    yy = (2.0*yy) - 1.0;
    
    double r = hypot(xx,yy) / M_SQRT2;  // 0..1
    
    
    double distance = 0.0;

    static size_t cached_sides = 0;
    static int64_t cached_seed = 0;
    static double cached_angle_range = 0.0;
    static std::vector<double> cached_factors;
    
    if ( (sides != cached_sides) || (cached_seed != seed)
        || (cached_angle_range != angle_range) )
        {
        cached_factors.resize( sides * 2 );
        for (size_t j = 0; j < sides; ++j)
            {
            double angle_offset = angle_range * hashXY_float( hash64((int64_t)j), seed );
            double angle = (j+angle_offset) * (2.0*M_PI / sides);
            cached_factors[2*j+0] = cos(angle);
            cached_factors[2*j+1] = sin(angle);
            }
        cached_sides = sides;
        cached_seed = seed;
        cached_angle_range = angle_range;
        }

    const double limit = 0.72;          // sqrt(2)/2 ?
    double const maxx = limit*limit;    // must match rr scale
    double r2 = std::min( limit, r );
    double rr = r2*r2;                  // must match maxx
    double teardrop = end_size * (arm_min + (1.0-arm_min)*sin( rr*M_PI/maxx ));

    for (size_t k = 0; k < sides; ++k)
        {
        double CA = cached_factors[2*k+0];
        double SA = cached_factors[2*k+1];
        double xtemp = CA * xx - SA * yy;
        double ytemp = SA * xx + CA * yy;
        if (xtemp >= 0.0)
            {
            double point = xtemp - fabs(ytemp) * teardrop;
            distance = std::max( point, distance );
            }
        }
    
    double value = scale * distance;
    value = std::max(0.0, std::min(1.0, value ));
    return 1.0 - value;
}

dot_description desc_splats( dot_splats, "splats",
                                3,
                                { "arms", "angle variation", "end size" },
                                {  6.0,  20.0,  1.2 },
                                {  3.0,   0.0,  0.1 },
                                { 20.0, 100.0,  4.0 } );

/******************************************************************************/

// very simple round dot function, alternating in checkerboard pattern
double dot_checkers( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double dist = hypot(fx,fy);
    double value = dist * scale;
    
    if (((ix^iy)&0x01) == 0)
        return value;
    else
        return 1.0 - value;
}

dot_description desc_checkers( dot_checkers, "checkers" );

/******************************************************************************/

// simple ellipse dot function
double dot_ellipse( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[0];
    double sx = EccX * cos( x * (2.0*M_PI) );
    double sy = EccY * cos( y * (2.0*M_PI) );
    double value = 0.5 + (sx+sy) / (2.0*(EccX+EccY));
    return 1.0-value;
}

dot_description desc_ellipse( dot_ellipse, "ellipse",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

/*
Ellipse is only 4 way symmetric
Ecc = 0.7

screen: ellipsespiral     8x8
bias: 0.0
unique values: 22
duplicates: 18
max duplication: 6

screen: ellipsespiral     8x8
bias: 2.71, 2.88
unique values: 64
duplicates: 0
max duplication: 1


screen: ellipsespiral     9x9
bias: 0.0
unique values: 25
duplicates: 24
max duplication: 4

screen: ellipsespiral     9x9
bias: 15.47
unique values: 78
duplicates: 3
max duplication: 2


screen: ellipsespiral     10x10
bias: 0.0
unique values: 36
duplicates: 32
max duplication: 4

screen: ellipsespiral     10x10
bias: 8.93
unique values: 93
duplicates: 7
max duplication: 2


screen: ellipsespiral    11x11
bias: 0.0
unique values: 34
duplicates: 33
max duplication: 8

screen: ellipsespiral    11x11
bias: 6.07, 6.09, 6.16
unique values: 109
duplicates: 12
max duplication: 2


screen: ellipsespiral    12x12
bias: 0.0
unique values: 49
duplicates: 45
max duplication: 4

screen: ellipsespiral    12x12
bias: 1.70
unique values: 125
duplicates: 17
max duplication: 3


screen: ellipsespiral    13x13
bias: 0.0
unique values: 42
duplicates: 42
max duplication: 8

screen: ellipsespiral    13x13
bias: 7.51, 7.55, 8.03, 12.67
unique values: 136
duplicates: 28
max duplication: 3


screen: ellipsespiral    14x14
bias: 0.0
unique values: 56
duplicates: 52
max duplication: 8

screen: ellipsespiral    14x14
bias: 5.83
unique values: 157
duplicates: 34
max duplication: 4


screen: ellipsespiral   15x15
bias: 0.0
unique values: 60
duplicates: 59
max duplication: 8

screen: ellipsespiral   15x15
bias: 5.21
unique values: 168
duplicates: 46
max duplication: 3


Optimum bias value is even less regular than for round dots
 */


// ellipse dot function, with spiral bias to get more tone values
// correct bias depends on the resolution of the screen - which we don't know here
double dot_ellipsespiral( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[0];
    const double bias_scale = opt.params[1] / (100.0*M_PI);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) / (2.0*(EccX+EccY));
    
    double sx = EccX * cos( x * (2.0*M_PI) );
    double sy = EccY * cos( y * (2.0*M_PI) );
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double bias = atan2( dy, dx );
    
    double value = 0.5 + bias * bias_scale + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return 1.0 - value;
}

dot_description desc_ellipsespiral( dot_ellipsespiral, "ellipsespiral",
                                2,
                                { "Eccentricity", "bias scale" },
                                { 0.70,    6.09 },
                                { 0.01,    0.00 },
                                { 4.00,  100.00 } );

/******************************************************************************/

// simple ellipse dot function, with random direction swap
double dot_ellipseRand( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[0];
    double ix = floor(x);
    double iy = floor(y);
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    if ( (cell_rand & 0x80) == 0)
        std::swap(x,y);
    
    double sx = EccX * cos( x * (2.0*M_PI) );
    double sy = EccY * cos( y * (2.0*M_PI) );
    double value = 0.5 + (sx+sy) / (2.0*(EccX+EccY));
    return 1.0-value;
}

dot_description desc_ellipserandom( dot_ellipseRand, "ellipse random",
                                1,
                                { "Eccentricity" },
                                { 0.5 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// simple ellipse dot function, rotated within cell
double dot_ellipse45( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.5;
    if (opt.params.size() > 0)
        EccY = opt.params[0];
    const double norm = 0.9 * sqrt(EccX+EccY) / hypot(EccX,EccY);

    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx -= 0.5;
    fy -= 0.5;
    
    double temp = fx;
    double rx = fx + fy;     // -1...+1
    double ry = temp - fy;   // -1...+1
    
    rx *= EccX;
    ry *= EccY;
    double round = norm * hypot(rx,ry);
    return 1.0-round;
}

dot_description desc_ellipse45( dot_ellipse45, "ellipse45",
                                1,
                                { "Eccentricity" },
                                { 0.5 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// simple ellipse dot function, rotated within cell
// sadly the opposing corners no longer line up well, leading to odd shapes in shadows
double dot_ellipse45Rand( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.5;
    if (opt.params.size() > 0)
        EccY = opt.params[0];
    const double norm = 0.9 * sqrt(EccX+EccY) / hypot(EccX,EccY);

    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    fx -= 0.5;
    fy -= 0.5;
    
    double temp = fx;
    double rx = fx + fy;     // -1...+1
    double ry = temp - fy;   // -1...+1
    
    int64_t cell_rand = hashXY( (int64_t)ix, (int64_t)iy, seed );
    if ( (cell_rand & 0x80) == 0)
        std::swap(rx,ry);
    
    rx *= EccX;
    ry *= EccY;
    double round = norm * hypot(rx,ry);
    return 1.0-round;
}

dot_description desc_ellipse45random( dot_ellipse45Rand, "ellipse45 random",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// simple ellipse dot function, rotated within cell, alternating directions
double dot_ellipse45alt( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.6;
    if (opt.params.size() > 0)
        EccY = opt.params[0];
    const double norm = 0.9 * sqrt(EccX+EccY) / hypot(EccX,EccY);

    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool odd = ((ix ^ iy) & 0x01) == 1; // checkerboard

    double fx = x - ix;
    double fy = y - iy;
    
    fx -= 0.5;
    fy -= 0.5;
    
    double temp = fx;
    double rx = fx + fy;     // -1...+1
    double ry = temp - fy;   // -1...+1
    
    if (odd) std::swap(rx,ry);
    
    rx *= EccX;
    ry *= EccY;

    double round = norm * hypot(rx,ry);
    return 1.0-round;
}

dot_description desc_ellipse45alt( dot_ellipse45alt, "ellipse45 alternating",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// simple ellipse dot function, rotated within cell, alternating directions
// but out of phase, creates interesting rosettes and a double dot pattern
double dot_ellipse45alt2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.7;
    if (opt.params.size() > 0)
        EccY = opt.params[0];

    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool odd = ((ix ^ iy) & 0x01) == 1; // checkerboard

    double rx = x + y;
    double ry = x - y;
    
    if (odd) std::swap(rx,ry);
    
    double sx = EccX * cos( rx * (2.0*M_PI) + M_PI );
    double sy = EccY * cos( ry * (2.0*M_PI) + M_PI );
    double value = 0.5 + (sx+sy) / (2.0*(EccX+EccY));
    return 1.0-value;
}

dot_description desc_ellipse45alt2( dot_ellipse45alt2, "ellipse45 alternating2",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// concave (kawaii) diamond
// still not well behaved, but ok for now
// TODO - normalization seems off, and concavity mixing
double dot_diamondconcave( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[0];
    const double concavity = opt.params[1];
    const double norm = 0.85 * sqrt(EccX + EccY) / hypot(EccX,EccY);
    const double limit = 0.5 * (EccX + EccY);
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;

    // scale 0..1, out from corners
    fx = EccX * (1.0 - fabs( 2.0*fx - 1.0 ));
    fy = EccY * (1.0 - fabs( 2.0*fy - 1.0 ));
    
    double fz = fx+fy;
    double round = 0.0;
    double diamond = 0.0;
    
    // change behavior across the diagonal
    if (fz > limit)
        {
        round = norm * hypot(fx,fy);
        diamond = 0.5 * limit * fz;
        }
    else
        {
        fx = EccX - fx;
        fy = EccY - fy;
        round = norm * hypot(fx,fy);
        diamond = 0.5 * limit * (fx + fy);
        round = 1.0 - round;
        diamond = 1.0 - diamond;
        }

    double value = (1.0-concavity)*diamond + concavity*round;

    return std::min( std::max( 0.0, value ), 1.0 );
}

dot_description desc_diamondconcave( dot_diamondconcave, "diamond concave",
                                2,
                                { "Eccentricity", "Concavity" },
                                {  0.8,   0.6 },
                                {  0.1,   0.0 },
                                {  4.0,   5.0 } );

/******************************************************************************/

// semi-circle dots
double dot_semicircle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    return 1.0 - sum;
}

dot_description desc_semicircle( dot_semicircle, "semicircle",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-circle dots
double dot_semicirclerand( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    return 1.0 - sum;
}

dot_description desc_semicirclerand( dot_semicirclerand, "semicircle random",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-circle dots
double dot_semicirclerand2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    if ((cell_id & 0x20) == 0)
        return sum;
    else
        return 1.0 - sum;
}

dot_description desc_semicirclerand2( dot_semicirclerand2, "semicircle random2",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-circle dots
double dot_semicirclerand3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        fx = 1.0 - fx;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    return 1.0 - sum;
}

dot_description desc_semicirclerand3( dot_semicirclerand3, "semicircle random3",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// double semi-circle dots
double dot_semicircle2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(0.25+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    fx = std::min( fx, 1.0 - fx );  // 0..+0.5 reflected
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    return 1.0 - sum;
}

dot_description desc_semicircle2( dot_semicircle2, "double semicircle",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// double semi-circle dots
double dot_semicircle2rand( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(0.25+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    fx = std::min( fx, 1.0 - fx );  // 0..+0.5 reflected
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    return 1.0 - sum;
}

dot_description desc_semicircle2rand( dot_semicircle2rand, "double semicircle random",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// double semi-circle dots
double dot_semicircle2rand2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(0.25+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    fx = std::min( fx, 1.0 - fx );  // 0..+0.5 reflected
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    if ((cell_id & 0x20) == 0)
        return sum;
    else
        return 1.0 - sum;
}

dot_description desc_semicircle2rand2( dot_semicircle2rand2, "double semicircle random2",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-round dots
double dot_semiround( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    sum = 2.0 * fabs( sum - 0.5 );  // reflect and rescale
    
    return 1.0 - sum;
}

dot_description desc_semiround( dot_semiround, "semiround",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-round dots
double dot_semiroundalt( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    if (((int64_t)ix & 1) == 0)
        fx = 1.0-fx;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    sum = 2.0 * fabs( sum - 0.5 );  // reflect and rescale
    
    return 1.0 - sum;
}

dot_description desc_semiroundalt( dot_semiroundalt, "semiround alt",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-round dots
double dot_semiroundrandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    sum = 2.0 * fabs( sum - 0.5 );  // reflect and rescale
    
    return 1.0 - sum;
}

dot_description desc_semiroundrandom( dot_semiroundrandom, "semiround random",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-round dots
double dot_semiroundrandom2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    if ((cell_id & 0x20) == 0)
        fx = 1.0 - fx;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    sum = 2.0 * fabs( sum - 0.5 );  // reflect and rescale
    
    return 1.0 - sum;
}

dot_description desc_semiroundrandom2( dot_semiroundrandom2, "semiround random2",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// semi-round dots
double dot_semiroundrandom3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / sqrt(1.0+0.25*EccY*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += offset * ix;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t cell_id = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((cell_id & 0x80) == 0)
        std::swap(fx,fy);
    
    if ((cell_id & 0x20) == 0)
        fx = 1.0 - fx;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * hypot(fx,fy);
    
    sum = 2.0 * fabs( sum - 0.5 );  // reflect and rescale
    
    if ((cell_id & 0x08) == 0)
        return sum;
    else
        return 1.0 - sum;
}

dot_description desc_semiroundrandom3( dot_semiroundrandom3, "semiround random3",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  1.0,  0.0 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// another kind of triangle dots
double dot_triangle2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccY = opt.params[0];
    const double norm = 1.0 / (1.0+0.5*EccY);
    const double offset = opt.params[1];
    
    double ix = floor(x);
    
    y += ix * offset;
    
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    
    fy -= 0.5;  // -0.5 .. +0.5
    fy *= EccY;
    
    double sum = norm * (fx + fabs(fy));
    
    return 1.0 - sum;
}

dot_description desc_triangle2( dot_triangle2, "triangle2",
                                2,
                                { "Eccentricity", "Column Offset" },
                                {  2.0,  0.5 },
                                {  0.1,  0.0 },
                                {  4.0,  1.0 } );

/******************************************************************************/

// euclidean round dot - old PostScript code, tweaked
// symmetric dots, but not the most even value distribution
double dot_euclidean( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    // scale -1 to 1
    fx = fabs( 2.0*fx - 1.0 );
    fy = fabs( 2.0*fy - 1.0 );
    
    double fz = fx+fy;
    double value = 0.0;
    
    if (fz <= 1.0)
        {
        // upper left triangle gets euclidian dist
        value = hypot(fx,fy);
        }
    else
        {
        // lower right triangle is inverse of uppper left
        fx = 1.0 - fx;
        fy = 1.0 - fy;
        value = 1.9 - hypot(fx,fy);
        }

    return std::min( std::max( 0.0, 0.527 * value ), 1.0 );
}

dot_description desc_euclidean( dot_euclidean, "euclidean" );

/******************************************************************************/

// diamond shaped dot function - similar to old PostScript code
double dot_diamond( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.6;
    if (opt.params.size() > 0)
        EccY = opt.params[0];
    const double norm = 1.0 / (EccX+EccY);
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    
    fx = EccX * fabs(fx);
    fy = EccY * fabs(fy);
    
    double sum = fx + fy;
    
    return 1.0 - sum * norm;
}

dot_description desc_diamond( dot_diamond, "diamond",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// diamond shaped dot function, with spiral bias to get more tone values
// correct bias depends on the resolution of the screen - which we don't know here
double dot_diamondspiral( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.7;
    double bias_scale = 2.5 / (100.0*M_PI);
    if (opt.params.size() > 0)
        {
        EccY = opt.params[0];
        bias_scale = opt.params[1] / (100.0*M_PI);  // with clipping it is safe to vary a bit
        }
    const double round_scale = (1.0-bias_scale) / (EccX+EccY);
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0; // so we have centered -1.0 to +1.0
    fy = (2.0*fy) - 1.0;
    double bias = atan2( fy, fx );
    
    fx = EccX * fabs(fx);
    fy = EccY * fabs(fy);
    
    double value = (0.5 + bias) * bias_scale + round_scale * (fx + fy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return 1.0 - value;
}

dot_description desc_diamondspiral( dot_diamondspiral, "diamondspiral",
                                2,
                                { "Eccentricity", "bias scale" },
                                { 0.70,    2.5 },
                                { 0.01,    0.0 },
                                { 4.00,  100.0 } );

/******************************************************************************/

// simple diamond dot function, alternating directions
double dot_diamondalt( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    double EccY = 0.6;
    if (opt.params.size() > 0)
        EccY = opt.params[0];
    const double norm = 1.0 / (EccX+EccY);
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool odd = ((ix ^ iy) & 0x01) == 1; // checkerboard

    double fx = x - ix;
    double fy = y - iy;
    
    if (odd) std::swap(fx,fy);
    
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    
    fx = EccX * fabs(fx);
    fy = EccY * fabs(fy);
    
    double sum = fx + fy;
    return 1.0 - sum * norm;
}

dot_description desc_diamondalt( dot_diamondalt, "diamond alternating",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// concave diamond dot function, alternating directions
double dot_diamondconcavealt( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[0];
    const double concavity = opt.params[1];
    const double norm = 0.85 * sqrt(EccX + EccY) / hypot(EccX,EccY);
    const double limit = 0.5 * (EccX + EccY);
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool odd = ((ix ^ iy) & 0x01) == 1; // checkerboard

    double fx = x - ix;
    double fy = y - iy;
    
    if (odd) std::swap(fx,fy);

    // scale 0..1, out from corners
    fx = EccX * (1.0 - fabs( 2.0*fx - 1.0 ));
    fy = EccY * (1.0 - fabs( 2.0*fy - 1.0 ));
    
    double fz = fx+fy;
    double round = 0.0;
    double diamond = 0.0;
    
    // change behavior across the diagonal
    if (fz > limit)
        {
        round = norm * hypot(fx,fy);
        diamond = 0.5 * limit * fz;
        }
    else
        {
        fx = EccX - fx;
        fy = EccY - fy;
        round = norm * hypot(fx,fy);
        diamond = 0.5 * limit * (fx + fy);
        round = 1.0 - round;
        diamond = 1.0 - diamond;
        }

    double value = (1.0-concavity)*diamond + concavity*round;

    return std::min( std::max( 0.0, value ), 1.0 );
}

dot_description desc_diamondconcavealt( dot_diamondconcavealt, "diamond concave alternating",
                                2,
                                { "Eccentricity", "Concavity" },
                                {  0.8,   0.6 },
                                {  0.1,   0.0 },
                                {  4.0,   5.0 } );

/******************************************************************************/

// very simple cross function - not quite inverse of square, sometimes useful
double dot_cross( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double dist = std::min( fabs(fx), fabs(fy) );
    return 1.0 - dist;
}

dot_description desc_cross( dot_cross, "cross" );

/******************************************************************************/

// very simple cross function - rotated 45 degrees
double dot_cross45( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edgeval = (opt.params.size() > 0) ? opt.params[0] : 0.05;
    const double edge = 0.5 - edgeval;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    fx -= 0.5;
    fy -= 0.5;
    
    if ( fabs(fx) > edge || fabs(fy) > edge)
        return 0.01;

    double tx = (fx + fy);
    double ty = (fx - fy);

    double dist = 2.0 * std::min( fabs(tx), fabs(ty) );
    dist = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - dist;
}

dot_description desc_cross45( dot_cross45, "cross45",
                            1,
                            { "edge limit" },
                            {  0.07 },
                            {  0.00 },
                            {  1.00 } );

/******************************************************************************/

// radar / angle dot function, dot shape tends to be blocks
double dot_angle( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double bias = atan2( dy, dx );
    double value = 0.5 + bias / (2.0*M_PI);
    return value;
}

dot_description desc_angle( dot_angle, "angle" );

/******************************************************************************/

// radar / angle dot function, with random offsets per cell
double dot_anglerandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double cell_rand = M_PI * hashXY_float( (int64_t)ix, (int64_t)iy, seed );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double angle = cell_rand + atan2( dy, dx );
    
    while (angle >= M_PI)
        angle -= 2.0*M_PI;
    
    double value = 0.5 + angle / (2.0*M_PI);
    return value;
}

dot_description desc_anglerandom( dot_anglerandom, "angle random" );

/******************************************************************************/

// radar / angle dot function - rotated 45 degrees, dot shape tends to be triangles
double dot_angle45( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    double bias = atan2( dx+dy, dx-dy );
    double value = 0.5 + bias / (2.0*M_PI);
    return value;
}

dot_description desc_angle45( dot_angle45, "angle45" );

/******************************************************************************/

// square dot function
double dot_square( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    //double value = 0.8 * sqrt(std::min( fx, fy )) + 0.1 * (fx + fy);  // better, but odd shapes
    double value = sqrt(std::min( fx, fy ));
    return value;
}

dot_description desc_square( dot_square, "square" );

/******************************************************************************/

// random corner square dot function
double dot_squarerandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t bits = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((bits & 0x08) != 0)
        fx = 1.0 - fx;
    
    if ((bits & 0x80) != 0)
        fy = 1.0 - fy;
    
    double value = sqrt(std::min( fx, fy ));
    return value;
}

dot_description desc_squarerandom( dot_squarerandom, "square random" );

/******************************************************************************/

// more symmetric square dot function, with bias options
double dot_square2( double x, double y, int64_t seed, const screen_options &opt )
{
    double vbias_scale = 0.0;
    double hbias_scale = 0.0;
    
    // just in case, this can work without parameters
    if (opt.params.size() >= 2)
        {
        vbias_scale = opt.params[0] / (100.0);
        hbias_scale = opt.params[1] / (100.0);
        }
    
    const double square_scale = (1.0-vbias_scale+hbias_scale);
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double valuey = std::min( fy, 1.0-fy );
    double valuex = std::min( fx, 1.0-fx );
    double value = sqrt(std::min( valuey, valuex ));
    value = 0.5 - 0.73 * value;
    value = (vbias_scale * fy) + (hbias_scale*fx) + (square_scale * value);

    bool even = (((int64_t)ix + (int64_t)iy) & 0x01) == 0;  // checkerboard pattern
    if (!even)
        value = 1.0 - value;
    
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_square2( dot_square2, "balanced square",
                            2,
                            { "vertical bias", "horizontal bias" },
                            {   0.0,   0.0 },
                            {   0.0,   0.0 },
                            {  50.0,  50.0 } );

/******************************************************************************/

// alternating in and out squares
double dot_square3( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double valuey = std::min( fy, 1.0-fy );
    double valuex = std::min( fx, 1.0-fx );
    double value = 1.5 * sqrt(std::min( valuey, valuex )) - 0.06;

    bool even = (((int64_t)ix + (int64_t)iy) & 0x01) == 0;  // checkerboard pattern
    if (even)
        value = 1.0 - value;
    
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_square3( dot_square3, "squares alternating" );

/******************************************************************************/

// an odd square function with reversal, that forms a square in a square
double dot_squareinsquare( double x, double y, int64_t seed, const screen_options &opt )
{
    const double breakpoint = opt.params[0];
    const double offset = opt.params[1];
    const double norm = sqrt(2.0);
    const double norm2 = 1.0 / breakpoint;
    
    double ix = floor(x);
    double fx = x - ix;
    
    y += offset * ix;
    
    double iy = floor(y);
    double fy = y - iy;
    
    double valuex = std::min( fx, 1.0-fx );
    double valuey = std::min( fy, 1.0-fy );
    double value = norm * sqrt(std::min( valuey, valuex ));
    
    if (value < (1.0-breakpoint))
        value = breakpoint + value;
    else
        value = (value - breakpoint) * norm2;
    
    // sometimes runs just slightly greater than 1
    value = std::min( 1.0, value );
    value = std::max( 0.0, value );
    return value;
}

dot_description desc_squareinsquare( dot_squareinsquare, "square in square",
                                    2,
                                    { "break point", "column offset" },
                                    {  0.5, 0.5 },
                                    {  0.0, 0.0 },
                                    {  1.0, 1.0 } );

/******************************************************************************/

// woven screen - alternating vertical and horizontal ramps
double dot_weave( double x, double y, int64_t seed, const screen_options &opt )
{
    double line_count = opt.params[0];
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool even = ((ix ^ iy) & 0x01) == 0;    // checkerboard pattern
    
    if (even)
        {
        y *= line_count;
        double iy2 = floor(y);
        double fy = y - iy2;
        return fy;
        }
    else
        {
        x *= line_count;
        double ix2 = floor(x);
        double fx = x - ix2;
        return fx;
        }
}

dot_description desc_weave( dot_weave, "basket weave",
                            1,
                            { "line count" },
                            {  2.0 },
                            {  1.0 },
                            { 20.0 } );

/******************************************************************************/

// another type of woven screen - alternating vertical and horizontal ramps
double dot_weavealt( double x, double y, int64_t seed, const screen_options &opt )
{
    double line_count = opt.params[0];
    int64_t ix = floor(x);
    
    bool even = ix & 1;
    
    if (even)
        {
        y *= line_count;
        double iy2 = floor(y);
        double fy = y - iy2;
        return fy;
        }
    else
        {
        x *= line_count;
        double ix2 = floor(x);
        double fx = x - ix2;
        return fx;
        }
}

dot_description desc_weavealt( dot_weavealt, "alternate weave",
                            1,
                            { "line count" },
                            {  2.0 },
                            {  1.0 },
                            { 20.0 } );

/******************************************************************************/

// woven fabric screen - alternating vertical and horizontal ramps
double dot_weave2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double value_distortion = opt.params[0] / 100.0;
    const double curve = opt.params[1] / 100.0;
    const double distort_scale = opt.params[2];
    const double weave = 1.0 - value_distortion;
    const int32_t octave_low = -4;
    const int32_t octave_high = 1;
    
    // should this have separate octaves for spatial and value distortion?
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash3 );
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash2 );

    x += distort_scale * dx;
    y += distort_scale * dy;
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    bool even = ((ix ^ iy) & 0x01) == 0;    // checkerboard pattern
    
    double dist = y;
    double cross = x;
    double dv = 0.0;
    
    if (even)
        {
        dist = x;
        cross = y;
        // vertical thread, stretch out the noise vertically/compress horizontally
        dv = ChaosBicubic( 11.0*x, y, octave_low, octave_high, seed ^ extraHash1 );
        }
    else
        {
        // horizontal thread, stretch out the noise horizontally/compress vertically
        dv = ChaosBicubic( x, 11.0*y, octave_low, octave_high, seed );
        }
    
    double result = fabs( sin(dist*M_PI) );
    double under = 1.0 - curve * fabs( cos( cross*M_PI ) ); // threads bend under/over
    
    result = (weave * under * result) + (value_distortion * dv);
    
    return std::min( 1.0, std::max( 0.0, result ) );
}

dot_description desc_weave2( dot_weave2, "fabric weave",
                            3,
                            { "thread variation", "thread curvature", "position distortion" },
                            {  25.0,  20.0,   1.5 },
                            {   0.0,   0.0,   0.0 },
                            {  90.0,  90.0,  50.0 } );

/******************************************************************************/

// brick grid
double dot_brick( double x, double y, int64_t seed, const screen_options &opt )
{
    const double width = opt.params[0];
    const double width_ratio = 1.0 / width;
    
    int64_t iy = floor(y);
    double fy = y - iy;
    
    x *= width_ratio;
    
    if ((iy & 0x01) == 1)       // even/odd displacement
        x += 0.5;
    
    double ix = floor(x);
    double fx = x - ix;
    double minx = std::min( fx, (1.0-fx) );
    
    minx *= width;
    
    double miny = std::min( fy, (1.0-fy) );
    double value = 2.0 * std::min( minx, miny );
    return value;
}

dot_description desc_brick( dot_brick, "brick",
                            1,
                            { "width" },
                            { 8.0 / 2.6666 },  // standard US brick nominal (with mortar)
                            { 0.1 },
                            { 20.0 } );

/******************************************************************************/

double dot_herringbonetile( double x, double y, int64_t seed, const screen_options &opt )
{
    double iy = floor(y);
    double ix = floor(x);
    double fy = y - iy;
    double fx = x - ix;
    
    iy += ix;
    
    int64_t key = (int64_t)iy % 4;
    if (key < 0) key += 4;   // negatives happen
    
    double minx = std::min( fx, (1.0-fx) );
    double miny = std::min( fy, (1.0-fy) );
    
    switch( key )
        {
        case 0: // Left side of brick, no right calc
            minx = fx;
            break;
        case 1: // Right side of brick, no left calc
            minx = (1.0-fx);
            break;
        case 2: // Top side of brick, no bottom calc
            miny = fy;
            break;
        case 3: // Botom side of brick, no top calc
            miny = (1.0 - fy);
            break;
        }
    
    double value = 2.0 * std::min( minx, miny );
    return value;
}

dot_description desc_herringbonetile( dot_herringbonetile, "herringbone tile" );

/******************************************************************************/

double dot_herringbonetile2( double x, double y, int64_t seed, const screen_options &opt )
{
    double iy = floor(y);
    double ix = floor(x);
    double fy = y - iy;
    double fx = x - ix;
    
    iy += ix;
    
    int64_t key = (int64_t)iy % 6;
    if (key < 0) key += 6;   // negatives happen
    
    double minx = std::min( fx, (1.0-fx) );
    double miny = std::min( fy, (1.0-fy) );
    
    switch( key )
        {
        case 0:     // top of vertical
            miny = fy;
            break;
        case 1:     // middle of vertical
            miny = 2 * minx;
            break;
        case 2:     // bottom of vertical
            miny = (1.0 - fy);
            break;
        case 3:     // Left of horizontal
            minx = fx;
            break;
        case 4:     // Middle of horizontal
            minx = 2 * miny;
            break;
        case 5:     // Right of horizontal
            minx = (1.0 - fx);
            break;
        }
    
    double value = 2.0 * std::min( minx, miny );
    return value;
}

dot_description desc_herringbonetile2( dot_herringbonetile2, "herringbone tile2" );

/******************************************************************************/

double dot_windmilltile( double x, double y, int64_t seed, const screen_options &opt )
{
    x /= 3.0;
    y /= 3.0;

    double iy = floor(y);
    double ix = floor(x);
    double fy = y - iy;
    double fx = x - ix;

/*
divided by thirds
center is square
offset 1x2 tiles on outside
    112
    452
    433

There has to be an easier way to write this...
    assign 9->5, reduce repeated code?
*/

    if (fx < (1.0/3.0))
        {
        if (fy < (1.0/3.0))
            {   // 1 horizontal
            fx *= (3.0/2.0);
            fy *= 3.0;
            }
        else
            {   // 4 vertical
            fx *= 3.0;
            fy -= (1.0/3.0);
            fy *= (3.0/2.0);
            }
        }
    else if (fx < (2.0/3.0))
        {
        if (fy < (1.0/3.0))
            {   // 1 top horizontal
            fx *= (3.0/2.0);
            fy *= 3.0;
            }
        else if (fy < (2.0/3.0))
            {   // 5 center
            fx -= (1.0/3.0);
            fy -= (1.0/3.0);
            fx *= 3.0;
            fy *= 3.0;
            }
        else
            {   // 3 bottom horizontal
            fx -= (1.0/3.0);
            fx *= (3.0/2.0);
            fy -= (2.0/3.0);
            fy *= 3.0;
            }
        }
    else
        {
        if (fy >= (2.0/3.0))
            {   // 3 horizontal
            fx -= (1.0/3.0);
            fx *= (3.0/2.0);
            fy -= (2.0/3.0);
            fy *= 3.0;
            }
        else
            {   // 2 vertical
            fx -= (2.0/3.0);
            fx *= 3.0;
            fy *= (3.0/2.0);
            }
        }
    
    double minx = std::min( fx, (1.0-fx) );
    double miny = std::min( fy, (1.0-fy) );
    
    double value = 2.0 * std::min( minx, miny );
    return value;
}

dot_description desc_windmilltile( dot_windmilltile, "windmill tile" );

/******************************************************************************/

double dot_chevrontile( double x, double y, int64_t seed, const screen_options &opt )
{
    const double ratio = opt.params[0];
    
    double ix = floor(x);
    
    if (((int64_t)ix & 0x01) == 1)       // even/odd columns
        y += ratio * x;
    else
        y -= ratio * x;
    
    double iy = floor(y);
    ix = floor(x);
    double fy = y - iy;
    double fx = x - ix;
    
    double minx = std::min( fx, (1.0-fx) );
    double miny = std::min( fy, (1.0-fy) );
    
    double value = 2.0 * std::min( minx, miny );
    return value;
}

dot_description desc_chevrontile( dot_chevrontile, "chevron tile",
                            1,
                            { "incline" },
                            { 1.5 },
                            { 0.0 },
                            { 20.0 } );

/******************************************************************************/

// just keep changing the seed and size until you get what you like ;-)
double dot_subgrid( double x, double y, int64_t seed, const screen_options &opt )
{
    const int32_t dot_width = round( opt.params[0] );
    const int32_t dot_height = round( opt.params[1] );
    const size_t total = dot_width*dot_height;
    
    static std::vector<float> cached_values;
    static int64_t cached_seed = 0;
    
    if ( (cached_values.size() != total) || (cached_seed != seed) )
        {
        cached_values.resize(total);
        // use full range of values
        for (size_t i = 0; i < total; ++i)
            cached_values[i] = (float(i)+0.5) / float(total);
        // then rearrange them into pseudo random order
        seeded_random_shuffle( cached_values.begin(), cached_values.end(), seed );
        cached_seed = seed;
        }
    
    double iy = floor(y);
    double ix = floor(x);
    double fy = y - iy;
    double fx = x - ix;
    
    double sy = fy * dot_height;
    double sx = fx * dot_width;
    
    int64_t dy = floor(sy);
    int64_t dx = floor(sx);
    
    // the edges of FP precision can cause odd errors
    if (dy >= dot_height)
        dy -= dot_height;
    if (dx >= dot_width)
        dx -= dot_width;
    if (dy < 0.0)
        dy += dot_height;
    if (dx < 0.0)
        dx += dot_width;
    
    assert( dx >= 0.0 );
    assert( dx < dot_width );
    assert( dy >= 0.0 );
    assert( dy < dot_height );

    double value = (double) cached_values[ dy*dot_width + dx ];
    return value;
}

dot_description desc_subgrid( dot_subgrid, "random subgrid",
                                2,
                                { "width", "height" },
                                {  6.0,   8.0 },
                                {  1.0,   1.0 },
                                { 20.0,  20.0 } );

/******************************************************************************/

double dot_multispiral( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = opt.params[0];
    const double inv_atan2 = 1.0 / (2.0*M_PI);
    
    double iy = floor(y);
    double ix = floor(x);
    double fy = y - iy - 0.5;
    double fx = x - ix - 0.5;   // centered +-0.5
    
    fy *= 2.0;  // centered +-1.0
    fx *= 2.0;
    
    // atan2 returns +-M_PI, change to range 0..1
    double d = (inv_atan2 * atan2(fx, fy)) + 0.5;
    
    // create spiral and scale multiples
    double r = scale * (d + hypot(fx,fy));
    // convert to fraction
    double temp = (r - floor(r));
    
    double value = temp;
    return value;
}

dot_description desc_multispiral( dot_multispiral, "multispiral",
                                1,
                                { "arms" },
                                {  2.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// triangle dot function
double dot_triangle( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double value = 0.5 * (fx + fy);
    return value;
}

dot_description desc_triangle( dot_triangle, "triangle" );

/******************************************************************************/

double dot_triangle3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = opt.params[0] * (1.0/ 100.0);
    const double ix = floor(x);
    const double iy = floor(y);
    double fx = (x - ix);
    double fy = (y - iy);
    
    double td = fx + fy;            // triangle distance 0..2
    double rd = (1.0-0.5)-fx+fy;    // reverse diagonal, centered around zero -1..+1

    double value = (0.5) * (td + scale*rd - 0.5*scale);
    
#if 0
// sort of normalizes, but ends up messier than the triangle distribution
    value = value - 0.5;
    bool negative = value < 0.0;
    value = pow(fabs(value), 0.8);
    if (negative)
        value = -value;
    value += 0.5;
#endif
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_triangle3( dot_triangle3, "triangle wedge",
                                1,
                                { "bias scale" },
                                {  15.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// random direction triangle dot function
double dot_trianglerandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t bits = hashXY( (int64_t)ix, (int64_t)iy, seed );
    
    if ((bits & 0x08) != 0)
        fx = 1.0 - fx;
    
    if ((bits & 0x80) != 0)
        fy = 1.0 - fy;
    
    double value = 0.5 * (fx + fy);
    return value;
}

dot_description desc_trianglerandom( dot_trianglerandom, "triangle random" );

/******************************************************************************/

// triangle with some tweaks^2
double dot_corners( double x, double y, int64_t seed, const screen_options &opt )
{
    const double noise = (1.0 / 100.0) * opt.params[0];
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    bool invert = (fx + fy) < 1.0;
    
    if (invert)
        {
        fx = 1.0 - fx;
        fy = 1.0 - fy;
        }

    fx *= fx;
    fy *= fy;

    double value = 0.34 + 0.35 * (fx + fy);
    
    // add noise for grain/randomness
    if (noise > 0.0)
        {
        value += noise * hashXY_float( x, y, seed );
        }
    
    if (invert)
        value = 1.0 - value;
    
    value = std::min(1.0,std::max(0.0,value));
    return value;
}

dot_description desc_corners( dot_corners, "corners",
                                1,
                                { "Noise" },
                                {   4.0 },
                                {   0.0 },
                                {  50.0 } );

/******************************************************************************/

// triangle with some tweaks^3
double dot_corners2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double noise = (1.0 / 100.0) * opt.params[0];
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    bool invert = (fx + fy) < 1.0;
    
    if (invert)
        {
        fx = 1.0 - fx;
        fy = 1.0 - fy;
        }

    fx *= fx * fx;
    fy *= fy * fy;

    double value = 0.45 + 0.3 * (fx + fy);
    
    // add noise for grain/randomness
    if (noise > 0.0)
        {
        value += noise * hashXY_float( x, y, seed );
        }
    
    if (invert)
        value = 1.0 - value;
    
    value = std::min(1.0,std::max(0.0,value));
    return value;
}

dot_description desc_corners2( dot_corners2, "corners2",
                                1,
                                { "Noise" },
                                {   4.0 },
                                {   0.0 },
                                {  50.0 } );

/******************************************************************************/

// TODO - find a general expression for the weights, make an arbitrary power version
// triangle with some tweaks^4
double dot_corners3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double noise = (1.0 / 100.0) * opt.params[0];
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    bool invert = (fx + fy) < 1.0;
    
    if (invert)
        {
        fx = 1.0 - fx;
        fy = 1.0 - fy;
        }

    fx *= fx;
    fy *= fy;

    fx *= fx;
    fy *= fy;
    
    double value = 0.47 + 0.3 * (fx + fy);
    
    // add noise for grain/randomness
    if (noise > 0.0)
        {
        value += noise * hashXY_float( x, y, seed );
        }
    
    if (invert)
        value = 1.0 - value;
    
    value = std::min(1.0,std::max(0.0,value));
    return value;
}

dot_description desc_corners3( dot_corners3, "corners3",
                                1,
                                { "Noise" },
                                {   4.0 },
                                {   0.0 },
                                {  50.0 } );

/******************************************************************************/

// incremental dot function, aka color chart, dots look like wedges
// correct bias depends on the resolution of the screen - which we don't know here
double dot_wedge( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scalex = opt.params[0] * (1.0/ 100.0);
    const double scaley = 1.0 - scalex;
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    double value = scaley * fy + scalex * fx;
    return value;
}

dot_description desc_wedge( dot_wedge, "wedge",
                                1,
                                { "bias scale" },
                                {  20.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// random 90 degree rotations of wedge
double dot_cuneiform( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scalex = opt.params[0] * (1.0/ 100.0);
    const double scaley = 1.0 - scalex;
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    int64_t cell = hashXY( ix, iy, seed );
    double fx = x - ix;
    double fy = y - iy;

    // random rotations, based on grid position
    if ((cell & 0x80) != 0)
        std::swap(fx,fy);
    
    if ((cell & 0x08) != 0)
        fx = 1.0 - fx;
    
    if ((cell & 0x800) != 0)
        fy = 1.0 - fy;
    
    double value = scaley * fy + scalex * fx;
    return value;
}

dot_description desc_cuneiform( dot_cuneiform, "cuneiform",
                                1,
                                { "bias scale" },
                                {  20.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// alternating vertical and horizontal wedges
double dot_zigzag( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scalex = opt.params[0] * (1.0/ 100.0);
    const double scaley = 1.0 - scalex;
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;

    // checkerboard pattern alternating vertical and horizontal
    if ( ((ix^iy)&0x01) != 0 )
        std::swap(fx,fy);

    double value = scaley * fy + scalex * fx;
    return value;
}

dot_description desc_zigzag( dot_zigzag, "zigzag",
                                1,
                                { "bias scale" },
                                {  20.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// wave like
double dot_wave( double x, double y, int64_t seed, const screen_options &opt )
{
    const double power = opt.params[0];   // 0.1 is really flat, 1.0 is normal, 10.0 is sort of reverse peaked
    int64_t ix = floor(x);
    double fx = x - ix;
    double displace = fabs( sin( y * M_PI ) );
    if (power != 1.0)
        displace = pow( displace, power );
    double value = displace - fx;
    if (value < 0.0)
        value = 1.0 + value;
    return value;
}

dot_description desc_wave( dot_wave, "wave",
                                1,
                                { "gamma" },
                                {  1.0 },
                                {  0.1 },
                                { 10.0 } );

/******************************************************************************/

// torus dot function - from old PostScript code
double dot_torus( double x, double y, int64_t seed, const screen_options &opt )
{
    double sx = cos( x * (2*M_PI) );
    double sy = cos( y * (2*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    value = 0.5 + 0.5 * cos( (3*M_PI) * value );
    return value;
}

dot_description desc_torus( dot_torus, "torus" );

/******************************************************************************/

// torus dot function, but cell localized
double dot_torus2( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double value = hypot(fx,fy);
    value = sin( (1.75*M_PI) * value + 0.25*M_PI );
    value = 0.5 + 0.5 * value;
    return 1.0 - value;
}

dot_description desc_torus2( dot_torus2, "torus2" );

/******************************************************************************/

// torus dot function, but cell localized
double dot_torus3( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double value = hypot(fx,fy);
    value = cos( (1.75*M_PI) * value + 0.25*M_PI );
    value = 0.5 + 0.5 * value;
    return 1.0 - value;
}

dot_description desc_torus3( dot_torus3, "torus3" );

/******************************************************************************/

// torus dot function, inverted in checkboard pattern
double dot_checkers2( double x, double y, int64_t seed, const screen_options &opt )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double value = hypot(fx,fy);
    value = cos( (1.75*M_PI) * value + 0.25*M_PI );
    value = 0.5 + 0.5 * value;
    
    if (((ix^iy)&0x01) == 0)
        return value;
    else
        return 1.0 - value;
}

dot_description desc_checkers2( dot_checkers2, "checkers2" );

/******************************************************************************/

// sorta torus dot function, but cell localized
double dot_bulllseye( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    fx = (2.0*fx) - 1.0;
    fy = (2.0*fy) - 1.0;
    double value = hypot(fx,fy);
    value = sin( (2.0*M_PI) * value );
    value = fabs(value);
    return 1.0 - value;
}

dot_description desc_bulllseye( dot_bulllseye, "bulllseye" );

/******************************************************************************/

// overlapping rings
double dot_ringsoverlap( double x, double y, int64_t seed, const screen_options &opt )
{
    const double norm = M_SQRT1_2;  // 1.0 / sqrt(2.0);
    const double top_limit = opt.params[1];
    const double rings = opt.params[0] / top_limit;
    
    x *= 0.5;
    y *= 0.5;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    if (((int64_t)iy & 0x01) == 0)
        fy = 1.0 - fy;
    
    if (((int64_t)ix & 0x01) == 0)
        fx = 1.0 - fx;
    
    double value = 0.0;
    double v1 = norm * hypot(fx,fy);
    double v2 = norm * hypot(1.0-fx,1.0-fy);
    
    if (v1 <= top_limit)
        value = v1;
    else
        value = v2;
    
    double wasted;
    value = modf(value*rings,&wasted);
    
    return 1.0 - value;
}

dot_description desc_ringsoverlap( dot_ringsoverlap, "rings overlapping",
                                2,
                                { "ring count", "top limit" },
                                {  5.0,  0.6, },
                                {  1.0,  0.1,},
                                { 20.0,  1.0 } );

/******************************************************************************/

// multi lines - simulating a manga technique of overlaying lines at many angles
double dot_multiline( double x, double y, int64_t seed, const screen_options &opt )
{
    const std::array<float,6> angleList = { 30, -30, 60, -60, 15, -15 };
    const size_t angle_count = angleList.size();

    static double cosList[ angle_count ];   // cache, because sincos was taking most of the time!
    static double sinList[ angle_count ];
    static bool initialized = false;
    
    if (!initialized)
        {
        for (size_t i=0; i < angle_count; ++i)
            {
            double angle = angleList[i] * (M_PI/180.0);
            double ca = cos(angle);
            double sa = sin(angle);
            cosList[i] = ca;
            sinList[i] = sa;
            }
        initialized = true;
        }
    
    const double coverage = opt.params[0];
    
    double value = 1.0;
    for (size_t i=0; i < angle_count; ++i)
        {
#if 1
        double CA = cosList[i];
        double SA = sinList[i];
#else
        double angle = angleList[i] * (M_PI/180.0);
        double CA = cos(angle);
        double SA = sin(angle);
#endif
        double rotx =  CA * x + SA * y;
        
        double ix = floor(rotx);
        double fx = rotx - ix;
        
        double tmp = 0.0;
        if (fx < coverage)
            {
            tmp = double(i+1) / double(angle_count+1);
            value = std::min( tmp, value );
            }
        }
    
    return 1.0 - value;
}

dot_description desc_multiline( dot_multiline, "multiline",
                                1,
                                { "line coverage" },
                                { 0.38 },
                                { 0.01 },
                                { 0.99 } );

/******************************************************************************/

// multi lines - simulating a manga technique of overlaying lines at many angles
double dot_multiline2( double x, double y, int64_t seed, const screen_options &opt )
{
    const std::array<float,12> angleList = { 30, -30, 60, -60, 45, -45, 22, -22, 15, -15, 0, 90 };
    const size_t angle_count = angleList.size();

    static double cosList[ angle_count ];   // cache, because sincos was taking most of the time!
    static double sinList[ angle_count ];
    static bool initialized = false;
    
    if (!initialized)
        {
        for (size_t i=0; i < angle_count; ++i)
            {
            double angle = angleList[i] * (M_PI/180.0);
            double ca = cos(angle);
            double sa = sin(angle);
            cosList[i] = ca;
            sinList[i] = sa;
            }
        initialized = true;
        }
    
    const double coverage = opt.params[0];
    
    double value = 1.0;
    for (size_t i=0; i < angle_count; ++i)
        {
#if 1
        double CA = cosList[i];
        double SA = sinList[i];
#else
        double angle = angleList[i] * (M_PI/180.0);
        double CA = cos(angle);
        double SA = sin(angle);
#endif
        double rotx =  CA * x + SA * y;
        
        double ix = floor(rotx);
        double fx = rotx - ix;
        
        if (fx < coverage)
            {
            double tmp = double(i+1) / double(angle_count+1);
            value = std::min( tmp, value );
            }
        }
    
    return 1.0 - value;
}

dot_description desc_multiline2( dot_multiline2, "multiline2",
                                1,
                                { "line coverage" },
                                { 0.29 },
                                { 0.01 },
                                { 0.99 } );

/******************************************************************************/

// multi lines - simulating a manga/hatching technique of overlaying lines at many angles
// plus filling in more density along the same angles for darker regions
// draw all angles first, then fill in gaps
double dot_multiline3( double x, double y, int64_t seed, const screen_options &opt )
{
    const std::array<float,8> angleList = { 16, -75, 60, -31, 75, -17, 23, -68 };   // off just a little from square grids
    const size_t angle_count = angleList.size();

    static double cosList[ angle_count ];   // cache, because sincos was taking most of the time!
    static double sinList[ angle_count ];
    static bool initialized = false;
    
    if (!initialized)
        {
        for (size_t i=0; i < angle_count; ++i)
            {
            double angle = angleList[i] * (M_PI/180.0);
            double ca = cos(angle);
            double sa = sin(angle);
            cosList[i] = ca;
            sinList[i] = sa;
            }
        initialized = true;
        }
    
    const size_t divisions = opt.params[1];
    const double subdivision = 1.0 / divisions;
    const double coverage = opt.params[0] * subdivision;
    const double denom = double(divisions*angle_count+1);
    
    double value = 1.0;
    for (size_t i=0; i < angle_count; ++i)
        {
#if 1
        double CA = cosList[i];
        double SA = sinList[i];
#else
        double angle = angleList[i] * (M_PI/180.0);
        double CA = cos(angle);
        double SA = sin(angle);
#endif
        double rotx =  CA * x + SA * y;
        
        double ix = floor(rotx);
        double fx = rotx - ix;
        
        for (size_t j = 0; fx >= 0.0 && j < divisions; ++j)
            {
            if (fx < coverage)
                {
                double tmp = double(j*angle_count+i+1) / denom; // could this be cached?  Not taking much time.
                value = std::min( tmp, value );
                }
            fx -= subdivision;
            }
        }

    return 1.0 - value;
}

dot_description desc_multiline3( dot_multiline3, "multiline3",
                                2,
                                { "line coverage", "subdivisions"},
                                { 0.40,   5 },
                                { 0.01,   1 },
                                { 0.99,  10 } );

/******************************************************************************/

// multi lines dashes - simulating a manga technique of overlaying lines at many angles
double dot_multilinedash( double x, double y, int64_t seed, const screen_options &opt )
{
    const std::array<float,12> angleList = { 30, -30, 60, -60, 45, -45, 22, -22, 15, -15, 0, 90 };
    const size_t angle_count = angleList.size();

    static double cosList[ angle_count ];   // cache, because sincos was taking most of the time!
    static double sinList[ angle_count ];
    static bool initialized = false;
    
    if (!initialized)
        {
        for (size_t i=0; i < angle_count; ++i)
            {
            double angle = angleList[i] * (M_PI/180.0);
            double ca = cos(angle);
            double sa = sin(angle);
            cosList[i] = ca;
            sinList[i] = sa;
            }
        initialized = true;
        }
    
    const double dash_count = opt.params[2];
    const double line_width = opt.params[0] * dash_count;
    const double dash_coverage = opt.params[1];
    
    double value = 1.0;
    for (size_t i=0; i < angle_count; ++i)
        {
#if 1
        double CA = cosList[i];
        double SA = sinList[i];
#else
        double angle = angleList[i] * (M_PI/180.0);
        double CA = cos(angle);
        double SA = sin(angle);
#endif
        double rotx = CA * x - SA * y;
        double roty = SA * x + CA * y;
        
        roty *= dash_count;
        
        double ix = floor(rotx);
        double iy = floor(roty);
        double fx = rotx - ix;
        double fy = roty - iy;
        
        fx *= dash_count;
        
        if (fx < line_width && fy < dash_coverage)
            {
            double tmp = double(i+1) / double(angle_count+1); // could this be cached?  Not taking much time.
            value = std::min( tmp, value );
            }
        }
    
    return 1.0 - value;
}

dot_description desc_multilinedash( dot_multilinedash, "multiline dash",
                                3,
                                { "line coverage", "dash coverage", "dash_count"},
                                { 0.45, 0.7, 2.0 },
                                { 0.01, 0.01, 1.0 },
                                { 0.99, 1.0, 100.0 } );

/******************************************************************************/

// multi lines dotted - simulating a manga technique of overlaying lines at many angles
double dot_multilinedots( double x, double y, int64_t seed, const screen_options &opt )
{
    const std::array<float,12> angleList = { 30, -30, 60, -60, 45, -45, 22, -22, 15, -15, 0, 90 };
    const size_t angle_count = angleList.size();

    static double cosList[ angle_count ];   // cache, because sincos was taking most of the time!
    static double sinList[ angle_count ];
    static bool initialized = false;
    
    if (!initialized)
        {
        for (size_t i=0; i < angle_count; ++i)
            {
            double angle = angleList[i] * (M_PI/180.0);
            double ca = cos(angle);
            double sa = sin(angle);
            cosList[i] = ca;
            sinList[i] = sa;
            }
        initialized = true;
        }
    
    const double dot_width = opt.params[0] / 2.0;
    const double dot_count = opt.params[1];
    
    double value = 1.0;
    for (size_t i=0; i < angle_count; ++i)
        {
#if 1
        double CA = cosList[i];
        double SA = sinList[i];
#else
        double angle = angleList[i] * (M_PI/180.0);
        double CA = cos(angle);
        double SA = sin(angle);
#endif
        double rotx = CA * x - SA * y;
        double roty = SA * x + CA * y;
        
        roty *= dot_count;
        
        double ix = floor(rotx);
        double iy = floor(roty);
        double fx = rotx - ix;
        double fy = roty - iy;
        
        // centered coords
        fx -= 0.5;
        fy -= 0.5;
        
        fx *= dot_count;
        
        double r = hypot(fx,fy);
        
        if (r < dot_width)
            {
            double tmp = double(i+1) / double(angle_count+1); // could this be cached?
            value = std::min( tmp, value );
            }
        }
    
    return 1.0 - value;
}

dot_description desc_multilinedots( dot_multilinedots, "multiline dots",
                                2,
                                { "dot coverage", "dot count" },
                                { 0.8, 2.5 },
                                { 0.01, 1.0 },
                                { 1.0, 100.0 } );

/******************************************************************************/

// scattered circles - similar to hatching/shading techniques
double dot_ringsscatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[0] / 20.0;

    double dist = Cell2DNearestDist( x, y, seed );
    double dist2 = Cell2DNearestDist( x, y, seed^extraHash1 );
    double dist3 = Cell2DNearestDist( x, y, seed^extraHash2 );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_ringsscatter( dot_ringsscatter, "rings scattered",
                                1,
                                { "size" },
                                {  6.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// scattered squares - similar to hatching/shading techniques
double dot_squarescatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[0] / 20.0;

    double dist = Cell2DNearestManhatten( x, y, seed );
    double dist2 = Cell2DNearestManhatten( x, y, seed^extraHash1 );
    double dist3 = Cell2DNearestManhatten( x, y, seed^extraHash2 );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_squarescatter( dot_squarescatter, "squares scattered",
                                1,
                                { "size" },
                                {  8.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// scattered triangles - similar to hatching/shading techniques
double dot_trianglescatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[0] / 20.0;

    double dist = 1.3 * Cell2DNearestTriangle( x, y, seed );
    double dist2 = 1.3 * Cell2DNearestTriangle( x, y, seed^extraHash1 );
    double dist3 = 1.3 * Cell2DNearestTriangle( x, y, seed^extraHash2 );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_trianglescatter( dot_trianglescatter, "triangles scattered",
                                1,
                                { "size" },
                                {  6.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// scattered hexagons - similar to hatching/shading techniques
double dot_hexagonscatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[0] / 20.0;

    double dist = 1.2 * Cell2DNearestHexagon( x, y, seed );
    double dist2 = 1.2 * Cell2DNearestHexagon( x, y, seed^extraHash1 );
    double dist3 = 1.2 * Cell2DNearestHexagon( x, y, seed^extraHash2 );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_hexagonscatter( dot_hexagonscatter, "hexagons scattered",
                                1,
                                { "size" },
                                {  6.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// scattered concave diamonds - similar to hatching/shading techniques
double dot_concavediamondscatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[0] / 20.0;

    const double factor = 0.7;
    double dist = factor * Cell2DNearestConcaveDiamond( x, y, seed );
    double dist2 = factor * Cell2DNearestConcaveDiamond( x, y, seed^extraHash1 );
    double dist3 = factor * Cell2DNearestConcaveDiamond( x, y, seed^extraHash2 );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.2 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_concavediamondscatter( dot_concavediamondscatter, "concave diamond scattered",
                                1,
                                { "size" },
                                {  4.0 },
                                {  1.0 },
                                { 20.0 } );

/******************************************************************************/

// scattered Ngons - similar to hatching/shading techniques
double dot_Ngonscatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[1] / 20.0;
    const size_t sides = round( opt.params[0] );
    const double adjustment = opt.params[2];
    const double scale = adjustment * ( (sides & 1) ? (1.4) : (1.2) );

    double dist = scale * Cell2DNearestNgon( x, y, seed, sides );
    double dist2 = scale * Cell2DNearestNgon( x, y, seed^extraHash1, sides );
    double dist3 = scale * Cell2DNearestNgon( x, y, seed^extraHash2, sides );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_Ngonscatter( dot_Ngonscatter, "Ngons scattered",
                                3,
                                { "sides", "size", "shadow adjustment",  },
                                {  5.0,   9.0,   1.0 },
                                {  3.0,   1.0,   0.1 },
                                { 20.0,  20.0,  10.0 } );

/******************************************************************************/

// scattered stars - similar to hatching/shading techniques
double dot_starscatter( double x, double y, int64_t seed, const screen_options &opt )
{
    const double radius = opt.params[1] / 20.0;
    const size_t sides = round( opt.params[0] );
    const double adjustment = opt.params[2];
    const double point = 1.0 / opt.params[3];
    const double scale = adjustment * 1.4;

    double dist = scale * Cell2DNearestStar( x, y, seed, sides, point );
    double dist2 = scale * Cell2DNearestStar( x, y, seed^extraHash1, sides, point );
    double dist3 = scale * Cell2DNearestStar( x, y, seed^extraHash2, sides, point );
    
    double value1 = fabs(dist-radius);
    double value2 = fabs(dist2-radius);
    double value3 = fabs(dist3-radius);
    
    double value = 2.0 * std::min(value1,std::min(value2,value3));
    
    value = std::min( 1.0, value);
    value = 1.0 - value;
    return value*value*value;
}

dot_description desc_starscatter( dot_starscatter, "stars scattered",
                                4,
                                { "sides", "size", "shadow adjustment", "point width" },
                                {  5.0,   5.0,   1.0,   1.0 },
                                {  3.0,   1.0,   0.1,   0.1 },
                                { 20.0,  20.0,  10.0,  10.0 } );

/******************************************************************************/

// stippled dots - simulating manual stippling with a round pen
// very slow
double dot_stipple( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[2]);
    const double radius = opt.params[0];
    const double variation = opt.params[1];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = Cell2DNearestDistAndID( x, y, seed_modify, point_id );
    
        if (dist <= radius)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    return 1.0 - value;
}

dot_description desc_stipple( dot_stipple, "stipple round",
                                3,
                                { "radius", "value variation", "dot count" },
                                { 0.33, 1.00, 10.0 },
                                { 0.01, 0.00,  4.0 },
                                { 0.99, 2.00, 25.0 } );

/******************************************************************************/

// stippled diamonds - simulating manual stippling with a diamond/square pen
// very slow
double dot_stipplediamond( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[2]);
    const double radius = opt.params[0];
    const double variation = opt.params[1];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = Cell2DNearestDistAndIDManhatten( x, y, seed_modify, point_id );
    
        if (dist <= radius)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    return 1.0 - value;
}

dot_description desc_stipplediamond( dot_stipplediamond, "stipple diamond",
                                3,
                                { "radius", "value variation", "dot count" },
                                { 0.45, 1.00, 10.0 },
                                { 0.01, 0.00,  4.0 },
                                { 0.99, 2.00, 25.0 } );

/******************************************************************************/

// stippled diamonds - simulating manual stippling with a diamond/square pen
// very slow
double dot_stippleconcavediamond( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[2]);
    const double radius = opt.params[0];
    const double variation = opt.params[1];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = 0.6 * Cell2DNearestDistAndIDConcaveDiamond( x, y, seed_modify, point_id );
    
        if (dist <= radius)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    return 1.0 - value;
}

dot_description desc_stippleconcavediamond( dot_stippleconcavediamond, "stipple concave diamond",
                                3,
                                { "radius", "value variation", "dot count" },
                                { 0.25, 1.30, 10.0 },
                                { 0.01, 0.00,  4.0 },
                                { 0.99, 2.00, 25.0 } );

/******************************************************************************/

// stippled rings - simulating buildup of manual shading with a small pen
// very slow
double dot_stipplering( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[3]);
    const double radius = opt.params[0];
    const double thickness = 0.5 * opt.params[1];
    const double variation = opt.params[2];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = Cell2DNearestDistAndID( x, y, seed_modify, point_id );
        
        dist = fabs(dist-radius);
    
        if (dist <= thickness)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stipplering( dot_stipplering, "stipple rings",
                                4,
                                { "radius", "ring thickness", "value variation", "dot count" },
                                { 0.33,  0.17,  1.50,  10.0 },
                                { 0.01,  0.01,  0.00,   4.0 },
                                { 2.00,  1.99,  2.00,  25.0 } );

/******************************************************************************/

// stippled squares - simulating buildup of manual shading with a small pen
// very slow
double dot_stipplesquares( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[3]);
    const double radius = opt.params[0];
    const double thickness = 0.5 * opt.params[1];
    const double variation = opt.params[2];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = Cell2DNearestDistAndIDManhatten( x, y, seed_modify, point_id );
        
        dist = fabs(dist-radius);
    
        if (dist <= thickness)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stipplesquares( dot_stipplesquares, "stipple squares",
                                4,
                                { "size", "square thickness", "value variation", "dot count" },
                                { 0.40,  0.20,  1.50,  10.0 },
                                { 0.01,  0.01,  0.00,   4.0 },
                                { 2.00,  1.99,  2.00,  25.0 } );

/******************************************************************************/

// stippled triangles - simulating buildup of manual shading with a small pen
// very slow
double dot_stippletriangles( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[3]);
    const double radius = opt.params[0];
    const double thickness = 0.5 * opt.params[1];
    const double variation = opt.params[2];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = 1.4 * Cell2DNearestDistAndIDTriangle( x, y, seed_modify, point_id );
        
        dist = fabs(dist-radius);
    
        if (dist <= thickness)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stippletriangles( dot_stippletriangles, "stipple triangles",
                                4,
                                { "size", "triangle thickness", "value variation", "dot count" },
                                { 0.40,  0.18,  1.20,  10.0 },
                                { 0.01,  0.01,  0.00,   4.0 },
                                { 2.00,  1.99,  2.00,  25.0 } );

/******************************************************************************/

// stippled hexagons - simulating buildup of manual shading with a small pen
// very slow
double dot_stipplehexagons( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t dot_count = round(opt.params[3]);
    const double radius = opt.params[0];
    const double thickness = 0.5 * opt.params[1];
    const double variation = opt.params[2];
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = 1.2 * Cell2DNearestDistAndIDHexagon( x, y, seed_modify, point_id );
        
        dist = fabs(dist-radius);
    
        if (dist <= thickness)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stipplehexagons( dot_stipplehexagons, "stipple hexagons",
                                4,
                                { "size", "hex thickness", "value variation", "dot count" },
                                { 0.40,  0.18,  1.20,  10.0 },
                                { 0.01,  0.01,  0.00,   4.0 },
                                { 2.00,  1.99,  2.00,  25.0 } );

/******************************************************************************/

// stippled Ngons - simulating buildup of manual shading with a small pen
// very slow
double dot_stippleNgons( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round( opt.params[0] );
    const double radius = opt.params[1];
    const double thickness = 0.5 * opt.params[2];
    const double variation = opt.params[3];
    const size_t dot_count = round(opt.params[4]);
    const double adjustment = ( (sides & 1) ? (1.4) : (1.2) );
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = adjustment * Cell2DNearestDistAndIDNgon( x, y, seed_modify, point_id, sides );
        
        dist = fabs(dist-radius);
    
        if (dist <= thickness)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stippleNgons( dot_stippleNgons, "stipple Ngons",
                                5,
                                { "sides", "size", "thickness", "value variation", "dot count" },
                                {  5.0,  0.45,  0.22,  1.20,  10.0 },
                                {  3.0,  0.01,  0.01,  0.00,   4.0 },
                                { 20.0,  2.00,  1.99,  2.00,  25.0 } );

/******************************************************************************/

// stippled stars - simulating buildup of manual shading with a small pen
// very slow
double dot_stipplestars( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round( opt.params[0] );
    const double point_width = 1.0 / opt.params[1];
    const double radius = opt.params[2];
    const double variation = opt.params[3];
    const size_t dot_count = round(opt.params[4]);
    const double adjustment = 1.8;
    const double scale = 1.0 / double(dot_count);
    const uint64_t useed(seed);
    
    double value = 1.0;
    for (size_t i = 1; i <= dot_count; ++i)
        {
        int64_t point_id = 0;
        int64_t seed_modify = rotate_left(useed,i) + i;
        double dist = adjustment * Cell2DNearestDistAndIDStar( x, y, seed_modify, point_id, sides, point_width );
    
        if (dist <= radius)
            {
            double offset = variation*(hash_to_double(point_id) - 0.5);
            double tmp = (i + offset - 0.25) * scale;
            value = std::min( tmp, value );
            }
        }
    
    value = std::max( 0.0, std::min( 1.0, value ) ); // because offset could exceed unit range
    value = 1.0 - value;
    return value;
}

dot_description desc_stipplestars( dot_stipplestars, "stipple stars",
                                5,
                                { "sides", "point width", "size", "value variation", "dot count" },
                                {  5.0,   1.0,   0.43,  1.20,  10.0 },
                                {  3.0,   0.1,   0.01,  0.00,   4.0 },
                                { 20.0,  10.0,   2.00,  2.00,  25.0 } );

/******************************************************************************/

// jittered dots
double dot_jitteredDot( double x, double y, int64_t seed, const screen_options &opt )
{
    int64_t point_id;
    double dist = Cell2DNearestDistAndID( x, y, seed, point_id );

#if 0
// TODO - doing this correctly requires knowing nearby distances to avoid showing cell boundaries
// even worse if enlarging (change to 0.5)
// right now it leaves blank spaces in shadows (shrink)
// or incomplete dots in light to midtones (enlarge)
    double temp = hash_to_double( point_id );
const double shrink_chance = 0.1;
    if ( temp < shrink_chance )
        dist *= 2.0;
#endif
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredDot( dot_jitteredDot, "jittered round" );

/******************************************************************************/

// difference of distances, gives Voronoi/Dirichlet edges
double dot_cellwalls( double x, double y, int64_t seed, const screen_options &opt )
{
    double dist = Cell2DCellWalls( x, y, seed, 1 );
    dist = 1.05 * sqrt(dist);
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_cellwalls( dot_cellwalls, "cell walls" );

/******************************************************************************/

// Conglomerate: mix of thresholded bicubic noise and jittered small dots
double dot_conglomerate( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.1 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = NoiseBicubic( x*large_scale, y*large_scale, seed );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate( dot_conglomerate, "rough conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {   8.0,   0.80 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// Conglomerate: mix of thresholded large dots and jittered small dots
double dot_conglomerate2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.01 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = 1.0 - Cell2DNearestDist( x*large_scale, y*large_scale, seed );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate2( dot_conglomerate2, "round conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {  15.0,   0.80 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// Conglomerate: mix of thresholded triangles and jittered small dots
double dot_conglomerate3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.01 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = 1.0 - Cell2DNearestTriangle( -y*large_scale, x*large_scale, seed );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate3( dot_conglomerate3, "triangle conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {  20.0,   0.85 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// Conglomerate: mix of thresholded diamonds and jittered small dots
double dot_conglomerate4( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.01 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = 1.0 - Cell2DNearestManhatten( x*large_scale, y*large_scale, seed );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate4( dot_conglomerate4, "diamond conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {  15.0,   0.80 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// Conglomerate: mix of thresholded stars and jittered small dots
double dot_conglomerate5( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.01 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = 1.0 - Cell2DNearestStar( y*large_scale, x*large_scale, seed, 5, 1.0 );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate5( dot_conglomerate5, "star conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {  20.0,   0.85 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// Conglomerate: mix of thresholded hexagons and jittered small dots
double dot_conglomerate6( double x, double y, int64_t seed, const screen_options &opt )
{
    const double large_scale = 0.01 * opt.params[0];
    const double large_threshold = opt.params[1];
    const double large_value_scale = 1.0 - large_threshold;

    double noise = 1.0 - Cell2DNearestHexagon( x*large_scale, y*large_scale, seed );

    double value;
    if (noise >= large_threshold)
        {
        // do the large stone
        value = (noise - large_threshold) / large_value_scale;
        }
    else
        {
        // else we go with small jittered dots
        value = Cell2DNearestDist( x, y, seed );
        }
    
    value = std::min( 1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_conglomerate6( dot_conglomerate6, "hexagon conglomerate",
                                2,
                                { "large scale", "large threshold" },
                                {  15.0,   0.85 },
                                {   1.0,   0.10 },
                                { 50.00,   0.99 } );

/******************************************************************************/

// jittered diamonds
double dot_jitteredDiamond( double x, double y, int64_t seed, const screen_options &opt )
{
    double dist = 0.7 * Cell2DNearestManhatten( x, y, seed );   // increase area covered, because diamonds cover less than circles
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredDiamond( dot_jitteredDiamond, "jittered diamond" );

/******************************************************************************/

double dot_jitteredConcaveDiamond( double x, double y, int64_t seed, const screen_options &opt )
{
    double dist = 0.4 * Cell2DNearestConcaveDiamond( x, y, seed );
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredConcaveDiamond( dot_jitteredConcaveDiamond, "jittered Concave Diamond" );

/******************************************************************************/

// jittered hexagons
double dot_jitteredHexagon( double x, double y, int64_t seed, const screen_options &opt )
{
    double dist = 1.2 * Cell2DNearestHexagon( x, y, seed );
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredHexagon( dot_jitteredHexagon, "jittered hexagon" );

/******************************************************************************/

// jittered triangles
double dot_jitteredTriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    double dist = 1.4 * Cell2DNearestTriangle( x, y, seed );
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredTriangle( dot_jitteredTriangle, "jittered triangle" );

/******************************************************************************/

// jittered Ngon
double dot_jitteredNgon( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round( opt.params[0] );
    const double adjustment = opt.params[1];
    const double scale = adjustment * ( (sides & 1) ? (1.4) : (1.2) );

    double dist = scale * Cell2DNearestNgon( x, y, seed, sides );
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredNgon( dot_jitteredNgon, "jittered Ngon",
                                2,
                                { "sides", "shadow adjustment" },
                                {  5.0,   1.0 },
                                {  3.0,   0.1 },
                                { 20.0,  10.0 } );

/******************************************************************************/

// jittered Star
double dot_jitteredStar( double x, double y, int64_t seed, const screen_options &opt )
{
    const size_t sides = round( opt.params[0] );
    const double adjustment = opt.params[1];
    const double point = 1.0 / opt.params[2];
    const double scale = adjustment * 1.4;

    double dist = scale * Cell2DNearestStar( x, y, seed, sides, point );
    
    double value = std::min( 1.0, std::max( 0.0, dist ));
    return 1.0 - value;
}

dot_description desc_jitteredStar( dot_jitteredStar, "jittered Star",
                                3,
                                { "sides", "shadow adjustment", "point width" },
                                {  5.0,   1.0,   1.0 },
                                {  3.0,   0.1,   0.1 },
                                { 20.0,  10.0,  10.0 } );

/******************************************************************************/

// distorted square dot
double dot_cloth( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -4;
    const int32_t octave_high = 1;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash1 );

    x += distort_scale * dx;
    y += distort_scale * dy;

    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    double value = sqrt(std::min( fx, fy ));
    return value;
}

dot_description desc_cloth( dot_cloth, "cloth",
                                1,
                                { "distortion" },
                                { 8.0 },
                                { 0.1 },
                                { 50.0 } );

/******************************************************************************/

// independently distorted square dot, inverted, very low frequencies
double dot_warpweft( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_x = opt.params[0];
    double distort_y = opt.params[1];
    const int32_t octave_low = -5;
    const int32_t octave_high = -1;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash3 );
    
    x += distort_x * dx;
    y += distort_y * dy;
    
    double ix = floor(x);
    double iy = floor(y);
    double vx = x - ix;
    double vy = y - iy;
    
    double value = std::min(vx,vy);
    return 1.0 - sqrt( value );
}

dot_description desc_warpweft( dot_warpweft, "warp and weft",
                                2,
                                { "horizontal distortion", "vertical distortion" },
                                {  8.0,   2.0 },
                                {  0.1,   1.0 },
                                { 20.0,  20.0 } );

/******************************************************************************/

// distorted line, low frequencies
double dot_warpedline( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -4;
    const int32_t octave_high = 0;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    x += distort_scale * dx;
    double ix = floor(x);
    double value = x - ix;
    return value;
}

dot_description desc_warpedline( dot_warpedline, "warped line",
                                1,
                                { "distortion" },
                                { 10.0 },
                                { 0.1 },
                                { 50.0 } );

/******************************************************************************/

// distorted line, high frequencies
double dot_jaggedline( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
    x += distort_scale * dx;
    double ix = floor(x);
    double value = x - ix;
    return value;
}

dot_description desc_jaggedline( dot_jaggedline, "jagged line",
                                1,
                                { "distortion" },
                                { 1.5 },
                                { 0.1 },
                                { 50.0 } );

/******************************************************************************/

// folded chaos
double dot_mokumegane( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -2;
    const int32_t octave_high = 1;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    
    double value = cos( distort_scale*M_PI*dx );
    
    value *= value;
    
    return value;
}

dot_description desc_mokumegane( dot_mokumegane, "mokumegane",
                                1,
                                { "banding" },
                                { 10.0 },
                                { 1.0 },
                                { 100.0 } );

/******************************************************************************/

// folded chaos
double dot_marble( double x, double y, int64_t seed, const screen_options &opt )
{
    const int32_t octave_low = -1;
    const int32_t octave_high = 4;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    dx = fabs( 2.0*dx - 1.0 );
    dx = fabs( 2.0*dx - 1.0 );
    dx = fabs( 2.0*dx - 1.0 );
    return std::min( 1.0, std::max( 0.0, dx ) );
}

dot_description desc_marble( dot_marble, "marble" );

/******************************************************************************/

// folded chaos
double dot_granite( double x, double y, int64_t seed, const screen_options &opt )
{
    const int32_t octave_low = 2;
    const int32_t octave_high = 5;
    
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    dx = fabs( 2.0*dx - 1.0 );
    dx = fabs( 2.0*dx - 1.0 );
    return std::min( 1.0, std::max( 0.0, dx ) );
}

dot_description desc_granite( dot_granite, "granite" );

/******************************************************************************/

// round with noise added
double dot_roundgrain1( double x, double y, int64_t seed, const screen_options &opt )
{
    double size_scale = opt.params[0];
    double value_scale = 0.01 * opt.params[1];
    
    // get -1...+1 noise (or close to it)
    double dv = 2.0 * (NoiseBicubic( x*size_scale, y*size_scale, seed ) - 0.5);

    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    
    value += value_scale * dv;
    value = std::min( 1.0, std::max( 0.0, value ) );
    return value;
}

dot_description desc_roundgrain1( dot_roundgrain1, "round grain additive",
                                2,
                                { "size", "amplitude" },
                                {   6.0,    8.0 },
                                {  0.01,    0.0 },
                                { 100.0,  200.0 } );

/******************************************************************************/

// round with noise multiplied
double dot_roundgrain2( double x, double y, int64_t seed, const screen_options &opt )
{
    double size_scale = opt.params[0];
    double value_scale = 0.01 * opt.params[1];
    
    // get 0..1 noise (or close to it)
    double dv = NoiseBicubic( x*size_scale, y*size_scale, seed );

    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    
    value *= (1.0 - (value_scale * dv) + (value_scale * 0.25));
    value = std::min( 1.0, std::max( 0.0, value ) );
    return value;
}

dot_description desc_roundgrain2( dot_roundgrain2, "round grain multiplicative",
                                2,
                                { "size", "amplitude" },
                                {   6.0,   17.0 },
                                {  0.01,    0.0 },
                                { 100.0,  100.0 } );

/******************************************************************************/

// distorted round dot grid
double dot_rounddistorted( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -4;
    const int32_t octave_high = 1;
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed );
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash2 );

    x += distort_scale * dx;
    y += distort_scale * dy;

    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    return value;
}

dot_description desc_rounddistorted( dot_rounddistorted, "round distorted",
                                1,
                                { "distortion" },
                                { 10.0 },
                                {  0.1 },
                                { 50.0 } );

/******************************************************************************/

// distorted round dot grid, lower frequency
double dot_rounddistorted2( double x, double y, int64_t seed, const screen_options &opt )
{
    double distort_scale = opt.params[0];
    const int32_t octave_low = -4;
    const int32_t octave_high = -3;
    double dx = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash1 );
    double dy = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash3 );

    x += distort_scale * dx;
    y += distort_scale * dy;

    double sx = cos( x * (2.0*M_PI) );
    double sy = cos( y * (2.0*M_PI) );
    double value = 0.5 + 0.25 * (sx+sy);
    return value;
}

dot_description desc_rounddistorted2( dot_rounddistorted2, "round distorted2",
                                1,
                                { "distortion" },
                                { 10.0 },
                                {  0.1 },
                                { 50.0 } );

/******************************************************************************/

// random angle areas, similar to random areas of hand hatching
double dot_randomangle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double cell_scale = 1.0/opt.params[0];
    const double angle_variation = opt.params[1]*M_PI/180.0;
    
    double cell = Cell2DNearestID( x*cell_scale, y*cell_scale, seed );

    double angle = angle_variation * cell;
    
    x = x * cos(angle) - y * sin(angle);

    double ix = floor(x);
    double value = x - ix;
    return value;
}

dot_description desc_randomangle( dot_randomangle, "random angles",
                                2,
                                { "block scale", "angle range" },
                                { 4.0, 180.0 },
                                { 0.5, 0.0 },
                                { 100.0, 180.0 } );

/******************************************************************************/

// jittered angle areas, similar to random areas of hand hatching
//  with variation in period and angle
double dot_hatched( double x, double y, int64_t seed, const screen_options &opt )
{
    const double cell_scale = 1.0/opt.params[0];
    const double angle_variation = opt.params[1]*M_PI/180.0;
    const double period_variation = opt.params[2] / 100.0;
    
    int64_t cell = Cell2DNearestHash( x*cell_scale, y*cell_scale, seed );
    int64_t cell2 = hash64(cell);
    
    double angle_rand = (cell >> (63-14)) / 16384.0;    // upper bits, signed result
    double period_rand = (cell2 >> (63-14)) / 16384.0;    // upper bits, signed result

#if 0
assert(angle_rand >= -1.0);
assert(angle_rand <= 1.0);
assert(period_rand >= -1.0);
assert(period_rand <= 1.0);
#endif

    double angle = angle_variation * angle_rand;
    
    x = x * cos(angle) - y * sin(angle);
    
    x *= (1.0 - period_variation * period_rand);

    double ix = floor(x);
    double value = x - ix;
    return value;
}

dot_description desc_hatched( dot_hatched, "hatched",
                            3,
                            { "block scale", "angle range", "thickness variation" },
                            {   6.0,  3.0,  15.0 },
                            {   0.5,  0.0,   0.0 },
                            { 100.0, 90.0, 100.0 } );

/******************************************************************************/

// thin lines with slowly varying angles
// TODO - need a better way to do this!
double dot_linetwisted( double x, double y, int64_t seed, const screen_options &opt )
{
    const int32_t octave_low = -4;
    const int32_t octave_high = -3;
    double angle_rand = ChaosBicubic( x, y, octave_low, octave_high, seed ^ extraHash2 );
    double t = 143.735 * angle_rand;
    double it = floor(t);
    double value = t - it;
    return value;
}

dot_description desc_linetwisted( dot_linetwisted, "twisted line" );

/******************************************************************************/

// random 45 degree segments that connect
double dot_diagonals( double x, double y, int64_t seed, const screen_options &opt )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t angle = hashXY( ix, iy, seed );

    if ((angle & 0x02) == 0)
        fx = 1.0 - fx;
    
    double d1 = fx + fy;
    double d2 = 2.0 - d1;
    
    d1 = fabs( d1 - 0.5 );
    d2 = fabs( d2 - 0.5 );
    
    double value = 2.0 * std::min( d1, d2 );
    return value;
}

dot_description desc_diagonals( dot_diagonals, "random diagonals" );

/******************************************************************************/

// random semicircle segments that connect
double dot_semicircles( double x, double y, int64_t seed, const screen_options &opt )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    double fx = x - ix;
    double fy = y - iy;
    
    int64_t angle = hashXY( ix, iy, seed );

    if ((angle & 0x02) == 0)
        fx = 1.0 - fx;

    double d1 = hypot(fx,fy);
    double d2 = hypot((1.0-fx), (1.0-fy));
    
    d1 = fabs( 2.0*d1 - 1.0 );
    d2 = fabs( 2.0*d2 - 1.0 );
    
    double value = 1.0 - std::min( d1, d2 );
    return value;
}

dot_description desc_semicircles( dot_semicircles, "random semicircles" );

/******************************************************************************/

// horizontal speed lines - similar to hand drawn (or rub down) manga patterns
double dot_speedline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double density = opt.params[0];
    const double density_variation = opt.params[1];
    const double start_density = 1.0 - (opt.params[2] / 100.0);
    const double start_variation = opt.params[3] / 100.0;

    int64_t iy = floor(y);
    int64_t yhash = hash64( iy, seed );
    double group_density = (hash_to_double(yhash) - 0.5) * density_variation;
    
    y *= (density + group_density);
    
    iy = floor(y);
    double fy = y - iy;
    int64_t yhash2 = hash64( iy, seed^extraHash3 );
    double group_start_vary = (hash_to_double(yhash2) - 0.5) * start_variation;
    
    fy *= (start_density + group_start_vary);

    double value = fy;
    return value;
}

dot_description desc_speedline( dot_speedline, "speed lines",
                            4,
                            { "line density", "line variation", "start point", "start variation" },
                            {  1.5,  1.0,  15.0,  20.0 },
                            {  0.2,  0.0,   0.0,   0.0 },
                            { 20.0, 20.0, 100.0, 100.0 } );

/******************************************************************************/

double dot_radialburst( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double start_density = 1.0 - (opt.params[1] / 100.0);
    const double start_variation = opt.params[2] / 100.0;
    const double start_ramp = opt.params[3];            // in halftone coordinate scale - this is bad
        // but what could it be relative to?
    
    double angle = (0.5 + atan2(y,x) / (2*M_PI));
    double radius = hypot(x,y);
    
    y = count * angle;

    double iy = floor(y);
    double fy = y - iy;

    int64_t yhash2 = hash64( (int64_t)iy, seed^extraHash3 );
    double start_vary = (hash_to_double(yhash2) - 0.5) * start_variation;
    
    fy *= (start_density + start_vary);
    
    if ((start_ramp > 0.0) && radius < start_ramp)
        fy *= (radius / start_ramp);

    double value = std::min( fy, start_density );
    return value;
}

dot_description desc_radialburst( dot_radialburst, "radial burst",
                            4,
                            { "line count", "start point", "start variation", "start ramp" },
                            {   100.0,   10.0,   10.0,   10.0 },
                            {     4.0,    0.0,    0.0,    0.0 },
                            { 10000.0,   99.0,  200.0,  500.0 } );

/******************************************************************************/

// radial speed lines - similar to hand drawn (or rub down) manga patterns
double dot_radialspeedline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double count = opt.params[0];
    const double line_variation = opt.params[1];
    const double start_density = 1.0 - (opt.params[2] / 100.0);
    const double start_variation = opt.params[3] / 100.0;
    const double start_ramp = opt.params[4];            // in halftone coordinate scale - this is bad
        // but what could it be relative to?
    const double swirl = 0.001 * opt.params[5];
    
    double radius = hypot(x,y);
    double angle = (radius * swirl) + (0.5 + atan2(y,x) / (2*M_PI));
    
    y = count * angle;
    
    int64_t iy = floor(y);
    int64_t yhash = hash64( iy, seed );
    double group_density = (hash_to_double(yhash) - 0.5) * line_variation;
    
    y = angle * (count + group_density);

    iy = floor(y);
    double fy = y - iy;

    int64_t yhash2 = hash64( iy, seed^extraHash3 );
    double start_vary = (hash_to_double(yhash2) - 0.5) * start_variation;
    
    fy *= (start_density + start_vary);
    
    if ((start_ramp > 0.0) && radius < start_ramp)
        fy *= (radius / start_ramp);

    double value = std::min( fy, start_density );
    return value;
}

// TODO - need to figure out reasonable parameter limits
// TODO - need fewer scum dots along edges of groups, need to quantize groups so we have full space for each
    // seems impossible to achieve near center (lines are too thin)
dot_description desc_radialspeedline( dot_radialspeedline, "radial speed lines",
                            6,
                            { "line count", "line variation", "start point", "start variation", "start ramp", "rotation" },
                            {     150,  50.0,  12.0,  25.0,  15.0,    0.0 },
                            {     4.0,   0.0,  0.00,   0.0,   0.0,  -50.0 },
                            { 10000.0, 200.0,  99.0, 200.0, 500.0,   50.0 } );

/******************************************************************************/

static
double subdivide_once( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);

    int64_t cell = hashXY( ix, iy, seed );

    if ((cell & 0x08) != 0)
        {
        x *= 2.0;
        y *= 2.0;
        seed += extraHash1;
        }
        
    ix = floor(x);
    iy = floor(y);
    int64_t cell2 = hashXY( ix, iy, seed );
    
    return dot(x,y,seed^cell2,opt);
}

/******************************************************************************/

static
double subdivide_twice( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot )
{
    // allow some larger dots
    x *= 0.5;
    y *= 0.5;
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);

    int64_t cell = hashXY( ix, iy, seed );

    if ((cell & 0x08) != 0)
        {
        x *= 2.0;
        y *= 2.0;
        seed += extraHash1;
        }

    if ((cell & 0x80) != 0)
        {
        x *= 2.0;
        y *= 2.0;
        seed += extraHash2;
        }
    
    return dot(x,y,seed^cell,opt);
}

/******************************************************************************/

// x and y should be scaled appropriately outside this function to allow larger dots
static
double subdivide_many( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot, int64_t current_level )
{
    assert(current_level < extraHashCount);
    current_level = std::min( current_level, (int64_t)extraHashCount );  // just in case
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);

    int64_t cell = hashXY( ix, iy, seed );
    int64_t cell2 = rotate_left((uint64_t)cell,7);

    for ( ; current_level > 0; --current_level)
        {
        if ((cell & (0x08 << current_level)) != 0)
            {
            x *= 2.0;
            y *= 2.0;
            seed += extraHashList[ current_level ];
            }

        // use subcell bits to determine if we should exit early
        // makes it looks a LOT more random
        int64_t dx = floor(x);
        int64_t dy = floor(y);
        cell2 = hashXY( dx, dy, seed );
        
        if ( (cell2 & 0x80) != 0)
            current_level--;
        }
    
    return dot(x,y,cell2^cell,opt);
}

/******************************************************************************/

// circle dot, sometimes subdivided one step
double dot_circlesubdivide( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_circle);
}

dot_description desc_circlesubdivide( dot_circlesubdivide, "circle subdivide" );

/******************************************************************************/

// circle dot, sometimes subdivided two steps
double dot_circlesubdivide2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_circle);
}

dot_description desc_circlesubdivide2( dot_circlesubdivide2, "circle subdivide2" );

/******************************************************************************/

// circle dot, sometimes subdivided multiple steps
double dot_circlesubdivide3( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_circle, 4);
}

dot_description desc_circlesubdivide3( dot_circlesubdivide3, "circle subdivide3" );

/******************************************************************************/

// square dot, sometimes subdivided one step
double dot_squaresubdivide( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_square);
}

dot_description desc_squaresubdivide( dot_squaresubdivide, "square subdivide" );

/******************************************************************************/

// square dot, sometimes subdivided two steps
double dot_squaresubdivide2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_square);
}

dot_description desc_squaresubdivide2( dot_squaresubdivide2, "square subdivide2" );

/******************************************************************************/

// square dot, sometimes subdivided multiple steps
double dot_squaresubdivide3( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_square, 4);
}

dot_description desc_squaresubdivide3( dot_squaresubdivide3, "square subdivide3" );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivide( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_ellipse45);
}

dot_description desc_ellipsesubdivide( dot_ellipsesubdivide, "ellipse45 subdivide",
                                1,
                                { "Eccentricity" },
                                { 0.5 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivide2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_ellipse45);
}

dot_description desc_ellipsesubdivide2( dot_ellipsesubdivide2, "ellipse45 subdivide2",
                                1,
                                { "Eccentricity" },
                                { 0.5 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided multiple steps
double dot_ellipsesubdivide3( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_ellipse45, 4);
}

dot_description desc_ellipsesubdivide3( dot_ellipsesubdivide3, "ellipse45 subdivide3",
                                1,
                                { "Eccentricity" },
                                { 0.5 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivideAlt( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_ellipse45alt);
}

dot_description desc_ellipsesubdivideAlt( dot_ellipsesubdivideAlt, "ellipse45alt subdivide",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivide2Alt( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_ellipse45alt);
}

dot_description desc_ellipsesubdivide2Alt( dot_ellipsesubdivide2Alt, "ellipse45alt subdivide2",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided multiple steps
double dot_ellipsesubdivide3Alt( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_ellipse45alt, 4);
}

dot_description desc_ellipsesubdivide3Alt( dot_ellipsesubdivide3Alt, "ellipse45alt subdivide3",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivideAlt2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_ellipse45alt2);
}

dot_description desc_ellipsesubdivideAlt2( dot_ellipsesubdivideAlt2, "ellipse45alt2 subdivide",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided one step
double dot_ellipsesubdivide2Alt2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_ellipse45alt2);
}

dot_description desc_ellipsesubdivide2Alt2( dot_ellipsesubdivide2Alt2, "ellipse45alt2 subdivide2",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// elliptical dot, sometimes subdivided multiple steps
double dot_ellipsesubdivide3Alt2( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_ellipse45alt2, 4);
}

dot_description desc_ellipsesubdivide3Alt2( dot_ellipsesubdivide3Alt2, "ellipse45alt2 subdivide3",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// diamond dot, sometimes subdivided one step
double dot_diamondsubdivide( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_diamond);
}

dot_description desc_diamondsubdivide( dot_diamondsubdivide, "diamond subdivide",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// diamond dot, sometimes subdivided one step
double dot_diamondsubdivide2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_diamond);
}

dot_description desc_diamondsubdivide2( dot_diamondsubdivide2, "diamond subdivide2",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// diamond dot, sometimes subdivided multiple steps
double dot_diamondsubdivide3( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_diamond, 4);
}

dot_description desc_diamondsubdivide3( dot_diamondsubdivide3, "diamond subdivide3",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// diamond and ellipse need Eccentricity values, but could be hard wired
static
double dot_randomshape_inner( double x, double y, int64_t seed, const screen_options &opt )
{
    switch (((uint64_t)seed) % 7)
        {
        default:
        case 0:
            return dot_circle(x,y,seed,opt);
            break;
        case 1:
            return dot_diamond(x,y,seed,opt);
            break;
        case 2:
            return dot_square(x,y,seed,opt);
            break;
        case 3:
            return dot_cross(x,y,seed,opt);
            break;
        case 4:
            return dot_triangle(x,y,seed,opt);
            break;
        case 5:
            return dot_torus3(x,y,seed,opt);
            break;
        case 6:
            return dot_ellipse45alt(x,y,seed,opt);
            break;
        }
}

double dot_randomshape( double x, double y, int64_t seed, const screen_options &opt )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    int64_t cell = hashXY( ix, iy, seed );
    return dot_randomshape_inner(x,y,cell,opt);
}

dot_description desc_randomshape( dot_randomshape, "random shapes",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// random dot, sometimes subdivided one step
double dot_randomsubdivide( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_once(x,y,seed,opt,dot_randomshape_inner);
}

dot_description desc_randomsubdivide( dot_randomsubdivide, "random subdivide",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// diamond dot, sometimes subdivided one step
double dot_randomsubdivide2( double x, double y, int64_t seed, const screen_options &opt )
{
    return subdivide_twice(x,y,seed,opt,dot_randomshape);
}

dot_description desc_randomsubdivide2( dot_randomsubdivide2, "random subdivide2",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

// diamond dot, sometimes subdivided multiple steps
double dot_randomsubdivide3( double x, double y, int64_t seed, const screen_options &opt )
{
    x *= 0.25;
    y *= 0.25;
    return subdivide_many(x,y,seed,opt,dot_randomshape, 4);
}

dot_description desc_randomsubdivide3( dot_randomsubdivide3, "random subdivide3",
                                1,
                                { "Eccentricity" },
                                { 0.6 },
                                { 0.01 },
                                { 1.0 } );

/******************************************************************************/

static
double one_grid( double fx, double fy, double angle, double line_scale, double overlap, bool closed, bool oneDirection )
{
    double CA = cos(angle);
    double SA = sin(angle);
    double rx = CA * fx - SA * fy;
    double ry = SA * fx + CA * fy;
    
    if (rx > (0.5+overlap))
        return 0.0;
    if (rx < (-0.5-overlap))
        return 0.0;

    if (ry > (0.5+overlap))
        return 0.0;
    if (ry < (-0.5-overlap))
        return 0.0;
    
    rx *= line_scale;
    ry *= line_scale;
    
    if (!closed)
        {
        rx += 0.5;
        ry += 0.5;
        }
    
    double irx = floor(rx);
    double iry = floor(ry);
    fx = rx - irx;
    fy = ry - iry;

    if (closed)
        {
        fx = 2.0 * fabs( fx-0.5 );
        fy = 2.0 * fabs( fy-0.5 );
        }
    
    double value = oneDirection ? fx : std::max( fx, fy );
    
    return value;
}

/******************************************************************************/

// randomly rotated line grids covering each other
// aka stamps
// TODO - should there be a white edge option?
double dot_randomgridcover( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = round( opt.params[0] );
    const double overlap = opt.params[1] / 100.0;
    const double line_scale = (line_count)/(1.0+2*overlap);
    const bool closed = (opt.params[2] >= 0.5);
    const bool oneDirection = (opt.params[3] >= 0.5);
    
    x *= 0.5;
    y *= 0.5;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix - 0.5;
    double fy = y - iy - 0.5;
    
    double value = 0.0;
    size_t existing_priority = 0;

    // search all neighbors for overlaps
    for (int64_t dy = -1; dy <= 1; ++dy)
        {
        for (int64_t dx = -1; dx <= 1; ++dx)
            {
            double angle = hashXY_float( (int64_t)ix-dx, (int64_t)iy-dy, seed ) * M_PI;
            size_t new_priority = hashXY( (int64_t)ix-dx, (int64_t)iy-dy, seed^extraHash3 );
            double tx = fx + dx;
            double ty = fy + dy;
            double newValue = one_grid( tx, ty, angle, line_scale, overlap, closed, oneDirection );
            
            if (newValue > 0.0 && new_priority > existing_priority)
                {
                existing_priority = new_priority;
                value = newValue;
                }
            }
        }
    
    return value;
}

dot_description desc_randomgridcover( dot_randomgridcover, "random grid covering",
                                4,
                                { "line count", "overlap", "closed edge", "single direction" },
                                {  4.0,   15.0,   1.0,   0.0 },
                                {  2.0,    0.0,   0.0,   0.0 },
                                { 20.0,  100.0,   1.0,   1.0 } );

/******************************************************************************/

// randomly rotated overlapping line grids
double dot_randomgridoverlap( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = round( opt.params[0] );
    const double overlap = opt.params[1] / 100.0;
    const double line_scale = (line_count)/(1.0+2*overlap);
    const bool closed = (opt.params[2] >= 0.5);
    const bool oneDirection = (opt.params[3] >= 0.5);
    
    x *= 0.5;
    y *= 0.5;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix - 0.5;
    double fy = y - iy - 0.5;
    
    double value = 0.0;

    // search all neighbors for overlaps
    for (int64_t dy = -1; dy <= 1; ++dy)
        {
        for (int64_t dx = -1; dx <= 1; ++dx)
            {
            int64_t gx = (int64_t)ix-dx;
            int64_t gy = (int64_t)iy-dy;
            double angle = hashXY_float( gx, gy, seed ) * M_PI;
            double offx = 0.4 * (hashXY_float( gx, gy, seed^extraHash1 ) - 0.5);
            double offy = 0.4 * (hashXY_float( gx, gy, seed^extraHash3 ) - 0.5);
            double tx = fx + dx + offx;
            double ty = fy + dy + offy;
            double newValue = one_grid( tx, ty, angle, line_scale, overlap, closed, oneDirection );
            value = std::max( value, newValue );
            }
        }
    
    return value*value;
}

dot_description desc_randomgridoverlap( dot_randomgridoverlap, "random grid overlapped",
                                4,
                                { "line count", "overlap", "closed edge", "single direction" },
                                {  6.0,   30.0,   0.0,   0.0 },
                                {  2.0,    0.0,   0.0,   0.0 },
                                { 20.0,  100.0,   1.0,   1.0 } );

/******************************************************************************/

// randomly rotated overlapping line grids - building up more grids as it gets darker
double dot_randomgridmulti( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_count = round( opt.params[0] );
    const double line_width = 1.0 - (opt.params[1] / 100.0);
    const size_t value_count = round(opt.params[2]);
    const double variation = opt.params[3];
    const double scale = 1.0 / double(value_count+1);
    const double overlap = opt.params[4] / 100.0;
    const double line_scale = (line_count)/(1.0+2*overlap);
    const bool closed = (opt.params[5] >= 0.5);
    const bool oneDirection = (opt.params[6] >= 0.5);
    
    x *= 0.5;
    y *= 0.5;
    
    double ix = floor(x);
    double iy = floor(y);
    double fx = x - ix - 0.5;
    double fy = y - iy - 0.5;
    
    double value = 1.0;

    // search all neighbors for overlaps
    for (int64_t dy = -1; dy <= 1; ++dy)
        {
        for (int64_t dx = -1; dx <= 1; ++dx)
            {
            int64_t gx = (int64_t)ix-dx;
            int64_t gy = (int64_t)iy-dy;
            // iterate gray levels
            for (size_t i = 1; i <= value_count; ++i)
                {
                int64_t seed_modified = rotate_left((uint64_t)seed,i);
                double angle = hashXY_float( gx, gy, seed_modified ) * M_PI;
                double offx = 0.4 * (hashXY_float( gx, gy, seed_modified^extraHash1 ) - 0.5);
                double offy = 0.4 * (hashXY_float( gx, gy, seed_modified^extraHash3 ) - 0.5);
                double tx = fx + dx + offx;
                double ty = fy + dy + offy;
                double newValue = one_grid( tx, ty, angle, line_scale, overlap, closed, oneDirection );
                if (newValue > line_width)
                    {
                    double block_id = hashXY( gx, gy, seed_modified^extraHash2 );
                    double offset = variation*(hash_to_double(block_id) - 0.5);
                    double tmp = (i + offset - 0.45) * scale;
                    value = std::min( value, tmp );
                    }
                }   // for values
            }   // for dx
        }   // for dy

    value = std::max( 0.0, value ); // because offset could exceed the range
    value = std::min( 1.0, value );
    value = 1.0 - value;
    return value*value;
}

dot_description desc_randomgridmulti( dot_randomgridmulti, "random grid multi",
                                7,
                                { "line count", "line width", "value count", "value variation", "overlap", "closed edge", "single direction" },
                                {  6.0,   12.0,    8.0,   1.00,    30.0,   0.0,   0.0 },
                                {  2.0,    1.0,    4.0,   0.00,     0.0,   0.0,   0.0 },
                                { 20.0,   99.0,   50.0,   2.00,   100.0,   1.0,   1.0 } );

/******************************************************************************/

// circle diamond - doesn't look that interesting
// circle random - not that interesting

// 3 times as many dot1 as dot2
// mimicking a hand drawn style by Stan Sakai
static
double three_to_one( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot1, dotfunc dot2 )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);

    if ( ((ix | iy) & 0x01) == 0)
        return dot2(x,y,seed,opt);
    else
        return dot1(x,y,seed,opt);
}

/******************************************************************************/

// circle triangle
double dot_circletri( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_trianglerandom);
}

dot_description desc_circletri( dot_circletri, "circle-triangle" );

/******************************************************************************/

// circle line
double dot_circleline( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_line);
}

dot_description desc_circleline( dot_circleline, "circle-line" );

/******************************************************************************/

// circle cross
double dot_circlecross( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_cross);
}

dot_description desc_circlecross( dot_circlecross, "circle-cross" );

/******************************************************************************/

// circle square
double dot_circlesquare( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_squarerandom);
}

dot_description desc_circlesquare( dot_circlesquare, "circle-square" );

/******************************************************************************/

double dot_circleconcentric( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_concentric);
}

dot_description desc_circleconcentric( dot_circleconcentric, "circle-concentric" );

/******************************************************************************/

double dot_circleangle( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_angle45);
}

dot_description desc_circleangle( dot_circleangle, "circle-angle" );

/******************************************************************************/

double dot_circletorus( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_torus2);
}

dot_description desc_circletorus( dot_circletorus, "circle-torus" );

/******************************************************************************/

double dot_circlediagonal( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_diagonals);
}

dot_description desc_circlediagonal( dot_circlediagonal, "circle-diagonal" );

/******************************************************************************/

double dot_circlesemicircle( double x, double y, int64_t seed, const screen_options &opt )
{
    return three_to_one(x,y,seed,opt,dot_circle,dot_semicircles);
}

dot_description desc_circlesemicircle( dot_circlesemicircle, "circle-semicircle" );

/******************************************************************************/

static
double checkerboard( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot1, dotfunc dot2 )
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);

    if ( ((ix + iy) & 0x01) == 0)
        return dot2(x,y,seed,opt);
    else
        return dot1(x,y,seed,opt);
}

/******************************************************************************/

// circle triangle
double dot_circletri2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_trianglerandom);
}

dot_description desc_circletri2( dot_circletri2, "circle-triangle-alternating" );

/******************************************************************************/

// circle line
double dot_circleline2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_line);
}

dot_description desc_circleline2( dot_circleline2, "circle-line-alternating" );

/******************************************************************************/

// circle cross
double dot_circlecross2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_cross);
}

dot_description desc_circlecross2( dot_circlecross2, "circle-cross-alternating" );

/******************************************************************************/

// circle square
double dot_circlesquare2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_squarerandom);
}

dot_description desc_circlesquare2( dot_circlesquare2, "circle-square-alternating" );

/******************************************************************************/

double dot_circleconcentric2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_concentric);
}

dot_description desc_circleconcentric2( dot_circleconcentric2, "circle-concentric-alternating" );

/******************************************************************************/

double dot_circleangle2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_angle45);
}

dot_description desc_circleangle2( dot_circleangle2, "circle-angle-alternating" );

/******************************************************************************/

double dot_circletorus2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_torus2);
}

dot_description desc_circletorus2( dot_circletorus2, "circle-torus-alternating" );

/******************************************************************************/

double dot_circlediagonal2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_diagonals);
}

dot_description desc_circlediagonal2( dot_circlediagonal2, "circle-diagonal-alternating" );

/******************************************************************************/

double dot_circlesemicircle2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_circle,dot_semicircles);
}

dot_description desc_circlesemicircle2( dot_circlesemicircle2, "circle-semicircle-alternating" );

/******************************************************************************/

// XOXO
double dot_crosstorus2( double x, double y, int64_t seed, const screen_options &opt )
{
    return checkerboard(x,y,seed,opt,dot_cross45,dot_torus2);
}

dot_description desc_crosstorus2( dot_crosstorus2, "cross45-torus-alternating",
                            1,
                            { "edge limit" },
                            {  0.05 },
                            {  0.00 },
                            {  1.00 } );

/******************************************************************************/

static
double random_two( double x, double y, int64_t seed, const screen_options &opt, dotfunc dot1, dotfunc dot2 )
{
    const double blank = (1.0/100.0) * opt.params[ opt.params.size()-1 ];
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    double value = hashXY_float( ix, iy, seed^extraHash1 );
    
    int64_t randval = hashXY( ix, iy, seed );
    
    if (value < blank)
        return 0.0;

// TODO - could also make this threshold based, with another parameter
    if ( (randval & 0x80) == 0 )
        return dot2(x,y,seed,opt);
    else
        return dot1(x,y,seed,opt);
}

/******************************************************************************/

// circle line and blanks -- almost Morse Code
double dot_circlelineRand( double x, double y, int64_t seed, const screen_options &opt )
{
    return random_two(x,y,seed,opt,dot_circle,dot_line_symmetric);
}

dot_description desc_circlelineRand( dot_circlelineRand, "circle-line-random",
                            1,
                            { "blank percent" },
                            {   5.0 },
                            {   0.0 },
                            {  99.9 } );

/******************************************************************************/

// circle diagonal random
double dot_circlediagonalRand( double x, double y, int64_t seed, const screen_options &opt )
{
    return random_two(x,y,seed,opt,dot_circle,dot_diagonals);
}

dot_description desc_circlediagonalRand( dot_circlediagonalRand, "circle-diagonal-random",
                            1,
                            { "blank percent" },
                            {   2.0 },
                            {   0.0 },
                            {  99.9 } );

/******************************************************************************/

// cross, torus random --- TIC TAC DOH!
double dot_cross45torusRand( double x, double y, int64_t seed, const screen_options &opt )
{
    return random_two(x,y,seed,opt,dot_cross45,dot_torus2);
}

dot_description desc_cross45torusRand( dot_cross45torusRand, "cross45-torus-random",
                            2,
                            { "edge limit", "blank percent" },
                            {   0.05,   0.0 },
                            {   0.00,   0.0 },
                            {   1.00,  99.9 } );

/******************************************************************************/

// TODO - specify relative scale for spill/bleach to make larger blobs for high res screens
static
double damage_overlay( double value_in, double x, double y, int64_t seed, double dirt, double ink_spill, double white_spill)
{
    const int32_t octave_low = 0;
    const int32_t octave_high = 1;
    const int32_t octave_lowA = 1;
    const int32_t octave_highA = 2;
    
    double result = value_in;
    
    if (white_spill > 0.0)
        {
        double u = ChaosBicubic( x, y, octave_lowA, octave_highA, seed^extraHash3 );
        if (u < white_spill)   // blobs of white or bleach
            return 0.0;
        }
    
    if (ink_spill > 0.0)
        {
        double z = ChaosBicubic( x, y, octave_lowA, octave_highA, seed^extraHash2 );
        if (z < ink_spill)   // blobs completely blocking the screen
            return 1.0;
        }
    
    if (dirt > 0.0)
        {
        // dust and damage altering the screen
        double v = ChaosBicubic( x, y, octave_low, octave_high, seed );
        double noise = dirt * (v - 0.5);
        result = std::min( 1.0, std::max( 0.0, result+noise ));
        }
    
    return result;
}

/******************************************************************************/

static
double damage_common( double x, double y, int64_t seed, const screen_options &opt, double damage,
                dotfunc dot, double dirt, double spill, double white_spill)
{
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    double cell_chance = hashXY_float( ix, iy, seed );
    
    double result = dot(x,y,seed,opt);

    if (cell_chance < damage)
        result = 0.0;

    result = damage_overlay( result, x, y, seed, dirt, spill, white_spill );
    
    return result;
}

/******************************************************************************/

// circle dot, sometimes dropping a dot, like damaged rubdown tone
double dot_circledamage( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[0] / 100.0;
    const double dirt = opt.params[1] / 100.0;
    const double spill = opt.params[2] / 100.0;
    const double bleach = opt.params[3] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_circle,dirt,spill,bleach);
}

dot_description desc_circledamage( dot_circledamage, "circle damage",
                                4,
                                { "dropout percent", "dirt percent", "spill percent", "bleach percent" },
                                {    1.0,  20.0,  15.0,   10.0 },
                                {    0.0,   0.0,   0.0,    0.0 },
                                {  100.0, 100.0,  99.0,   99.0 } );

/******************************************************************************/

// square dot, sometimes dropping a dot, like damaged rubdown tone
double dot_squaredamage( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[0] / 100.0;
    const double dirt = opt.params[1] / 100.0;
    const double spill = opt.params[2] / 100.0;
    const double bleach = opt.params[3] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_square,dirt,spill,bleach);
}

dot_description desc_squaredamage( dot_squaredamage, "square damage",
                                4,
                                { "dropout percent", "dirt percent", "spill percent", "bleach percent" },
                                {    1.0,  20.0,  15.0,   10.0 },
                                {    0.0,   0.0,   0.0,    0.0 },
                                {  100.0, 100.0,  99.0,   99.0 } );

/******************************************************************************/

// ellipse dot, sometimes dropping a dot, like damaged rubdown tone
double dot_ellipsedamage( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[1] / 100.0;
    const double dirt = opt.params[2] / 100.0;
    const double spill = opt.params[3] / 100.0;
    const double bleach = opt.params[4] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_ellipse,dirt,spill,bleach);
}

dot_description desc_ellipsedamage( dot_ellipsedamage, "ellipse damage",
                                5,
                                { "Eccentricity", "dropout percent", "dirt percent", "spill percent", "bleach percent"   },
                                {  0.70,   1.0,  20.0,  15.0,   10.0 },
                                {  0.01,   0.0,   0.0,   0.0,    0.0 },
                                {  4.00, 100.0, 100.0, 100.0,   99.0 } );

/******************************************************************************/

// ellipse dot, random direction changes, lots of damage
double dot_ellipsedamage2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[1] / 100.0;
    const double dirt = opt.params[2] / 100.0;
    const double spill = opt.params[3] / 100.0;
    const double bleach = opt.params[4] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_ellipseRand,dirt,spill,bleach);
}

dot_description desc_ellipsedamage2( dot_ellipsedamage2, "ellipse damage2",
                                5,
                                { "Eccentricity", "dropout percent", "dirt percent", "spill percent", "bleach percent"   },
                                {  0.40,   0.0,  50.0,  35.0,    0.0 },
                                {  0.01,   0.0,   0.0,   0.0,    0.0 },
                                {  4.00, 100.0,  99.0,  99.0,   99.0 } );

/******************************************************************************/

// ellipse45 dot, random direction changes, lots of damage
double dot_ellipsedamage45( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[1] / 100.0;
    const double dirt = opt.params[2] / 100.0;
    const double spill = opt.params[3] / 100.0;
    const double bleach = opt.params[4] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_ellipse45,dirt,spill,bleach);
}

dot_description desc_ellipsedamage45( dot_ellipsedamage45, "ellipse damage45",
                                5,
                                { "Eccentricity", "dropout percent", "dirt percent", "spill percent", "bleach percent"   },
                                {  0.70,   0.0,  30.0,  15.0,   10.0 },
                                {  0.01,   0.0,   0.0,   0.0,    0.0 },
                                {  4.00, 100.0,  99.0,  99.0,   99.0 } );

/******************************************************************************/

// ellipse45 dot, random direction changes, lots of damage
double dot_ellipsedamage45Rand( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[1] / 100.0;
    const double dirt = opt.params[2] / 100.0;
    const double spill = opt.params[3] / 100.0;
    const double bleach = opt.params[4] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_ellipse45Rand,dirt,spill,bleach);
}

dot_description desc_ellipsedamage45random( dot_ellipsedamage45Rand, "ellipse damage45 random",
                                5,
                                { "Eccentricity", "dropout percent", "dirt percent", "spill percent", "bleach percent"   },
                                {  0.50,   0.0,  30.0,  15.0,   10.0 },
                                {  0.01,   0.0,   0.0,   0.0,    0.0 },
                                {  4.00, 100.0,  99.0,  99.0,   99.0 } );

/******************************************************************************/

// diamond dot, sometimes dropping a dot, like damaged rubdown tone
double dot_diamonddamage( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[1] / 100.0;
    const double dirt = opt.params[2] / 100.0;
    const double spill = opt.params[3] / 100.0;
    const double bleach = opt.params[4] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_diamond,dirt,spill,bleach);
}

dot_description desc_diamonddamage( dot_diamonddamage, "diamond damage",
                                5,
                                { "Eccentricity", "dropout percent", "dirt percent", "spill percent", "bleach percent"   },
                                {  0.70,   1.0,  20.0,  15.0,   10.0 },
                                {  0.01,   0.0,   0.0,   0.0,    0.0 },
                                {  4.00, 100.0, 100.0, 100.0,   99.0 } );

/******************************************************************************/

// line dot, sometimes dropping a dot, like damaged rubdown tone
double dot_linedamage( double x, double y, int64_t seed, const screen_options &opt )
{
    const double damage = opt.params[0] / 100.0;
    const double dirt = opt.params[1] / 100.0;
    const double spill = opt.params[2] / 100.0;
    const double bleach = opt.params[3] / 100.0;
    return damage_common(x,y,seed,opt,damage,dot_line, dirt, spill, bleach);
}

dot_description desc_linedamage( dot_linedamage, "line damage",
                                4,
                                { "dropout percent", "dirt percent", "spill percent", "bleach percent" },
                                {    0.0,  20.0,  12.0,   10.0 },
                                {    0.0,   0.0,   0.0,    0.0 },
                                {  100.0, 100.0,  99.0,   99.0 } );

/******************************************************************************/

// line dot mix
// line fills in quickly, dot fills in slowly
// Kuma Kuma Kuma Wallpaper ;-)
double dot_linedot( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_break = opt.params[0];
    const double offset = opt.params[1];
    const double center = 0.5 * line_break;
    const double norm = 1.0 / sqrt(2.0);
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    
    if (fx < line_break)
        {
        y += offset * ix;
        
        double iy = floor(y);
        double fy = y - iy;
        
        fx = 2.0 * (fx - center);    // -linebreak .. linebreak
        fy = 2.0 * (fy - 0.5);        // -1 .. 1 centered
        
        double dist = hypot(fx,fy);
        value = 1.0 - dist * norm;
        }
    
    return value;
}

dot_description desc_linedot( dot_linedot, "line dot",
                            2,
                            { "line break", "column offset" },
                            { 0.9,  0.5, },
                            { 0.00, 0.0, },
                            { 1.00, 1.0 } );

/******************************************************************************/

// H&V line dot mix
// lines fill in quickly, dot fills in slowly
// looks like tech wall texture ;-)
double dot_linebox( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_break = opt.params[0];
    const double offset = opt.params[1];
    const double center = 0.5 * line_break;
    const double norm = 1.0 / hypot(line_break,line_break);
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;

    y += offset * ix;
    
    double iy = floor(y);
    double fy = y - iy;
    
    if (fy >= line_break)
        {
        value = fy;
        }
    else if (fx < line_break)
        {
        
        fx = 2.0 * (fx - center);    // -linebreak .. linebreak
        fy = 2.0 * (fy - center);    // -1 .. 1 centered
        
        double dist = hypot(fx,fy);
        value = 1.0 - dist * norm;
        }
    
    return value;
}

dot_description desc_linebox( dot_linebox, "box dot",
                            2,
                            { "line break", "column offset" },
                            { 0.9,  0.5, },
                            { 0.00, 0.0, },
                            { 1.00, 1.0 } );

/******************************************************************************/

// line - dashed line mix
// lines fill in quickly, dashes fills in slowly
double dot_linedash( double x, double y, int64_t seed, const screen_options &opt )
{
    const double line_break = opt.params[0];
    const double dash_length = opt.params[1];
    const double dash_gap = opt.params[2];
    const double offset = opt.params[3];
    const double center = 0.5 * line_break;
    double ix = floor(x);
    double fx = x - ix;
    double value = fx;
    
    if (fx < line_break)
        {
        y /= dash_length;
        
        y += offset * ix;
    
        double iy = floor(y);
        double fy = y - iy;

// TODO - can I get rounded end caps?
        if (fy < dash_gap)
            return 0.01;
    
        fx = 2.0 * (fx - center);    // -linebreak .. linebreak
        
        value = 1.0 - ( fabs( fx ) / line_break );
        value = std::min( 1.0, std::max( 0.0, value ));
        }
    
    return value;
}

dot_description desc_linedash( dot_linedash, "line dash",
                            4,
                            { "line break", "dash length", "dash gap", "column offset" },
                            { 0.80,  2.5,  0.15,  0.5, },
                            { 0.00,  0.1,  0.00,  0.0, },
                            { 1.00, 10.0,  1.00,  1.0 } );

/******************************************************************************/

// for algorithm testing and documentation, see https://gitlab.com/chriscox/hex-coordinates
double dot_hexagons( double x, double y, int64_t seed, const screen_options &opt )
{
    const double sqrt3 = sqrt(3.0);
    double t = sqrt3 * y;
    double temp1 = floor( t + x );
    double temp2 = ( t - x );
    double temp3 = ( 2 * x + 1 );
    double qf = (temp1 + temp3) / 3.0;
    double rf = (temp1 + temp2) / 3.0;
    double q = floor( qf );
    double r = floor( rf );
    // at this point we have integer cell coordinates in q and r, but they are not useful for fractional coordinates

// TODO - can I calculate this, correctly offset, without calculating Q and R ?

    // so we make some better correlated coordinates along x, +-60
    double tf = 0.5 * (t+x);
    double xf = x + 0.5;
    double zf = 0.5 * temp2;

    // and offset them a bit to match the hexagons we already defined
    if (((int64_t)(q+r) & 0x01) != 0)
        tf += 0.5;
    
    if (((int64_t)r & 0x01) != 0)
        xf += 0.5;
    
    if (((int64_t)q & 0x01) != 0)
        zf += 0.5;

    // get the fractions
    tf = tf - floor( tf );
    xf = xf - floor( xf );
    zf = zf - floor( zf );

    // fold the fractional coords so we get the highest value in the center
    tf = std::min( tf, 1.0 - tf );
    xf = std::min( xf, 1.0 - xf );
    zf = std::min( zf, 1.0 - zf );
    
    // then take minimums to get our hex cone
    // and multiply by 2 because we only have 0.5 as a maximum due to folding
    double value = 2.0 * std::min( xf, std::min( zf, tf ) );

    // ... and we're done!
    return value;
}

dot_description desc_hexagons( dot_hexagons, "hexagons" );

/******************************************************************************/

// high frequency chaos
double dot_mezzotint( double x, double y, int64_t seed, const screen_options &opt )
{
    const int32_t octave_low = 2;
    const int32_t octave_high = 4;
    double v = ChaosBicubic( x, y, octave_low, octave_high, seed );
    v = (1.5 * (v-0.5)) + 0.55;  // scale and shift range a little for better tone coverage
    double value = std::min( 1.0, std::max( 0.0, v ));
    return value;
}

dot_description desc_mezzotint( dot_mezzotint, "mezzotint" );

/******************************************************************************/

// horizontally stretched noise
double dot_noise_stretched( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 2;
    const int32_t octave_high = 3;
    
    assert( length > 0.0 );
    
    x /= length;
    double result = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
    
    result = 1.2 * (result - 0.5) + 0.5;
    
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    return result;
}

dot_description desc_noise_stretched( dot_noise_stretched, "noise stretched",
                                1,
                                { "Length" },
                                {    5.0 },
                                {    0.5 },
                                {  100.0 } );

/******************************************************************************/

// average of horizontally and vertically stretched noise
double dot_noise_cross( double x, double y, int64_t seed, const screen_options &opt )
{
    const double lengthH = opt.params[0];
    const double lengthV = opt.params[1];
    const int32_t octave_low = 2;
    const int32_t octave_high = 3;
    
    assert( lengthH > 0.0 );
    assert( lengthV > 0.0 );
    
    double tx = x / lengthH;
    double ty = y / lengthV;
    double v1 = ChaosBicubic( tx, y, octave_low, octave_high, seed^extraHash1 );
    double v2 = ChaosBicubic( x, ty, octave_low, octave_high, seed^extraHash3 );

    double result = 0.5 * (v1 + v2);
    
    result = 1.8 * (result - 0.5) + 0.5;
    
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    return result;
}

dot_description desc_noise_cross( dot_noise_cross, "noise cross",
                                2,
                                { "Length Horizontal", "Length Vertical" },
                                {    8.0,  12.0 },
                                {    0.5,   0.5 },
                                {  100.0, 100.0 } );

/******************************************************************************/

double dot_paper_fiber( double x, double y, int64_t seed, const screen_options &opt )
{
    const double lengthH = opt.params[0];
    const double lengthV = opt.params[1];
    const int32_t octave_low = 2;
    const int32_t octave_high = 3;
    
    assert( lengthH > 0.0 );
    assert( lengthV > 0.0 );
    
    double tx = x / lengthH;
    double ty = y / lengthV;
    double v1 = ChaosBicubic( tx, y, octave_low, octave_high, seed^extraHash2 );
    double v2 = ChaosBicubic( x, ty, octave_low, octave_high, seed^extraHash4 );

    double result = std::max( v1, v2 );

    result = 1.3 * (result - 0.5) + 0.4;

    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    return result;
}

dot_description desc_paper_fiber( dot_paper_fiber, "paper fiber",
                                2,
                                { "Length Horizontal", "Length Vertical" },
                                {   11.0,  17.0 },
                                {    0.5,   0.5 },
                                {  100.0, 100.0 } );

/******************************************************************************/

// difference of stretched noise
double dot_etched( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 3;
    const int32_t octave_high = 3;
    
    assert( length > 0.0 );
    
    x /= length;
    double v1 = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash2 );
    double v2 = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash3 );

    double result = fabs( v2 - v1 );
    
    result = 1.1 * result;
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return sqrt(result);
}

dot_description desc_etched( dot_etched, "etched",
                            1,
                            { "Length" },
                            {    4.0 },
                            {    0.5 },
                            {   50.0 } );

/******************************************************************************/

// stretched noise
double dot_grain( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 3;
    const int32_t octave_high = 3;
    
    assert( length > 0.0 );
    
    x /= length;
    double v1 = ChaosBicubic( x, y, octave_low, octave_high, seed );
    
    double result = v1;
    
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    return result;
}

dot_description desc_grain( dot_grain, "grain",
                            1,
                            { "Length" },
                            {    2.0 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

// stretched noise
double dot_moss( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 1;
    const int32_t octave_high = 4;
    assert( length > 0.0 );
    
    x /= length;
    double v1 = ChoasSubtractionBicubic( x, y, octave_low, octave_high, seed );

    double result = 0.5 * (v1 + 0.65);
    
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    return result;
}

dot_description desc_moss( dot_moss, "moss",
                            1,
                            { "Length" },
                            {    2.0 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

// stretched noise
double dot_smoke( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 1;
    const int32_t octave_high = 4;
    assert( length > 0.0 );
    
    x /= length;
    double v1 = ChoasDifferenceBicubic( x, y, octave_low, octave_high, seed );
    double result = sqrt(v1);
    
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return result;
}

dot_description desc_smoke( dot_smoke, "smoke",
                            1,
                            { "Length" },
                            {    2.0 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

// stretched noise
double dot_spill( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 0;
    const int32_t octave_high = 1;
    
    assert( length > 0.0 );
    
    x /= length;
    double v1 = MultifractalBicubic( x, y, octave_low, octave_high, seed );
    double result = 1.3 * (v1 - 0.6) + 0.5;

    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return result;
}

dot_description desc_spill( dot_spill, "spill",
                            1,
                            { "Length" },
                            {    2.0 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

// difference of stretched noise
double dot_ripples( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    const int32_t octave_low = 1;           // lower octaves looks like water/caustics
    const int32_t octave_high = 1;
    
    assert( length > 0.0 );
    
    x /= length;
    double v1 = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash1 );
    double v2 = ChaosBicubic( x, y, octave_low, octave_high, seed^extraHash3 );

    double result = fabs( v2 - v1 );
    
    result = 1.1 * result;
    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return sqrt(result);
}

dot_description desc_ripples( dot_ripples, "ripples",
                            1,
                            { "Length" },
                            {    4.0 },
                            {    0.5 },
                            {   50.0 } );

/******************************************************************************/

// stretched noise mixture
double dot_spill2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    
    assert( length > 0.0 );
    
    x /= length;
    
    x *= 3.0;
    y *= 3.0;

    double v2 = NoiseBicubic( x, y, seed^extraHash2 );
    double v3 = NoiseBicubic( x, y+0.5, seed^extraHash3 );
    double v4 = NoiseBicubic( y, x+0.5, seed^extraHash4 );

    double result = 0.333 * (v2 + v3 + v4);
    result = 2.0 * result - 0.5;

    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return result;
}

dot_description desc_spill2( dot_spill2, "spill2",
                            1,
                            { "Length" },
                            {    2.2 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

// stretched noise mixture
double dot_spill3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double length = opt.params[0];
    
    assert( length > 0.0 );
    
    x /= length;
    
    x *= 3.0;
    y *= 3.0;

    double v1 = NoiseBicubic( x+y, x-y, seed );
    double v2 = NoiseBicubic( x, y, seed^extraHash1 );
    double v3 = NoiseBicubic( x, y+0.5, seed^extraHash2 );
    double v4 = NoiseBicubic( y-x, x+y+0.5, seed^extraHash3 );

    double result = 0.25 * (v1 + v2 + v3 + v4);
    result = 2.5 * result - 0.75;

    result = std::min( 1.0, result );
    result = std::max( 0.0, result );
    
    return result;
}

dot_description desc_spill3( dot_spill3, "spill3",
                            1,
                            { "Length" },
                            {    2.2 },
                            {    0.5 },
                            {   20.0 } );

/******************************************************************************/

double dot_fishscales( double x, double y, int64_t seed, const screen_options &opt )
{
    const double pattern_height = 1.0;     // = radius limit
    const double pattern_width = pattern_height * opt.params[1];
    const double power = 2.0 + opt.params[0];
    const double power_inverse = 1.0 / power;
    double value = 0.0;
    
    // transform coordinates to tile relative?
    double tx = x / pattern_width;
    double ty = y / pattern_height;
    
    double left = floor(tx) * pattern_width;
    double top = floor(ty) * pattern_height;
    double bottom = top + pattern_height;
    double right = left + pattern_width;
    double cx = left + 0.5 * pattern_width;
    double cy = top + 0.5 * pattern_height;
    double lower = cy + pattern_height;
    
    if (power == 2.0)
        {
        // check each origin location (center, LR, LL, center-height) in order
        double new_value = hypot( x-cx, y-cy );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = hypot( x-left, y-bottom );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = hypot( x-right, y-bottom );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = hypot( x-cx, y-lower );
        if (new_value <= pattern_height) value = new_value;
        
        return value / pattern_height;
        }
    else
        {
        // check each origin location (center, LR, LL, center-height) in order
        double new_value = pow( fabs(x-cx), power ) + pow( fabs(y-cy), power );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = pow( fabs(x-left), power ) + pow( fabs(y-bottom), power );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = pow( fabs(x-right), power ) + pow( fabs(y-bottom), power );
        if (new_value <= pattern_height) value = new_value;
        
        new_value = pow( fabs(x-cx), power ) + pow( fabs(y-lower), power );
        if (new_value <= pattern_height) value = new_value;
        
        value = value / pattern_height;
        value = pow( value, power_inverse );
        return value;
        }
    
}

dot_description desc_fishscales( dot_fishscales, "fish scales",
                            2,
                            { "squareness", "horizontal spacing" },
                            {   0.0,    2.0 },
                            {   0.0,    1.4 },
                            {  20.0,    3.0 } );

/******************************************************************************/

double dot_lizardscales( double x, double y, int64_t seed, const screen_options &opt )
{
    const double pattern_height = 1.0;     // = radius limit
    const double pattern_width = pattern_height * opt.params[0];
    double value = 0.0;
    
    // transform coordinates to tile relative?
    double tx = x / pattern_width;
    double ty = y / pattern_height;
    
    double left = floor(tx) * pattern_width;
    double top = floor(ty) * pattern_height;
    double bottom = top + pattern_height;
    double right = left + pattern_width;
    double cx = left + 0.5 * pattern_width;
    double cy = top + 0.5 * pattern_height;
    double lower = cy + pattern_height;

    // check each origin location (TL, TR, center, LR, LL, center-height) in order
    double new_value;

    new_value = fabs(x-left) + fabs(y-top);
    if (new_value <= pattern_height) value = new_value;
    
    new_value = fabs(x-right) + fabs(y-top);
    if (new_value <= pattern_height) value = new_value;
    
    new_value = fabs(x-cx) + fabs(y-cy);
    if (new_value <= pattern_height) value = new_value;
    
    new_value = fabs(x-left) + fabs(y-bottom);
    if (new_value <= pattern_height) value = new_value;
    
    new_value = fabs(x-right) + fabs(y-bottom);
    if (new_value <= pattern_height) value = new_value;
    
    new_value = fabs(x-cx) + fabs(y-lower);
    if (new_value <= pattern_height) value = new_value;
    
    return value / pattern_height;
}

dot_description desc_lizardscales( dot_lizardscales, "lizard scales",
                            1,
                            { "horizontal spacing" },
                            {  2.0 },
                            {  1.0 },
                            {  3.0 } );

/******************************************************************************/

double common_overlapscales( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func dot, double r_limit )
{
    const double pattern_height = 1.0;     // = radius limit
    const double pattern_width = pattern_height;
    const double row_offset = opt.params[0];     // 0.0 to 1.0
    double value = 0.0;
    
    x += row_offset * y;
    
    // transform coordinates to tile relative?
    double ty = y / pattern_height;
    double tx = x / pattern_width;
    
    double ix = floor(tx);
    double iy = floor(ty);
    
    double left = ix * pattern_width;
    double top = iy * pattern_height;
    double bottom = top + pattern_height;
    double right = left + pattern_width;

    int64_t cell_id = 0;    // hashXY( (int64_t)ix, (int64_t)iy, seed ); // TODO - modify per location!  Not currently used.

    // check each origin location (TL, TR, LR, LL) in order
    double new_value;
    
    new_value = dot(x-left,y-top, cell_id, left, top, 0.0, 1.0 );
    if (new_value <= r_limit) value = new_value;
    
    new_value = dot(x-left,y-bottom, cell_id, left, bottom, 0.0, 1.0 );
    if (new_value <= r_limit) value = new_value;
    
    new_value = dot(x-right,y-top, cell_id, right, top, 0.0, 1.0 );
    if (new_value <= r_limit) value = new_value;
    
    new_value = dot(x-right,y-bottom, cell_id, right, bottom, 0.0, 1.0 );
    if (new_value <= r_limit) value = new_value;
    
    return value / r_limit;
}

/******************************************************************************/

  // square == brick with problems
  // circle == fish scales
  // diamond == lizard scales
  // others don't work well
double dot_overlapscale( double x, double y, int64_t seed, const screen_options &opt )
{
    const double rlimit = 0.5 * sqrt(2.0);
    return common_overlapscales( x, y, seed, opt, circle_dist_func, rlimit );
}

dot_description desc_overlapscale( dot_overlapscale, "overlap scales",
                            1,
                            { "row skew" },
                            {  0.5 },
                            {  0.0 },
                            {  1.0 } );

/******************************************************************************/

// random angle lines, with limits on length (turns into dots in shadows)
double dot_confettiline( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = sqrt(2.0);
    const double limit = 0.5 * scale * opt.params[0];
    double ix = floor(x);
    double iy = floor(y);
    double cell_angle = M_PI * hash64_float( hashXY( (int64_t)ix, (int64_t)iy, seed ) );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);
    
    if (r > limit)
        return 0.0;
    
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    
    double value = fabs(dv) * scale;    // symmetrical from center
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - sqrt(value);
}

dot_description desc_confettiline( dot_confettiline, "confetti line",
                                1,
                                { "length limit" },
                                { 0.7 },
                                { 0.1 },
                                { 1.0 } );

/******************************************************************************/

// random angle lines, with limits on length (turns into dots in shadows)
double dot_confettiline2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 2.0 * sqrt(2.0);
    const double limit = 0.5 * scale * opt.params[0];
    const double spread = opt.params[1];
    double ix = floor(x);
    double iy = floor(y);
    double cell_angle = M_PI * hash64_float( hashXY( (int64_t)ix, (int64_t)iy, seed ) );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);
    
    if (r > limit)
        return 0.0;
    
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    
    double value = fabs(dv) * scale;    // symmetrical from center
    
    value = fabs(value - spread);       // and doubled
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - sqrt(value);
}

dot_description desc_confettiline2( dot_confettiline2, "confetti double line",
                                2,
                                { "length limit", "spread" },
                                {  0.7,   0.6 },
                                {  0.1,   0.10 },
                                {  1.0,   1.00 } );

/******************************************************************************/

// random angle lines, with limits on length (turns into dots in shadows)
double dot_confettiline3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = opt.params[1];
    const double limit = 0.5 * opt.params[0];
    double ix = floor(x);
    double iy = floor(y);
    double cell_angle = M_PI * hash64_float( hashXY( (int64_t)ix, (int64_t)iy, seed ) );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;

    double r = hypot(dy,dx);
    
    if (r > limit)
        return 0.0;
        
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    
    double value = dv * scale;    // symmetrical from center
    
    value = fabs( floor(value) - value );  // multiple lines
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - value;
}

dot_description desc_confettiline3( dot_confettiline3, "confetti multi line",
                                2,
                                { "length limit", "count" },
                                {  2.0,    2.0 },
                                {  0.1,    1.0 },
                                {  2.0,   20.0 } );

/******************************************************************************/

// random wedges, with limits on length (turns into dots in shadows)
double dot_confettiwedge( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = sqrt(2.0);
    const double limit = 0.5 * scale * opt.params[0];
    const double spread = opt.params[1];
    double ix = floor(x);
    double iy = floor(y);
    double cell_angle = 2.0 * M_PI * hash64_float( hashXY( (int64_t)ix, (int64_t)iy, seed ) );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    double dt = dx * SA + dy * CA;
    
    double value = (0.1 + fabs(dv) - spread*dt) * scale;
    
    if (r > limit)
        return 0.0;
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - value;
}

dot_description desc_confettiwedge( dot_confettiwedge, "confetti wedge",
                                2,
                                { "length limit", "spread" },
                                {  0.7,   0.25 },
                                {  0.1,   0.10 },
                                {  1.0,   1.00 } );

/******************************************************************************/

// random wedges, with limits on length (turns into dots in shadows)
double dot_confettiV( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = sqrt(2.0);
    const double limit = 0.5 * scale * opt.params[0];
    const double spread = opt.params[1];
    double ix = floor(x);
    double iy = floor(y);
    double cell_angle = 2.0 * M_PI * hash64_float( hashXY( (int64_t)ix, (int64_t)iy, seed ) );
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    double dt = dx * SA + dy * CA;
    
    double value = (0.1 + fabs(dv) - spread*dt) * scale;
    
    value = fabs(value - 0.3);
    
    if (r > limit)
        return 0.0;
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - sqrt(value);
}

dot_description desc_confettiV( dot_confettiV, "confetti V",
                                2,
                                { "length limit", "spread" },
                                {  0.7,   0.5 },
                                {  0.1,   0.10 },
                                {  1.0,   1.00 } );

/******************************************************************************/

// aligned wedges
double dot_alignedV( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = sqrt(2.0);
    const double limit = 0.5 * scale * opt.params[0];
    const double spread = opt.params[1];
    const double cell_angle = (M_PI/180.0) * opt.params[2];
    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);
    double CA = cos(cell_angle);
    double SA = sin(cell_angle);
    
    double dv = dx * CA - dy * SA;
    double dt = dx * SA + dy * CA;
    
    double value = (0.1 + fabs(dv) - spread*dt) * scale;
    
    value = fabs(value - 0.3);
    
    if (r > limit)
        return 0.0;
    
    value = std::min( 1.0, std::max(0.0, value ));
    
    return 1.0 - sqrt(value);
}

dot_description desc_alignedV( dot_alignedV, "aligned V",
                                3,
                                { "length limit", "spread", "rotation" },
                                {  0.7,   0.5,    90.0 },
                                {  0.1,   0.10,    0.0 },
                                {  1.0,   1.00,  360.0 } );

/******************************************************************************/

// concentric rings of shapes
double concentric_common2( double x, double y, int64_t seed, const screen_options &opt, inner_dot_func fun, double ring_spacing, double dot_spacing, double rlimit )
{
    const double dot_size = opt.params[0];
assert(dot_spacing > 0.0);
    dot_spacing = 1.0 / dot_spacing;
    
    double r = hypot(x,y);          // 0..INF
    
    if (r > (rlimit+0.5))
        return 0.0;

#if 0
// single shape in center, doesn't always look right
    if (r < (0.5*ring_spacing))
        {
        // single dot in center
        if (r > dot_size)
            return 0.01;
        else
            return 1.0 - (r / dot_size);
        }
#endif

    r /= ring_spacing;
    
    const double angle_adjust = 0.0;    // (1.0*M_PI);
    double in_angle = atan2(x,y) + angle_adjust;      // -M_PI..M_PI
    double ir = floor(r-0.5);
    
    double scaled_radius = (ir+0.5)*ring_spacing;

    // allowing fractions and hiding the seam doesn't get straight lines
    double dots_allowed = std::max(1.0, floor( dot_spacing * scaled_radius*2.0*M_PI ));

#if 0
//FAIL
    const double dot_base = 3.0;
    dots_allowed = dot_base* floor( (dots_allowed+(dot_base-0.01)) / dot_base ); // make dots a multiple of 4
#endif

    double dots_factor = dots_allowed / (2.0*M_PI);       // assuming radius 1.0, midway in ring

    double angle = in_angle * dots_factor;
    
    double ia = floor(angle);

    // handle wrap/discontinuity in quantized angle around +-PI
    if ((2*ia) >= dots_allowed)
        ia -= dots_allowed;
    if ((2*ia) <= -dots_allowed)
        ia += dots_allowed;

    int64_t cell_id = hashXY( (int64_t)ir, (int64_t)ia, seed );

    double cr = scaled_radius + 0.5;
    double ca = ((ia+0.5)/dots_factor) - angle_adjust;
    
    double cx = cr * sin(ca);
    double cy = cr * cos(ca);
    
    double dist = fun( (cx-x), (cy-y), cell_id, cx, cy, ca, dot_size );

// DEBUG
#if 0
    dist = 0.5 + (ia/dots_factor) / (2.0*M_PI);
    return std::min(1.0, std::max(0.0, dist));      // debug - shows modified angle values
#elif 0
    return hash_to_double( cell_id );      // debug - shows cell/hash problems along angle wrap at +-PI
#endif
    
    if (dist > dot_size)
        return 0.01;
    else
        return 1.0 - (dist / dot_size);

}

/******************************************************************************/

double dot_samekomon( double x, double y, int64_t seed, const screen_options &opt )
{
    //const double dot_size = opt.params[0];
    const double pattern_height = round( opt.params[1] );       // = radius limit
    const double pattern_width = pattern_height * 2.0 - 0.75;    // TODO - must overlap at top and sides
    const double ring_spacing = 1.0;
    const double dot_spacing = 1.0; // TODO - should be higher or lower to make match at corners?

    double value = 0.0;
    
    // transform coordinates to tile relative?
    double tx = x / pattern_width;
    double ty = y / pattern_height;
    
    double left = floor(tx) * pattern_width;
    double top = floor(ty) * pattern_height;
    double bottom = top + pattern_height;
    double right = left + pattern_width;
    double cx = left + 0.5 * pattern_width;
    double cy = top + 0.5 * pattern_height;
    double lower = top + 1.5 * pattern_height;
    
    // check each origin location (center, LR, LL, center-height) in order
    double new_value = concentric_common2( x-cx, y-cy, seed, opt, circle_dist_func, ring_spacing, dot_spacing, pattern_height );
    if (new_value > 0.0) value = new_value;
    
    new_value = concentric_common2( x-left, y-bottom, seed, opt, circle_dist_func, ring_spacing, dot_spacing, pattern_height );
    if (new_value > 0.0) value = new_value;
    
    new_value = concentric_common2( x-right, y-bottom, seed, opt, circle_dist_func, ring_spacing, dot_spacing, pattern_height );
    if (new_value > 0.0) value = new_value;
    
    new_value = concentric_common2( x-cx, y-lower, seed, opt, circle_dist_func, ring_spacing, dot_spacing, pattern_height );
    if (new_value > 0.0) value = new_value;
    
    return value;
}

dot_description desc_samekomon( dot_samekomon, "samekomon",     // not really sharkskin, but a derivative fabric pattern
                                2,
                                { "dot size", "pattern size" },
                                {  0.6,  10.0, },
                                {  0.1,   4.0, },
                                { 1.50,  50.0 } );

/******************************************************************************/

double dot_offgridlines( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double reverse_point = opt.params[1];
    pointFloat offPt;
    
    (void) Offgrid_PointToSegment( x, seed, edge_limit, offPt );
    
    double width = offPt.y - offPt.x;

    // 0..1 range
    double value = (x - offPt.x) / width;

    if (reverse_point > 0.0)
        {
        value = 1.0 - value;
        if (value < reverse_point)
            value = reverse_point - value;
        value = 1.0 - value;
        }
    
    return value;
}

dot_description desc_offgridlines( dot_offgridlines, "irregular lines",
                                2,
                                { "min size", "reversal limit" },
                                {  0.3,   0.0 },
                                {  0.02,  0.0 },
                                {  1.0,   1.0  } );

/******************************************************************************/

double dot_offgridrect( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double reverse_point = opt.params[1];
    const double bias_scale = opt.params[2] / (100.0*M_PI);
    const double round_scale = (1.0-bias_scale);
    rectFloat offRect;
    
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // 0..1 range
    double fx = (x - offRect.left) / width;
    double fy = (y - offRect.top) / height;
    
    double bias = 0.5 + atan2( fy, fx ) * (1.0 / M_PI);

    double minx = std::min( fx, (1.0-fx) );
    double miny = std::min( fy, (1.0-fy) );
    double value = 2.0 * std::min( minx, miny );
    value = sqrt(value);

    if (reverse_point > 0.0)
        {
        value = 1.0 - value;
        if (value < reverse_point)
            value = reverse_point - value;
        value = 1.0 - value;
        }
    
    value = bias * bias_scale + round_scale * value;
    value = std::max( 0.0, std::min( 1.0, value ) );
    
    return value;
}

dot_description desc_offgridrect( dot_offgridrect, "irregular brick",
                                3,
                                { "min size", "stone limit", "sprial bias scale"  },
                                {  0.3,   0.0,    0.0 },
                                {  0.02,  0.0,    0.0 },
                                {  1.0,   1.0,  100.0  } );

/******************************************************************************/

double dot_offgridlines2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double reverse_point = opt.params[1];
    const double bias_scale = opt.params[2] / (100.0);
    const double round_scale = (1.0-bias_scale);
    rectFloat offRect;
    
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // 0..1 range
    double fx = (x - offRect.left) / width;
    double fy = (y - offRect.top) / height;

    double value = fy * bias_scale + round_scale * fx;
    value = std::max( 0.0, std::min( 1.0, value ) );

    if (reverse_point > 0.0)
        {
        value = 1.0 - value;
        if (value < reverse_point)
            value = reverse_point - value;
        value = 1.0 - value;
        }
    
    return value;
}

dot_description desc_offgridlines2( dot_offgridlines2, "irregular lines2",
                                3,
                                { "min size", "stone limit", "wedge bias scale" },
                                {  0.3,   0.0,    5.0 },
                                {  0.02,  0.0,    0.0 },
                                {  1.0,   1.0,  100.0 } );

/******************************************************************************/

double dot_offgridlinesRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double bias_scale = opt.params[1] / (100.0);
    const double round_scale = (1.0-bias_scale);
    rectFloat offRect;
    
    pointInt gridPoint = Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;
    
    int64_t cell_hash = hashXY( gridPoint.x, gridPoint.y, seed );

    // 0..1 range
    double fx = (x - offRect.left) / width;
    double fy = (y - offRect.top) / height;
    
    if ((cell_hash & 0x80) == 0)
        fx = 1.0 - fx;

    if ((cell_hash & 0x08) == 0)
        fy = 1.0 - fy;
    
    if ((cell_hash & 0x20) == 0)
        std::swap(fx,fy);

    double value = fy * bias_scale + round_scale * fx;
    value = std::max( 0.0, std::min( 1.0, value ) );
    
    return value;
}

dot_description desc_offgridlinesRandom( dot_offgridlinesRandom, "irregular lines Random",
                                2,
                                { "min size", "wedge bias scale" },
                                {  0.2,     5.0 },
                                {  0.02,    0.0 },
                                {  1.0,   100.0 } );

/******************************************************************************/

double dot_offgriddiamond( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double reverse_point = opt.params[1];
    const double bias_scale = opt.params[2] / (100.0*M_PI);
    const double round_scale = (1.0-bias_scale);
    rectFloat offRect;
    
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // -1..+1 range
    double cx = 0.5 * (offRect.left + offRect.right);
    double cy = 0.5 * (offRect.top + offRect.bottom);
    
    double fx = (2.0 / width) * (x - cx);
    double fy = (2.0 / height) * (y - cy);
    
    double bias = 0.5 + atan2( fy, fx ) * (1.0 / M_PI);
    
    double dist = fabs(fx) + fabs(fy);
    dist *= 0.5;

    if (dist < reverse_point)
        dist = reverse_point - dist;

    double value = 1.0 - dist;
    
    value = bias * bias_scale + round_scale * value;
    value = std::max( 0.0, std::min( 1.0, value ) );
    
    return value;
}

dot_description desc_offgriddiamond( dot_offgriddiamond, "irregular diamond",
                                3,
                                { "min size", "stone limit", "sprial bias scale" },
                                {  0.40,  0.0,    5.0 },
                                {  0.02,  0.0,    0.0 },
                                {  1.00,  1.0,  100.0 } );

/******************************************************************************/

double dot_offgridtriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    rectFloat offRect;
    
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;
    
    double fx = (x - offRect.left)  / width;
    double fy = (y - offRect.top) / height;
    
    double dist = 0.5 * (fx + fy);
    
    return dist;
}

dot_description desc_offgridtriangle( dot_offgridtriangle, "irregular triangle",
                                1,
                                { "min size" },
                                {  0.40 },
                                {  0.02 },
                                {  1.00 } );

/******************************************************************************/

double dot_offgridround( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const double reverse_point = opt.params[1];
    const double power = 2.0 + opt.params[2];
    const double bias_scale = opt.params[3] / (100.0*M_PI);
    const double round_scale = (1.0-bias_scale);
    const double inverse_power = 1.0 / power;
    rectFloat offRect;
    
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // -1..+1 range
    double cx = 0.5 * (offRect.left + offRect.right);
    double cy = 0.5 * (offRect.top + offRect.bottom);
    
    double fx = (2.0 / width) * (x - cx);
    double fy = (2.0 / height) * (y - cy);
    
    double bias = 0.5 + atan2( fy, fx ) * (1.0 / M_PI);
    
    double dist;

    if (power == 2.0)
        {
        dist = hypot(fx,fy);
        dist *= 1.0 / sqrt(2.0);
        }
    else
        {
        const double scale = 1.0 / pow(2.0, inverse_power );
        dist = pow( fabs(fx), power ) + pow( fabs(fy), power );
        dist = pow( dist, inverse_power );
        dist *= scale;
        }

    if (dist < reverse_point)
        dist = reverse_point - dist;

    double value = 1.0 - dist;
    
    value = bias * bias_scale + round_scale * value;
    value = std::max( 0.0, std::min( 1.0, value ) );
    
    return value;
}

dot_description desc_offgridround( dot_offgridround, "irregular round",
                                4,
                                { "min size", "stone limit", "squareness", "sprial bias scale" },
                                {  0.30,  0.0,   0.0,    5.0 },
                                {  0.02,  0.0,   0.0,    0.0 },
                                {  1.00,  1.0,  10.0,  100.0 } );

/******************************************************************************/

double dot_offgridrandom( double x, double y, int64_t seed, const screen_options &opt )
{
    const double scale = 1.0 / sqrt(2.0);
    const double edge_limit = 0.5 * opt.params[0];
    rectFloat offRect;
    
    pointInt gridPoint = Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // -1..+1 range
    double cx = 0.5 * (offRect.left + offRect.right);
    double cy = 0.5 * (offRect.top + offRect.bottom);
    
    double fx = (2.0 /width) * (x - cx);
    double fy = (2.0 / height) * (y - cy);

    int64_t cell_id = hashXY( gridPoint.x, gridPoint.y, seed );
    double dist = random_dist_func(fx,fy, cell_id, cx, cy, 0.0, 1.0 );
    double value = 1.0 - (dist * scale);
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_offgridrandom( dot_offgridrandom, "irregular random",
                                1,
                                { "min size" },
                                {  0.40, },
                                {  0.02 },
                                {  1.00  } );

/******************************************************************************/

// note - top^3 looks a little more like drips, but boring
double dot_drips( double x, double y, int64_t seed, const screen_options &opt )
{
    const double horiz_scale = opt.params[0];
    const double vert_offset = 0.01 * opt.params[1];    // 0..1.0

    x *= horiz_scale;
    
    double iy = floor(y);
    double fy = y - iy;     // 0..1
    
    double top = NoiseBicubic( x, iy+vert_offset, seed );
    double bottom = NoiseBicubic( x, iy+1.0+vert_offset, seed );

    top = 0.5 * (cos( top * M_PI ) + 1.0);  // 0..1
    bottom = 0.5 * (cos( bottom * M_PI ) + 1.0);  // 0..1
    bottom += 1.0;
    
    if (fy < top)
        {
        // go up one row and recalc limits
        bottom = top;
        top = NoiseBicubic( x, iy-1.0+vert_offset, seed );
        top = 0.5 * (cos( top * M_PI ) + 1.0);  // 0..1
        top -= 1.0;
        }
    
    double range = bottom - top;
    range = std::max( 1.0e-10, range ); // > zero so we don't divide by zero
    
    assert( fy >= top );
    assert( fy <= bottom );

    double value = (fy - top) / range;
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_drips( dot_drips, "paint drips",
                                2,
                                { "drip scale", "smoothness" },
                                {   3.17,    0.0 },
                                {   0.10,    0.0  },
                                { 100.00,  100.0  } );

/******************************************************************************/

// multiple dots in grid
double dot_dotgroup( double x, double y, int64_t seed, const screen_options &opt )
{
    size_t count = round( opt.params[0] );
    double group_radius = 0.5 * opt.params[1];
    const double offset = opt.params[2];

    double ix = floor(x);
    double fx = x - ix - 0.5;
    
    y += offset * ix;
    
    double iy = floor(y);
    double fy = y - iy - 0.5;
    
    // dot in center
    double distance = hypot( fx, fy );
    
    // multiple dots around center
    for (size_t i = 0; i < count; ++i)
        {
        double angle = i * (2.0 * M_PI / count);
        // rearrange so point is always up
        double cy = -group_radius * cos( angle );
        double cx = group_radius * sin( angle );
        double dist = hypot( fx - cx, fy - cy );
        distance = std::min( dist, distance );
        }
    
    double value = 2.2 * distance;
    value = std::min(1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_dotgroup( dot_dotgroup, "dot group",
                                3,
                                { "count", "radius", "column offset" },
                                {   5.0,    0.6,   0.333 },
                                {   3.0,    0.0,   0.0 },
                                {  20.0,    2.0,   1.0 } );

/******************************************************************************/

// multiple dots in grid
double dot_dotgroup2( double x, double y, int64_t seed, const screen_options &opt )
{
    size_t count = round( opt.params[0] );
    double group_radius = 0.5 * opt.params[1];
    const double offset = opt.params[2];

    double ix = floor(x);
    double fx = x - ix - 0.5;
    
    y += offset * ix;
    
    double iy = floor(y);
    double fy = y - iy - 0.5;
    
    // no dot in center
    double distance = 200.0;
    
    // multiple dots around center
    for (size_t i = 0; i < count; ++i)
        {
        double angle = i * (2.0 * M_PI / count);
        // rearrange so point is always up
        double cy = -group_radius * cos( angle );
        double cx = group_radius * sin( angle );
        double dist = hypot( fx - cx, fy - cy );
        distance = std::min( dist, distance );
        }
    
    double value = 2.2 * distance;
    value = std::min(1.0, std::max( 0.0, value ));
    return 1.0 - value;
}

dot_description desc_dotgroup2( dot_dotgroup2, "dot group no center",
                                3,
                                { "count", "radius", "column offset" },
                                {   4.0,    0.6,   0.333 },
                                {   3.0,    0.0,   0.0 },
                                {  20.0,    2.0,   1.0 } );

/******************************************************************************/

// return -1 to +1
inline
double hashXY_offset( int64_t ix, int64_t iy, int64_t seed )
{
    return (2.0 * hashXY_float( ix, iy, seed )) - 1.0;
}

/******************************************************************************/

bool pointInPoly(const int nvert, const double *vertx, const double *verty, double x, double y)
{
  bool inside = false;
  for (int i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>y) != (verty[j]>y)) &&
	 (x < (vertx[j]-vertx[i]) * (y-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       inside = !inside;
  }
  return inside;
}

/******************************************************************************/

inline
bool pointInQuad_inner(const double x, const double y,
                const double x0, const double y0, const double x1, const double y1,
                const double x2, const double y2, const double x3, const double y3 )
{
    bool inside = false;
    if ( ((y0>y) != (y3>y)) &&
        (x < (x3-x0) * (y-y0) / (y3-y0) + x0) )
            inside = !inside;
    if ( ((y1>y) != (y0>y)) &&
        (x < (x0-x1) * (y-y1) / (y0-y1) + x1) )
            inside = !inside;
    if ( ((y2>y) != (y1>y)) &&
        (x < (x1-x2) * (y-y2) / (y1-y2) + x2) )
            inside = !inside;
    if ( ((y3>y) != (y2>y)) &&
        (x < (x2-x3) * (y-y3) / (y2-y3) + x3) )
            inside = !inside;
    return inside;
}

/******************************************************************************/

// y = a*x + b, x = c*y + d ---- fails at vertical, or horizontal
// f = a*x + b*y + c   --- needs normalization, but should give sign easily, f=0 is on line
//          a = (y2-y1), b = (x2-x1), c = x2y1 - y2*x1
// can normalize dividing by hypot(a,b)
inline
double linefunc( double x, double y, double p1x, double p1y, double p2x, double p2y )
{
    double a = p2y - p1y;
    double b = p2x - p1x;
    double c = p2x * p1y - p2y * p1x;   // negative cross product
 //   double c = b * p1y - a * p1x;   // same result values -- just not collecting terms
    return (a * x) - (b * y) + c;   // cross product of point delta minus cross of vector
}

inline
double lineDistNorm( double p1x, double p1y, double p2x, double p2y )
{
    double a = p2y - p1y;
    double b = p2x - p1x;
    return hypot(a,b);
}

/******************************************************************************/

inline
double cross2D( double p1x, double p1y, double p2x, double p2y )
{
    return p1x * p2y - p1y * p2x;
}

/******************************************************************************/

// more or less from https://stackoverflow.com/questions/808441/inverse-bilinear-interpolation
void
inverseLerp2D(  const double x, const double y,
                const double x0, const double y0, const double x1, const double y1,
                const double x2, const double y2, const double x3, const double y3,
                double &s, double &t)
{
    const double epsilon = 1e-10;
    
    s = 0.0;
    t = 0.0;
    
    double a  = cross2D( x0-x, y0-y, x0-x2, y0-y2 );
    double b = 0.5 * ( cross2D( x0-x, y0-y, x1-x3, y1-y3 ) + cross2D( x1-x, y1-y, x0-x2, y0-y2 ) );
    double c  = cross2D( x1-x, y1-y, x1-x3, y1-y3 );

    double denom = a - 2 * b + c;
    
    if ( fabs(denom) < epsilon )
        {
        // denominator is zero, we have a linear solution (and a line instead of a quad)
        double denom2 = a - c;
        if (fabs(denom2) < epsilon)
            s = 0.0;
        else
            s = a / (a-c);
        
        assert( !std::isnan(s) );    // so we can debug
        }
    else
        {
        // quadratic solution
        double tempInner = b*b - a * c; // can be negative if corner points are in wrong order
        assert( tempInner >= 0.0 );
        
        double temp = sqrt( tempInner );
        double s1 = ((a-b) - temp) / denom;
        
        if ( (s1 >= 0.0) && (s1 <= 1.0) )
            s = s1;
        else
            {
            double s2 = ((a-b) + temp) / denom;
            s = s2;
            }
        
        assert( !std::isnan(s) );    // so we can debug
        }

    // now we should have a valid s == dx coordinate
    // use that to solve for t == dy
    double denomX = (1.0-s) * (x0-x2) + s * (x1-x3);
    double denomY = (1.0-s) * (y0-y2) + s * (y1-y3);
    
    assert( (fabs(denomX) >= epsilon) || (fabs(denomY) >= epsilon) );

    // which denominator is more useful?
    if ( fabs( denomX ) > fabs( denomY ) )
        t = ( (1.0-s) * (x0-x) + s * (x1-x) ) / denomX;
    else
        t = ( (1.0-s) * (y0-y) + s * (y1-y) ) / denomY;

    assert( !std::isnan(t) );    // so we can debug
}

/******************************************************************************/

bool
pointInQuad( const double x, const double y, const int64_t seed, const double fx, const double fy, const int64_t ix, const int64_t iy, const double jitterX, const double jitterY,
    double &dx, double &dy )
{
    // check that the search doesn't drift too far away
    assert( fabs(fx - x) <= 5.0 );
    assert( fabs(fy - y) <= 5.0 );

    double ULX = fx + jitterX * hashXY_offset( ix, iy, seed^hashJitterX );
    double ULY = fy + jitterY * hashXY_offset( ix, iy, seed^hashJitterY );

    double URX = fx + 1.0 + jitterX * hashXY_offset( ix+1, iy, seed^hashJitterX );
    double URY = fy + jitterY * hashXY_offset( ix+1, iy, seed^hashJitterY );

    double BLX = fx + jitterX * hashXY_offset( ix, iy+1, seed^hashJitterX );
    double BLY = fy + 1.0 + jitterY * hashXY_offset( ix, iy+1, seed^hashJitterY );

    double BRX = fx + 1.0 + jitterX * hashXY_offset( ix+1, iy+1, seed^hashJitterX );
    double BRY = fy + 1.0 + jitterY * hashXY_offset( ix+1, iy+1, seed^hashJitterY );
    
    // make sure we don't get too degenerate on our quads
    assert( ULY <= BLY );
    assert( URX >= ULX );
    assert( URY <= BRY );
    assert( BRX >= BLX );
    
    assert( ULX >= (fx-0.5) );
    assert( ULX <= (fx+0.5) );
    assert( URX >= (fx+1.0-0.5) );
    assert( URX <= (fx+1.0+0.5) );
    assert( BLX >= (fx-0.5) );
    assert( BLX <= (fx+0.5) );
    assert( BRX >= (fx+1.0-0.5) );
    assert( BRX <= (fx+1.0+0.5) );
    
    assert( ULY >= (fy-0.5) );
    assert( ULY <= (fy+0.5) );
    assert( BLY >= (fy+1.0-0.5) );
    assert( BLY <= (fy+1.0+0.5) );
    assert( URY >= (fy-0.5) );
    assert( URY <= (fy+0.5) );
    assert( BRY >= (fy+1.0-0.5) );
    assert( BRY <= (fy+1.0+0.5) );

    if (!pointInQuad_inner(x, y,
            ULX, ULY, URX, URY,
            BRX, BRY, BLX, BLY ))
        return false;
    
    // now solve for fractional coordinates
    inverseLerp2D( x, y,
            ULX, ULY, URX, URY,
            BLX, BLY, BRX, BRY,
            dx, dy);
    
    return true;
}

/******************************************************************************/

void
jitterGrid_inner(  double x,  double y, const int64_t seed, const screen_options &opt,
                double &dx, double &dy, int64_t &ix, int64_t &iy )
{
    double jitterX = opt.params[0] / 200.0; // so effective jitter range is -0.5 to +0.5
    double jitterY = opt.params[1] / 200.0;
    
    double fx = floor(x);
    double fy = floor(y);
    
    ix = fx;
    iy = fy;
    dx = dy = 0.0;  // safety

    bool found = false;
    
    // simple side test and movement won't work if quads become concave, darnit
    // and jitter around 70% leads to some concave shapes

    // first start with the most likely quad - the one centered on this grid point
    if (pointInQuad(x,y,seed,fx,fy,ix,iy,jitterX,jitterY,dx,dy))
        found = true;
    
    if (!found)
        {
        // try 3x3 area, excluding 0 already tested
        for (int64_t uy = -1; uy <= 1 && !found; ++uy)
            for (int64_t ux = -1; ux <= 1 && !found; ++ux)
                {
                if (ux == 0 && uy == 0)
                    continue;
                
                if (pointInQuad(x,y,seed,fx+ux,fy+uy,ix+ux,iy+uy,jitterX,jitterY,dx,dy))
                    {
                    ix += ux;
                    iy += uy;
                    found = true;
                    }
                }
        }
    
    if (!found)
        {
        // try 5x5 area, excluding 1,0 block already tested
        for (int64_t uy = -2; uy <= 2 && !found; ++uy)
            for (int64_t ux = -2; ux <= 2 && !found; ++ux)
                {
                if ( std::max( std::abs(ux), std::abs(uy) ) <= 1)
                    continue;
                
                if (pointInQuad(x,y,seed,fx+ux,fy+uy,ix+ux,iy+uy,jitterX,jitterY,dx,dy))
                    {
                    ix += ux;
                    iy += uy;
                    found = true;
                    }
                }
        }

    assert( found == true );
    
    return;
}

/******************************************************************************/

double dot_jitterQuads( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);
    
    assert( dx >= 0.0 && dx <= 1.0 );
    assert( dy >= 0.0 && dy <= 1.0 );

    // return ((ix+iy) & 0x01) ? 1.0  : 0.0;     // checkerboard - good for debugging, not for halftoning

    double value = sqrt(std::min( dx, dy ));
    return value;
}

dot_description desc_jitterQuads( dot_jitterQuads, "jittered grid quads",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterQuadRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    int64_t cell_hash = hashXY(ix,iy,seed^extraHash1);
    
    if ((cell_hash & 0x80) == 0)
        dx = 1.0 - dx;

    if ((cell_hash & 0x08) == 0)
        dy = 1.0 - dy;
    
    double value = sqrt(std::min( dx, dy ));
    return value;
}

dot_description desc_jitterQuadsRandom( dot_jitterQuadRandom, "jittered grid quads random",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterTriangle( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    double value = 0.5 * (dx + dy);
    return value;
}

dot_description desc_jitterTriangle( dot_jitterTriangle, "jittered grid triangle",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterTriangleRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);
    
    assert( dx >= 0.0 && dx <= 1.0 );
    assert( dy >= 0.0 && dy <= 1.0 );

    int64_t cell_hash = hashXY(ix,iy,seed^extraHash2);

    if ((cell_hash & 0x80) == 0)
        dx = 1.0 - dx;

    if ((cell_hash & 0x08) == 0)
        dy = 1.0 - dy;

    double value = 0.5 * (dx + dy);
    return value;
}

dot_description desc_jitterTriangleRandom( dot_jitterTriangleRandom, "jittered grid triangle random",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterDiamond( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    double value = (fabs(dx-0.5) + fabs(dy-0.5));
    return value;
}

dot_description desc_jitterDiamond( dot_jitterDiamond, "jittered grid diamond",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterRound( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[2] / (100.0*M_PI);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) * M_SQRT2;
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    double bias = atan2( dy-0.5, dx-0.5 );
    double value = bias_scale * bias + round_scale * hypot( dx-0.5, dy-0.5 );
    value = std::max( 0.0, std::min( 1.0, value ) );
    return 1.0 - value;
}

dot_description desc_jitterRound( dot_jitterRound, "jittered grid round1",
                                3,
                                { "jitterX", "jitterY", "bias scale" },
                                {  60.0,   60.0,    4.4 },
                                {   0.0,    0.0,    0.0  },
                                { 100.0,  100.0,  100.0  } );

/******************************************************************************/

double dot_jitterRound2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double bias_scale = opt.params[2] / (100.0*M_PI);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) * 0.24;

    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    double sx = cos( dx * (2.0*M_PI) );
    double sy = cos( dy * (2.0*M_PI) );

    double bias = atan2( dy-0.5, dx-0.5 );
    double value = 0.5 + bias_scale * bias + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return value;
}

dot_description desc_jitterRound2( dot_jitterRound2, "jittered grid round2",
                                3,
                                { "jitterX", "jitterY", "bias scale" },
                                {  60.0,   40.0,    4.0 },
                                {   0.0,    0.0,    0.0  },
                                { 100.0,  100.0,  100.0  } );

/******************************************************************************/

double dot_jitterEllipse( double x, double y, int64_t seed, const screen_options &opt )
{
    const double EccX = 1.0;
    const double EccY = opt.params[2];
    const double bias_scale = opt.params[3] / (100.0*M_PI);  // with clipping it is safe to vary a bit
    const double round_scale = (1.0-bias_scale) / (2.05*(EccX+EccY));

    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    double sx = EccX * cos( dx * (2.0*M_PI) );
    double sy = EccY * cos( dy * (2.0*M_PI) );

    double bias = atan2( dy-0.5, dx-0.5 );
    double value = 0.5 + bias_scale * bias + round_scale * (sx+sy);
    value = std::max( 0.0, std::min( 1.0, value ) );
    return 1.0-value;
}

dot_description desc_jitterEllipse( dot_jitterEllipse, "jittered grid ellipse",
                                4,
                                { "jitterX", "jitterY", "Eccentricity", "bias scale" },
                                {  60.0,   40.0,   0.70,     4.1 },
                                {   0.0,    0.0,   0.01,     0.0  },
                                { 100.0,  100.0,   4.00,   100.0 } );

/******************************************************************************/

double dot_jitterLine( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);

    return std::max( 0.0, std::min( 1.0, dx ) );
}

dot_description desc_jitterLine( dot_jitterLine, "jittered grid line",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

double dot_jitterLineRandom( double x, double y, int64_t seed, const screen_options &opt )
{
    double dx = 0.0;
    double dy = 0.0;
    int64_t ix = 0;
    int64_t iy = 0;

    jitterGrid_inner(x,y,seed,opt,dx,dy,ix,iy);
    
    assert( dx >= 0.0 && dx <= 1.0 );
    assert( dy >= 0.0 && dy <= 1.0 );

    int64_t cell_hash = hashXY(ix,iy,seed^extraHash2);

    if ((cell_hash & 0x80) == 0)
        dx = 1.0 - dx;

    if ((cell_hash & 0x08) == 0)
        dy = 1.0 - dy;
    
    double value = ((cell_hash & 0x20) == 0) ? dx : dy;
    
    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_jitterLineRandom( dot_jitterLineRandom, "jittered grid line random",
                                2,
                                { "jitterX", "jitterY" },
                                {  60.0,   40.0 },
                                {   0.0,    0.0  },
                                { 100.0,  100.0  } );

/******************************************************************************/

// NEEDS MORE WORK
double dot_heart( double x, double y, int64_t seed, const screen_options &opt )
{
    double ix = floor(x);
    double iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;
    double dy = fy - 0.5; // so we have centered -0.5 to +0.5
    double dx = fx - 0.5;
    
    double r = hypot(dy,dx);

// NEEDS MORE WORK
// need fold that is positive y>0, negative y<0, continuous

    double temp = fabs(dx);
    double fold = temp * sin(dy*2.0*M_PI);
    
    if (dy < 0.0)
        fold *= 2.0;
    
//    double value = 0.5 + 0.5 * fold;
//    double value = r;
    double value = r + 0.5 * fold;


    value = std::min( 1.0, std::max(0.0, value ));
    
    return value;       // 1.0 - value; for final
}

dot_description desc_heart( dot_heart, "hearts" );

/******************************************************************************/

const bool kUseDiagonalScan = true;

static
double microcluster_inner( double x, double y, const int64_t seed, const screen_options &opt,
                        const int divisions, const double offset, dotfunc dot, const bool useDiagonalScan = false )
{
    assert( divisions > 1 );
    assert( divisions < 23 );
    
    const double offset_scale = offset / (divisions*divisions);
    
    const double scale_dot = 1.0 - offset;
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;

    int64_t bigCell = hashXY( ix, iy, seed );

    fx *= divisions;
    fy *= divisions;
    
    int64_t ix2 = floor(fx);
    int64_t iy2 = floor(fy);
    
    double fx2 = fx - ix2;
    double fy2 = fy - iy2;
    
    int64_t innerCell = hashXY( ix2, iy2, seed^extraHash2 );

    int64_t index = 0;
    
    if (!useDiagonalScan)
        {
        // 0101 0202     Z/N raster scan - shows linear artifacts by value, but easy to calculate
        // 2323 1313
        index = iy2 * divisions + ix2;
        //int64_t index = ix2 * divisions + iy2;
        }
    else
        {
        // 0202 0303      X shape-- shows 45 degree artifacts by value
        // 3131 2121
        
        /*
            (d*d)-index)%(d*d)       index reversal plus 1 mod
            index never greater than (d*d), so can conditionally set zero, or subtract
                still leads to linear artifacts!!!
            
            0123 -> 0321
            01  -> 03
            23     21
            
            012345678 -> 087654321
            012 -> 087
            345    654
            678    321
         */
        
        /*
            (y*(d+1) + (x*d)) % (d*d)          offset grid diagonal
            (d-1)*(d+1) + (d-1)*d = d*d+1+d*d-d = 2*d*d +(1-d)
            for d>1, sum never greater than 2*d*d-1, so can just conditionally subtract instead of mod
                shows some artifacts on diagonal
                would be nice to break it up more -- maybe checkerboard direction changes?
            
            0123 -> 0231
            01 -> 02            y=0 -> 0
            23    31            y=1 -> 3
            
            012345678 -> 036471825
            012 -> 036036      y=0 -> 0
            345    471471      y=1 -> 4
            678    825825      y=2 -> 8
            
            0123456789ABCDEF -> 048C59D1AE26F37B
            0123 -> 048C        y=0 -> 0
            4567    59D1        y=1 -> 5
            89AB    AE26        y=2 -> A
            CDEF    F37B        y=3 -> F
         */
         
         /*
            (index*(d+1)) % (d*d)
                also diagonal
            
            0123 -> 0321
            012 345 678 -> 048 372 615
                048048
                372372
                615615
            0123 4567 89AB CDEF -> 05AF 49E3 8D27 C16B
                 05AF
                 49E3
                 8D27
                 C16B
          */
         /*
            (index*(d-1)) % (d*d)
                not good for 2x2
                some diagonal, but less ordered
            
            0123 -> 0123
            012 345 678 -> 024 681 357
                 024024
                 681681
                 357357
            0123 4567 89AB CDEF -> 0369 CF25 8BE1 47AD
                03690369
                CF25CF25
                8BE18BE1
                47AD47AD
          */
         /*
            (index*23) % (d*d)
                little diagonal, but less ordered
            
            0123 -> 0321
            012 345 678 -> 051 627 384
                051051
                627627
                384384
            0123 4567 89AB CDEF -> 07E5 C3A1 8F6D 4B29
                07E5
                C3A1
                8F6D
                4B29
          */
#if 0
        index = iy2 * (divisions+1) + ix2 * divisions;
        if (index >= (divisions*divisions))
            index -= (divisions*divisions);
#else
        index = iy2 * divisions + ix2;
        //index = index * (divisions-1);    // fewer diagonals, but still shows some
        index = index * (23);               // best so far
        index = index % (divisions*divisions);
#endif
        }
    
    assert( index >= 0 && index < (divisions*divisions) );

    return index * offset_scale + scale_dot * dot(fx2,fy2,seed^innerCell^bigCell,opt);
}

/******************************************************************************/

// similar to microcluster, but uses normal dot grid
static
double macrocluster_inner( double x, double y, const int64_t seed, const screen_options &opt,
                        const int divisions, const double offset, dotfunc dot )
{
    assert( divisions > 1 );
    assert( divisions < 23 );
    
    const double offset_scale = offset / (divisions*divisions);
    
    const double scale_dot = 1.0 - offset;
    
    int64_t ix = floor(x);
    int64_t iy = floor(y);
    
    double fx = x - ix;
    double fy = y - iy;

    int64_t bigCell = hashXY( ix, iy, seed );
    
    int64_t ix2 = ix % divisions;
    int64_t iy2 = iy % divisions;
    
    if (ix2 < 0)
        ix2 += divisions;
    if (iy2 < 0)
        iy2 += divisions;

    // 0202 0303      X shape-- shows 45 degree artifacts by value
    // 3131 2121
    
    int64_t index = iy2 * divisions + ix2;
    //index = index * (divisions-1);    // fewer diagonals, but still shows some
    index = index * (23);               // best so far
    index = index % (divisions*divisions);
    
    assert( index >= 0 && index < (divisions*divisions) );

    return index * offset_scale + scale_dot * dot(fx,fy,bigCell,opt);
}

/******************************************************************************/

// circle dot, cluster 2x2
double dot_micro_circle( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 2;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_circle,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_circle( dot_micro_circle, "microcluster2 circle" );

/******************************************************************************/

// round dot, cluster 2x2
double dot_micro_round( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 2;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_round( dot_micro_round, "microcluster2 round" );

/******************************************************************************/

// ellipse dot, cluster 2x2
double dot_micro_ellipse( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 2;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_ellipse,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_ellipse( dot_micro_ellipse, "microcluster2 ellipse",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// circle dot, cluster 3x3
double dot_micro_circle3( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 3;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_circle,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_circle3( dot_micro_circle3, "microcluster3 circle" );

/******************************************************************************/

// round dot, cluster 3x3
double dot_micro_round3( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 3;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_round3( dot_micro_round3, "microcluster3 round" );

/******************************************************************************/

// ellipse dot, cluster 3x3
double dot_micro_ellipse3( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 3;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_ellipse,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_ellipse3( dot_micro_ellipse3, "microcluster3 ellipse",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

// circle dot, cluster 4x4
double dot_micro_circle4( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 4;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_circle,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_circle4( dot_micro_circle4, "microcluster4 circle" );

/******************************************************************************/

// round dot, cluster 4x4
double dot_micro_round4( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 4;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_round4( dot_micro_round4, "microcluster4 round" );

/******************************************************************************/

// ellipse dot, cluster 4x4
double dot_micro_ellipse4( double x, double y, int64_t seed, const screen_options &opt )
{
    const int divisions = 4;
    const double offset = 0.04;

    double value = microcluster_inner(x,y,seed,opt,divisions,offset,dot_ellipse,kUseDiagonalScan);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_micro_ellipse4( dot_micro_ellipse4, "microcluster4 ellipse",
                                1,
                                { "Eccentricity" },
                                { 0.7 },
                                { 0.01 },
                                { 4.0 } );

/******************************************************************************/

double dot_offgrid_micro_round2( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const int divisions = 2;
    const double offset = 0.04;

    rectFloat offRect;
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // 0..1 range + 0.25 to remove dots from edges (1.0/(2.0*divisions))
    double fx = 0.25 + (x - offRect.left) / width;
    double fy = 0.25 + (y - offRect.top) / height;

    double value = microcluster_inner(fx,fy,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_offgrid_micro_round2( dot_offgrid_micro_round2, "irregular microcluster2 round",
                                1,
                                { "min size" },
                                {  0.40, },
                                {  0.02 },
                                {  1.00  } );

/******************************************************************************/

double dot_offgrid_micro_round3( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const int divisions = 3;
    const double offset = 0.04;

    rectFloat offRect;
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // 0..1 range + 1/6 to remove dots from edges (1.0/(2.0*divisions))
    double fx = (1.0/6.0) + (x - offRect.left) / width;
    double fy = (1.0/6.0) + (y - offRect.top) / height;

    double value = microcluster_inner(fx,fy,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_offgrid_micro_round3( dot_offgrid_micro_round3, "irregular microcluster3 round",
                                1,
                                { "min size" },
                                {  0.40, },
                                {  0.02 },
                                {  1.00  } );

/******************************************************************************/

double dot_offgrid_micro_round4( double x, double y, int64_t seed, const screen_options &opt )
{
    const double edge_limit = 0.5 * opt.params[0];
    const int divisions = 4;
    const double offset = 0.04;

    rectFloat offRect;
    (void) Offgrid_PointToCell( x, y, seed, edge_limit, offRect );
    
    double width = offRect.right - offRect.left;
    double height = offRect.bottom - offRect.top;

    // 0..1 range + 0.125 to remove dots from edges (1.0/(2.0*divisions))
    double fx = 0.125 + (x - offRect.left) / width;
    double fy = 0.125 + (y - offRect.top) / height;

    double value = microcluster_inner(fx,fy,seed,opt,divisions,offset,dot_round,kUseDiagonalScan);
    
    value = std::min(1.0, std::max( 0.0, value ));
    return value;
}

dot_description desc_offgrid_micro_round4( dot_offgrid_micro_round4, "irregular microcluster4 round",
                                1,
                                { "min size" },
                                {  0.40, },
                                {  0.02 },
                                {  1.00  } );

/******************************************************************************/

// circle dot, macro cluster
double dot_macro_circle( double x, double y, int64_t seed, const screen_options &opt )
{
    const double offset = (1.0/100.0) * opt.params[0];
    const int divisions = round( opt.params[1] );

    double value = macrocluster_inner(x,y,seed,opt,divisions,offset,dot_circle);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_macro_circle( dot_macro_circle, "macrocluster circle",
                                2,
                                { "offset scale", "divisions" },
                                {    4.0,   2.0 },
                                {    0.0,   2.0 },
                                {  100.0,  20.0 } );

/******************************************************************************/

// round dot, macro cluster
double dot_macro_round( double x, double y, int64_t seed, const screen_options &opt )
{
    const double offset = (1.0/100.0) * opt.params[0];
    const int divisions = round( opt.params[1] );

    double value = macrocluster_inner(x,y,seed,opt,divisions,offset,dot_round);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_macro_round( dot_macro_round, "macrocluster round",
                                2,
                                { "offset scale", "divisions" },
                                {    4.0,   2.0 },
                                {    0.0,   2.0 },
                                {  100.0,  20.0 } );

/******************************************************************************/

// ellipse dot, macro cluster
double dot_macro_ellipse( double x, double y, int64_t seed, const screen_options &opt )
{
    const double offset = (1.0/100.0) * opt.params[1];
    const int divisions = round( opt.params[2] );

    double value = macrocluster_inner(x,y,seed,opt,divisions,offset,dot_ellipse);

    return std::max( 0.0, std::min( 1.0, value ) );
}

dot_description desc_macro_ellipse( dot_macro_ellipse, "macrocluster ellipse",
                                3,
                                { "Eccentricity", "offset scale", "divisions" },
                                { 0.70,    4.0,   2.0 },
                                { 0.01,    0.0,   2.0 },
                                { 4.00,  100.0,  20.0 } );

// offgrid already changes the size of each dot, distributing the quantization levels fairly randomly. So macro clusters don't make sense for it.

/******************************************************************************/

// TODO -- more organized speedlines?  clusters
// TODO -- more regular speedlines, with dropouts?
// TODO - bomb burst / warp trails - similar to radial, but with many shorter lines, flare at outward end

// TODO - general aperiodic tiles (projection)
// https://www.gregegan.net/APPLETS/12/deBruijnNotes.html

// TODO - scribbles, single line thickening or many overlapping?
// sinusoid with offset to form sort of a line, overlap mulitple?   similar to random lines
// could localize with bezier segments

// TODO - heart shape

// TODO - concentric dots with damage (damage, spill, bleach)

// TODO - concentric scales (overlap angularly, maybe radial as well)

// TODO - concentric speed lines (break alsong angle(s))

// Other cell/stipple patterns?  Concave shapes?
// Partial moon? Difference of circles?

// symmetric pairs of lines displaced in or outward in patterns - similar to Chinese palace window decorations
// random or with some global pattern?  How to keep line segments connected?

/* samekomon - sharkskin dots (semi-circle overlap)
    test for semi-circle placement, then concentric based on that (offset by center of semi-circle)
 tile has circle origins at corners and center
 rlimit = tile height
 test center, LR, LL, center-rlimit with rlimit to get layering right
    common patterns seem to be 10 dots / rlimit
    width is not a simple multiple - about 190/112 =- 1.69642857
        (2*rlimit) - 1 cell for overlap?
*/

// TODO - bitmap sampled screen (which can also do duty with rasterized vector shapes)
    // needs preprocessing on the bitmap image -- EDM?
    // needs interpolation of bitmap coordinates
    // can I rotate the image within the cell as well as the entire screen? Probably only if the bitmap fades to black on edges.  But even returning black outside boundary would work.
    // Or go modulo?  (likely to cause artifacts)

/******************************************************************************/
/******************************************************************************/

typedef int64_t mappingxy ( int64_t x, int64_t y, int64_t d );
typedef int64_t mappingx ( int64_t x, int64_t d );

/******************************************************************************/

// diagonal mapping of square
// works for all d
static
int64_t mapping1_xy( int64_t x, int64_t y, int64_t d )
{
    int64_t index = y * (d+1) + x * d;
#if 0
    index = index % (d*d);
#else
    if (index >= (d*d))
        index -= (d*d);
#endif
    return index;
}

// works for all d
static
int64_t mapping1_x( int64_t x, int64_t d )
{
    int64_t index = (x/d) * (d+1) + (x%d) * d;
#if 0
    index = index % (d*d);
#else
    if (index >= (d*d))
        index -= (d*d);
#endif
    return index;
}

/******************************************************************************/

// works for all d
static
int64_t mapping2_x( int64_t x, int64_t d )
{
    int64_t index = x*(d+1);
    index = index % (d*d);
    return index;
}

/******************************************************************************/

// works for all d
static
int64_t mapping3_x( int64_t x, int64_t d )
{
    int64_t index = x*(d-1);
    index = index % (d*d);
    return index;
}

/******************************************************************************/

// only works for d != N*23, so just use d<23
static
int64_t mapping4_x( int64_t x, int64_t d )
{
    int64_t index = x*(23);
    index = index % (d*d);
    return index;
}

/******************************************************************************/

static
bool unit_test_one_mappingxy( mappingxy mapfunc, const std::string label, int64_t dlimit = 0 )
{
    if (dlimit == 0)
        dlimit = 50;
    
    for (int64_t d = 1; d < dlimit; ++d)
        {
        std::vector<uint8_t> index_used( d*d );
        std::fill( index_used.begin(), index_used.end(), 0 );
        
        for ( int64_t y = 0; y < d; ++y )
            for (int64_t x = 0; x < d; ++x)
                {
                int64_t index = mapfunc(x,y,d);
                assert( index >= 0 );
                assert( index < d*d );
                index_used[index] ++;
                }
        
        // make sure all indices are used only once
        for (auto &k: index_used)
            {
            if (k != 1) {
                std::cout << "error in mapping " << label << ", d=" << d << "\n";
                return false;
                }
            }
        }
    
    return true;
}
/******************************************************************************/

static
bool unit_test_one_mappingx( mappingx mapfunc, const std::string label, int64_t dlimit = 0 )
{
    if (dlimit == 0)
        dlimit = 50;

    for (int64_t d = 1; d < dlimit; ++d)
        {
        std::vector<uint8_t> index_used( d*d );
        std::fill( index_used.begin(), index_used.end(), 0 );
        
        for (int64_t x = 0; x < d*d; ++x)
            {
            int64_t index = mapfunc(x,d);
            assert( index >= 0 );
            assert( index < d*d );
            index_used[index] ++;
            }
        
        // make sure all indices are used only once
        for (auto &k: index_used)
            {
            if (k != 1) {
                std::cout << "error in mapping " << label << ", d=" << d << "\n";
                return false;
                }
            }
        }
    
    return true;
}

/******************************************************************************/

void unit_test_halftone()
{
#if DEBUG
    // permutations of indices modulo divison*division
    // currently used by microcluster code
    unit_test_one_mappingxy( mapping1_xy, "mapping1_xy" );
    
    unit_test_one_mappingx( mapping1_x, "mapping1_x" );
    unit_test_one_mappingx( mapping2_x, "mapping2_x" );
    unit_test_one_mappingx( mapping3_x, "mapping3_x" );
    unit_test_one_mappingx( mapping4_x, "mapping4_x", 23 );
#endif

}

/******************************************************************************/
/******************************************************************************/

// Our master list of dot function descriptions.
// This could also be broken down into classes of halftones (dots, lines, manga, creative, compound, other)
std::vector<dot_description> dotList = {

#if 1

#if DEBUG || LEARNING
    desc_threshold,
    desc_random,
    
    desc_roundhalf,
    desc_roundqurter,
    desc_roundlinear,
    desc_roundbilinear,
#endif
    
    desc_line,
    desc_line_symmetric,
    desc_sawblade,
    desc_sawtooth,
    desc_wave,
    desc_concentric,
    desc_concentricwaves,
    desc_spiralline,
    desc_radial,
    desc_radial2,
    desc_radial3,
    desc_radialzigzag,
    desc_radialzigzag2,
    desc_swirl,
    desc_swirl2,
    desc_waveyline,     // can be slow
    desc_waveygrid,     // can be slow
    desc_waveygrid2,    // can be slow
    desc_herringbone,   // can be slow
    desc_bentgrids,     // can be slow
    desc_bentgrids2,    // can be slow
    desc_strokerandom,
    desc_linegradient,
    desc_warpedline,
    desc_jaggedline,        // kinda slow
    desc_zigzag,
    desc_weave,
    desc_weavealt,
    desc_weave2,            // slow
    desc_dashdot,
    desc_drips,
    desc_radial_moire_linear,
    desc_radial_moire_circle,
    desc_radial_moire_random,
    desc_concentric_moire_linear,
    desc_concentric_moire_circle,
    desc_concentric_moire_random,
    desc_concentriPoly,
    desc_radialPoly,
    
    desc_round,
    desc_roundspiral,
    desc_euclidean,
    desc_circle,
    desc_squircle,
    desc_multiround,
    desc_cone,
    desc_ellipse,
    desc_ellipsespiral,
    desc_ellipserandom,
    desc_ellipse45,
    desc_ellipse45random,
    desc_ellipse45alt,
    desc_ellipse45alt2,
    desc_coildot,
    desc_coildot2,
    desc_coildot3,
    desc_semicircle,
    desc_semicirclerand,
    desc_semicirclerand2,
    desc_semicirclerand3,
    desc_semicircle2,
    desc_semicircle2rand,
    desc_semicircle2rand2,
    desc_semiround,
    desc_semiroundalt,
    desc_semiroundrandom,
    desc_semiroundrandom2,
    desc_semiroundrandom3,
    desc_square,
    desc_square2,
    desc_square3,
    desc_squarerandom,
    desc_cross,
    desc_cross45,
    desc_diamond,
    desc_diamondspiral,
    desc_diamond2,
    desc_diamondalt,
    desc_diamondconcave,
    desc_diamondconcavealt,
    desc_triangle,
    desc_triangle2,
    desc_triangle3,
    desc_trianglerandom,
    desc_corners,
    desc_corners2,
    desc_corners3,
    desc_hexagon,
    desc_Ngon,
    desc_stars,
    desc_splats,
    desc_torus,
    desc_torus2,
    desc_torus3,
    desc_bulllseye,
    desc_wedge,
    desc_angle,
    desc_angle45,
    desc_anglerandom,
    desc_checkers,
    desc_checkers2,
    desc_hexagons,
    desc_lineRandom,
    desc_lineRandom2,
    desc_line_symmetric_random,
    desc_line_symmetric_random2,
    desc_quartercircle,
    desc_quartercirclerandom,
    desc_quartercirclerandom2,
    desc_quartercirclealt1,
    desc_quartercirclealt2,
    desc_petal,
    desc_petalalt,
    desc_splitcircle,
    desc_splitcircleAlt,
    desc_splitcircleAngle,
    desc_splitcircleAngleAlt,
    desc_splitsquare,
    desc_splitsquareAlt,
    desc_splitsquareAngle,
    desc_splitsquareAngleAlt,
    desc_splitdiamond,
    desc_splitdiamondAlt,
    desc_splitdiamondAngle,
    desc_splitdiamondAngleAlt,
    desc_splittriangle,
    desc_splittriangleAlt,
    desc_splithexagon,
    desc_splithexagonAlt,
    desc_splithexagonAngle,
    desc_splithexagonAngleAlt,
    desc_dotgroup,
    desc_dotgroup2,
    desc_roundgrain1,
    desc_roundgrain2,
    
    desc_duallines,
    desc_hexlines,
    desc_quadlines,
    desc_Nlines,
    desc_moirelines,
    desc_moiredots,
    desc_moiredots4,
    desc_multiline,
    desc_multiline2,
    desc_multiline3,
    desc_multilinedash,
    desc_multilinedots,
    desc_brokenline,
    desc_rope,
    desc_rope2,
    desc_stipple,               // slow
    desc_stipplediamond,        // slow
    desc_stippleconcavediamond, // slow
    desc_stipplering,           // slow
    desc_stipplesquares,        // slow
    desc_stippletriangles,      // slow
    desc_stipplehexagons,       // slow
    desc_stippleNgons,          // extra slow
    desc_stipplestars,          // extra slow
    desc_jitteredDot,
    desc_jitteredDiamond,       // TODO - "jittered" or "scattered"? solid versus open?
    desc_jitteredConcaveDiamond,
    desc_jitteredTriangle,
    desc_jitteredHexagon,
    desc_jitteredNgon,      // can be slow
    desc_jitteredStar,      // can be slow
    desc_speedline,
    desc_radialburst,
    desc_radialspeedline,
    desc_OffsetLines,
    desc_OffsetLines2,
    desc_randomlines,       // can be slow
    desc_randomwaves,       // can be slow
    desc_randomwaves2,      // can be slow
    desc_circletri,
    desc_circleline,
    desc_circlecross,
    desc_circlesquare,
    desc_circleconcentric,
    desc_circleangle,
    desc_circletorus,
    desc_circlediagonal,
    desc_circlesemicircle,
    desc_circletri2,
    desc_circleline2,
    desc_circlecross2,
    desc_circlesquare2,
    desc_circleconcentric2,
    desc_circleangle2,
    desc_circletorus2,
    desc_circlediagonal2,
    desc_circlesemicircle2,
    desc_crosstorus2,           // XOXO
    desc_circlelineRand,
    desc_circlediagonalRand,
    desc_cross45torusRand,
    desc_randomgridoverlap,      // slow
    desc_randomgridcover,        // slow
    desc_randomgridmulti,        // slow
    desc_ringsscatter,
    desc_squarescatter,
    desc_concavediamondscatter,
    desc_trianglescatter,
    desc_hexagonscatter,
    desc_hexagonscatter,
    desc_Ngonscatter,        // slow
    desc_starscatter,        // slow
    desc_fishscales,
    desc_lizardscales,
    desc_overlapscale,
    desc_ringsoverlap,
    
    desc_cloth,             // kinda slow
    desc_warpweft,          // kinda slow
    desc_mokumegane,        // kinda slow
    desc_marble,            // kinda slow
    desc_granite,           // kinda slow
    desc_linetwisted,       // semi-slow
    desc_concentricdistorted,   // kinda slow
    desc_radialdistorted,   // kinda slow
    desc_randomangle,       // kinda slow
    desc_hatched,
    desc_diagonals,
    desc_semicircles,
    desc_randomSquares,
    desc_randomCircles,
    desc_randomRound,
    desc_brick,
    desc_herringbonetile,
    desc_herringbonetile2,
    desc_windmilltile,
    desc_chevrontile,
    desc_rounddistorted,     // slow
    desc_rounddistorted2,   // kinda slow
    desc_circledamage,
    desc_squaredamage,
    desc_diamonddamage,
    desc_ellipsedamage,
    desc_ellipsedamage2,
    desc_ellipsedamage45,
    desc_ellipsedamage45random,
    desc_linedamage,
    desc_concentricdots,
    desc_concentricdiamonds,
    desc_concentricsquares,
    desc_concentriccrosses,
    desc_concentricradial,
    desc_concentricdash,
    desc_concentricrandom,
    desc_spiraldots,
    desc_spiraldiamonds,
    desc_spiralsquares,
    desc_spiralcrosses,
    desc_spiralrandom,
    desc_tunnelline,
    desc_tunneldots,
    desc_tunneldiamonds,
    desc_tunnelsquares,
    desc_tunnelradial,
    desc_tunneltriangle,
    desc_tunnelhexagon,
    desc_perspectiveLineHoriz,
    desc_perspectiveLineVertical,
    desc_perspectiveDots,
    desc_perspectiveDiamonds,
    desc_perspectiveSquares,
    desc_perspectiveCrosses,
    desc_perspectiveHex,
    desc_perspectiveTriangle,
    desc_perspectiveTriangle2,
    desc_perspectiveRandom,
    desc_randomshape,
    desc_cuneiform,
    desc_linedot,
    desc_linebox,
    desc_linedash,
    desc_squareinsquare,
    desc_mezzotint,
    desc_noise_stretched,
    desc_grain,
    desc_moss,
    desc_smoke,
    desc_spill,
    desc_spill2,
    desc_spill3,
    desc_noise_cross,
    desc_paper_fiber,
    desc_etched,
    desc_ripples,
    desc_offgridlines,
    desc_offgridlines2,
    desc_offgridlinesRandom,
    desc_offgridrect,
    desc_offgriddiamond,
    desc_offgridtriangle,
    desc_offgridround,
    desc_offgridrandom,
    desc_confettiline,
    desc_confettiline2,
    desc_confettiline3,
    desc_confettiwedge,
    desc_confettiV,
    desc_alignedV,
    desc_cellwalls,
    desc_subgrid,
    desc_multispiral,
    desc_slowdash,
    desc_slowtriangle,
    desc_conglomerate,
    desc_conglomerate2,
    desc_conglomerate3,
    desc_conglomerate4,
    desc_conglomerate5,
    desc_conglomerate6,
    desc_jitterQuads,
    desc_jitterQuadsRandom,
    desc_jitterDiamond,
    desc_jitterLine,
    desc_jitterLineRandom,
    desc_jitterTriangle,
    desc_jitterTriangleRandom,
    desc_jitterRound,
    desc_jitterRound2,
    desc_jitterEllipse,
    
    desc_circlesubdivide,
    desc_circlesubdivide2,
    desc_circlesubdivide3,
    desc_squaresubdivide,
    desc_squaresubdivide2,
    desc_squaresubdivide3,
    desc_diamondsubdivide,
    desc_diamondsubdivide2,
    desc_diamondsubdivide3,
    desc_ellipsesubdivide,
    desc_ellipsesubdivide2,
    desc_ellipsesubdivide3,
    desc_ellipsesubdivideAlt,
    desc_ellipsesubdivide2Alt,
    desc_ellipsesubdivide3Alt,
    desc_ellipsesubdivideAlt2,
    desc_ellipsesubdivide2Alt2,
    desc_ellipsesubdivide3Alt2,
    desc_randomsubdivide,
    desc_randomsubdivide2,
    desc_randomsubdivide3,
    
    desc_macro_circle,
    desc_macro_round,
    desc_macro_ellipse,
    desc_micro_circle,
    desc_micro_round,
    desc_micro_ellipse,
    desc_micro_circle3,
    desc_micro_round3,
    desc_micro_ellipse3,
    desc_micro_circle4,
    desc_micro_round4,
    desc_micro_ellipse4,
    desc_offgrid_micro_round2,
    desc_offgrid_micro_round3,
    desc_offgrid_micro_round4,

#endif


//    desc_samekomon,       // sigh, still haven't gotten it right
//    desc_heart,           // IN PROGRESS


};

/******************************************************************************/

// a subset of halftone methods to be profiled with power spectra
std::vector<dot_description> power_halftone_list = {

    desc_round,
    desc_roundspiral,
    desc_ellipse,
    desc_ellipsespiral,
    desc_offgridround,
    desc_coildot3,

#if 0
// want to study
    desc_jitterRound2,
    desc_jitterEllipse,
    
    desc_macro_circle,
    desc_macro_round,
    desc_macro_ellipse,

    desc_micro_circle,
    desc_micro_round,
    desc_micro_ellipse,
    
    desc_micro_circle3,
    desc_micro_round3,
    desc_micro_ellipse3,
    
    desc_micro_circle4,
    desc_micro_round4,
    desc_micro_ellipse4,

    desc_roundgrain1,
    desc_roundgrain2,
#endif

};

/******************************************************************************/
/******************************************************************************/
