//
//  main.cpp
//  Halftone Tools
//
//  Created by Chris Cox on June 5, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

/*

Background info:
https://en.wikipedia.org/wiki/Halftone
https://en.wikipedia.org/wiki/Dither
https://www.getty.edu/conservation/publications_resources/pdf_publications/pdf/atlas_halftone.pdf





Screen preset: line count/lpi, angle, offset/center, invert, dither, jitter, random seed?, independent of dot settings
    **** Could add additive and multiplicative noise to all screens, and thus to presets
    **** Could distortion also be part of screen settings? Would need octave and scale controls. Probably not a good idea.

Dot/Shape preset: controls shape of dot/pattern, independent of screen settings (but uses random seed)

 */

#include <cstdio>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include "hash.hpp"
#include "image.hpp"
#include "halftone.hpp"
#include "dither.hpp"
#include "spectra.hpp"
using namespace std;

/******************************************************************************/
/******************************************************************************/

int64_t shared_seed = hash64((int64_t)0x8675309); // totally random number ;-)
size_t width = 1000;        // reasonable size for tests and examples
size_t height = 1000;
double image_ppi = 96.0;  // display resolution, more or less
double screen_lpi = 6.36;  // coarse screen for demonstrating dot shapes
bool doExamples = true;
bool doSines = false;
bool doDocumentation = false;
bool doDensityChart = false;
bool doPowerSpectra = false;
const char *halftone_algorithm = NULL;
double screen_angle = 0.0;         // default is angle 0 (putting most things vertical)
double screen_offsetX = 0.0;       // default is centered
double screen_offsetY = 0.0;       // default is centered
bool dither = false;
bool invert = false;
double jitter = 0.0;
    // TODO - additive noise size, additive noise amplitude
    // TODO - mult noise size, mult noise amplitude


vector<string> infiles;

/******************************************************************************/
/******************************************************************************/

void RunAllAlgorithms( const image &input, const char *input_name, const int64_t seed, const geom_transform &trans, bool saveScreen = true )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    
    size_t width = input.width;
    size_t height = input.height;

    // iterate over all dot functions and output screen examples
    for ( auto &desc : dotList )
        {
        screen_options opts ( desc );
        opts.invert = invert;
        opts.dither = dither;
        opts.jitter = jitter;

        image screen;
        CreateScreenGeneral( screen, width, height, trans, desc.dot, seed, opts );
        if (saveScreen)
            {
            snprintf( out_name, name_limit, "screen_%s.pgm", desc.name );
            screen.WritePGM( out_name );
            }
        
        image output;
        input.ApplyThresholdScreen( screen, output );
        snprintf( out_name, name_limit, "%s_%s.pbm", input_name, desc.name );
        output.WritePBM( out_name );
        }

    // iterate over all dither functions and output examples
    for ( auto &desc : ditherList )
        {
        dither_options opts ( desc );
        image output;
        desc.func( output, input, seed, opts );
        snprintf( out_name, name_limit, "%s_dither_%s.pbm", input_name, desc.name );
        output.WritePBM( out_name );
        }
}

/******************************************************************************/

void DoGradientExamples( const size_t width, const size_t height, const int64_t seed, const geom_transform &trans )
{
    // create gradient example image
    image gradient( width, height );
    gradient.RenderGradientQuarters();
    gradient.WritePGM( "gradient_input.pgm" );
    
    RunAllAlgorithms( gradient, "gradient", seed, trans );
}

/******************************************************************************/

void DoSineExamples( const size_t width, const size_t height, const int64_t seed, const geom_transform &trans )
{
    // create sine example image
    image sines( width, height );
    sines.RenderSinesQuarters();
    sines.WritePGM( "sines_input.pgm" );
    
    RunAllAlgorithms( sines, "sines", seed, trans );
}

/******************************************************************************/

void CreateDensityChart( double image_ppi )
{
    // target 8 inches square for now
    // code does support non-square sizes
    size_t size = 8 * image_ppi;
    image density( size, size );
    density.RenderDensityTestChart( image_ppi );
    density.WritePGM( "density_chart.pgm" );
}

/******************************************************************************/

void DoDocumentationExamples( const int64_t seed, const geom_transform &trans )
{
#if XCODE_BUILD
    const char *text_path = "../../testfiles/documentation_strip_text.pgm";
#else
    const char *text_path = "./testfiles/documentation_strip_text.pgm";
#endif

    // create documentation example strip
    image gradient( 800, 150 );
    gradient.RenderDocumentationQuarters();
    gradient.WritePGM( "documentation_input.pgm" );
    
    // read prepared strip with text added
    image documentation_strip;
    documentation_strip.ReadPGM( text_path );
    
    RunAllAlgorithms( documentation_strip, "doc", seed, trans, false );
}

/******************************************************************************/


void RunNamedAlgorithm( const char *algorithm, const image &input, const char *input_name, const int64_t seed, const geom_transform &trans )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    
    size_t width = input.width;
    size_t height = input.height;
    
    if ( strcmp(algorithm,"all") == 0 || strcmp(algorithm,"All") == 0 )
        {
        RunAllAlgorithms( input, input_name, seed, trans, false );
        return;
        }
    
    bool found = false;

// TODO - this only works as long as the names are not localized!
// this will need a more robust solution for localization

    // find this named algorithm among halftone screen functions, run it
    for ( auto &desc : dotList )
        {
        if ( strcmp( desc.name, algorithm ) != 0 )
            continue;
        
        found = true;

        screen_options opts ( desc );
        
        image screen;
        CreateScreenGeneral( screen, width, height, trans, desc.dot, seed, opts );
//        snprintf( out_name, name_limit, "screen_%s.pgm", desc.name );
//        screen.WritePGM( out_name );
        
        image output;
        input.ApplyThresholdScreen( screen, output );
        snprintf( out_name, name_limit, "%s_%s.pbm", input_name, desc.name );
        output.WritePBM( out_name );
        
        break;
        }


    // find this named algorithm among dither functions, run it
    for ( auto &desc : ditherList )
        {
        if ( strcmp( desc.name, algorithm ) != 0 )
            continue;
        
        found = true;
        
        dither_options opts ( desc );
        image output;
        desc.func( output, input, seed, opts );
        snprintf( out_name, name_limit, "%s_dither_%s.pbm", input_name, desc.name );
        output.WritePBM( out_name );
        
        break;
        }
    
    if (!found)
        {
        cout << "Could not find algorithm named: " << algorithm << "\n";
        }
}

/******************************************************************************/
/******************************************************************************/

static
void print_usage(const char *argv[])
{
    cout << "Usage: " << argv[0] << " <args> input_files\n";
    cout << "\t-seed S     Random number seed (default " << shared_seed << ")\n";
    cout << "\t-width D    Width of example output files (default " << width << ")\n";
    cout << "\t-height D   Height of example output files (default " << height << ")\n";
    cout << "\t-ppi F      Pixels Per Inch of input image (default " << image_ppi << ")\n";
    cout << "\t-ppc(m) F   Pixels Per Centimeter of input image (default " << image_ppi/cm_per_inch << ")\n";
    cout << "\t-lpi F      Lines Per Inch of halftone screen (default " << screen_lpi << ")\n";
    cout << "\t-lpc(m) F   Lines Per Centimeter of halftone screen  (default " << screen_lpi/cm_per_inch << ")\n";
    cout << "\t-alg(orithm) A    Name of halftone algorithm to use (default: all)\n";
    cout << "\t-rot(ation) A     Screen rotation angle in degrees (default: " << screen_angle << " )\n";
    cout << "\t-offsetX Off      Screen offset from center, in output pixels (default: " << screen_offsetX << " )\n";
    cout << "\t-offsetY Off      Screen offset from center, in output pixels (default: " << screen_offsetY << " )\n";
    
    cout << "\t-examples E       Should it output gradient examples of all algorithms (default: " << (doExamples ? "true" : "false") << ")\n";
    cout << "\t-sines S          Should it output sine examples of all algorithms (default: " << (doSines ? "true" : "false") << ")\n";
    cout << "\t-densitychart E   Should it output density test chart (default: " << (doDensityChart ? "true" : "false") << ")\n";
    cout << "\t-documentation E  Should it output documentation images (default: " << (doDocumentation ? "true" : "false") << ")\n";
    cout << "\t-dopower P        Should it output power spectra for selected algorithms (default: " << (doPowerSpectra ? "true" : "false") << ")\n";
    cout << "\t-dither D         Should halftone screens be dithered (default: " << (dither ? "true" : "false") << ")\n";
    cout << "\t-jitter P         Percentage jitter for samples in halftone screens (default: " << jitter << ")\n";
    cout << "\t-invert I         Should halftone screens be inverted (default: " << (invert ? "true" : "false") << ")\n";
}

/******************************************************************************/

static
void parse_arguments( int argc, const char *argv[] )
{
    for ( int c = 1; c < argc; ++c )
        {
        
        if ( strcmp( argv[c], "-seed" ) == 0
            && c < (argc-1) )
            {
            int64_t temp = (int64_t)atoll( argv[c+1] );
            shared_seed = hash64(temp);
            ++c;
            }
        else if ( strcmp( argv[c], "-width" ) == 0
            && c < (argc-1) )
            {
            width = (size_t)atoll( argv[c+1] );
            ++c;
            }
        else if ( strcmp( argv[c], "-height" ) == 0
            && c < (argc-1) )
            {
            height = (size_t)atoll( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-alg" ) == 0
                || strcmp( argv[c], "-algorithm" ) == 0)
            && c < (argc-1) )
            {
            halftone_algorithm = argv[c+1];
            ++c;
            }
        else if ( strcmp( argv[c], "-ppi" ) == 0
            && c < (argc-1) )
            {
            image_ppi = atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-ppc" ) == 0
                || strcmp( argv[c], "-ppcm" ) == 0)
            && c < (argc-1) )
            {
            image_ppi = atof( argv[c+1] ) / cm_per_inch;
            ++c;
            }
        else if ( (strcmp( argv[c], "-ppmm" ) == 0)
            && c < (argc-1) )
            {
            image_ppi = atof( argv[c+1] ) / mm_per_inch;
            ++c;
            }
        else if ( strcmp( argv[c], "-lpi" ) == 0
            && c < (argc-1) )
            {
            screen_lpi = atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-lpc" ) == 0
                || strcmp( argv[c], "-lpcm" ) == 0)
            && c < (argc-1) )
            {
            screen_lpi = atof( argv[c+1] ) / cm_per_inch;
            ++c;
            }
        else if ( (strcmp( argv[c], "-lpmm" ) == 0)
            && c < (argc-1) )
            {
            screen_lpi = atof( argv[c+1] ) / mm_per_inch;
            ++c;
            }
        else if ( (strcmp( argv[c], "-jitter" ) == 0)
            && c < (argc-1) )
            {
            jitter = atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-examples" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            doExamples = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-sines" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            doSines = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-documentation" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            doDocumentation = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-dopower" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            doPowerSpectra = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-dither" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            dither = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-invert" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            invert = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-densitychart" ) == 0 )
            && c < (argc-1) )
            {
            long value = atol( argv[c+1] );
            doDensityChart = (value != 0);
            ++c;
            }
        else if ( (strcmp( argv[c], "-rot" ) == 0
                || strcmp( argv[c], "-rotation" ) == 0)
            && c < (argc-1) )
            {
            screen_angle = atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-offsetX" ) == 0)
            && c < (argc-1) )
            {
            screen_offsetX = atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-offsetY" ) == 0)
            && c < (argc-1) )
            {
            screen_offsetY = atof( argv[c+1] );
            ++c;
            }
        else if ( strcmp( argv[c], "-help" ) == 0
                || strcmp( argv[c], "--help" ) == 0
                )
            {
            print_usage( argv );
            exit (0);
            }
        else if (argv[c][0] == '-')
            {
            // unrecognized option
            cout << "Unrecognized or unterminated option: " << argv[c] << "\n";
            print_usage( argv );
            exit (1);
            }
        else
            {
            infiles.push_back( argv[c] );
            }
        
        }
}

/******************************************************************************/
/******************************************************************************/

int main(int argc, const char * argv[])
{
#if 0
    // test page for inkjet
    image_ppi = 1200;
#endif
#if 0
    // test page for laser
    image_ppi = 600;
#endif
#if 0
    // 8x8 inches, 85 lpi --- decent resolution, but shows dot shape
    width = 8*image_ppi;
    height = 8*image_ppi;
//    screen_lpi = image_ppi / 50.0;   // 50x50 dot for demonstrations
    screen_lpi = image_ppi / 20.0;   // 20x20 dot
//    screen_lpi = image_ppi / 10.0;   // 10x10 dot
//    screen_lpi = image_ppi / 7.0;   // 7x7 dot
//    screen_lpi = 85.0;    //  7x7 dot at 600 dpi
#endif
#if 0
    doDocumentation = true;
    doExamples = false;
#endif
#if 0
// small example dot threshold arrays for documentation
const int dots_per_line = 11;       // 8..15, 256
    image_ppi = 600;
    width = 1*dots_per_line;
    height = 1*dots_per_line;
    screen_lpi = image_ppi / dots_per_line;
    jitter = 0.0;
    dither = false;
#endif
#if 0
// test density chart
    image_ppi = 600;
    doDensityChart = true;
    doDocumentation = false;
    doExamples = false;
    doPowerSpectra = false;
#endif
#if 0
// debugging power spectra
    doPowerSpectra = true;
    image_ppi = 600;
    screen_lpi = image_ppi / 10.0;   // 10x10 dot
    screen_angle = 45.0;
    jitter = 0.0;
    dither = false;
#endif



    unit_test_halftone();
    // add other unit tests as needed


    parse_arguments( argc, argv );
    
    double ratio = screen_lpi / image_ppi;  // ratio == scale == lpi / ppi == lpcm / ppcm
    
    geom_transform transform;
    transform.scale = ratio;
    transform.rotation = screen_angle;
    transform.offsetX = screen_offsetX;
    transform.offsetY = screen_offsetY;

    // for now, always run examples
    if (doExamples)
        {
        // create synthetic test image, apply, save examples of threshold image and thresholded test image
        DoGradientExamples( width, height, shared_seed, transform );
        }

    // Another test image, looking at response to changes in values
    // mostly useful for error diffusion testing/debugging
    if (doSines)
        {
        DoSineExamples( width, height, shared_seed, transform );
        }

    if (doDocumentation)
        {
        // create synthetic test image, apply, save examples of thresholded test image
        DoDocumentationExamples( shared_seed, transform );
        }
    
    if (doDensityChart)
        {
        // create density chart image at specified image ppi
        CreateDensityChart( image_ppi );
        }
    
    if (doPowerSpectra)
        {
        // create power spectra spreadsheets for all dither algorithms
        createPowerSpectra( transform );
        }

    // iterate any input files
    for ( auto &onefile : infiles )
        {
        const char *in_name = onefile.c_str();
        
        image in_image;
        in_image.ReadPGM( onefile.c_str() );
        
        if (halftone_algorithm == NULL)
            RunAllAlgorithms(in_image, in_name, shared_seed, transform );
        else
            RunNamedAlgorithm( halftone_algorithm, in_image, in_name, shared_seed, transform );
        }

    return 0;
}

/******************************************************************************/
/******************************************************************************/
