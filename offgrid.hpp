//
//  offgrid.hpp
//  Offgrid demonstration source
//      https://gitlab.com/chriscox/offgrid
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef offgrid_hpp
#define offgrid_hpp

#include <cstdint>
#include "image.hpp"

/******************************************************************************/

pointFloat Offgrid_CellToSegment( int64_t ix, int64_t seed, double edge_limit );
pointInt Offgrid_PointToSegment( double x, int64_t seed, double edge_limit, pointFloat &pt_out );

rectFloat Offgrid_CellToRect( int64_t ix, int64_t iy, int64_t seed, double edge_limit );
pointInt Offgrid_PointToCell( double x, double y, int64_t seed, double edge_limit, rectFloat &rect_out );

/******************************************************************************/

#endif /* offgrid_hpp */
