//
//  spectra.cpp
//  Halftone Tools
//
//  Created by Chris Cox on Feb 5, 2024.
//
//  MIT License, Copyright (C) Chris Cox 2024
//

/*
    TODO - output charts to SVG along with CSV
        (Should be faster than waiting on Excel or Numbers)

 */
 
#include <cstdio>
#include <cmath>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "hash.hpp"
#include "image.hpp"
#include "halftone.hpp"
#include "dither.hpp"
#include "SVG.hpp"
using namespace std;

/******************************************************************************/
/******************************************************************************/

// from Numerical Recipes, some years ago
// forward transform only
// assumes interleaved real and imaginary data
void FFT( double* data, size_t nn )
{
    size_t n, mmax, m, j, istep, i;
    double wtemp, wr, wpr, wpi, wi, theta;
    double tempr, tempi;
    
    // make sure the input size is a power of 2
    assert( (nn & (nn-1)) == 0 );
 
    // reverse-binary reindexing
    n = nn << 1;
    j = 1;
    for ( i = 1; i < n; i += 2 ) {
        if (j>i) {
            swap(data[j-1], data[i-1]);
            swap(data[j+0], data[i+0]);
        }
        m = nn;
        while ( (m >= 2) && (j > m) ) {
            j -= m;
            m >>= 1;
        }
        j += m;
    }
 
    // the Danielson-Lanczos section
    mmax = 2;
    while ( n > mmax ) {
        istep = (mmax << 1);
        theta = -(2.0*M_PI/mmax);
        wtemp = sin(0.5 * theta);
        wpr = -2.0 * wtemp * wtemp;
        wpi = sin(theta);
        wr = 1.0;
        wi = 0.0;
        for ( m = 1; m < mmax; m += 2 ) {
            for ( i = m; i <= n; i += istep) {
                j = i + mmax;
                tempr = wr * data[j-1] - wi * data[j+0];
                tempi = wr * data[j+0] + wi * data[j-1];
 
                data[j-1] = data[i-1] - tempr;
                data[j] = data[i] - tempi;
                data[i-1] += tempr;
                data[i] += tempi;
            }
            wtemp = wr;
            wr += wr * wpr - wi * wpi;
            wi += wi * wpr + wtemp * wpi;
        }
        mmax = istep;
    }
}

/******************************************************************************/

// from Numerical Recipes, some years ago
// forward transform only
void RealFFT( double *data, size_t n )
{
    size_t i,i1,i2,i3,i4,np3;
    const double c1 = 0.5;
    const double c2 = -0.5;
    double h1r,h1i,h2r,h2i;
    double wr,wi,wpr,wpi,wtemp,theta;
    
    // make sure the input size is a power of 2
    assert( (n & (n-1)) == 0 );

    FFT(data,n>>1); // The forward transform is here.

    theta = M_PI /(double) (n>>1);
    wtemp = sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi = sin(theta);
    wr = 1.0+wpr;
    wi = wpi;
    np3 = n+3;

    for ( i = 2; i <= (n>>2); i++ ) { // Case i=1 done separately below.
        i1 = i + i - 1;
        i2 = 1 + i1;
        i3 = np3 - i2;
        i4 = 1 + i3;
        h1r = c1*(data[i1-1]+data[i3-1]); // The two separate transforms are separated out of data
        h1i = c1*(data[i2-1]-data[i4-1]);
        h2r = -c2*(data[i2-1]+data[i4-1]);
        h2i = c2*(data[i1-1]-data[i3-1]);
        
        data[i1-1] = h1r+wr*h2r-wi*h2i; // recombine to form the true transform of the original real data
        data[i2-1] = h1i+wr*h2i+wi*h2r;
        data[i3-1] = h1r-wr*h2r+wi*h2i;
        data[i4-1] = -h1i+wr*h2i+wi*h2r;
        
        wtemp = wr;
        wr = wr*wpr-wi*wpi+wr; // The recurrence.
        wi = wi*wpr+wtemp*wpi+wi;
    }

#if 0
    h1r = data[0];
    data[0] = h1r + data[1]; // Squeeze the first and last data together to get them all within the original array.
    data[1] = h1r - data[1];
#endif

}

/******************************************************************************/

// assert( rows == cols );
// assert( rowStep == cols );
template <typename T >
void transpose_inplace(T *src, size_t cols)
{
    int64_t rowStep = cols;
    for ( size_t k = 0; k < cols; ++k ) {
        for ( size_t j = k+1 ; j < cols ; ++j ) {     // because we're flipping across the diagonal
            std::swap( src[ k*rowStep+j ], src[ j*rowStep+k ] );
        }
    }
}

/******************************************************************************/

// assert( rows == cols );
// assert( rowStep == 2*cols );
// for interleaved complex data
template <typename T >
void transpose_complex_inplace(T *src, size_t cols)
{
    int64_t rowStep = 2*cols;
    for ( size_t k = 0; k < cols; ++k ) {
        for ( size_t j = k+1 ; j < cols ; ++j ) {     // because we're flipping across the diagonal
            std::swap( src[ k*rowStep+(2*j)+0 ], src[ j*rowStep+(2*k)+0 ] );
            std::swap( src[ k*rowStep+(2*j)+1 ], src[ j*rowStep+(2*k)+1 ] );
        }
    }
}

/******************************************************************************/

template <typename T >
void makePowerAndScale(T *data, size_t count, T scale)
{
    for ( size_t j = 0 ; j < count ; ++j ) {
        T real = data[2*j+0];
        T imag = data[2*j+1];
        data[j] = hypot(real,imag) * scale;
    }
}

/******************************************************************************/

template <typename T >
void swapQuadrants(T *src, size_t size)
{
    int64_t rowStep = size;
    size_t size2 = size / 2;
    
    // swap UL <-> BR
    for ( size_t y = 0 ; y < size2; ++y ) {
        for ( size_t x = 0; x < size2; ++x ) {
            std::swap( src[ y*rowStep+x ], src[ (y+size2)*rowStep+(x+size2) ] );
        }
    }
    
    // swap UR <-> BL
    for ( size_t y = 0 ; y < size2; ++y ) {
        for ( size_t x = 0; x < size2; ++x ) {
            std::swap( src[ y*rowStep+(x+size2) ], src[ (y+size2)*rowStep+x ] );
        }
    }
}

/******************************************************************************/

void DebugDumpDoubles( double *data, size_t size, const char *label, const char *name, bool isComplex = false )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    static int count = 1;
    snprintf( out_name, name_limit, "%s_%s_%d.pfm", label, name, count );
    size_t width = isComplex ? 2*size : size ;
    WritePFM( out_name, data, width, size );
    ++count;
}

/******************************************************************************/

void DebugDumpImage( const image &input, const char *label, const char *name )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    static int count = 1;
    snprintf( out_name, name_limit, "%s_%s_%d.pfm", label, name, count );
    input.WritePBM( out_name );
    ++count;
}

/******************************************************************************/

// return FFT of a section of the image
// not very efficient, but good enough for creating power spectrum averages
void imageFFT( const image &input, pointInt &topLeft, size_t size, vector<double> &output )
{
    // make sure the input size is a power of 2
    assert( (size & (size-1)) == 0 );
    
    // copy specified image area to our output double array
    size_t output_pixels = size * size;
    output.resize( 2*output_pixels );
    input.CopyRectToDoubleComplex( topLeft, size, &(output[0]) );

    // do horizontal FFTs in-place on array
    for (size_t y = 0; y < size; ++y )
        {
        double *rowPtr = &(output[y*2*size+0]);
        FFT(rowPtr, size);
        }
    
    // transpose
    transpose_complex_inplace( &(output[0]), size );
 
    // do vertical FFTs in-place on array (currently transposed)
    for (size_t y = 0; y < size; ++y )
        {
        double *rowPtr = &(output[y*2*size+0]);
        FFT(rowPtr, size);
        }
    
    // transpose again to get original orientation for output data
    transpose_complex_inplace( &(output[0]), size );

    // convert complex to real power, and normalize
    // converts buffer to single sample real stored in first size*size values
    makePowerAndScale( &(output[0]), size * size, 1.0/size );

    // swap quadrants to make this centered
    swapQuadrants( &(output[0]), size );
}

/******************************************************************************/

void addRadialSpectra( vector<double> &input, vector<double> &avg, vector<double> &avg_sq, vector<size_t> &count, size_t size )
{
    assert(count.size() == avg.size() );
    assert(count.size() == avg_sq.size() );
    
    for (size_t y = 0; y < size; ++y )
        {
        int64_t rely = y - (size/2);
        
        for (size_t x = 0; x < size; ++x )
            {
            int64_t relx = x - (size/2);
            double value = input[y*size+x];
            size_t index = floor(hypot(relx,rely));
            
            assert( index < avg.size() );
            
            if (index > 0)      // ignore DC values
                {
                count[index] ++;
                avg[index] += value;
                avg_sq[index] += value*value;
                }
            }
        }
}

/******************************************************************************/
/*
normalization?  Looks like it isn't that regular based on gray value

round
0.128
0.260
0.486 / 0.513
0.889 / 0.897
1.265

noise
0.155
0.215
0.295 / 0.295
0.385 / 0.385
0.445

FSS
0.172
0.225
0.425 / 0.420
0.0
0.642 / 0.283

 */
// count includes all samples - radially, and between tiles
void scaleRadialSpectra( vector<double> &avg, vector<double> &avg_sq, vector<size_t> &count )
{
    const size_t max = avg.size();
    assert(count.size() == avg.size() );
    assert(count.size() == avg_sq.size() );
    
    for (size_t i = 0; i < max; ++i)
        {
        size_t num = count[i];
        if (num > 0)
            {
            double mean = avg[i] / num;
            double mean2 = mean * mean;
            
            avg[i] = mean;  // now the averaged power spectra
            
            double var = (avg_sq[i] - mean2*num);
            if (num > 1)
                var /= (num-1);
            
            double anisotropy = var;
            if (mean2 > 0.0)
                anisotropy /= mean2;
            else
                anisotropy = 999.0;      // not infinite, but larger than values should be
            
            avg_sq[i] = anisotropy;     // now the radial anisotropy
            }
        else
            {
            avg_sq[i] = 1.0;    // an anistropy of zero doesn't make sense
            }
        }
}

/******************************************************************************/

void writeRadialSpectra( vector<double> &spectra, const char *label, const char *name )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    static int count = 1;
    snprintf( out_name, name_limit, "%s_%s_%d.csv", label, name, count );
    
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(out_name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", out_name);
        exit(-1);
        }
    
    fprintf(outfile,"%s\n", label );
    
    // write each entry
    for (auto &value: spectra )
        {
        fprintf(outfile,"%g\n", value );
        }
    
    fprintf(outfile,"\n\n");
    
    // and close the file
    fclose(outfile);

    ++count;
}

/******************************************************************************/

inline double
lerp ( double a, double b, double fraction )
{
    return a + (b-a) * fraction;
}

/******************************************************************************/

typedef std::map<float,std::vector<double>> radial_power_map;

static
void writeRadialSpreadsheet( radial_power_map &sheet, const char *label, const char *name, double max_graph_value )
{
    const size_t name_limit = 1024;
    char out_name[name_limit];
    snprintf( out_name, name_limit, "%s_%s.csv", name, label );
    char svg_name[name_limit];
    snprintf( svg_name, name_limit, "%s_%s.svg", name, label );
    string title = string(name) + string("_") + string(label);
    
    FILE *outfile = NULL;
    
    // see if we can create or update this filename
    if((outfile=fopen(out_name,"wb"))==NULL)
        {
        fprintf(stderr,"Could not create output file %s\n", out_name);
        exit(-1);
        }
    
    // keys
    size_t count = 0;
    size_t shades = 0;
    fprintf(outfile,"\"index\"," );
    for (auto & [key,list] : sheet )
        {
        fprintf(outfile,"\"%f\",", key );
        count = std::max( count, list.size() );
        shades++;
        }
    fprintf(outfile,"\n");

    for (size_t i=0; i < count; ++i)
        {
        fprintf(outfile,"%lu,", i );
        for (auto & [key,list] : sheet )
            {
            fprintf(outfile,"%f,", list[i] );
            }
        fprintf(outfile,"\n");
        }
    
    fprintf(outfile,"\n\n");
    
    // and close the file
    fclose(outfile);
    outfile = NULL; // safety
    
    


// start SVG output for spectra charts
// coordinates are in points
    svgObjects.clear();
    std::ofstream out( svg_name );


    // calc width and height information
    double svg_font_size = 12.0;
    double axis_font_size = 9.0;
    string svg_font = "Times";
    string svg_font_style = "Normal";
    string svg_align_center = "center";
    string svg_align_right = "right";
    double svg_separation = 2.5 * svg_font_size;     // >= 2 * font size
    double tick_width = 5;
    double svg_axis_width = axis_font_size*4 + tick_width + 2;
    double svg_axis_height = axis_font_size + tick_width + 2;    // must include labels
    double graph_width = count*2;
    double graph_height = 100.0;
    double svg_width = graph_width + svg_axis_width;  // points * scale + vertical axis
    double svg_height = graph_height + svg_axis_height;     // scale + horizontal axis and labels
    double graph_left = svg_axis_width;
    
    double svg_doc_width = ceil( svg_width + svg_axis_width );   // in points
    double svg_doc_height = ceil( (svg_height+svg_separation) * shades );

    // add document white background
    point2D topLeft( 0, 0 );
    point2D bottomRight( svg_doc_width, svg_doc_height );
    AddSVGRect( svgObjects, topLeft, bottomRight, true );
    
    // write document title, just to keep things clear
    point2D title_point( svg_doc_width/2, svg_font_size/2 + 2 );
    AddSVGText( svgObjects, title_point, title, svg_font_size, svg_font, svg_font_style, svg_align_center );

    double vertical_start = svg_font_size;
    double value_min = 0.0;
    double value_max = max_graph_value;

    double value_scale = graph_height / (value_max - value_min);
    double index_scale = graph_width / count;
    
    for (auto & [key,list] : sheet )
        {
        string density = to_string( key );
        SVGStartGroup( svgObjects, density );

        // white background
        topLeft = point2D( 0, vertical_start-svg_font_size );
        bottomRight = point2D( graph_left + graph_width + tick_width, vertical_start + svg_height + 1.5 * svg_font_size );
        AddSVGRect( svgObjects, topLeft, bottomRight, true );
        
        // add label from key
        point2D label_point( svg_width/2, vertical_start + svg_height + svg_axis_height - 2 );
        string label = string("gray value ") + density;
        AddSVGText( svgObjects, label_point, label, svg_font_size, svg_font, svg_font_style, svg_align_center );
        
        // vertical axis
        SVGStartGroup( svgObjects, "vertical axis" );
        point2D start( graph_left, vertical_start + 0 );
        point2D end( graph_left, vertical_start + graph_height );
        AddSVGLine( svgObjects, start, end );
        
        // ticks and values for vertical
        size_t vertical_tick_count = 10;
        for (size_t i = 0; i <= vertical_tick_count; ++i)
            {
            double fraction = double(i) / double(vertical_tick_count);
            double value = lerp( value_min, value_max, fraction );
            double pos = lerp( 0, graph_height, fraction );
            
            point2D start( graph_left, vertical_start + graph_height - pos );
            point2D end( graph_left-tick_width, vertical_start + graph_height - pos );
            AddSVGLine( svgObjects, start, end );

            char label_buf[20];
            snprintf( label_buf, 20, "%.1f", value );
            string label(label_buf);
            end.x -= 2;
            end.y += axis_font_size/4;
            AddSVGText( svgObjects, end, label, axis_font_size, svg_font, svg_font_style, svg_align_right );
            }
        
        SVGEndGroup( svgObjects );
        
        
        // horizontal axis
        SVGStartGroup( svgObjects, "horizontal axis" );
        start = point2D( graph_left, vertical_start + graph_height + 0 );
        end = point2D( graph_left+graph_width, vertical_start + graph_height + 0 );
        AddSVGLine( svgObjects, start, end );
        
        size_t horizontal_tick_step = 20;
        size_t xx = 0;
        while (xx <= count)
            {
            point2D start( graph_left + xx*index_scale, vertical_start + graph_height );
            point2D end( graph_left + xx*index_scale, vertical_start + graph_height + tick_width );
            AddSVGLine( svgObjects, start, end );

            string label = to_string(xx);
            end.y += 0.8 * axis_font_size;
            AddSVGText( svgObjects, end, label, axis_font_size, svg_font, svg_font_style, svg_align_center );
            
            xx += horizontal_tick_step;
            }
        
        SVGEndGroup( svgObjects );
        
        
        // write list values
        pointList graph;
        graph.resize(count);
        for (size_t i=0; i < count; ++i)
            {
            double value = list[i];
            
            // pin value range
            value = std::max( value, value_min );
            value = std::min( value, value_max );
            
            // SVG is vertically inverted... sometimes.
            graph[i] = point2D( graph_left + i*index_scale, vertical_start + (value_max-value)*value_scale );
            }
        AddSVGPolyline( svgObjects, graph, false, false );
        
        SVGEndGroup( svgObjects );
        
        // offset to next graph
        vertical_start += (svg_height + svg_separation);
        }
    
    point2D size( svg_doc_width, svg_doc_height );
    ExportSVGFile( out, svgObjects, size );
    out.close();

}

/******************************************************************************/

void createOneSpectra( const image &outImage, const double value, const size_t sample_size,
                        const size_t boundary_size, const size_t sample_count,
                        radial_power_map &power_sheet,
                        radial_power_map &anisotropy_sheet,
                        const char *name )
{
// TODO: hate to reallocate these all the time, but also hate passing in so many values

    // FFT complex value storage
    vector<double> spectraStorage( 2 * sample_size * sample_size );
    
    // averaged radial power storage
    size_t max_radius = ceil( sample_size/2 * sqrt(2.0) );
    std::vector<double> radial_power( max_radius, 0.0 );
    std::vector<double> radial_power_square( max_radius, 0.0 );
    std::vector<size_t> radial_count( max_radius, 0 );
    
    // double energy = value * (1.0 - value);       // useless for normalization - different methods have very different scales

#if 0
// DEBUG - save the dithered input image, for documentation and debugging
DebugDumpImage( outImage, "spectra_input", name );
#endif

    // loop over image area to create power spectra samples
    for (size_t y = 0; y < sample_count; ++y )
        {
        for (size_t x = 0; x < sample_count; ++x )
            {
            pointInt topLeft( boundary_size+x*sample_size, boundary_size+y*sample_size );
            
            // create power spectra
            imageFFT( outImage, topLeft, sample_size, spectraStorage );

#if 0
// DEBUG - save the 2D spectra, for documentation and debugging
DebugDumpDoubles( &(spectraStorage[0]), sample_size, "spectra_result", name );
#endif

            // convert 2D spectra to 1D radial spectra, add to radial power array
            addRadialSpectra( spectraStorage, radial_power, radial_power_square, radial_count, sample_size );
        
            }   // end x loop
        }   // end y loop over sample tiles
    
    // average radial spectra, and calc anisotropy
    // radial_power has average power spectra
    // radial_power_square has anistropy
    scaleRadialSpectra( radial_power, radial_power_square, radial_count );

    // store into spreadsheets
    power_sheet[ value ] = radial_power;
    anisotropy_sheet[ value ] = radial_power_square;
}

/******************************************************************************/

void createPowerSpectra( const geom_transform &trans )
{
// values used in Robert Ulichney's "Digital Halftoning"
//const std::vector<float> test_values = { 1.0/32.0, 1.0/16.0, 1.0/8.0, 1.0/4.0, 1.0/2.0, 3.0/4.0, 7.0/8.0 };

// more complete set of values - hitting a lot of problem areas for error diffusion and dot screens
const std::vector<float> test_values = { 1.0/32.0, 1.0/16.0, 1.0/8.0, 1.0/4.0, 1.0/3.0, 1.0/2.0, 2.0/3.0, 3.0/4.0, 7.0/8.0, 15.0/16.0, 31.0/32.0 };

    // we want this to be reproducible
    const int64_t seed = hash64((int64_t)0x8675309);
    const size_t sample_size = 256;       // must be a power of 2!
    const size_t boundary_size = sample_size/2;     // skip edge effects
    const size_t sample_count = 4;        // really sqrt of sample count
    const size_t image_size = 2*boundary_size + sample_size * sample_count;

    // make sure the input size is a power of 2
    assert( (sample_size & (sample_size-1)) == 0 );

    image testImage( image_size, image_size );
    image outImage( image_size, image_size );
    
    radial_power_map power_sheet;
    radial_power_map anisotropy_sheet;


// TODO: need better abstraction, combine more of these loops

    // loop over dither algorithms
    for ( auto &desc : power_ditherList )
        {
        dither_options opts ( desc );
        power_sheet.clear();
        anisotropy_sheet.clear();
    
        // loop over test values
        for( auto value : test_values )
            {
            // fill input image with specified value
            uint8_t value8 = (255.0 * value);
            testImage.FillRectClipped( 0, 0, image_size, image_size, value8 );
            
            // dither/halftone this image
// capture testImage, outImage, desc, seed, opts
            desc.func( outImage, testImage, seed, opts );

            createOneSpectra( outImage, value, sample_size,
                        boundary_size, sample_count,
                        power_sheet, anisotropy_sheet,
                        desc.name );
            
            }   // end loop over test values
        
        // output radial spectra data as spreadsheets
        writeRadialSpreadsheet( power_sheet, "spectra", desc.name, 1.0 );
        writeRadialSpreadsheet( anisotropy_sheet, "anisotropy", desc.name, 20.0 );
        
        }   // end loop over dither algorithms


    // need one more temp image for halftone screen
    image screen( image_size, image_size );

    // loop over halftone shader algorithms
    for ( auto &desc : power_halftone_list )
        {
        screen_options opts ( desc );
        power_sheet.clear();
        anisotropy_sheet.clear();
    
        // loop over test values
        for( auto value : test_values )
            {
            // fill input image with specified value
            uint8_t value8 = (255.0 * value);
            testImage.FillRectClipped( 0, 0, image_size, image_size, value8 );
            
            // dither/halftone this image
// capture testImage, outImage, screen, trans, desc, seed, opts
            CreateScreenGeneral( screen, testImage.width, testImage.height, trans, desc.dot, seed, opts );
            testImage.ApplyThresholdScreen( screen, outImage );

            createOneSpectra( outImage, value, sample_size,
                        boundary_size, sample_count,
                        power_sheet, anisotropy_sheet,
                        desc.name );
            
            }   // end loop over test values
        
        // output radial spectra data as spreadsheets
        writeRadialSpreadsheet( power_sheet, "spectra", desc.name, 1.0 );
        writeRadialSpreadsheet( anisotropy_sheet, "anisotropy", desc.name, 20.0 );
        
        }   // end loop over halftone algorithms
}

/******************************************************************************/
/******************************************************************************/
