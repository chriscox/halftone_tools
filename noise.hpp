/*

GeneticImages/noise.h

Copyright (C) 1995-2023 Chris Cox

*/

#ifndef noiseh_included
#define noiseh_included

/******************************************************************************/

double NoisePointSample( double xx, double yy, int64_t seed );
double NoiseBilinear( double xx, double yy, int64_t seed );
double NoiseBicubic( double xx, double yy, int64_t seed );

/******************************************************************************/

double ChaosPointSample( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChaosBilinear( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChaosBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );

/******************************************************************************/

double MultifractalBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChoasSubtractionBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChoasDifferenceBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChoasSandBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChoasLichenBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChaosThresholdBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double ChaosTurbulenceBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );
double SpottedBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set );

/******************************************************************************/

#endif  /* noiseh_included */
