/*

GeneticImages/cells.h

Copyright (C) 1995-2023 Chris Cox

*/

#ifndef cellsh_included
#define cellsh_included

#include <vector>
#include <cassert>

/******************************************************************************/

struct CellReturnData
{
    CellReturnData ( double dd, int64_t id, double xx, double yy ) :
        distsq(dd), point_id(id), x(xx), y(yy) {}

    CellReturnData () {}

public:
    double distsq;    // stored as distance^2, to minimize sqrt calcs
    double x, y;
    int64_t point_id;
};

typedef std::vector<CellReturnData> distanceList;

/******************************************************************************/

struct {
    bool operator()(const CellReturnData &a, const CellReturnData &b) const
        { if (a.distsq < b.distsq)  return true;
          if (a.distsq > b.distsq)  return false;
          
          // equal dist falls through to here, to make sure we always have a stable order
          return (a.point_id < b.point_id);
        }
} cellDistanceLess;

/******************************************************************************/

struct {
    bool operator()(const CellReturnData &a, const CellReturnData &b) const
        { if (a.x < b.x)  return true;
          if (a.x > b.x)  return false;
          
          // equal values falls through to here, to make sure we always have a stable order
          return (a.point_id < b.point_id);
        }
} cellXLess;

/******************************************************************************/

struct {
    bool operator()(const CellReturnData &a, const CellReturnData &b) const
        { if (a.y < b.y)  return true;
          if (a.y > b.y)  return false;
          
          // equal values falls through to here, to make sure we always have a stable order
          return (a.point_id < b.point_id);
        }
} cellYLess;

/******************************************************************************/

double Cell2DNearestDist( double x, double y, int64_t seed );
double Cell2DNearestID( double x, double y, int64_t seed );
int64_t Cell2DNearestHash( double x, double y, int64_t seed );
double Cell2DNearestDistAndID( double x, double y, int64_t seed, int64_t &id );
double Cell2DSecondDist( double x, double y, int64_t seed, size_t count );
double Cell2DCellWalls( double x, double y, int64_t seed, size_t count );

double Cell2DNearestManhatten( double x, double y, int64_t seed );
double Cell2DNearestIDManhatten( double x, double y, int64_t seed );
double Cell2DNearestDistAndIDManhatten( double x, double y, int64_t seed, int64_t &id );

double Cell2DNearestTriangle( double x, double y, int64_t seed );
double Cell2DNearestDistAndIDTriangle( double x, double y, int64_t seed, int64_t &id );

double Cell2DNearestHexagon( double x, double y, int64_t seed );
double Cell2DNearestDistAndIDHexagon( double x, double y, int64_t seed, int64_t &id );

double Cell2DNearestNgon( double x, double y, int64_t seed, size_t sides );
double Cell2DNearestDistAndIDNgon( double x, double y, int64_t seed, int64_t &id, size_t sides );

double Cell2DNearestStar( double x, double y, int64_t seed, size_t sides, double point );
double Cell2DNearestDistAndIDStar( double x, double y, int64_t seed, int64_t &id, size_t sides, double point );

double Cell2DNearestConcaveDiamond( double x, double y, int64_t seed );
double Cell2DNearestDistAndIDConcaveDiamond( double x, double y, int64_t seed, int64_t &id );

/******************************************************************************/

#endif  /* cellsh_included */
