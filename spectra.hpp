//
//  spectra.hpp
//  halftone_tools
//
//  Created by Chris Cox on Feb 5, 2024.
//
//  MIT License, Copyright (C) Chris Cox 2024
//

#ifndef spectra_h
#define spectra_h

#include "halftone.hpp"

/******************************************************************************/

extern void createPowerSpectra( const geom_transform &trans );

/******************************************************************************/

#endif /* spectra_h */
