# Halftone Tools
#

INCLUDE = -I.

CPPFLAGS = -std=c++17 $(INCLUDE) -O3

CPPLIBS = -lm

BINARIES = halftone

all : $(BINARIES)

sources:  main.cpp halftone.cpp image.cpp hash.cpp cells.cpp noise.cpp dither.cpp offgrid.cpp spectra.cpp SVG.cpp

headers:  halftone.hpp image.hpp hash.hpp cells.hpp noise.hpp dither.hpp offgrid.hpp spectra.hpp SVG.hpp

main.o: main.cpp $(headers)

halftone.o: halftone.cpp $(headers)

cells.o: cells.cpp $(headers)

noise.o: noise.cpp $(headers)

dither.o: dither.cpp $(headers)

offgrid.o: offgrid.cpp $(headers)

image.o: image.cpp $(headers)

hash.o: hash.cpp $(headers)

spectra.o: spectra.cpp $(headers)

SVG.o: SVG.cpp $(headers)

halftone: main.o halftone.o image.o hash.o cells.o noise.o dither.o offgrid.o spectra.o SVG.o
	$(CXX) $(CPPFLAGS) $^ -o $@


test: all
	./halftone -examples 0 -alg Sierra ./testfiles/*.pgm ./testfiles/*.ppm ./testfiles/*.pfm
	./halftone -examples 1
	rm -f *.pbm *.pgm *.pfm *.csv *.svg ./testfiles/*.pbm


# declare some targets to be fakes without real dependencies
.PHONY : clean

# remove all the stuff we build and write during tests
clean : 
	rm -f *.pbm *.pgm *.pfm *.csv *.svg ./testfiles/*.pbm *.o $(BINARIES)
