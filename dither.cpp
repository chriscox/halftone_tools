//
//  dither.cpp
//  Halftone Tools
//
//  Created by Chris Cox on June 25, 2023
//

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <array>
#include "hash.hpp"
#include "image.hpp"
#include "halftone.hpp"
#include "dither.hpp"


/******************************************************************************/

const int64_t extraHash1 = hash64((int64_t)0xAABBCCDDEEFF);
const int64_t extraHash2 = hash64((int64_t)0x424242424242);
const int64_t extraHash3 = hash64((int64_t)0xFEEDFACEBEEF);

/******************************************************************************/

template< typename T >
void fill_noise( T *buffer, size_t count, T noise_range, int64_t seed )
{
    const int64_t gideon = hash64((int64_t)0x875020079);
    const int64_t seed2 = seed ^ gideon;
    for (size_t x = 0; x < count; ++x)
        {
        int64_t point_hash = hash64(x,seed2);
        T noise = (noise_range * (point_hash % 1024)) / 1024;
        buffer[x] = noise;
        }
}

/******************************************************************************/
/******************************************************************************/

// useful for debugging power spectra
void dither_debugpattern( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        for (size_t x = 0; x < width; ++x)
            {
            uint8_t value = 0;
            
            // different frequencies
            // and give them a non-zero phase
            
            if ( (x % 4) == 2 )   value = 255;        // power data horizontal only
            
            if ( (y % 16) == 6 )  value = 255;        // power data vertical only
            
            if ( ((x+y) % 32) == 3 )  value = 255;    // power data on diagonal only

            output.WritePixelUnclipped( x, y, value );
            }
        }
}

dither_description desc_debugpattern( dither_debugpattern, "debug pattern" );

/******************************************************************************/

// simplest dither: don't, just threshold
// useful for debugging
void dither_dthreshold( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const uint8_t threshold = (uint8_t)floor( (255.0/100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        for (size_t x = 0; x < width; ++x)
            {
            uint8_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = (pixel < threshold) ? 0 : 255;
            output.WritePixelUnclipped( x, y, value );
            }
        }
}

dither_description desc_dthreshold( dither_dthreshold, "threshold",
                                1,
                                { "threshold" },
                                {  50.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// dither with noise, also simple
// also useful for debugging and demonstrating why it's a horrible halftone strategy
void dither_noise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        for (size_t x = 0; x < width; ++x)
            {
            uint8_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            // preserve pure black and pure white
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                int64_t point_hash = hash64( hashXY((int64_t)x,(int64_t)y,seed) );  // shows some patterns with just XY
                uint32_t noise = point_hash & 0xFF;
                value = (pixel < noise) ? 0 : 255;
                }
            
            output.WritePixelUnclipped( x, y, value );
            }
        }
}

dither_description desc_noise( dither_noise, "noise" );

/******************************************************************************/

// simplest error diffusion: forward
// produces lots of patterns
void dither_forward( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    
    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        int32_t error = 0;
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            // preserve pure black and pure white
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                error = 0;
                }
            else
                {
                int32_t comp = pixel + error;
                value = (comp < 128) ? 0 : 255;
                int32_t diff = pixel - (int32_t)value;
                error += (error_weight * diff + 50) / 100;
                }
            
            output.WritePixelUnclipped( x, y, value );
            }
        }
}

dither_description desc_forward( dither_forward, "forward",
                                1,
                                { "error carried" },
                                { 100.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// forward error diffusion, with noise
// produces lots of patterns, but fuzzier
void dither_forwardnoise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    for (size_t y = 0; y < height; ++y)
        {
        int32_t error = 0;
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            // preserve pure black and pure white
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                error = 0;
                }
            else
                {
                int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed);
                int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                int32_t comp = pixel + error ;
                value = (comp < (128+noise)) ? 0 : 255;
                int32_t diff = pixel - (int32_t)value;
                error += (error_weight * diff + 50) / 100;
                }
            
            output.WritePixelUnclipped( x, y, value );
            }
        }
}

dither_description desc_forwardnoise( dither_forwardnoise, "forward noise",
                                2,
                                { "error carried", "noise range" },
                                { 100.0,  32.0 },
                                {   0.0,   0.0 },
                                { 100.0, 100.0 } );
                            
/******************************************************************************/

// diagonal weight
// produces lots of patterns
void dither_diagonal( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    memset( current_error, 0, (width+2)*sizeof(int32_t) );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = current_error[x];
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                value = (comp < 128) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;

            // spread errors
            next_error[x+1] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_diagonal( dither_diagonal, "diagonal" );
                            
/******************************************************************************/

// diagonal weight, with noise
// produces lots of patterns, but fuzzier
void dither_diagonalnoise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    fill_noise( &buffers[0], (width+2), 2*noise_range, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = current_error[x];
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed);
                int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                int32_t comp = pixel + error;
                value = (comp < (128+noise)) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;
            diff = (error_weight * diff + 50) / 100;

            // spread errors
            next_error[x+1] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_diagonalnoise( dither_diagonalnoise, "diagonal noise",
                                2,
                                { "error carried", "noise range" },
                                { 100.0,  16.0 },
                                {   0.0,   0.0 },
                                { 100.0, 100.0 } );
                            
/******************************************************************************/

// two weights
// produces some patterns, but fuzzier
void dither_twoposition( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    memset( current_error, 0, (width+2)*sizeof(int32_t) );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = (current_error[x] + 1) / 2;
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                value = (comp < 128) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;

            // spread errors
            current_error[x+1] += diff;
            next_error[x+0] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_twoposition( dither_twoposition, "two position" );
                            
/******************************************************************************/

// two weights, with noise
// produces some patterns, but fuzzier
void dither_twopositionnoise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    fill_noise( &buffers[0], (width+2), 2*noise_range, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = (current_error[x] + 1) / 2;
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed);
                int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                int32_t comp = pixel + error;
                value = (comp < (128+noise)) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;
            diff = (error_weight * diff + 50) / 100;

            // spread errors
            current_error[x+1] += diff;
            next_error[x+0] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_twopositionnoise( dither_twopositionnoise, "two position noise",
                                2,
                                { "error carried", "noise range" },
                                { 100.0,   8.0 },
                                {   0.0,   0.0 },
                                { 100.0, 100.0 } );
                            
/******************************************************************************/

// three weights
// produces some patterns
void dither_threeposition( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    memset( current_error, 0, (width+2)*sizeof(int32_t) );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = (current_error[x] + 1) / 3;
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                value = (comp < 128) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;

            // spread errors
            current_error[x+1] += diff;
            next_error[x+0] += diff;
            next_error[x+1] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_threeposition( dither_threeposition, "three position" );

/******************************************************************************/

// three weights, with noise
// produces some patterns, but fuzzier
void dither_threepositionnoise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    fill_noise( &buffers[0], (width+2), 2*noise_range, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = (current_error[x] + 1) / 3;
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed);
                int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                int32_t comp = pixel + error;
                value = (comp < (128+noise)) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;
            diff = (error_weight * diff + 50) / 100;

            // spread errors
            current_error[x+1] += diff;
            next_error[x+0] += diff;
            next_error[x+1] += diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_threepositionnoise( dither_threepositionnoise, "three position noise",
                                2,
                                { "error carried", "noise range" },
                                { 100.0,   8.0 },
                                {   0.0,   0.0 },
                                { 100.0, 100.0 } );
                            
/******************************************************************************/

// randomly positioned error
// produces a few odd patterns, some low freq components
void dither_randomposition( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    memset( current_error, 0, (width+2)*sizeof(int32_t) );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = current_error[x];
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                value = (comp < 128) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;

            int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
            size_t index = (uint64_t)point_hash % 3;
            
            // spread error
            switch( index )
                {
                default:
                case 0:
                    current_error[x+1] += diff;
                    break;
                case 1:
                    next_error[x+1] += diff;
                    break;
                case 2:
                    next_error[x+0] += diff;
                    break;
                }
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_randomposition( dither_randomposition, "random position" );
                            
/******************************************************************************/

// randomly positioned error, with noise
// produces few patterns, some low freq components
void dither_randompositionnoise( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t error_weight = int32_t( opt.params[0] );
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    fill_noise( &buffers[0], (width+2), 2*noise_range, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
            
            int32_t error = current_error[x];
            int32_t comp = pixel + error;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                int64_t point_hash = hashXY((int64_t)x,(int64_t)y,seed);
                int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                int32_t comp = pixel + error;
                value = (comp < (128+noise)) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;
            diff = (error_weight * diff + 50) / 100;

            int64_t point_hash2 = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
            size_t index = (uint64_t)point_hash2 % 3;
            
            // spread error
            switch( index )
                {
                default:
                case 0:
                    current_error[x+1] += diff;
                    break;
                case 1:
                    next_error[x+1] += diff;
                    break;
                case 2:
                    next_error[x+0] += diff;
                    break;
                }
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_randompositionnoise( dither_randompositionnoise, "random position noise",
                                2,
                                { "error carried", "noise range" },
                                { 100.0,   8.0 },
                                {   0.0,   0.0 },
                                { 100.0, 100.0 } );

/******************************************************************************/

// random position with noise and serpentine scan
// produces a slight pattern at 50%, some low freq components
void dither_randposserpentine( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];
    
    fill_noise( &buffers[0], (width+2), 2*noise_range, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = current_error[x];
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                int64_t point_hash2 = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
                size_t index = (uint64_t)point_hash2 % 3;
                
                // spread error
                switch( index )
                    {
                    default:
                    case 0:
                        current_error[x+1] += diff;
                        break;
                    case 1:
                        next_error[x+1] += diff;
                        break;
                    case 2:
                        next_error[x+0] += diff;
                        break;
                    }
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = current_error[x];
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                int64_t point_hash2 = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
                size_t index = (uint64_t)point_hash2 % 3;
                
                // spread error
                switch( index )
                    {
                    default:
                    case 0:
                        current_error[x-1] += diff;
                        break;
                    case 1:
                        next_error[x-1] += diff;
                        break;
                    case 2:
                        next_error[x+0] += diff;
                        break;
                    }
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_randposserpentine( dither_randposserpentine, "Random Position Serpentine",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// original Floyd-Steiberg dithering
// produces a lot of patterns
// https://en.wikipedia.org/wiki/Floyd–Steinberg_dithering
void dither_floydsteinberg( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];

    fill_noise( &buffers[0], (width+2), 2*16, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, width*sizeof(int32_t) );
        
        for (size_t x = 0; x < width; ++x)
            {
            int32_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;
                
            int32_t err = (current_error[x] + 8) / 16;
            int32_t comp = pixel + err;
            
            // preserve pure black and pure white - no scum dots!
            if (pixel == 0 || pixel == 255)
                {
                value = pixel;
                }
            else
                {
                value = (comp < 128) ? 0 : 255;
                }
            
            int32_t diff = comp - (int32_t)value;

            // spread errors
            current_error[x+1] += 7 * diff;
            next_error[x-1] += 3 * diff;
            next_error[x+0] += 5 * diff;
            next_error[x+1] += 1 * diff;
            
            output.WritePixelUnclipped( x, y, value );
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_FloydSteinberg( dither_floydsteinberg, "Floyd-Steinberg" );

/******************************************************************************/

// Floyd-Steiberg dithering with serpentine scan
// and add noise to threshold
// produces some patterns
void dither_floydsteinbergSerpentine( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+width+2];
    
    fill_noise( &buffers[0], (width+2), 2*16, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 7 * diff;
                next_error[x-1] += 3 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 7 * diff;
                next_error[x-1] += 1 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 3 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_FloydSteinbergSerpentine( dither_floydsteinbergSerpentine, "Floyd-Steinberg Serpentine",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Jarvis-Judice-Ninke
// produces a few patterns
void dither_JJN( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*48, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 7 * diff;
                current_error[x+2] += 5 * diff;
                next_error[x-2] += 3 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+2] += 3 * diff;
                two_error[x-2] += 1 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+2] += 1 * diff;
            
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 7 * diff;
                current_error[x-2] += 5 * diff;
                next_error[x+2] += 3 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x-2] += 3 * diff;
                two_error[x+2] += 1 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x-2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_JJN( dither_JJN, "Jarvis-Judice-Ninke",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

//  Stucki
void dither_stucki( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*42, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 21) / 42;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 8 * diff;
                current_error[x+2] += 4 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 8 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-2] += 1 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 4 * diff;
                two_error[x+1] += 2 * diff;
                two_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 21) / 42;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 8 * diff;
                current_error[x-2] += 4 * diff;
                next_error[x+2] += 2 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+0] += 8 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x-2] += 2 * diff;
                two_error[x+2] += 1 * diff;
                two_error[x+1] += 2 * diff;
                two_error[x+0] += 4 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x-2] += 1 * diff;

                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_Stucki( dither_stucki, "Stucki",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

//  Atkinson
void dither_atkinson( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*8, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
            
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 4) / 8;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 1 * diff;
                current_error[x+2] += 1 * diff;
                next_error[x-1] += 1 * diff;
                next_error[x+0] += 1 * diff;
                next_error[x+1] += 1 * diff;
                two_error[x+0] += 1 * diff;

                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 4) / 8;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 1 * diff;
                current_error[x-2] += 1 * diff;
                next_error[x+1] += 1 * diff;
                next_error[x+0] += 1 * diff;
                next_error[x-1] += 1 * diff;
                two_error[x+0] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_Atkinson( dither_atkinson, "Atkinson",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

//  Atkinson, normalized for 100% error diffusion
void dither_atkinson2( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*6, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
            
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 3) / 6;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 1 * diff;
                current_error[x+2] += 1 * diff;
                next_error[x-1] += 1 * diff;
                next_error[x+0] += 1 * diff;
                next_error[x+1] += 1 * diff;
                two_error[x+0] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 3) / 6;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 1 * diff;
                current_error[x-2] += 1 * diff;
                next_error[x+1] += 1 * diff;
                next_error[x+0] += 1 * diff;
                next_error[x-1] += 1 * diff;
                two_error[x+0] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_Atkinson2( dither_atkinson2, "Atkinson2",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Burkes
void dither_burkes( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+width+4];
    
    fill_noise( &buffers[0], (width+4), 2*32, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 8 * diff;
                current_error[x+2] += 4 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 8 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;

                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 8 * diff;
                current_error[x-2] += 4 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 8 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_Burkes( dither_burkes, "Burkes",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Sierra original
void dither_sierra( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*32, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 5 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 5 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_Sierra( dither_sierra, "Sierra",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Sierra 2 row - ick
void dither_sierra2( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+width+4];
    
    fill_noise( &buffers[0], (width+4), 2*16, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 4 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 1 * diff;
                next_error[x-1] += 2 * diff;
                next_error[x+0] += 3 * diff;
                next_error[x+1] += 2 * diff;
                next_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 4 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 1 * diff;
                next_error[x-1] += 2 * diff;
                next_error[x+0] += 3 * diff;
                next_error[x+1] += 2 * diff;
                next_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_Sierra2( dither_sierra2, "Sierra2",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Fan - not that great
void dither_fan( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+3) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+width+1];
    
    fill_noise( &buffers[0], (width+3), 2*16, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 7 * diff;
                next_error[x-2] += 1 * diff;
                next_error[x-1] += 3 * diff;
                next_error[x+0] += 5 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 7 * diff;
                next_error[x+2] += 1 * diff;
                next_error[x+1] += 3 * diff;
                next_error[x+0] += 5 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_Fan( dither_fan, "Fan",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Fan and Shiau
void dither_fan_shiau( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[3];
    int32_t *next_error = &buffers[3+width+1];
    
    fill_noise( &buffers[0], (width+4), 2*16, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 8 * diff;
                next_error[x-3] += 1 * diff;
                next_error[x-2] += 1 * diff;
                next_error[x-1] += 2 * diff;
                next_error[x+0] += 4 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 8) / 16;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 8 * diff;
                next_error[x+3] += 1 * diff;
                next_error[x+2] += 1 * diff;
                next_error[x+1] += 2 * diff;
                next_error[x+0] += 4 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_Fan_Shiau( dither_fan_shiau, "Fan Shiau",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Park, Kang, Kim
void dither_PKK1( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    const int32_t divisor = 512;
    const int32_t weight1 = round(0.292 * divisor);
    const int32_t weight2 = round(0.275 * divisor);
    const int32_t weight3 = round(0.3772 * divisor);
    const int32_t weight4 = round(0.0248 * divisor);
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[3];
    int32_t *next_error = &buffers[3+width+1];
    
    fill_noise( &buffers[0], (width+4), 2*divisor, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += weight1 * diff;
                next_error[x-2] += weight4 * diff;
                next_error[x-1] += weight3 * diff;
                next_error[x+0] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += weight1 * diff;
                next_error[x+2] += weight4 * diff;
                next_error[x+1] += weight3 * diff;
                next_error[x+0] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_PKK1( dither_PKK1, "Park Kang Kim1",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Park, Kang, Kim
void dither_PKK2( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    const int32_t divisor = 512;
    const int32_t weight1 = round(0.4463 * divisor);
    const int32_t weight2 = round(0.1519 * divisor);
    const int32_t weight3 = round(0.2641 * divisor);
    const int32_t weight4 = round(0.0598 * divisor);
    const int32_t weight5 = round(0.0779 * divisor);
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[3];
    int32_t *next_error = &buffers[3+width+1];
    
    fill_noise( &buffers[0], (width+4), 2*divisor, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += weight1 * diff;
                next_error[x-3] += weight5 * diff;
                next_error[x-2] += weight4 * diff;
                next_error[x-1] += weight3 * diff;
                next_error[x+0] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += weight1 * diff;
                next_error[x+3] += weight5 * diff;
                next_error[x+2] += weight4 * diff;
                next_error[x+1] += weight3 * diff;
                next_error[x+0] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_PKK2( dither_PKK2, "Park Kang Kim2",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Allebach, Wong
void dither_Allebach_Wong( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( 255.0 * opt.params[0] / 100.0 );
    const int32_t divisor = 512;
    const int32_t weight1 = round(0.2911 * divisor);
    const int32_t weight2 = round(0.2258 * divisor);
    const int32_t weight3 = round(0.3457 * divisor);
    const int32_t weight4 = round(0.1373 * divisor);
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+4) );
    int32_t *current_error = &buffers[3];
    int32_t *next_error = &buffers[3+width+1];
    
    fill_noise( &buffers[0], (width+4), 2*divisor, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ( (y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += weight1 * diff;
                next_error[x-1] += weight4 * diff;
                next_error[x+0] += weight3 * diff;
                next_error[x+1] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (divisor/2)) / divisor;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += weight1 * diff;
                next_error[x+1] += weight4 * diff;
                next_error[x+0] += weight3 * diff;
                next_error[x-1] += weight2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // swap row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_Allebach_Wong( dither_Allebach_Wong, "Allebach Wong",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Eschbach "Error Diffusion with Homogeneous Highlight and Shadow Response", 1997
//      deltas need to be based on input image level to work well - paper doesn't give exact details
//      still needs noise to avoid patterns
void dither_eschbach( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    const int32_t threshold_delta = (int32_t)( (1024.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*32, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        int32_t threshold_offset = 0;
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                int32_t delta_black = (pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = ((255-pixel) * threshold_delta + 512) / 1024;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }
                
                threshold_offset += ((value == 0) ? -delta_black : delta_white);
                
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 5 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                int32_t delta_black = (pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = ((255-pixel) * threshold_delta + 512) / 1024;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }
                
                threshold_offset += ((value == 0) ? -delta_black : delta_white);
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 5 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

// 0 looks just like Sierra, as expected
// 2 shows coarse quantization in gradients (very image dependent)
// 5 shows coarse quantization in gradients
// 10 shows quantization in gradients
// 15 shows quantization in gradients
// 17 shows some quantization in gradients
// 20 shows some checkerboard at 50%, and some quantization, some vertical lines, but increasing noise slighly helps
// 25 shows some visible vertical patterns
// 30 shows some large patterns
// 50 shows huge patterns
// 70 shows huge patterns
// 90 shows huge patterns (almost like forward only)
dither_description desc_Eschbach( dither_eschbach, "Eschbach",
                                2,
                                { "noise range", "threshold offset scale" },
                                {   6.0,   20.0 },
                                {   0.0,    0.0 },
                                { 100.0,  100.0 } );

/******************************************************************************/

// Eschbach threshold offset extended to 2 dimensions
//      deltas need to be based on input image level to work well - paper doesn't give exact details
//      still needs noise to avoid patterns
void dither_Eschbach2( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    const int32_t threshold_delta = (int32_t)( (1024.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];

    // assign a bit to each position for our 4 bit, 16 entry LUT
    enum  {
        bit_UpLeft   =  1,
        bit_UpCenter =  2,
        bit_UpRight  =  4,
        bit_Previous =  8,
     };
 
    int32_t SurroundLUT[ 16 ];

    // Assume white pixels (paper color) outside of bounds.
    for (int k = 0; k < 16; ++k)
        {
        //bool UL = k & bit_UpLeft;
        bool UC = k & bit_UpCenter;
        //bool UR = k & bit_UpRight;
        bool LL = k & bit_Previous;
        
        //int32_t black_pixels = UL + UC + UR + LL;
        int32_t black_pixels = UC + LL;
        //int32_t black_pixels = UL + LL;
        //int32_t black_pixels = UR + LL;
        
        SurroundLUT[ k ] = black_pixels;
        }
 
    
    fill_noise( &buffers[0], 2*(width+4), 2*32, seed );
    
    for (int64_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        int32_t threshold_offset = 0;
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                int index = 0;
                
                if (y > 0)
                    {
                    if (x > 0)
                        index += (output.ReadPixelUnclipped( x-1, y-1 ) == 0) ? bit_UpLeft : 0;
                        
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? bit_UpCenter : 0;
                    
                    if (x < (width-1))
                        index += (output.ReadPixelUnclipped( x+1, y-1 ) == 0) ? bit_UpRight : 0;
                    }
                
                if (x > 0)
                    index += (output.ReadPixelUnclipped( x-1, y ) == 0) ? bit_Previous : 0;
                
                int32_t black_count = SurroundLUT[index];
                int32_t white_count = 2 - black_count;
                
                int32_t delta_black = (black_count * pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = (white_count * (255-pixel) * threshold_delta + 512) / 1024;
                
                threshold_offset += (delta_white - delta_black);    // should be reduced?
                
                threshold_offset = std::max( -126, std::min( threshold_offset, 126 ));
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }

                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x+1] += 5 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 16) / 32;
                int32_t comp = pixel + err;
                
                int index = 0;
                
                if (y > 0)
                    {
                    if (x > 0)
                        index += (output.ReadPixelUnclipped( x-1, y-1 ) == 0) ? bit_UpRight : 0;
                        
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? bit_UpCenter : 0;
                    
                    if (x < (width-1))
                        index += (output.ReadPixelUnclipped( x+1, y-1 ) == 0) ? bit_UpLeft : 0;
                    }
                
                if (x < (width-1))
                    index += (output.ReadPixelUnclipped( x+1, y ) == 0) ? bit_Previous : 0;
                
                int32_t black_count = SurroundLUT[index];
                int32_t white_count = 2 - black_count;
                
                int32_t delta_black = (black_count * pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = (white_count * (255-pixel) * threshold_delta + 512) / 1024;
                
                threshold_offset += (delta_white - delta_black);    // should be reduced?
                
                threshold_offset = std::max( -126, std::min( threshold_offset, 126 ));
                
                // preserve pure black and pure white
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                // spread errors
                current_error[x-1] += 5 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

// 4 points
// long, broad worms - WHY?
// STRONG alternating rows (because of 3 top pixels to 1 prev pixel)

// Prev + UC
// 0 matches Sierra, as expected
// 5 shows quantization and diagonal lines
// 10 shows diagonal lines
// 20 shows diagonal lines
// 25 shows diagonal lines
// 30 shows some diagonal lines
// 35 shows some diagonal lines
// 40 shows some alternating, some diagonals
// 50 shows some diagonals
// 60 shows some diagonals and alternating at 50%
// 70 shows vertical alternating patterns at 25,50,75%
// See what happens if I reduce diagonal error weights
    // helps a little, but still shows same diagonals
// Try with just FS weights
    // some improvement, some worse - doesn't change long diagonals
// Try random direction errors -- seems to work well, break out into new function

// Prev + UL
// 10 shows lots of diagonals
// 30 shows some diagonals
// 35 shows some diagonals, faint structures
// 40 shows some diagonals, odd structure at 50%
// 50 shows some diagonal lines, some trees at 50%
// 70 shows strong patterns at 25,50,75%

// Prev + UR
// 20 shows strong diagonals
// 30 shows some diagonals, some verticals
// 40 shows some diagonals, some verticals
// 50 shows strong vertical lines
// 70 shows very strong vertical lines

dither_description desc_Eschbach2( dither_Eschbach2, "Eschbach 2D",
                                2,
                                { "noise range", "threshold offset scale" },
                                {   2.0,   30.0 },
                                {   0.0,    0.0 },
                                { 100.0,  100.0 } );

/******************************************************************************/

// Eschbach threshold offset extended to 2 dimensions
//  with random error direction
//      deltas need to be based on input image level to work well - paper doesn't give exact details
//      still needs some noise to avoid patterns
void dither_Eschbach2a( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    const int32_t threshold_delta = (int32_t)( (1024.0 / 100.0) * opt.params[1] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*32, seed );
    
    for (int64_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        int32_t threshold_offset = 0;
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = current_error[x];
                int32_t comp = pixel + err;
                
                int index = 0;
                
                if (y > 0)
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? 1 : 0;
                
                if (x > 0)
                    index += (output.ReadPixelUnclipped( x-1, y ) == 0) ? 1 : 0;
                
                int32_t black_count = index;
                int32_t white_count = 2 - black_count;
                
                int32_t delta_black = (black_count * pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = (white_count * (255-pixel) * threshold_delta + 512) / 1024;
                
                threshold_offset += (delta_white - delta_black);    // should be reduced?
                
                threshold_offset = std::max( -126, std::min( threshold_offset, 126 ));
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }

                int32_t diff = comp - (int32_t)value;

                int64_t point_hash2 = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
                size_t index2 = (uint64_t)point_hash2 % 3;
                
                // spread error
                switch( index2 )
                    {
                    default:
                    case 0:
                        current_error[x+1] += diff;
                        break;
                    case 1:
                        next_error[x+1] += diff;
                        break;
                    case 2:
                        next_error[x+0] += diff;
                        break;
                    }
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = current_error[x];
                int32_t comp = pixel + err;
                
                int index = 0;
                
                if (y > 0)
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? 1 : 0;
                
                if (x < (width-1))
                    index += (output.ReadPixelUnclipped( x+1, y ) == 0) ? 1 : 0;
                
                int32_t black_count = index;
                int32_t white_count = 2 - black_count;
                
                int32_t delta_black = (black_count * pixel * threshold_delta + 512) / 1024;
                int32_t delta_white = (white_count * (255-pixel) * threshold_delta + 512) / 1024;
                
                threshold_offset += (delta_white - delta_black);    // should be reduced?
                
                threshold_offset = std::max( -126, std::min( threshold_offset, 126 ));
                
                // preserve pure black and pure white
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = 128 + noise + threshold_offset;
                    
                    value = (comp < threshold) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;

                int64_t point_hash2 = hashXY((int64_t)x,(int64_t)y,seed^extraHash1);
                size_t index2 = (uint64_t)point_hash2 % 3;
                
                // spread error
                switch( index2 )
                    {
                    default:
                    case 0:
                        current_error[x-1] += diff;
                        break;
                    case 1:
                        next_error[x-1] += diff;
                        break;
                    case 2:
                        next_error[x+0] += diff;
                        break;
                    }
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

// Prev + UC
// 0 matches Sierra, as expected
// 5 shows quantization and faint diagonal lines
// 10 shows faint diagonal lines, more grain
// 20 shows some diagonal lines, grainy
// 30 shows faint diagonal lines, grainy
// 35 shows no obvious patterns, grainy
// 40 shows no obvious patterns, short worms, grainy
// 50 shows some vertical, some worms, grainy
// 60 shows vertical patterns, worms
// 70 shows vertical patterns, worms
// 90 shows vertical patterns, more worms

// Not pinning the offset creates alternating lines and LONG dendritic worms

dither_description desc_Eschbach2a( dither_Eschbach2a, "Eschbach 2Da",
                                2,
                                { "noise range", "threshold offset scale" },
                                {   2.0,   35.0 },
                                {   0.0,    0.0 },
                                { 100.0,  100.0 } );

/******************************************************************************/
/******************************************************************************/

static
void matrix_inner( image &output, const image &input,
                    const int32_t *matrix, const int32_t matrix_width, const int32_t matrix_height )
{
    for (size_t y = 0; y < input.height; ++y)
        {
        int32_t yoffset = (y%matrix_height)*matrix_width;
        for (size_t x = 0; x < input.width; ++x)
            {
            int32_t xoffset = x % matrix_width;
            uint8_t pixel = input.ReadPixelUnclipped( x, y );
            uint8_t value = 0;

            int32_t threshold = matrix[ yoffset + xoffset ];
            value = (pixel < threshold) ? 0 : 255;
            
            output.WritePixelUnclipped( x, y, value );
            }
        }
}

/******************************************************************************/

// matrix dither, Bayer 2x2
void dither_Bayer2( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 2;
    const size_t mheight = 2;
    int32_t matrix[mwidth * mheight] = { 32, 160, 223, 96 };

#if 0
    // adjust original Bayer matrix for better results, get even shadows and highlights
    int32_t Bayer[] = { 0, 2, 3, 1 };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k)
        printf("%d, ", 32 + (255*Bayer[k] + 2) / 4 );
    printf("\n");
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_Bayer2( dither_Bayer2, "Bayer2" );

/******************************************************************************/

// matrix dither, Bayer 4x4
void dither_Bayer4( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 8, 136, 40, 167, 199, 72, 231, 104, 56, 183, 24, 151, 247, 120, 215, 88 };

#if 0
    // adjust original Bayer matrix for better results, get even shadows and highlights
    int32_t Bayer[] = { 0, 16, 4, 20, 24, 8, 28, 12, 6, 22, 2, 18, 30, 14, 26, 10 };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k)
        printf("%d, ", 8 + (255*Bayer[k] + 16) / 32 );
    printf("\n");
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_Bayer4( dither_Bayer4, "Bayer4" );

/******************************************************************************/

// matrix dither, Bayer 8x8
void dither_Bayer8( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 8;
    const size_t mheight = 8;
    int32_t matrix[mwidth * mheight] = { 2, 130, 34, 161, 10, 137, 42, 169, 193, 66, 225, 98, 201, 74, 233, 106, 50, 177, 18, 145, 58, 185, 26, 153, 241, 114, 209, 82, 249, 122, 217, 90, 14, 141, 46, 173, 6, 133, 38, 165, 205, 78, 237, 110, 197, 70, 229, 102, 62, 189, 30, 157, 54, 181, 22, 149, 253, 126, 221, 94, 245, 118, 213, 86 };

#if 0
    // adjust original Bayer matrix for better results, get even shadows and highlights
    int32_t Bayer[] = { 0, 32, 8, 40, 2, 34, 10, 42, 48, 16, 56, 24, 50, 18, 58, 26, 12, 44, 4, 36, 14, 46, 6, 38, 60, 28, 52, 20, 62, 30, 54, 22, 3, 35, 11, 43, 1, 33, 9, 41, 51, 19, 59, 27, 49, 17, 57, 25, 15, 47, 7, 39, 13, 45, 5, 37, 63, 31, 55, 23, 61, 29, 53, 21 };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k)
        printf("%d, ", 2 + (255*Bayer[k] + 32) / 64 );
    printf("\n");
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_Bayer8( dither_Bayer8, "Bayer8" );

/******************************************************************************/

// matrix dither, Clustered 4x4
void dither_cluster4( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 91, 181, 166, 76, 196, 31, 16, 151, 211, 46, 61, 136, 106, 226, 241, 121 };

#if 0
    // adjust original matrix for better results, get even shadows and highlights
    int32_t cluster[] = { 6, 12, 11, 5,
                        13, 2, 1, 10,
                        14, 3, 4, 9,
                        7, 15, 16, 8  };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k) {
        int32_t value = 0 + (255*cluster[k] + 32) / 17;
        printf("%d, ", value);
        matrix[k] = value;
    }
    printf("\n");
#endif

#if 1
// TODO - this looks better for print
    for (size_t k = 0; k < mwidth*mheight; ++k)
        matrix[k] = 255 - matrix[k];
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_cluster4( dither_cluster4, "Cluster4" );

/******************************************************************************/

// matrix dither, improved Clustered 4x4
void dither_cluster4cc( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 76, 196, 211, 241, 91, 136, 181, 226, 31, 121, 151, 166, 16, 46, 106, 61 };

#if 0
    // adjust Chris's improved matrix for better results, get even shadows and highlights
    int32_t ccmatrix[] = { 5, 13, 14, 16,
                        6,  9, 12, 15,
                        2,  8, 10, 11,
                        1,  3,  7,  4 };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k) {
        int32_t value = 0 + (255*ccmatrix[k] + 32) / 17;
        printf("%d, ", value);
        matrix[k] = value;
    }
    printf("\n");
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_cluster4cc( dither_cluster4cc, "Cluster4cc" );

/******************************************************************************/

// matrix dither, Clustered 8x8
void dither_cluster8( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    
    output.Allocate(width,height);
    
    const size_t mwidth = 8;
    const size_t mheight = 8;
    int32_t matrix[mwidth * mheight] = { 249, 229, 193, 145, 149, 197, 233, 253, 225, 189, 141, 86, 90, 153, 201, 237, 185, 137, 82, 42, 46, 94, 157, 205, 133, 78, 38, 14, 2, 18, 50, 98, 130, 74, 34, 10, 6, 22, 54, 102, 181, 126, 70, 30, 26, 58, 106, 161, 221, 177, 122, 66, 62, 110, 165, 209, 245, 217, 173, 118, 114, 169, 213, 241 };

#if 0
    // adjust original matrix for better results, get even shadows and highlights
    int32_t cluster[] = { 62,57,48,36,37,49,58,63,56,47,35,21,22,38,50,59,46,34,20,10,11,23,39,51,33,19,9,3,0,4,12,24,32,18,8,2,1,5,13,25,45,31,17,7,6,14,26,40,55,44,30,16,15,27,41,52,61,54,43,29,28,42,53,60 };
    printf("\n");
    for (size_t k = 0; k < mwidth*mheight; ++k)
        printf("%d, ", 2 + (255*cluster[k] + 32) / 64 );
    printf("\n");
#endif

    matrix_inner( output, input, matrix, mwidth, mheight );
}

dither_description desc_cluster8( dither_cluster8, "Cluster8" );

/******************************************************************************/

// dither with JJN error weights
static
void matrix_diffuse_inner( image &output, const image &input,
                    const int64_t seed, const dither_options &opt,
                    const int32_t *matrix, const int32_t matrix_width, const int32_t matrix_height )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);

    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    fill_noise( &buffers[0], 2*(width+4), 2*48, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        size_t yoffset = (y%matrix_height)*matrix_width;
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t xoffset = x % matrix_width;
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = matrix[ yoffset + xoffset ];
                    
                    value = (comp < (threshold+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 7 * diff;
                current_error[x+2] += 5 * diff;
                next_error[x-2] += 3 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+2] += 3 * diff;
                two_error[x-2] += 1 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t xoffset = x % matrix_width;
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    int32_t threshold = matrix[ yoffset + xoffset ];
                    
                    value = (comp < (threshold+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 7 * diff;
                next_error[x-2] += 3 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+2] += 3 * diff;
                two_error[x-2] += 1 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

/******************************************************************************/

// Matrix dither with error diffusion
void dither_Bayer2diffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 2;
    const size_t mheight = 2;
    int32_t matrix[mwidth * mheight] = { 32, 160, 223, 96 };
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_Bayer2diffuse( dither_Bayer2diffuse, "Bayer2 Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Matrix dither with error diffusion
void dither_Bayer4diffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 8, 136, 40, 167, 199, 72, 231, 104, 56, 183, 24, 151, 247, 120, 215, 88 };
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_Bayer4diffuse( dither_Bayer4diffuse, "Bayer4 Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Matrix dither with error diffusion
void dither_Bayer8diffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 8;
    const size_t mheight = 8;
    int32_t matrix[mwidth * mheight] = { 2, 130, 34, 161, 10, 137, 42, 169, 193, 66, 225, 98, 201, 74, 233, 106, 50, 177, 18, 145, 58, 185, 26, 153, 241, 114, 209, 82, 249, 122, 217, 90, 14, 141, 46, 173, 6, 133, 38, 165, 205, 78, 237, 110, 197, 70, 229, 102, 62, 189, 30, 157, 54, 181, 22, 149, 253, 126, 221, 94, 245, 118, 213, 86 };
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_Bayer8diffuse( dither_Bayer8diffuse, "Bayer8 Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Matrix dither with error diffusion
void dither_cluster4diffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 91, 181, 166, 76, 196, 31, 16, 151, 211, 46, 61, 136, 106, 226, 241, 121 };

#if 1
// TODO - this looks better for print
    for (size_t k = 0; k < mwidth*mheight; ++k)
        matrix[k] = 255 - matrix[k];
#endif
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_cluster4diffuse( dither_cluster4diffuse, "Cluster4 Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Matrix dither with error diffusion
void dither_cluster4ccdiffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 4;
    const size_t mheight = 4;
    int32_t matrix[mwidth * mheight] = { 76, 196, 211, 241, 91, 136, 181, 226, 31, 121, 151, 166, 16, 46, 106, 61 };
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_cluster4ccdiffuse( dither_cluster4ccdiffuse, "Cluster4cc Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Matrix dither with error diffusion
void dither_cluster8diffuse( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t mwidth = 8;
    const size_t mheight = 8;
    int32_t matrix[mwidth * mheight] = { 249, 229, 193, 145, 149, 197, 233, 253, 225, 189, 141, 86, 90, 153, 201, 237, 185, 137, 82, 42, 46, 94, 157, 205, 133, 78, 38, 14, 2, 18, 50, 98, 130, 74, 34, 10, 6, 22, 54, 102, 181, 126, 70, 30, 26, 58, 106, 161, 221, 177, 122, 66, 62, 110, 165, 209, 245, 217, 173, 118, 114, 169, 213, 241 };
    
    matrix_diffuse_inner( output, input, seed, opt, matrix, mwidth, mheight );
}

dither_description desc_cluster8diffuse( dither_cluster8diffuse, "Cluster8 Diffuse",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Incorporate dot overlap model for laser printer
// Pappas and Neuhoff, 1995
// plus correction for low isolated dot density in cheap laser printers by ccox
void dither_PappasNeuhoff( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    double ratio = opt.params[1];
    const int32_t isolated_dot_value = 255.0 * (1.0 - opt.params[2]);
    const bool opaque = (opt.params[2] > 0.5);
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];

    double r2 = ratio*ratio;
    double temp1 = asin(1.0/(ratio*sqrt(2.0)));
    double temp2 = sqrt(2.0*r2 - 1.0);
    double alpha = 0.25 * temp2 + 0.5 * r2 * temp1 - 0.5;
    double beta = 0.125 * M_PI * r2 - 0.5 * r2 * temp1 - 0.25 * temp2 + 0.25;
    if (beta < 1.0e-10)      // numerical errors end up with 5.5e-17 for ratio == 1.0
        beta = 0.0;
    double gamma = 0.5 * r2 * asin( sqrt((r2-1.0)/r2) ) - 0.5 * sqrt( r2 - 1.0 ) - beta;
    //printf("ratio = %g\nalpha = %g\nbeta = %g\ngamma = %g\n", ratio, alpha, beta, gamma );    // DEBUG


    // assign a bit to each position for our 4 bit, 16 entry LUT
    enum  {
        bit_UpLeft   =  1,
        bit_UpCenter =  2,
        bit_UpRight  =  4,
        bit_Previous =  8,
     };
 
    int32_t InkModelLUT[ 16 ];

    // Assume white pixels (paper color) outside of bounds.
    for (int k = 0; k < 16; ++k)
        {
        double total_ink = 0.0;
        
        bool UL = k & bit_UpLeft;
        bool UC = k & bit_UpCenter;
        bool UR = k & bit_UpRight;
        bool LL = k & bit_Previous;
        
        if (UC)  total_ink += alpha;
        if (LL)  total_ink += alpha;

        if (opaque)
            {
            // opaque, overlap doesn't increase density
            if (UC && LL)  total_ink -= gamma;
            if (!UC && UR) total_ink += beta;
            if (!UC && !LL && UL) total_ink += beta;
            
            assert( total_ink >= 0.0 );
            assert( total_ink <= 1.0 );
            }
        else
            {
            // transparent, overlap DOES increase density
            if (UL)  total_ink += beta;
            if (UR)  total_ink += beta;
            // density can be greater than 1.0!
            }
        
        int32_t model_value = int32_t( 255.0 * (1.0 - total_ink) );
        model_value = std::min( 255, model_value );
        model_value = std::max( 0, model_value );
        
        InkModelLUT[ k ] = model_value;
        }
    
    // verify that no ink equals white
    assert( InkModelLUT[0] == 255 );
 
    
    fill_noise( &buffers[0], 2*(width+4), 2*48, seed );
    
    for (int64_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }

                int32_t diff = comp - (int32_t)value;
                
                int index = 0;
                
                if (y > 0)
                    {
                    if (x > 0)
                        index += (output.ReadPixelUnclipped( x-1, y-1 ) == 0) ? bit_UpLeft : 0;
                        
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? bit_UpCenter : 0;
                    
                    if (x < (width-1))
                        index += (output.ReadPixelUnclipped( x+1, y-1 ) == 0) ? bit_UpRight : 0;
                    }
                
                if (x > 0)
                    index += (output.ReadPixelUnclipped( x-1, y ) == 0) ? bit_Previous : 0;

                if (value == 0)
                    {
                    if (index == 0)
                        diff = comp - isolated_dot_value;
                    }
                else
                    {
                    int32_t model_value = InkModelLUT[ index ];
                    diff = comp - model_value;
                    }
                
                // spread errors
                current_error[x+1] += 7 * diff;
                current_error[x+2] += 5 * diff;
                next_error[x-2] += 3 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+2] += 3 * diff;
                two_error[x-2] += 1 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 24) / 48;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;
                
                int index = 0;
                
                if (y > 0)
                    {
                    if (x > 0)
                        index += (output.ReadPixelUnclipped( x-1, y-1 ) == 0) ? bit_UpRight : 0;
                        
                    index += (output.ReadPixelUnclipped( x+0, y-1 ) == 0) ? bit_UpCenter : 0;
                    
                    if (x < (width-1))
                        index += (output.ReadPixelUnclipped( x+1, y-1 ) == 0) ? bit_UpLeft : 0;
                    }
                
                if (x < (width-1))
                    index += (output.ReadPixelUnclipped( x+1, y ) == 0) ? bit_Previous : 0;

                if (value == 0)
                    {
                    if (index == 0)
                        diff = comp - isolated_dot_value;
                    }
                else
                    {
                    int32_t model_value = InkModelLUT[ index ];
                    diff = comp - model_value;
                    }
                
                // spread errors
                current_error[x-1] += 7 * diff;
                current_error[x-2] += 5 * diff;
                next_error[x+2] += 3 * diff;
                next_error[x+1] += 5 * diff;
                next_error[x+0] += 7 * diff;
                next_error[x-1] += 5 * diff;
                next_error[x-2] += 3 * diff;
                two_error[x+2] += 1 * diff;
                two_error[x+1] += 3 * diff;
                two_error[x+0] += 5 * diff;
                two_error[x-1] += 3 * diff;
                two_error[x-2] += 1 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_PappasNeuhoff( dither_PappasNeuhoff, "Pappas-Neuhoff-Toner",
                                4,
                                { "noise range", "relative dot size", "isolated dot density", "opaque" },
                                {   4.0,  1.50,  0.50,  1.0 },
                                {   0.0,  1.00,  0.01,  0.0 },
                                { 100.0,  2.00,  2.00,  1.0 } );

dither_description desc_PappasNeuhoff2( dither_PappasNeuhoff, "Pappas-Neuhoff-Ink",
                                4,
                                { "noise range", "relative dot size", "isolated dot density", "opaque" },
                                {   4.0,  1.20,  1.00,  0.0 },
                                {   0.0,  1.00,  0.01,  0.0 },
                                { 100.0,  2.00,  2.00,  1.0 } );

/******************************************************************************/

const int32_t modulation_unity = 1024;

int32_t modulation_interpolation( int32_t x )
{
    const size_t length = 9;
    int32_t keys[ length ] = { 0, 44, 64, 85, 95, 102, 107, 112, 127 };
    float values[ length ] = { 0.0, 0.34, 0.50, 1.00, 0.17, 0.50, 0.70, 0.79, 1.0 };
    
    if (x > 127)
        x = 255 - x;
    
    assert( x >= 0 );
    assert( x <= 127 );
    
    // find lower bound in keys
    size_t index = 0;
    for (index = 0; index < length; ++index)
        if (keys[index+1] >= x)
            break;
    
    assert( index >= 0 );
    assert( index < (length-1) );
    assert( keys[index] <= x );
    assert( keys[index+1] >= x );
    
    // interpolate values
    int32_t low_key = keys[ index ];
    int32_t high_key = keys[ index+1 ];
    float low_value = values[ index ];
    float high_value = values[ index+ 1 ];
    
    float key_fraction = (x - low_key) / float(high_key - low_key);
    float value = low_value + key_fraction * (high_value - low_value);
    
    assert( value >= 0.0 );
    assert( value <= 1.0 );
    
    // convert fraction to integer
    int32_t result = int32_t(modulation_unity * value);
    return result;
}

/******************************************************************************/

const int32_t weight_unity = 128;

void error_weight_interpolation( int32_t x, int32_t &d10, int32_t &d11, int32_t &d01 )
{
    const size_t length = 18;
    int32_t keys[ length ] = { 0, 1, 2, 3, 4, 10, 22, 32, 44, 64, 72, 77, 85, 95, 102, 107, 112, 127 };
    int32_t value10[ length ] = { 13, 1300249, 214114, 351854, 801100, 704075, 46613, 47482, 43024, 36411, 38477, 40503, 35865, 34117, 35464, 16477, 33360, 35269 };
    int32_t value11[ length ] = { 0, 0, 287, 0, 0, 297466, 31917, 30617, 42131, 43219, 53843, 51547, 34108, 36899, 35049, 18810, 37954, 36066 };
    int32_t value01[ length ] = { 5, 499250, 99357, 199965, 490999, 303694, 21469, 21900, 14826, 20369, 7678, 7948, 30026, 28983, 29485, 14712, 28685, 28664 };

    if (x > 127)
        x = 255 - x;
    
    assert( x >= 0 );
    assert( x <= 127 );
    
    // find lower bound in keys
    size_t index = 0;
    for (index = 0; index < length; ++index)
        if (keys[index+1] >= x)
            break;
    
    assert( index >= 0 );
    assert( index < (length-1) );
    assert( keys[index] <= x );
    assert( keys[index+1] >= x );
    
    // normalize and interpolate values
    int32_t low_key = keys[ index ];
    int32_t high_key = keys[ index+1 ];
    float key_fraction = (x - low_key) / float(high_key - low_key);
    
    float sum_low  = value10[index+0] + value11[index+0] + value01[index+0];
    float sum_high = value10[index+1] + value11[index+1] + value01[index+1];
    
    float low10 = value10[index+0]/sum_low;
    float high10 = value10[index+1]/sum_high;
    float a10 = low10 + key_fraction * (high10 - low10);
    
    float low11 = value11[index+0]/sum_low;
    float high11 = value11[index+1]/sum_high;
    float a11 = low11 + key_fraction * (high11 - low11);
    
    float low01 = value01[index+0]/sum_low;
    float high01 = value01[index+1]/sum_high;
    float a01 = low01 + key_fraction * (high01 - low01);
    
    d10 = a10 * weight_unity;
    d11 = a11 * weight_unity;
    d01 = a01 * weight_unity;
}

/******************************************************************************/

// Zhao, Fang 2003 - modulate threshold noise and coefficients based on pixel value
void dither_ZhaoFang( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] + 0.5 );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 2 * (width+2) );
    int32_t *current_error = &buffers[1];
    int32_t *next_error = &buffers[1+1*(width+1)];
    
    // build our pixel value based LUTs
// DEFERRED - this would be slightly faster as single array of 4 values
    int32_t modulation[256];
    int32_t d10[256];
    int32_t d11[256];
    int32_t d01[256];
    for (int32_t i = 0; i < 256; ++i)
        {
        int32_t a10, a11, a01;
        modulation[i] = modulation_interpolation(i);
        error_weight_interpolation( i, a10, a11, a01 );
        d10[i] = a10;
        d11[i] = a11;
        d01[i] = a01;
        }
    
    fill_noise( &buffers[0], (width+2), 2*weight_unity, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( next_error, 0, (width+1)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (weight_unity/2)) / weight_unity;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t local_range = (noise_range * modulation[ pixel ] + (modulation_unity/2)) / modulation_unity;
                    int32_t noise = (local_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += d10[pixel] * diff;
                next_error[x-1] += d11[pixel] * diff;
                next_error[x+0] += d01[pixel] * diff;
            
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + (weight_unity/2)) / weight_unity;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t local_range = (noise_range * modulation[ pixel ] + (modulation_unity/2)) / modulation_unity;
                    int32_t noise = (local_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += d10[pixel] * diff;
                next_error[x+1] += d11[pixel] * diff;
                next_error[x+0] += d01[pixel] * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        std::swap(next_error,current_error);
        }
}

dither_description desc_ZhaoFang( dither_ZhaoFang, "Zhao-Fang",
                                1,
                                { "noise range" },
                                {  50.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

inline double
lerp ( double a, double b, double fraction )
{
    return a + (b-a) * fraction;
}

// only need high noise values near extremes (black and white)
// TODO - could use less near white, most noise needed near black
int32_t threshold_range( int32_t x, double noise_min, double noise_max )
{
    const size_t length = 5;
    int32_t keys[ length ] = { 0, 20, 25, 45, 127 };
    float values[ length ] = { 0.0, 0.2, 0.5, 1.0, 1.0 };
    
    if (x > 127)
        x = 255 - x;
    
    assert( x >= 0 );
    assert( x <= 127 );
    
    // find lower bound in keys
    size_t index = 0;
    for (index = 0; index < length; ++index)
        if (keys[index+1] >= x)
            break;
    
    assert( index >= 0 );
    assert( index < (length-1) );
    assert( keys[index] <= x );
    assert( keys[index+1] >= x );
    
    // interpolate values
    int32_t low_key = keys[ index ];
    int32_t high_key = keys[ index+1 ];
    float low_value = values[ index ];
    float high_value = values[ index+ 1 ];
    
    float key_fraction = (x - low_key) / float(high_key - low_key);
    float value = low_value + key_fraction * (high_value - low_value);
    
    assert( value >= 0.0 );
    assert( value <= 1.0 );
    
    // convert unity range to specified value range
    value = lerp( noise_max, noise_min, value );
    
    // convert fraction to integer
    int32_t result = int32_t(value + 0.5);
    return result;
}

/******************************************************************************/

// Sierra with threshold noise modulation based on pixel value
void dither_ThresholdModulation( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const double noise_min = (255.0 / 100.0) * opt.params[0];
    const double noise_max = (255.0 / 100.0) * opt.params[1];
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    // build our pixel value based LUTs
    int32_t modulation[256];
    for (int32_t i = 0; i < 256; ++i)
        {
        modulation[i] = threshold_range(i,noise_min,noise_max);
        }
    
    fill_noise( &buffers[0], 2*(width+4), 8*32, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 31) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t local_range = modulation[ pixel ];
                    int32_t noise = (local_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 5 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 31) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t local_range = (modulation[ pixel ] + (modulation_unity/2)) / modulation_unity;
                    int32_t noise = (local_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (128+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 5 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_ThresholdModulation( dither_ThresholdModulation, "Threshold Modulation",
                                2,
                                { "noise minimum", "noise maximum" },
                                {   4.0,   38.0 },
                                {   0.0,   20.0 },
                                {  20.0,  100.0 } );

/******************************************************************************/

// Threshold only needs changes near extremes (black and white)
// and black needs more help than white
int32_t threshold_offset( int32_t x )
{
    const size_t length = 7;
    int32_t keys[ length ] = { 0, 32, 127, 223, 255 };
    float values[ length ] = { 0.07, 0.4, 0.5, 0.6, 0.8 };
    
    assert( x >= 0 );
    assert( x <= 255 );
    
    // find lower bound in keys
    size_t index = 0;
    for (index = 0; index < length; ++index)
        if (keys[index+1] >= x)
            break;
    
    assert( index >= 0 );
    assert( index < (length-1) );
    assert( keys[index] <= x );
    assert( keys[index+1] >= x );
    
    // interpolate values
    int32_t low_key = keys[ index ];
    int32_t high_key = keys[ index+1 ];
    float low_value = values[ index ];
    float high_value = values[ index+ 1 ];
    
    float key_fraction = (x - low_key) / float(high_key - low_key);
    float value = low_value + key_fraction * (high_value - low_value);
    
    assert( value >= 0.0 );
    assert( value <= 1.0 );
    
    // convert fraction to integer
    int32_t result = int32_t(255.0 * value + 0.5);
    return result;
}

/******************************************************************************/

// Sierra with threshold based on pixel value
void dither_ThresholdOffset( image &output, const image &input,
                    const int64_t seed, const dither_options &opt )
{
    const size_t width = input.width;
    const size_t height = input.height;
    const int32_t noise_range = (int32_t)( (255.0 / 100.0) * opt.params[0] );
    
    output.Allocate(width,height);
    
    std::vector<int32_t> buffers( 3 * (width+4) );
    int32_t *current_error = &buffers[2];
    int32_t *next_error = &buffers[2+1*(width+4)];
    int32_t *two_error = &buffers[2+2*(width+4)];
    
    // build our pixel value based LUTs
    int32_t threshold[256];
    for (int32_t i = 0; i < 256; ++i)
        {
        threshold[i] = threshold_offset(i);
        }
    
    fill_noise( &buffers[0], 2*(width+4), 8*32, seed );
    
    for (size_t y = 0; y < height; ++y)
        {
        // zero next row errors
        memset( two_error, 0, (width+2)*sizeof(int32_t) );
        
        // even rows go forward, odd goes backward
        if ((y & 1) == 0)
            {
        
            for (int64_t x = 0; x < width; ++x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 31) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (threshold[pixel]+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x+1] += 5 * diff;
                current_error[x+2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        else
            {
            for (int64_t x = width-1; x >= 0; --x)
                {
                int32_t pixel = input.ReadPixelUnclipped( x, y );
                uint8_t value = 0;
                
                int32_t err = (current_error[x] + 31) / 32;
                int32_t comp = pixel + err;
                
                // preserve pure black and pure white - no scum dots!
                if (pixel == 0 || pixel == 255)
                    {
                    value = pixel;
                    }
                else
                    {
                    int64_t point_hash = hashXY(x,(int64_t)y,seed);
                    int32_t noise = (noise_range * (point_hash % 1024) + 512) / 1024;
                    
                    value = (comp < (threshold[pixel]+noise)) ? 0 : 255;
                    }
                    
                int32_t diff = comp - (int32_t)value;
                
                // spread errors
                current_error[x-1] += 5 * diff;
                current_error[x-2] += 3 * diff;
                next_error[x-2] += 2 * diff;
                next_error[x-1] += 4 * diff;
                next_error[x+0] += 5 * diff;
                next_error[x+1] += 4 * diff;
                next_error[x+2] += 2 * diff;
                two_error[x-1] += 2 * diff;
                two_error[x+0] += 3 * diff;
                two_error[x+1] += 2 * diff;
                
                output.WritePixelUnclipped( x, y, value );
                }
            }
        
        // rotate error row buffers
        int32_t *tmp = current_error;
        current_error = next_error;
        next_error = two_error;
        two_error = tmp;
        }
}

dither_description desc_ThresholdOffset( dither_ThresholdOffset, "Threshold Offset",
                                1,
                                { "noise range" },
                                {   4.0 },
                                {   0.0 },
                                { 100.0 } );

/******************************************************************************/

// Found Ulichney! Now where are the rest of my halftoning books?
// Looks like 2 of them disappeared from my desk at Adobe.

// TODO - Xiangyu Y. Hu "Simple gradient-based error-diffusion method" - vary noise inverse to image gradient

// Lattice-Boltzman - ugly AF!, exaggerates edges and contrast

/*
Preserving pure black and white works better, always.
Adding noise to the threshold works better, always.
Serpentine scan works better, always.
Threshold based on pixel value seems to work well, but needs testing.
Noise range based on pixel value may help, but needs testing.

FS works well at very low and very large values (0-24, 255-240)
    better than JJN, Stucki, Sierra, etc.
    but fails at quarter and midtones, creates snakes or partial checkerboards

Changing weights without careful modulation results in artifacts.
But ZhaoFang works decently.

*/

/******************************************************************************/
/******************************************************************************/

std::vector<dither_description> ditherList =
{

#if DEBUG || LEARNING
    desc_debugpattern,
    desc_dthreshold,
    desc_noise,
    desc_forward,
    desc_forwardnoise,
    desc_diagonal,
    desc_diagonalnoise,
    desc_twoposition,
    desc_twopositionnoise,
    desc_threeposition,
    desc_threepositionnoise,
    desc_randomposition,
    desc_randompositionnoise,
#endif
    
    desc_randposserpentine,             // has some low freq components, but no obvious patterns
    desc_FloydSteinberg,                // not great as-is
    desc_FloydSteinbergSerpentine,      // OK, sort of the baseline for real-world dithering
    desc_JJN,               // BEST, tie
    desc_Stucki,                // close, but has a few patterns
    desc_Atkinson,              // clogged shadows and highlights
    desc_Atkinson2,             // slight patterns at 1/4 and 3/4
    desc_Burkes,                // worse than FS!
    desc_Sierra,            // BEST, tie
    desc_Sierra2,               // worse than FS!
    desc_Fan,                   // similar to FS
    desc_Fan_Shiau,             // a little better
    desc_PKK1,                  // vertical linear artifacts instead of snakes
    desc_PKK2,                  // a little better than PKK1
    desc_Allebach_Wong,         // has worms at 50,25,75 percent
    desc_Eschbach,              // pretty good, but shows some vertical lines (due to 1D feedback)
    desc_Eschbach2,             // some improvement, but artifacts amplified in some places
    desc_Eschbach2a,            // decent
    desc_ZhaoFang,              // OK, not bad for a 3 weight, 2 line model - but has some visible bands
    desc_ThresholdModulation,   // GOOD, improves extreme values, need to test with real images
    desc_ThresholdOffset,       // GOOD, improves extreme values, need to test with real images
    desc_PappasNeuhoff,     // GOOD, Laser-toner model, needs testing to see how well it compensates
    desc_PappasNeuhoff2,    // GOOD, Inkjet-ink model, needs testing to see how well it compensates
    
    desc_Bayer2,                // not useful
    desc_Bayer4,                // patterns, really bad for ink/toner
    desc_Bayer8,                // patterns, really bad for ink/toner
    desc_cluster4,              // not great for ink/toner
    desc_cluster8,              // bad round dot, not great for ink/toner
    desc_cluster4cc,            // slightly better clustering, better for ink/toner
    
    desc_Bayer2diffuse,         // lots of patterns
    desc_Bayer4diffuse,         // lots of patterns
    desc_Bayer8diffuse,         // lots of patterns
    desc_cluster4diffuse,       // lots of patterns
    desc_cluster4ccdiffuse,     // not as bad, some patterns
    desc_cluster8diffuse,       // not good

};

/******************************************************************************/

// a subset of dither methods to be profiled with power spectra
std::vector<dither_description> power_ditherList =
{
#if DEBUG
    desc_debugpattern,
#endif
    desc_noise,
    desc_FloydSteinberg,
    desc_FloydSteinbergSerpentine,
    desc_randposserpentine,
    desc_JJN,
    desc_Sierra,
    desc_PappasNeuhoff,
    desc_PappasNeuhoff2,
    desc_Bayer4,
    desc_cluster4cc,
    desc_ThresholdOffset,
};

/******************************************************************************/
/******************************************************************************/
