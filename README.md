# Halftone Tools by Chris Cox

Tools for testing and working with halftone patterns, dithering, and related algorithms.<P>
Currently over 350 halftone screens, and about 25 dither and error diffusion functions.
Also generates a density test chart.
Plus random things for creating documentation to go with this project.
Averaged Radial Power Spectra and Anisotropy measurements are work in progress: output to CSV spreadsheet and SVG graphs.


## Instructions
Download the source code, read the source code, compile on the command line, run the code, look at all the pretty pictures.<P>
Reading the wiki might help, too: https://gitlab.com/chriscox/halftone_tools/-/wikis/home

## License
MIT License

## Example Images
![Low Res Examples](Examples.png "Halftone Example Image")
