/*

GeneticImages/noise.cpp

Copyright (C) 1995-2023 Chris Cox

*/

#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <cassert>
#include "hash.hpp"
#include "noise.hpp"


/******************************************************************************/

inline
double octave_scale( double x, int32_t octave )
{
    if (octave < 0)
        x /= double(1LL<<(-octave));
    else
        x *= double(1LL<<octave);
    
    return x;
}

/******************************************************************************/

const size_t CUBIC_BIN_BITS = 10;
const size_t CUBIC_BINS = (1L<<CUBIC_BIN_BITS);

/* precomputed cubic interpolation coeffecients */
double CubicA[ CUBIC_BINS+1 ];
double CubicB[ CUBIC_BINS+1 ];

static
void build_cubic_splines()
{
    const double spline_a = -0.5;
    
    static bool splines_inited = false;
    if (splines_inited)
        return;
        
    splines_inited = true;

    // build CubicB    [0.0..1.0]
    // build CubicA [1.0..2.0]
    for (int i = 0; i <= CUBIC_BINS; ++i)
        {
        double x1 = double(i) / double(CUBIC_BINS);
        double x2 = x1 * x1;
        double x3 = x2 * x1;
        CubicB[i] = ( (spline_a+2.0)*x3 - (spline_a+3.0)*x2 + 1.0 );
        
        double y1 = double(i+CUBIC_BINS) / double(CUBIC_BINS);
        double y2 = y1 * y1;
        double y3 = y2 * y1;
        CubicA[i] = ( spline_a*y3 - (5.0*spline_a)*y2 + (8.0*spline_a)*y1 - (4.0*spline_a) );
        }

#if 0
    printf("CubicA[] = { ");
    for (int i = 0; i <= CUBIC_BINS; ++i)
        printf("%lf, ", CubicA[i] );
    printf(" };\n");

    printf("CubicB[] = { ");
    for (int i = 0; i <= CUBIC_BINS; ++i)
        printf("%lf, ", CubicB[i] );
    printf(" };\n");

// errors should be well distributed, summing to almost exactly 1
    printf("Sum[] = { ");
    for (int i = 0; i <= CUBIC_BINS; ++i)
        printf("%lg, ", (CubicA[i] + CubicB[i] + CubicA[(CUBIC_BINS-i)] + CubicB[(CUBIC_BINS-i)]) );
    printf(" };\n");

#endif

}

/******************************************************************************/

double
NoisePointSample( double x, double y, int64_t seed )
{
    int64_t xx = floor( x );
    int64_t yy = floor( y );

    double sum = hashXY_float( xx, yy, seed );
    
    return sum;
}

/******************************************************************************/

inline double
lerp ( double a, double b, double fraction )
{
    return a + (b-a) * fraction;
}

/******************************************************************************/

double
NoiseBilinear( double x, double y, int64_t seed )
{
    int64_t xx = floor( x );
    int64_t yy = floor( y );
    
    double xfraction = (x - xx);
    double yfraction = (y - yy);
    
    double sum1 = hashXY_float( xx+0, yy+0, seed );
    double sum2 = hashXY_float( xx+0, yy+1, seed );
    double sum3 = hashXY_float( xx+1, yy+0, seed );
    double sum4 = hashXY_float( xx+1, yy+1, seed );
    
    sum1 = lerp( sum1, sum2, yfraction );
    sum3 = lerp( sum3, sum4, yfraction );
    
    double sum = lerp( sum1, sum3, xfraction );
    
    return sum;
}

/******************************************************************************/

double
NoiseBicubic( double x, double y, int64_t seed )
{
    int64_t xx = floor( x );
    int64_t yy = floor( y );
    
    uint64_t xfraction = (uint64_t)floor( (x - xx) * (CUBIC_BINS-1) );
    uint64_t yfraction = (uint64_t)floor( (y - yy) * (CUBIC_BINS-1) );

    uint64_t xfrac_inverse = CUBIC_BINS - xfraction;
    uint64_t yfrac_inverse = CUBIC_BINS - yfraction;

    build_cubic_splines();

// can be special cased for xfraction and yfraction === 0, nust sure how much time that saves

    double c1 = CubicA[xfraction];
    double c2 = CubicB[xfraction];
    double c3 = CubicB[xfrac_inverse];
    double c4 = CubicA[xfrac_inverse];

    int64_t hashx0 = hash64(xx-1);
    int64_t hashx1 = hash64(xx+0);
    int64_t hashx2 = hash64(xx+1);
    int64_t hashx3 = hash64(xx+2);

    double sum1, sum2, sum3, sum4;
    
    sum1 = c1 * hash64_float( hashx0 ^ ((yy-1)^seed) );
    sum2 = c1 * hash64_float( hashx0 ^ ((yy+0)^seed) );
    sum3 = c1 * hash64_float( hashx0 ^ ((yy+1)^seed) );
    sum4 = c1 * hash64_float( hashx0 ^ ((yy+2)^seed) );

    sum1 += c2 * hash64_float( hashx1 ^ ((yy-1)^seed) );
    sum2 += c2 * hash64_float( hashx1 ^ ((yy+0)^seed) );
    sum3 += c2 * hash64_float( hashx1 ^ ((yy+1)^seed) );
    sum4 += c2 * hash64_float( hashx1 ^ ((yy+2)^seed) );

    sum1 += c3 * hash64_float( hashx2 ^ ((yy-1)^seed) );
    sum2 += c3 * hash64_float( hashx2 ^ ((yy+0)^seed) );
    sum3 += c3 * hash64_float( hashx2 ^ ((yy+1)^seed) );
    sum4 += c3 * hash64_float( hashx2 ^ ((yy+2)^seed) );

    sum1 += c4 * hash64_float( hashx3 ^ ((yy-1)^seed) );
    sum2 += c4 * hash64_float( hashx3 ^ ((yy+0)^seed) );
    sum3 += c4 * hash64_float( hashx3 ^ ((yy+1)^seed) );
    sum4 += c4 * hash64_float( hashx3 ^ ((yy+2)^seed) );

    double sum = (CubicA[yfraction] * sum1) + (CubicB[yfraction] * sum2) + (CubicB[yfrac_inverse] * sum3) + (CubicA[yfrac_inverse] * sum4);

    return sum;
}

/******************************************************************************/

// octave noise
double
ChaosPointSample( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );

    int64_t seed = hash64( set + octave_high );
    double t = NoisePointSample( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        t = (t + NoisePointSample( xx, yy, seed ) ) * 0.5;
    }

    return t;
}

/******************************************************************************/

// octave noise
double
ChaosBilinear( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBilinear( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        t = (t + NoiseBilinear( xx, yy, seed ) ) * 0.5;
    }

    return t;
}

/******************************************************************************/

// octave noise
double
ChaosBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        t = (t + NoiseBicubic( xx, yy, seed ) ) * 0.5;
    }

    return t;
}

/******************************************************************************/

// octave noise
double
MultifractalBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    const double offset = 0.1;
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed ) + offset;
    if (t < offset) t = offset;

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed ) + offset;  // values aren't always 0...1, because of cubic interpolation
        if (temp < offset) temp = offset;
        t = sqrt( t * temp );
    }

    return t;
}

/******************************************************************************/

// octave noise
double
SpottedBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_high-1; i > octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed );  // values aren't always 0...1, because of cubic interpolation
        t = 2.0 * t * temp;
    }
    
    // octave_low
    xx *= 0.5;
    yy *= 0.5;
    seed = hash64( set + octave_low );
    auto temp = NoiseBicubic( xx, yy, seed );  // values aren't always 0...1, because of cubic interpolation
    t = t * temp;

    return t;
}

/******************************************************************************/

// octave noise
double
ChoasSubtractionBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );

    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed );
        t = (0.5 + t - temp);
    }

    return t;
}

/******************************************************************************/

// octave noise
double
ChoasDifferenceBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );

    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed );
        t = fabs(t - temp);
    }

    return t;
}

/******************************************************************************/

// octave noise
double
ChoasSandBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );

    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed );
        t += temp;
    }

    t = (t / double(octave_high - octave_low + 1));

    return t;
}

/******************************************************************************/

// octave noise
double
ChoasLichenBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_low );
    double yy = octave_scale( y, octave_low );

    int64_t seed = hash64( set + octave_low );
    double t = NoiseBicubic( xx, yy, seed );

    for ( int32_t i = octave_low+1; i <= octave_high; ++i ) {
        xx *= 2.0;
        yy *= 2.0;
        seed = hash64( set + i );
        auto temp = NoiseBicubic( xx, yy, seed );
        t = (0.5 + t - temp);
        if (t < 0.0)    t = 0.0;
        if (t > 1.0)    t = 1.0;
    }

    return t;
}

/******************************************************************************/

inline constexpr double
threshold( double x )
{
    if (x > 0.5)
        return 1.0;
    else
        return 0.0;
}

/******************************************************************************/

// octave noise
double
ChaosThresholdBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );
    t = threshold(t);

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        double t2 = NoiseBicubic( xx, yy, seed );
        t2 = threshold(t2);
        t = (t + t2 ) * 0.5;
    }

    return t;
}

/******************************************************************************/

inline constexpr double
solarize( double x )
{
    return fabs( (2.0 * x) - 1.0 );
}

/******************************************************************************/

// octave noise
double
ChaosTurbulenceBicubic( double x, double y, int32_t octave_low, int32_t octave_high, int64_t set )
{
    double xx = octave_scale( x, octave_high );
    double yy = octave_scale( y, octave_high );
    
    int64_t seed = hash64( set + octave_high );
    double t = NoiseBicubic( xx, yy, seed );
    t = solarize(t);

    for ( int32_t i = octave_high-1; i >= octave_low; --i ) {
        xx *= 0.5;
        yy *= 0.5;
        seed = hash64( set + i );
        double t2 = NoiseBicubic( xx, yy, seed );
        t2 = solarize(t2);
        t = (t + t2) * 0.5;
    }

    return t;
}

/******************************************************************************/
/******************************************************************************/
