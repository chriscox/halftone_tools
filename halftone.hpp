//
//  halftone.hpp
//  Halftone Tools
//
//  Created by Chris Cox on June 5, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef halftone_hpp
#define halftone_hpp

#include <cstdint>
#include <vector>
#include <cassert>
#include <cstring>
#include "image.hpp"

/******************************************************************************/

// forward declarations
struct screen_options;

/******************************************************************************/

// TODO - should this move to geometry or transform source file?
struct geom_transform {
    geom_transform() : rotation(0.0), scale(1.0), offsetX(0.0), offsetY(0.0) {}
    
    pointFloat apply( const pointFloat &in ) const;

    double rotation;    // in degrees, for UI simplicity
    double scale;       // any reason to scale asymmetrically?  Might be in app software.
    double offsetX;
    double offsetY;
    
    // caching calculated values would be nice, but need invalidation logic
    // maybe function for inval, called when values changed in UI?
};

/******************************************************************************/

// pixel shader halftone function - taking raw coordinates
// cell size is always 1.0x1.0
typedef double dotfunc( double x, double y, const int64_t seed, const screen_options &opt );

/******************************************************************************/

struct dot_description {

    // simple version for no parameters
    dot_description( dotfunc *f, const char *n ) : dot(f), name(n), param_count(0) {}
    
    dot_description( dotfunc *f, const char *n, size_t c,
                std::vector<const char *> &&names, std::vector<double> &&def,
                std::vector<double> &&pmin, std::vector<double> &&pmax ) :
                dot(f), name(n), param_count(c),
                param_names(names), param_defaults(def),
                param_min(pmin), param_max(pmax)
                {}
    
    dotfunc *dot;
    const char *name;
    
    size_t param_count;
    std::vector<const char *>param_names;   // could be localizable strings
    std::vector<double>param_defaults;
    
    std::vector<double>param_min;   // parameter limits for UI
    std::vector<double>param_max;
};

// global list of all dot function descriptors
extern std::vector<dot_description> dotList;
extern std::vector<dot_description> power_halftone_list;

/******************************************************************************/

struct screen_options {
    screen_options() : dither(false), invert(false), jitter(0.0), name(NULL) {}
    
    screen_options( const dot_description &dot ) : dither(false), jitter(0.0), invert(false)
        {
        params = dot.param_defaults;
        assert( params.size() == dot.param_count );
        name = dot.name;
        }
    
    // debugging and documentation aid
    const char *name;
    
    // threshold generator options
    bool dither;
    bool invert;
    double jitter;  // percentage 0.0..100.0
    // TODO - additive noise size, additive noise amplitude
    // TODO - mult noise size, mult noise amplitude
    
    // dot function options
    std::vector<double> params;
};

/******************************************************************************/

// Generate a specific halftone threshold array image
// output is allocated inside
void CreateScreenGeneral( image &output, const size_t width, const size_t height,
                        const geom_transform &trans,
                        dotfunc dotgen, const int64_t seed,
                        const screen_options &opt  );

/******************************************************************************/

// Generate a tile of a specific halftone threshold array
// output must already be alocated
void CreateScreenTile(  image &output,
                        const pointInt topLeft,
                        const pointInt bottomRight,
                        const pointInt centerPt,
                        const geom_transform &trans,
                        dotfunc dotgen, const int64_t seed,
                        const screen_options &opt );

/******************************************************************************/

void unit_test_halftone();

/******************************************************************************/

#endif /* halftone_hpp */
