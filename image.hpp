//
//  image.hpp
//  Halftone Tools
//
//  Created by Chris Cox on May 24, 2023.
//
//  MIT License, Copyright (C) Chris Cox 2023
//

#ifndef image_hpp
#define image_hpp

#include <cstdint>

/******************************************************************************/

const double cm_per_inch = 2.54;
const double mm_per_inch = 25.4;

/******************************************************************************/

struct pointInt
{
    pointInt() : x(0), y(0) {}
    
    pointInt( int64_t xx, int64_t yy ) :
        x(xx), y(yy) {}

    int64_t x, y;
};

inline pointInt operator+(const pointInt& a, const pointInt& b) {
    return pointInt(a.x+b.x, a.y+b.y);
}

inline pointInt operator-(const pointInt& a, const pointInt& b) {
    return pointInt(a.x-b.x, a.y-b.y);
}

inline pointInt& operator+=(pointInt& a, const pointInt& b) {
    a.x += b.x;
    a.y += b.y;
    return a;
}

inline pointInt& operator-=(pointInt& a, const pointInt& b) {
    a.x -= b.x;
    a.y -= b.y;
    return a;
}

/******************************************************************************/

struct pointFloat
{
    pointFloat() : x(0.0), y(0.0) {}
    
    pointFloat( double xx, double yy ) :
        x(xx), y(yy) {}

    double x, y;
};

inline pointFloat operator+(const pointFloat& a, const pointFloat& b) {
    return pointFloat(a.x+b.x, a.y+b.y);
}

inline pointFloat operator-(const pointFloat& a, const pointFloat& b) {
    return pointFloat(a.x-b.x, a.y-b.y);
}

inline pointFloat& operator+=(pointFloat& a, const pointFloat& b) {
    a.x += b.x;
    a.y += b.y;
    return a;
}

inline pointFloat& operator-=(pointFloat& a, const pointFloat& b) {
    a.x -= b.x;
    a.y -= b.y;
    return a;
}


/******************************************************************************/

struct rectInt
{
    rectInt() :  top(0), left(0), bottom(0), right(0) {}
    
    rectInt( int64_t t, int64_t l, int64_t b, int64_t r) :
        top(t), left(l), bottom(b), right(r) {}
    
    inline bool isValid()
        { return (top < bottom) && (left < right); }

    int64_t top, left, bottom, right;
};

/******************************************************************************/

struct rectFloat
{
    rectFloat() : top(0.0), left(0.0), bottom(0.0), right(0.0) {}
    
    rectFloat( double t, double l, double b, double r) :
        top(t), left(l), bottom(b), right(r) {}
    
    // also fails for NaNs
    inline bool isValid()
        { return (top < bottom) && (left < right); }
    
    double top, left, bottom, right;
};

/******************************************************************************/

enum
{
    kFloatRGBFormat,
    kFloatGrayFormat,
    k16BitRGBFormat,
    k16BitGrayFormat,
    k8BitRGBFormat,
    k8BitGrayFormat,
    kBitmapFormat,
};

/******************************************************************************/

// a minimal image buffer implementation
struct image
{
    image() : width(0), height(0), buffer(NULL) {}
    
    image( size_t width, size_t height ) : buffer(NULL) {
        Allocate( width, height );
    }
    
    ~image() {
        width = height = 0;
        delete[] buffer;
        buffer = NULL;  // for debugging, and so memory reference leak checking doesn't get false positives
    }
    
    void Allocate( size_t width, size_t height );

    void WritePGM( const char *name ) const;
    void WritePBM( const char *name ) const;

    void ReadPGM( const char *name );
    
    /// Write a pixel value, clipped to image bounds
    void WritePixel( int64_t x, int64_t y, uint8_t value );
    
    /// Write a pixel value, without clipping coordinates (caller must ensure coordinates are valid)
    void WritePixelUnclipped( int64_t x, int64_t y, uint8_t value );
    
    /// Read a pixel value, clipped to image bounds
    uint8_t ReadPixel( int64_t x, int64_t y ) const;
    
    /// Read a pixel value, without clipping coordinates (caller must ensure coordinates are valid)
    uint8_t ReadPixelUnclipped( int64_t x, int64_t y ) const;
    
    /// Fill specified rectangular area with value, after clipping to image bounds
    void FillRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value );
    
    /// Outline the given rectangle with the number of pixels specified
    void OutlineRectClipped( int64_t top, int64_t left, int64_t bottom, int64_t right, uint8_t value, int64_t weight );
    
    /// Apply a halftone threshold image, create output image
    void ApplyThresholdScreen( const image &screen, image &output ) const;
    
    void DrawVerticalGradientUnclipped( int64_t top, int64_t left, int64_t bottom, int64_t right, float topValue, float bottomValue );

    void DrawHorizontalGradientUnclipped( int64_t top, int64_t left, int64_t bottom, int64_t right, float leftValue, float rightValue );
    
    /// fill image with 4 sample gradients
    void RenderGradientQuarters();
    
    /// fill image with octave sine waves in different orientations
    void RenderSinesQuarters();
    
    /// fill horizontal image with 4 sample areas
    void RenderDocumentationQuarters();
    
    /// fill image with density test chart
    void RenderDensityTestChart( double image_ppi );
    
    void CopyRectToDouble( const pointInt &topLeft, size_t size, double *output ) const;
    void CopyRectToDoubleComplex( const pointInt &topLeft, size_t size, double *output ) const;

public:
    size_t width;
    size_t height;
    uint8_t *buffer;
};

/******************************************************************************/

void WritePFM( const char *name, double *in_data, size_t width, size_t height );

/******************************************************************************/

#endif /* image_hpp */
