/*

GeneticImages/cells.cpp

Copyright (C) 2023 Chris Cox


Worley style cell functions
    https://en.wikipedia.org/wiki/Worley_noise


*/

#include <cstdio>
#include <cmath>
#include <algorithm>
#include "hash.hpp"
#include "cells.hpp"

/******************************************************************************/

const int64_t Xhash = hash64((int64_t)0x4122686681);
const int64_t Yhash = hash64((int64_t)0x4D534122686273);
const int64_t ZeroHash = hash64((int64_t)0);

/******************************************************************************/
/******************************************************************************/

typedef void Cell2DInnerFunc( int64_t ix, int64_t iy,
        double xfract, double yfract, int64_t seed,
        CellReturnData &distances, size_t sides, double adjust );

typedef void Cell2DMultInnerFunc( int64_t ix, int64_t iy,
        double xfract, double yfract, size_t point_count, int64_t seed,
        distanceList &distances, size_t sides, double adjust );

/******************************************************************************/

void Cell2DInnerDistMultiple( int64_t ix, int64_t iy, double xfract, double yfract, size_t point_count, int64_t seed, distanceList &distances, size_t sides=0, double adjust=0.0  )
{
    int64_t cell_id = hashXY( ix, iy, seed );

	for (size_t j = 0; j < point_count; ++j)
        {
        int64_t inner_point_id = cell_id ^ hash64((int64_t)j);

        double px = hash64_float( inner_point_id ^ Xhash );
        double py = hash64_float( inner_point_id ^ Yhash );

        double xx = xfract - px;
        double yy = yfract - py;
        double distance = (xx * xx) + (yy * yy);

        distances.emplace_back( CellReturnData( distance, inner_point_id, ix+px, iy+py ) );
        }
}

/******************************************************************************/

void Cell2DInnerDistSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides=0, double adjust=0.0 )
{
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    double distance = (xx * xx) + (yy * yy);

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerManhattenSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides=0, double adjust=0.0 )
{
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    double distance = fabs(xx) + fabs(yy);

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerHexagonSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides=0, double adjust=0.0 )
{
    const double C60 = cos(60.0*(M_PI/180.0));  // cos(-x) = cos(x)
    const double S60 = sin(60.0*(M_PI/180.0));  // sin(-x) = -sin(x)
    
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    
    double x1 = C60 * xx - S60 * yy;
    double x2 = C60 * xx + S60 * yy;

#if 1
    double distance = std::max( fabs(xx), std::max( fabs(x1), fabs(x2) ) );
#else
// also works, but needs a different scale (0.6 instead of 1.2)
    double distance = fabs(xx) + fabs(x1) + fabs(x2);
#endif

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerTriangleSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides=0, double adjust=0.0 )
{
    const double C60 = cos(60.0*(M_PI/180.0));  // cos(-x) = cos(x)
    const double S60 = sin(60.0*(M_PI/180.0));  // sin(-x) = -sin(x)
    
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    
    double x1 = C60 * xx - S60 * yy;
    double x2 = C60 * xx + S60 * yy;

    double distance = std::max( -xx, std::max( x1, x2 ) );

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerNgonSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides, double adjust=0.0 )
{
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    
    double distance = 0.0;

// see notes in dot_Ngon about atan2 and polar approach
#if 1
// about 1 second, no visible time in sincos
    static size_t cached_sides = 0;
    static std::vector<double> cached_factors;
    
    if (sides != cached_sides)
        {
        cached_factors.resize( sides * 2 );
        for (size_t j = 0; j < sides; ++j)
            {
            double angle = j * 2.0*M_PI / sides;
            cached_factors[2*j+0] = cos(angle);
            cached_factors[2*j+1] = sin(angle);
            }
        cached_sides = sides;
        }
    
    if ( (sides & 3) == 0)
        {   // multiple of 4, can use fabs, y, and quarter the side count
        distance = std::max( fabs(xx), fabs(yy) );
        for (size_t k = 1; k < (sides/4); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            distance = std::max( fabs(xtemp), distance );
            distance = std::max( fabs(ytemp), distance );
            }
        }
    else if ( (sides & 1) == 0)
        {   // multiple of 2, can use fabs and half the side count
        distance = fabs(xx);
        for (size_t k = 1; k < (sides/2); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            distance = std::max( fabs(xtemp), distance );
            }
        }
    else
        {   // odd, have to run all angles
        distance = std::max(0.0,xx);
        for (size_t k = 1; k < sides; ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            distance = std::max( xtemp, distance );
            }
        }
#else
// about 5 seconds, 3.5 seconds in sincos
    if ( (sides & 3) == 0)
        {   // multiple of 4, can use fabs, y, and quarter the side count
        distance = std::max( fabs(xx), fabs(yy) );
        for (size_t k = 1; k < (sides/4); ++k)
            {
            double angle = k * 2.0*M_PI / sides;
            double CA = cos(angle);
            double SA = sin(angle);
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            distance = std::max( fabs(ytemp), distance );
            distance = std::max( fabs(xtemp), distance );
            }
        }
    else if ( (sides & 1) == 0)
        {   // multiple of 2, can use fabs and half the side count
        distance = fabs(xx);
        for (size_t k = 1; k < (sides/2); ++k)
            {
            double angle = k * 2.0*M_PI / sides;
            double CA = cos(angle);
            double SA = sin(angle);
            double xtemp = CA * xx - SA * yy;
            distance = std::max( fabs(xtemp), distance );
            }
        }
    else
        {   // odd, have to run all angles
        distance = std::max(0.0,xx);
        for (size_t k = 1; k < sides; ++k)
            {
            double angle = k * 2.0*M_PI / sides;
            double CA = cos(angle);
            double SA = sin(angle);
            double xtemp = CA * xx - SA * yy;
            distance = std::max( xtemp, distance );
            }
        }
#endif

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerStarSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides, double adjust )
{
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    
    double distance = 0.0;

    static size_t cached_sides = 0;
    static double cached_adjust = -99.0;
    static std::vector<double> cached_factors;
    static double cached_ratio = 0.0;
    
    if ( (sides != cached_sides) || (adjust != cached_adjust) )
        {
        cached_factors.resize( sides * 2 );
        for (size_t j = 0; j < sides; ++j)
            {
            double angle = j * 2.0*M_PI / sides;
            cached_factors[2*j+0] = cos(angle);
            cached_factors[2*j+1] = sin(angle);
            }
        double width = adjust * tan(M_PI/double(sides));
        cached_ratio = std::max( 1.0e-6, std::min( 0.7, width ));
        cached_sides = sides;
        cached_adjust = adjust;
        }

    if ((sides & 3) == 0)
        {   // multiple of 4, can use fabs, y, and quarter the side count
        double point1 = fabs(xx) - cached_ratio*fabs(yy);
        double point2 = fabs(yy) - cached_ratio*fabs(xx);
        distance = std::max( point1, distance );
        distance = std::max( point2, distance );
        for (size_t k = 1; k < (sides/4); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            double point1 = fabs(xtemp) - cached_ratio*fabs(ytemp);
            double point2 = fabs(ytemp) - cached_ratio*fabs(xtemp);
            distance = std::max( point1, distance );
            distance = std::max( point2, distance );
            }
        }
    else if ( (sides & 1) == 0)
        {   // multiple of 2, can use fabs and half the side count
        double point = fabs(xx) - cached_ratio*fabs(yy);
        distance = std::max(0.0,point);
        for (size_t k = 1; k < (sides/2); ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            double point = fabs(xtemp) - cached_ratio*fabs(ytemp);
            distance = std::max( point, distance );
            }
        }
    else
        {   // odd, have to run all angles
        if (xx >= 0.0)
            {
            double point = xx - cached_ratio*fabs(yy);
            distance = std::max(0.0,point);
            }
        for (size_t k = 1; k < sides; ++k)
            {
            double CA = cached_factors[2*k+0];
            double SA = cached_factors[2*k+1];
            double xtemp = CA * xx - SA * yy;
            double ytemp = SA * xx + CA * yy;
            if (xtemp >= 0.0)
                {
                double point = xtemp - cached_ratio*fabs(ytemp);
                distance = std::max( point, distance );
                }
            }
        }

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/

void Cell2DInnerConcaveDiamondSingle( int64_t ix, int64_t iy, double xfract, double yfract, int64_t seed, CellReturnData &distances, size_t sides=0, double adjust=0.0 )
{
    int64_t cell_id = hashXY( ix, iy, seed );

    int64_t inner_point_id = cell_id ^ ZeroHash;

    double px = hash64_float( inner_point_id ^ Xhash );
    double py = hash64_float( inner_point_id ^ Yhash );

    double xx = xfract - px;
    double yy = yfract - py;
    
    xx = fabs(xx);
    yy = fabs(yy);
    
    // factor = 0.5
    double distance = pow( (sqrt(xx) + sqrt(yy)), 4.0 );

    // test and keep dist^2 to minimize sqrt calls and dependencies
    if (distance < distances.distsq) {
        distances.distsq = distance;
        distances.point_id = inner_point_id;
        distances.x = ix+px;
        distances.y = iy+py;
        }
}

/******************************************************************************/
/******************************************************************************/

// make this reusable for several inner functions
void Cell2DGridTest( double x, double y, int64_t seed, CellReturnData &distlist, Cell2DInnerFunc inner, size_t sides = 0, double adjust=0.0 )
{
    double fx = floor(x);
    double fy = floor(y);
    
    int64_t ix = (int64_t)fx;
    int64_t iy = (int64_t)fy;
    
    double xfract = x - fx;
    double yfract = y - fy;
    
    // check center grid cell
    inner( ix, iy, xfract, yfract, seed, distlist, sides, adjust );

// TODO - ccox - benchmark this, make sure the conditional tests really are faster

    // check 8 neighbor grid cells (some might be closer than inside our grid cell)
    // North
    if (distlist.distsq > (yfract*yfract))
        inner( ix, iy-1, xfract, yfract+1.0, seed, distlist, sides, adjust );

    // South
    if (distlist.distsq > ((1.0-yfract)*(1.0-yfract)))
        inner( ix, iy+1, xfract, yfract-1.0, seed, distlist, sides, adjust );

    // East
    if (distlist.distsq > ((1.0-xfract)*(1.0-xfract)))
        inner( ix+1, iy, xfract-1.0, yfract, seed, distlist, sides, adjust );

    // West
    if (distlist.distsq > (xfract*xfract))
        inner( ix-1, iy, xfract+1.0, yfract, seed, distlist, sides, adjust );

    // NW
    double temp1 = std::min(xfract,yfract); // appproximation
    if (distlist.distsq > (temp1*temp1))
        inner( ix-1, iy-1, xfract+1.0, yfract+1.0, seed, distlist, sides, adjust );

    // NE
    double temp2 = std::min(1.0-xfract,yfract); // appproximation
    if (distlist.distsq > (temp2*temp2))
        inner( ix+1, iy-1, xfract-1.0, yfract+1.0, seed, distlist, sides, adjust );

    // SW
    double temp3 = std::min(xfract,1.0-yfract); // appproximation
    if (distlist.distsq > (temp3*temp3))
        inner( ix-1, iy+1, xfract+1.0, yfract-1.0, seed, distlist, sides, adjust );

    // SE
    double temp4 = std::min(1.0-xfract,1.0-yfract); // appproximation
    if (distlist.distsq > (temp4*temp4))
        inner( ix+1, iy+1, xfract-1.0, yfract-1.0, seed, distlist, sides, adjust );
    
}

/******************************************************************************/

// TODO - could make conditional if results were kept sorted and N-1 entry was tested
//  with sort at end, we don't know enough info to restrict tests
void Cell2DGridTestMultiple( double x, double y, size_t count, int64_t seed, distanceList &distlist, Cell2DMultInnerFunc inner, size_t sides = 0, double adjust=0.0 )
{
    double fx = floor(x);
    double fy = floor(y);
    
    int64_t ix = (int64_t)fx;
    int64_t iy = (int64_t)fy;
    
    double xfract = x - fx;
    double yfract = y - fy;
    
    // check center grid cell
    inner( ix, iy, xfract, yfract, count, seed, distlist, sides, adjust );

    // check 8 neighbor grid cells (some might be closer than inside our grid cell)
    // North
    inner( ix, iy-1, xfract, yfract+1.0, count, seed, distlist, sides, adjust );

    // South
    inner( ix, iy+1, xfract, yfract-1.0, count, seed, distlist, sides, adjust );

    // East
    inner( ix+1, iy, xfract-1.0, yfract, count, seed, distlist, sides, adjust );

    // West
    inner( ix-1, iy, xfract+1.0, yfract, count, seed, distlist, sides, adjust );

    // NW
    inner( ix-1, iy-1, xfract+1.0, yfract+1.0, count, seed, distlist, sides, adjust );

    // NE
    inner( ix+1, iy-1, xfract-1.0, yfract+1.0, count, seed, distlist, sides, adjust );

    // SW
    inner( ix-1, iy+1, xfract+1.0, yfract-1.0, count, seed, distlist, sides, adjust );

    // SE
    inner( ix+1, iy+1, xfract-1.0, yfract-1.0, count, seed, distlist, sides, adjust );
    
}


/******************************************************************************/

double Cell2DNearestDist( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerDistSingle );
    
    // convert dist^2 to distance
    return sqrt( dist.distsq );
}

/******************************************************************************/

double Cell2DSecondDist( double x, double y, int64_t seed, size_t count )
{
    distanceList distlist;
    distlist.reserve( count * 9 );

    Cell2DGridTestMultiple( x, y, count, seed, distlist, Cell2DInnerDistMultiple );
    
    // sort the distance list, smallest first
    std::sort( distlist.begin(), distlist.end(), cellDistanceLess );
    
    // return second closest distance
    // convert dist^2 to distance
    return sqrt( distlist[1].distsq );
}

/******************************************************************************/

double Cell2DCellWalls( double x, double y, int64_t seed, size_t count )
{
    distanceList distlist;
    distlist.reserve( count * 9 );

    Cell2DGridTestMultiple( x, y, count, seed, distlist, Cell2DInnerDistMultiple );
    
    // sort the distance list, smallest first
    std::sort( distlist.begin(), distlist.end(), cellDistanceLess );
    
    // convert dist^2 to distance
    // return difference of first and second distances
    // (greater - lesser) >= 0
    double value = sqrt( distlist[1].distsq ) - sqrt( distlist[0].distsq );
    return value;
}

/******************************************************************************/

double Cell2DNearestDistAndID( double x, double y, int64_t seed, int64_t &id )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerDistSingle );
    
    // convert dist^2 to distance
    id = dist.point_id;
    return sqrt( dist.distsq );
}

/******************************************************************************/

double Cell2DNearestManhatten( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerManhattenSingle );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDManhatten( double x, double y, int64_t seed, int64_t &id )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerManhattenSingle );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestID( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerDistSingle );
    
    return hash_to_double( dist.point_id );
}

/******************************************************************************/

int64_t Cell2DNearestHash( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerDistSingle );
    
    return dist.point_id;
}

/******************************************************************************/

double Cell2DNearestHexagon( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerHexagonSingle );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDHexagon( double x, double y, int64_t seed, int64_t &id )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerHexagonSingle );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestTriangle( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerTriangleSingle );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDTriangle( double x, double y, int64_t seed, int64_t &id )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerTriangleSingle );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestNgon( double x, double y, int64_t seed, size_t sides )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerNgonSingle, sides );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDNgon( double x, double y, int64_t seed, int64_t &id, size_t sides )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerNgonSingle, sides );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestConcaveDiamond( double x, double y, int64_t seed )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerConcaveDiamondSingle );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDConcaveDiamond( double x, double y, int64_t seed, int64_t &id )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerConcaveDiamondSingle );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestStar( double x, double y, int64_t seed, size_t sides, double point )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerStarSingle, sides, point );
    
    return dist.distsq;
}

/******************************************************************************/

double Cell2DNearestDistAndIDStar( double x, double y, int64_t seed, int64_t &id, size_t sides, double point )
{
    CellReturnData dist;
    dist.distsq = 1.0e12;  // initial value should be something huge and impossible

    Cell2DGridTest( x, y, seed, dist, Cell2DInnerStarSingle, sides, point );
    
    id = dist.point_id;
    return dist.distsq;
}

/******************************************************************************/
/******************************************************************************/
